var fs=require('fs');
var deleteFolderRecursive = function(path) {
	if( fs.existsSync(path) ) {
		fs.readdirSync(path).forEach(function(file){
			var curPath = path + '/' + file;
			if(fs.lstatSync(curPath).isDirectory()) { // recurse
				deleteFolderRecursive(curPath);
			} else { // delete file
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(path);
	}
};
var copyRecursiveSync = function(dir, filelist) {
	var _arr_exclude=['.git','.vscode','.eslintignore','.eslintrc.js','.gitignore','_update','__update','cache','log','update','electron','node_modules'
		,'index_dev.js','updatepack.js','debug.log','client0.bat','client1.bat','client2.bat','README.md','jsconfig.json'
		,'ansi-styles','dist','viewer','verimot_v4.exe','verimot_v40.exe','Newtonsoft.Json.dll','package-lock.json','config.json','status.json'];
	var files = fs.readdirSync(dir);
	filelist = filelist || [];
	files.forEach(function(file) {
		var curPath = dir + '/' + file;
		if(_arr_exclude.indexOf(file)>-1){
			if(curPath.indexOf('src')===-1){
				return;
			}
		}
		if(curPath.indexOf('src/node_modules/')>-1&&curPath.indexOf('src/node_modules/kaitek.km')===-1){
			return;
		}
		if(curPath.indexOf('src/node_modules/kaitek.km/examples')!==-1
		||curPath.indexOf('src/node_modules/kaitek.km/nbproject')!==-1
		||curPath.indexOf('src/node_modules/kaitek.km/src')!==-1
		||curPath.indexOf('src/node_modules/kaitek.km/vendor')!==-1
		){
			return;
		}
		var updPath = dir.replace(__dirname,'')+'/';
		if (fs.statSync(curPath).isDirectory()) {
			fs.mkdirSync(__dirname+'/__update'+updPath+file);
			filelist = copyRecursiveSync(curPath, filelist);
			filelist.push(curPath);
		}
		else {
			filelist.push(curPath);
			fs.writeFileSync(__dirname+'/__update'+updPath+file, fs.readFileSync(curPath));
		}
	});
	return filelist;
};
try {
	fs.accessSync(__dirname+'/version.txt');
	var content = fs.readFileSync(__dirname+'/version.txt');
	var n_v=parseInt(content)+1;
	var pjson = require('./src/package.json');
	var npm_v=pjson.version;
	try {
		fs.accessSync(__dirname+'/__update');
		deleteFolderRecursive(__dirname+'/__update');
	} catch (error) {
		fs.mkdirSync(__dirname+'/__update');
	}
	try {
		fs.accessSync(__dirname+'/update.bat');
		fs.unlinkSync(__dirname+'/update.bat');
	} catch (error) {
		//console.log(error);
	}
	fs.writeFileSync(__dirname+'/version.txt', n_v.toString());
	copyRecursiveSync(__dirname);
	var exec = require('child_process').exec;
	var cbfn_exec=function(error, stdout, stderr){
		console.log(`stdout: ${stdout}`);
		if (error !== null) {
			console.log(`stderr: ${stderr}`);
			console.log(`exec error: ${error}`);
		}else{
			deleteFolderRecursive(__dirname+'/__update');
			fs.unlinkSync(__dirname+'/update.bat');
		}
	};
	var upddir=__dirname.replace('verimotRT-Client','verimotRT-Server');
	var command='for /d %%X in ('+__dirname+'/__update/) do "c:\\Program Files\\7-Zip\\7z.exe" a "'+upddir+'/update/verimotRT-Client_'+npm_v+'.'+n_v+'.zip" "%%X\\"';
	fs.writeFileSync(__dirname+'/update.bat', command);
	//console.log(command);
	exec(__dirname+'/update.bat',cbfn_exec);
} catch (error) {
	console.log(error);
}