let domain=require('domain');
let base = require('./src');
let _ip=require('ip').address();
let _domain = domain.create();
_domain.on('error', function(err){
	console.error('domain-Error: ', err);
	base.writeError(err,_ip,'index-domain-error');
});
_domain.on('uncaughtException', function(err) {
	console.log('domain-uncaughtException: ',err);
	base.writeError(err,_ip,'index-domain-uncaughtException');
});
_domain.run(function(){
	base.startApp();
});