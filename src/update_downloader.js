const crypto = require('crypto');
const fs=require('fs');
const http=require('http');
const processModule = require('process');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});
let _pid=false;
let _status='IDLE';
let arr=[];
let __basepath=false;
//Sentry.context(function () {
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'start_update_downloader':{
		_pid=obj.pid;
		__basepath=obj.__basepath;
		processModule.send({event:'update_downloader_started',pid:_pid});
		break;
	}
	case 'update_download':{
		arr.push(obj.options);
		if(_status==='IDLE'){
			_status='WORKING';
			download();
		}
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	//processModule.send(_pid + ': ' + e);
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(300);
	}, 1000);
	return;
});
let download=()=>{
	if(arr.length>0){
		let _itemdownload=arr.shift();
		let _downloadfilepath=_itemdownload.basepath+_itemdownload.path;
		try {
			if(fs.existsSync(_downloadfilepath)){
				fs.unlinkSync(_downloadfilepath);
			}
		} catch (error) {
			Sentry.captureException(error);
		}
		let downloadfile = fs.createWriteStream(_downloadfilepath);
		let request = http.request(_itemdownload, function (resp, err) {
			if (err) {
				Sentry.captureException(err);
				downloadfile.end();
				fs.unlink(_downloadfilepath,function(){
					setTimeout(function(){
						process.exit(301);
					}, 1000);
				});
			} else {
				resp.on('data', function (data) {
					downloadfile.write(data);
				});
				resp.on('error',function(err){
					Sentry.captureException(err);
					downloadfile.end();
					fs.unlink(_downloadfilepath,function(){
						setTimeout(function(){
							process.exit(302);
						}, 1000);
					});
				});
				resp.on('end',function(){
					downloadfile.end();
					let shasum = crypto.createHash('sha256');
					try {
						let s = fs.ReadStream(_downloadfilepath);
						s.on('data', function(d) { shasum.update(d); });
						s.on('end', function() {
							let d = shasum.digest('hex');
							if(_itemdownload.hash==d){
								let extract = require('extract-zip');
								extract(__basepath+_itemdownload.path, { dir: __basepath+'/' }).then(function(){
									let exec = require('child_process').exec;
									exec('cd '+__basepath+'\\src && node_modules\\.bin\\sequelize.cmd db:migrate  ',(error, stdout, stderr) => {
										console.log(`stdout: ${stdout}`);
										if (error !== null) {
											console.log(`stderr: ${stderr}`);
											console.log(`exec error: ${error}`);
										}
										fs.unlink(_downloadfilepath,function(){
											setTimeout(function(){
												processModule.send({event:'update_downloader_complete',path:_downloadfilepath,version:_itemdownload.version});
												process.exit(309);
											}, 1000);
										});
									});
								}).catch(function(err) {
									fs.unlink(_downloadfilepath,function(){
										setTimeout(function(){
											process.exit(310);
										}, 1000);
									});
								});
								//async function extractor () {
								//	try {
								//		await extract(__basepath+_itemdownload.path, { dir: __basepath+'/' });
								//		let exec = require('child_process').exec;
								//		exec('cd '+__basepath+'\\src && node_modules\\.bin\\sequelize.cmd db:migrate  ',(error, stdout, stderr) => {
								//			console.log(`stdout: ${stdout}`);
								//			if (error !== null) {
								//				console.log(`stderr: ${stderr}`);
								//				console.log(`exec error: ${error}`);
								//			}
								//			fs.unlink(_downloadfilepath,function(){
								//				setTimeout(function(){
								//					processModule.send({event:'update_downloader_complete',path:_downloadfilepath,version:_itemdownload.version});
								//					process.exit(309);
								//				}, 1000);
								//			});
								//		});
								//	} catch (err) {
								//		// handle any errors
								//		fs.unlink(_downloadfilepath,function(){
								//			setTimeout(function(){
								//				process.exit(310);
								//			}, 1000);
								//		});
								//	}
								//}
								//extractor();
							}else{
								//Sentry.captureBreadcrumb({
								//	message: 'update file hash different',
								//	data : {
								//		application:JSON.parse(JSON.stringify({updateFile:_itemdownload.hash,downloaded:d}))
								//	}
								//});
								downloadfile.end();
								fs.unlink(_downloadfilepath,function(){
									setTimeout(function(){
										process.exit(303);
									}, 1000);
								});
							}
							if(_status=='WORKING'){
								_status='IDLE';
								if(arr.length>0){
									download();
								}
							}
						});
					} catch (error) {
						Sentry.captureException(error);
						fs.unlink(_downloadfilepath,function(){
							setTimeout(function(){
								process.exit(304);
							}, 1000);
						});
					}
				});
			}
		});
		request.on('error',function(err){
			Sentry.captureException(err);
			downloadfile.end();
			fs.unlink(_downloadfilepath,function(){
				setTimeout(function(){
					process.exit(305);
				}, 1000);
			});
			console.log('err-2----------');
			console.log(err);
		});
		request.end();
	}else{
		_status='IDLE';
	}
};
//});