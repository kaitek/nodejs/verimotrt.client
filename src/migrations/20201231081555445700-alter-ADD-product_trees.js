'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'product_trees',
		schema: 'public'
	  },
	  'notificationtime',
	  Sequelize.INTEGER
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};