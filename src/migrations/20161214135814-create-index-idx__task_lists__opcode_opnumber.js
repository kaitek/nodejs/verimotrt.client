'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_lists',
		['opcode','opnumber'],
  { 	  indexName: 'idx__task_lists__opcode_opnumber'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_lists', 'idx__task_lists__opcode_opnumber');
  }
};