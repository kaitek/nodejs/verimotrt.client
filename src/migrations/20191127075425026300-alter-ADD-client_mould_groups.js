'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'client_mould_groups',
		schema: 'public'
	  },
	  'operatorcount',
	  Sequelize.INTEGER
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};