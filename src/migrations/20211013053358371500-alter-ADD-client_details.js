'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'client_details',
		schema: 'public'
	  },
	  'triggertype',
	  Sequelize.STRING(50)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};