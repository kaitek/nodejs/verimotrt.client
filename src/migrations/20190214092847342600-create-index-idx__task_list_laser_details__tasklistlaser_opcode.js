'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_list_laser_details',
		['tasklistlaser','opcode'],
  { 	  indexName: 'idx__task_list_laser_details__tasklistlaser_opcode'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_list_laser_details', 'idx__task_list_laser_details__tasklistlaser_opcode');
  }
};