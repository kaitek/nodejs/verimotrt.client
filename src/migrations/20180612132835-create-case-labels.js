'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('case_labels', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      erprefnumber: {
        type: Sequelize.STRING(50)
      },
      time: {
        type: Sequelize.DATE
      },
      status: {
        type: Sequelize.STRING(50)
      },
      canceldescription: {
        type: Sequelize.STRING(255)
      },
      canceltime: {
        type: Sequelize.DATE
      },
      cancelusername: {
        type: Sequelize.STRING(100)
      },
      synced: {
        type: Sequelize.INTEGER
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('case_labels');
  }
};