'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'material_versions',
		['mastername'],
  { 	  indexName: 'idx__material_versions__mastername'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('material_versions', 'idx__material_versions__mastername');
  }
};