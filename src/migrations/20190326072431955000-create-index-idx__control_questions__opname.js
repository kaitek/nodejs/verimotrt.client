'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'control_questions',
		['opname'],
  { 	  indexName: 'idx__control_questions__opname'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('control_questions', 'idx__control_questions__opname');
  }
};