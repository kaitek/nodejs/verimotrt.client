'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'operation_authorizations',
		['employee'],
  { 	  indexName: 'idx__operation_authorizations__employee'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('operation_authorizations', 'idx__operation_authorizations__employee');
  }
};