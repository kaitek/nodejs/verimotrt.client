'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'client_lost_details',
		schema: 'public'
	  },
	  'refsayi',
	  Sequelize.INTEGER
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};