'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('overtimes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      clients: {
        type: Sequelize.TEXT
      },
      name: {
        type: Sequelize.STRING(255)
      },
      repeattype: {
        type: Sequelize.STRING(50)
      },
      days: {
        type: Sequelize.STRING(255)
      },
      startday: {
        type: Sequelize.DATEONLY
      },
      starttime: {
        type: Sequelize.STRING(5)
      },
      finishday: {
        type: Sequelize.DATEONLY
      },
      finishtime: {
        type: Sequelize.STRING(5)
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('overtimes');
  }
};