'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('_sync_messages', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      messageId: {
        type: Sequelize.STRING
      },
      tryCount: {
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING(50)
      },
      ack: {
        type: Sequelize.BOOLEAN
      },
      queued: {
        type: Sequelize.INTEGER
      },
      data: {
        type: Sequelize.JSON
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('_sync_messages');
  }
};