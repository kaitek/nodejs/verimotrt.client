'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'task_reject_details',
		schema: 'public'
	  },
	  'isexported',
	  Sequelize.BOOLEAN
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};