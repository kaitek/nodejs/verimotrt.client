'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_cases',
		['opname'],
  { 	  indexName: 'idx__task_cases__opname'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_cases', 'idx__task_cases__opname');
  }
};