'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_lists',
		['record_id'],
  { 	  indexName: 'idx__task_lists__record_id'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_lists', 'idx__task_lists__record_id');
  }
};