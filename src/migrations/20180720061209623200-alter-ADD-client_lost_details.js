'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'client_lost_details',
		schema: 'public'
	  },
	  'opsayi',
	  Sequelize.INTEGER
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};