'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'client_mould_groups',
		['client','code'],
  { 	  indexName: 'idx__client_mould_groups__client_code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('client_mould_groups', 'idx__client_mould_groups__client_code');
  }
};