'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'client_production_details',
		['type','day','jobrotation'],
  { 	  indexName: 'idx__client_production_details__type_day_jobrotation'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('client_production_details', 'idx__client_production_details__type_day_jobrotation');
  }
};