'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'product_trees',
		['code'],
  { 	  indexName: 'idx__product_trees__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('product_trees', 'idx__product_trees__code');
  }
};