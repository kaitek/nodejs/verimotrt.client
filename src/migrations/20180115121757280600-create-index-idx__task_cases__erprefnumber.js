'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_cases',
		['erprefnumber'],
  { 	  indexName: 'idx__task_cases__erprefnumber'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_cases', 'idx__task_cases__erprefnumber');
  }
};