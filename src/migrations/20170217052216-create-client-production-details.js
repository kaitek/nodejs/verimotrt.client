'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('client_production_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING(50)
      },
      client: {
        type: Sequelize.STRING(50)
      },
      day: {
        type: Sequelize.DATEONLY
      },
      jobrotation: {
        type: Sequelize.STRING(50)
      },
      tasklist: {
        type: Sequelize.STRING(200)
      },
      time: {
        type: Sequelize.DATE
      },
      mould: {
        type: Sequelize.STRING(50)
      },
      mouldgroup: {
        type: Sequelize.STRING(50)
      },
      opcode: {
        type: Sequelize.STRING(50)
      },
      opnumber: {
        type: Sequelize.INTEGER
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      opdescription: {
        type: Sequelize.STRING(255)
      },
      leafmask: {
        type: Sequelize.STRING(50)
      },
      leafmaskcurrent: {
        type: Sequelize.STRING(50)
      },
      production: {
        type: Sequelize.INTEGER
      },
      productioncurrent: {
        type: Sequelize.INTEGER
      },
      employee: {
        type: Sequelize.STRING(50)
      },
      erprefnumber: {
        type: Sequelize.STRING(50)
      },
      taskfinishtype: {
        type: Sequelize.STRING(50)
      },
      taskfinishdescription: {
        type: Sequelize.STRING(255)
      },
      taskfromerp: {
        type: Sequelize.BOOLEAN
      },
      calculatedtpp: {
        type: Sequelize.DECIMAL(10,4)
      },
      energy: {
        type: Sequelize.DECIMAL(10,4)
      },
      gap: {
        type: Sequelize.INTEGER
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('client_production_details');
  }
};