'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('task_reject_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING(50)
      },
      client: {
        type: Sequelize.STRING(50)
      },
      day: {
        type: Sequelize.DATEONLY
      },
      jobrotation: {
        type: Sequelize.STRING(50)
      },
      rejecttype: {
        type: Sequelize.STRING(50)
      },
      materialunit: {
        type: Sequelize.STRING(50)
      },
      tasklist: {
        type: Sequelize.STRING(200)
      },
      time: {
        type: Sequelize.DATE
      },
      mould: {
        type: Sequelize.STRING(50)
      },
      mouldgroup: {
        type: Sequelize.STRING(50)
      },
      opcode: {
        type: Sequelize.STRING(50)
      },
      opnumber: {
        type: Sequelize.INTEGER
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      opdescription: {
        type: Sequelize.STRING(255)
      },
      quantity_reject: {
        type: Sequelize.INTEGER
      },
      quantity_retouch: {
        type: Sequelize.INTEGER
      },
      quantity_scrap: {
        type: Sequelize.INTEGER
      },
      employee: {
        type: Sequelize.STRING(50)
      },
      erprefnumber: {
        type: Sequelize.STRING(50)
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('task_reject_details');
  }
};