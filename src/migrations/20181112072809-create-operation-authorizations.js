'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('operation_authorizations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      employee: {
        type: Sequelize.STRING(50)
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      l1: {
        type: Sequelize.INTEGER
      },
      l2: {
        type: Sequelize.INTEGER
      },
      l3: {
        type: Sequelize.INTEGER
      },
      l4: {
        type: Sequelize.INTEGER
      },
      l5: {
        type: Sequelize.INTEGER
      },
      time: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('operation_authorizations');
  }
};