'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('task_list_laser_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tasklistlaser: {
        type: Sequelize.STRING(50)
      },
      opcode: {
        type: Sequelize.STRING(50)
      },
      opnumber: {
        type: Sequelize.INTEGER
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      erprefnumber: {
        type: Sequelize.STRING(50)
      },
      leafmask: {
        type: Sequelize.INTEGER
      },
      productcount: {
        type: Sequelize.INTEGER
      },
      productdonecount: {
        type: Sequelize.INTEGER
      },
      productdoneactivity: {
        type: Sequelize.INTEGER
      },
      productdonejobrotation: {
        type: Sequelize.INTEGER
      },
      scrapactivity: {
        type: Sequelize.INTEGER
      },
      scrapjobrotation: {
        type: Sequelize.INTEGER
      },
      scrappart: {
        type: Sequelize.INTEGER
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('task_list_laser_details');
  }
};