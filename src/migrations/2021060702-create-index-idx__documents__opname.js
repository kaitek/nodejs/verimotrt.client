'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'documents',
		['opname'],
  { 	  indexName: 'idx__documents__opname'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('documents', 'idx__documents__opname');
  }
};