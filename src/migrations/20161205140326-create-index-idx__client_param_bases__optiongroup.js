'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'client_param_bases',
		['optiongroup'],
  { 	  indexName: 'idx__client_param_bases__optiongroup'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('client_param_bases', 'idx__client_param_bases__optiongroup');
  }
};