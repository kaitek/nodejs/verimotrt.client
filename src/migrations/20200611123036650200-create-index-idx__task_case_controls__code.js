'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_case_controls',
		['code'],
  { 	  indexName: 'idx__task_case_controls__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_case_controls', 'idx__task_case_controls__code');
  }
};