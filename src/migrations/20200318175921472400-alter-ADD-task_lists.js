'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'task_lists',
		schema: 'public'
	  },
	  'idx',
	  Sequelize.STRING(60)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};