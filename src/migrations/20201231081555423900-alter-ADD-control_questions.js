'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'control_questions',
		schema: 'public'
	  },
	  'path',
	  Sequelize.STRING(255)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};