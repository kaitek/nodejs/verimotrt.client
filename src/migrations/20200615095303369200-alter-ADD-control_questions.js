'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'control_questions',
		schema: 'public'
	  },
	  'clients',
	  Sequelize.STRING(255)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};