'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'client_mould_details',
		['client','clientmouldgroup','mouldgroup'],
  { 	  indexName: 'idx__client_mould_details__client_clientmouldgroup_mouldgroup'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('client_mould_details', 'idx__client_mould_details__client_clientmouldgroup_mouldgroup');
  }
};