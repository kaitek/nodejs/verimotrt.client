'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.changeColumn('task_cases','packcapacity',{
		type: Sequelize.DECIMAL(10,4)
	  }
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};