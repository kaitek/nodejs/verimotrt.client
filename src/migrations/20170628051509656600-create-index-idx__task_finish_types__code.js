'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_finish_types',
		['code'],
  { 	  indexName: 'idx__task_finish_types__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_finish_types', 'idx__task_finish_types__code');
  }
};