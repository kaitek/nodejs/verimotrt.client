'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'product_trees',
		schema: 'public'
	  },
	  'client',
	  Sequelize.STRING(50)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};