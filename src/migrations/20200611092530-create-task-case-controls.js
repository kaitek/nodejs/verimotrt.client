'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('task_case_controls', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING(50)
      },
      opcode: {
        type: Sequelize.STRING(50)
      },
      packcapacity: {
        type: Sequelize.DECIMAL(10, 4)
      },
      quantityremaining: {
        type: Sequelize.DECIMAL(10, 4)
      },
      clients: {
        type: Sequelize.STRING(255)
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('task_case_controls');
  }
};