'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('reworks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      client: {
        type: Sequelize.STRING(50)
      },
      reworktype: {
        type: Sequelize.STRING(50)
      },
      code: {
        type: Sequelize.STRING(60)
      },
      opcode: {
        type: Sequelize.STRING(50)
      },
      opnumber: {
        type: Sequelize.INTEGER
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      opdescription: {
        type: Sequelize.STRING(255)
      },
      tpp: {
        type: Sequelize.DECIMAL(10, 4)
      },
      islisted: {
        type: Sequelize.BOOLEAN
      },
      listorder: {
        type: Sequelize.INTEGER
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('reworks');
  }
};