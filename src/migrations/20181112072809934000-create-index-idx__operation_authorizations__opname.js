'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'operation_authorizations',
		['opname'],
  { 	  indexName: 'idx__operation_authorizations__opname'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('operation_authorizations', 'idx__operation_authorizations__opname');
  }
};