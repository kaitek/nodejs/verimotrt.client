'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'employees',
		['code'],
  { 	  indexName: 'idx__employees__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('employees', 'idx__employees__code');
  }
};