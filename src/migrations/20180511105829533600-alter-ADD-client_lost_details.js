'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'client_lost_details',
		schema: 'public'
	  },
	  'sourcelosttype',
	  Sequelize.STRING(50)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};