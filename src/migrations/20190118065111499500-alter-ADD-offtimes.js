'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'offtimes',
		schema: 'public'
	  },
	  'clearonovertime',
	  Sequelize.BOOLEAN
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};