'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'client_details',
		['client'],
  { 	  indexName: 'idx__client_details__client'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('client_details', 'idx__client_details__client');
  }
};