'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'employees',
		schema: 'public'
	  },
	  'teamleader',
	  Sequelize.STRING(50)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};