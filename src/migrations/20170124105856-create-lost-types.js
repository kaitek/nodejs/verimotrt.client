'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('lost_types', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING(50)
      },
      description: {
        type: Sequelize.STRING(255)
      },
      isemployeerequired: {
        type: Sequelize.BOOLEAN
      },
      isexpertemployeerequired: {
        type: Sequelize.BOOLEAN
      },
      time: {
        type: Sequelize.INTEGER
      },
      isoperationnumberrequired: {
        type: Sequelize.BOOLEAN
      },
      ioevent: {
        type: Sequelize.STRING(50)
      },
      extcode:{
        type: Sequelize.STRING(50)
      },
      listorder:{
        type: Sequelize.INTEGER
      },
      islisted:{
        type: Sequelize.BOOLEAN
      },
      closenoemployee:{
        type: Sequelize.BOOLEAN
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('lost_types');
  }
};