'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'_sync_messages',
		['messageId'],
  { 	  indexName: 'idx___sync_messages__messageId'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('_sync_messages', 'idx___sync_messages__messageId');
  }
};