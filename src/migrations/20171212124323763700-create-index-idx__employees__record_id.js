'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'employees',
		['record_id'],
  { 	  indexName: 'idx__employees__record_id'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('employees', 'idx__employees__record_id');
  }
};