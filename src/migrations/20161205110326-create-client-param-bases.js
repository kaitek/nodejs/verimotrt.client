'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('client_param_bases', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      optiongroup: {
        type: Sequelize.STRING(100)
      },
      name: {
        type: Sequelize.STRING(50)
      },
      label: {
        type: Sequelize.STRING(100)
      },
      optiontype: {
        type: Sequelize.STRING(50)
      },
      options: {
        type: Sequelize.TEXT
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('client_param_bases');
  }
};