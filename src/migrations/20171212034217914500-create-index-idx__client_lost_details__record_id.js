'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'client_lost_details',
		['record_id'],
  { 	  indexName: 'idx__client_lost_details__record_id'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('client_lost_details', 'idx__client_lost_details__record_id');
  }
};