'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('task_cases', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      movementdetail: {
        type: Sequelize.INTEGER
      },
      client: {
        type: Sequelize.STRING(50)
      },
      erprefnumber: {
        type: Sequelize.STRING(50)
      },
      casetype: {
        type: Sequelize.STRING(5)
      },
      opcode: {
        type: Sequelize.STRING(50)
      },
      opnumber: {
        type: Sequelize.INTEGER
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      stockcode: {
        type: Sequelize.STRING(50)
      },
      stockname: {
        type: Sequelize.STRING(60)
      },
      quantityused: {
        type: Sequelize.INTEGER
      },
      casename: {
        type: Sequelize.STRING(50)
      },
      packcapacity: {
        type: Sequelize.INTEGER
      },
      feeder: {
        type: Sequelize.STRING(5)
      },
      preptime: {
        type: Sequelize.INTEGER
      },
      quantityremaining: {
        type: Sequelize.INTEGER
      },
      quantitydeliver: {
        type: Sequelize.INTEGER
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      finishtype: {
        type: Sequelize.STRING(255)
      },
      conveyor: {
        type: Sequelize.INTEGER
      },
      goodsplanner: {
        type: Sequelize.STRING(255)
      },
      deliverytime: {
        type: Sequelize.DATE
      },
      status: {
        type: Sequelize.STRING(50)
      },
      caselot: {
        type: Sequelize.STRING(50)
      },
      casenumber: {
        type: Sequelize.STRING(50)
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('task_cases');
  }
};