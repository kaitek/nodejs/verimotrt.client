'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_lists',
		['finish'],
  { 	  indexName: 'idx__task_lists__finish'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_lists', 'idx__task_lists__finish');
  }
};