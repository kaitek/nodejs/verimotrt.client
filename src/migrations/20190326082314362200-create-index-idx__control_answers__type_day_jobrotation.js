'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'control_answers',
		['type','day','jobrotation'],
  { 	  indexName: 'idx__control_answers__type_day_jobrotation'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('control_answers', 'idx__control_answers__type_day_jobrotation');
  }
};