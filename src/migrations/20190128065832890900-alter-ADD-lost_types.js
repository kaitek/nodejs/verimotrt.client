'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'lost_types',
		schema: 'public'
	  },
	  'isdescriptionrequired',
	  Sequelize.BOOLEAN
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};