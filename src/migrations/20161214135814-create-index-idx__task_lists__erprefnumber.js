'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_lists',
		['erprefnumber'],
  { 	  indexName: 'idx__task_lists__erprefnumber'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_lists', 'idx__task_lists__erprefnumber');
  }
};