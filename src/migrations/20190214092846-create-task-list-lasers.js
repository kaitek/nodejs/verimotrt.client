'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('task_list_lasers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING(50)
      },
      description: {
        type: Sequelize.STRING(255)
      },
      production: {
        type: Sequelize.INTEGER
      },
      tfdescription: {
        type: Sequelize.STRING(255)
      },
      plannedstart: {
        type: Sequelize.DATE
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      client1: {
        type: Sequelize.STRING(50)
      },
      client2: {
        type: Sequelize.STRING(50)
      },
      client3: {
        type: Sequelize.STRING(50)
      },
      client4: {
        type: Sequelize.STRING(50)
      },
      client5: {
        type: Sequelize.STRING(50)
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('task_list_lasers');
  }
};