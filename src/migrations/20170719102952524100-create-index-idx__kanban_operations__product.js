'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'kanban_operations',
		['product'],
  { 	  indexName: 'idx__kanban_operations__product'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('kanban_operations', 'idx__kanban_operations__product');
  }
};