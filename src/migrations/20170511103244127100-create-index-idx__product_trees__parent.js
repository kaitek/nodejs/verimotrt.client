'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'product_trees',
		['parent'],
  { 	  indexName: 'idx__product_trees__parent'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('product_trees', 'idx__product_trees__parent');
  }
};