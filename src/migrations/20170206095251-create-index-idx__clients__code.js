'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'clients',
		['code'],
  { 	  indexName: 'idx__clients__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('clients', 'idx__clients__code');
  }
};