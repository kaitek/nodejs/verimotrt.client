'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('mould_groups', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING(50)
      },
      mould: {
        type: Sequelize.STRING(50)
      },
      productionmultiplier: {
        type: Sequelize.INTEGER
      },
      intervalmultiplier: {
        type: Sequelize.DECIMAL(10, 4)
      },
      counter: {
        type: Sequelize.INTEGER
      },
      setup: {
        type: Sequelize.INTEGER
      },
      cycletime: {
        type: Sequelize.DECIMAL(10,  4)
      },
      lotcount: {
        type: Sequelize.INTEGER
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('mould_groups');
  }
};