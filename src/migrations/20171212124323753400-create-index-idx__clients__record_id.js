'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'clients',
		['record_id'],
  { 	  indexName: 'idx__clients__record_id'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('clients', 'idx__clients__record_id');
  }
};