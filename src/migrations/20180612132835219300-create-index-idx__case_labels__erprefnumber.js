'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'case_labels',
		['erprefnumber'],
  { 	  indexName: 'idx__case_labels__erprefnumber'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('case_labels', 'idx__case_labels__erprefnumber');
  }
};