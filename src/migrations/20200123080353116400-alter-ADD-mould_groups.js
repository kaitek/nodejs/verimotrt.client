'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'mould_groups',
		schema: 'public'
	  },
	  'program',
	  Sequelize.STRING(4)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};