'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'job_rotations',
		['code'],
  { 	  indexName: 'idx__job_rotations__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('job_rotations', 'idx__job_rotations__code');
  }
};