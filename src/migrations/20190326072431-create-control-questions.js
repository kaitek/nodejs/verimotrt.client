'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('control_questions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING(50)
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      question: {
        type: Sequelize.STRING(255)
      },
      answertype: {
        type: Sequelize.STRING(50)
      },
      valuerequire: {
        type: Sequelize.STRING(100)
      },
      valuemin: {
        type: Sequelize.STRING(100)
      },
      valuemax: {
        type: Sequelize.STRING(100)
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('control_questions');
  }
};