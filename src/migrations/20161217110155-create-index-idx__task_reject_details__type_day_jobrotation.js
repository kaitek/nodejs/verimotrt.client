'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_reject_details',
		['type','day','jobrotation'],
  { 	  indexName: 'idx__task_reject_details__type_day_jobrotation'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_reject_details', 'idx__task_reject_details__type_day_jobrotation');
  }
};