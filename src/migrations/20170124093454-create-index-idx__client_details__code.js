'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'client_details',
		['code'],
  { 	  indexName: 'idx__client_details__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('client_details', 'idx__client_details__code');
  }
};