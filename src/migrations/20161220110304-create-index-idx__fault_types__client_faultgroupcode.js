'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'fault_types',
		['client','faultgroupcode'],
  { 	  indexName: 'idx__fault_types__client_faultgroupcode'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('fault_types', 'idx__fault_types__client_faultgroupcode');
  }
};