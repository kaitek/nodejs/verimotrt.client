'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('control_answers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING(50)
      },
      client: {
        type: Sequelize.STRING(50)
      },
      day: {
        type: Sequelize.DATEONLY
      },
      jobrotation: {
        type: Sequelize.STRING(50)
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      employee: {
        type: Sequelize.STRING(50)
      },
      time: {
        type: Sequelize.DATE
      },
      question: {
        type: Sequelize.STRING(255)
      },
      answertype: {
        type: Sequelize.STRING(50)
      },
      valuerequire: {
        type: Sequelize.STRING(100)
      },
      valuemin: {
        type: Sequelize.STRING(100)
      },
      valuemax: {
        type: Sequelize.STRING(100)
      },
      answer: {
        type: Sequelize.STRING(100)
      },
      description: {
        type: Sequelize.STRING(255)
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('control_answers');
  }
};