'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'task_lists',
		schema: 'public'
	  },
	  'offtime',
	  Sequelize.INTEGER
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};