'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'client_lost_details',
		schema: 'public'
	  },
	  'sourcedescriptionlost',
	  Sequelize.STRING(255)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};