'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('task_lists', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING(60)
      },
      erprefnumber: {
        type: Sequelize.STRING(50)
      },
      opcode: {
        type: Sequelize.STRING(50)
      },
      opnumber: {
        type: Sequelize.INTEGER
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      opdescription: {
        type: Sequelize.STRING(255)
      },
      productcount: {
        type: Sequelize.INTEGER
      },
      productdonecount: {
        type: Sequelize.INTEGER
      },
      deadline: {
        type: Sequelize.DATEONLY
      },
      client: {
        type: Sequelize.STRING(50)
      },
      type: {
        type: Sequelize.STRING(50)
      },
      tpp: {
        type: Sequelize.DECIMAL(10, 4)
      },
      productdoneactivity: {
        type: Sequelize.INTEGER
      },
      productdonejobrotation: {
        type: Sequelize.INTEGER
      },
      scrapactivity: {
        type: Sequelize.INTEGER
      },
      scrapjobrotation: {
        type: Sequelize.INTEGER
      },
      scrappart: {
        type: Sequelize.INTEGER
      },
      taskfromerp:{
        type: Sequelize.BOOLEAN
      },
      tfddescription:{
        type: Sequelize.STRING(255)
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('task_lists');
  }
};