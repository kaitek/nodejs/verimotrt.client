'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'mould_groups',
		['mould','code'],
  { 	  indexName: 'idx__mould_groups__mould_code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('mould_groups', 'idx__mould_groups__mould_code');
  }
};