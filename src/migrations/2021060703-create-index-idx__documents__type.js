'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'documents',
		['type'],
  { 	  indexName: 'idx__documents__type'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('documents', 'idx__documents__type');
  }
};