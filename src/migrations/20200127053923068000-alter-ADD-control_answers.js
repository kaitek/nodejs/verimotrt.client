'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'control_answers',
		schema: 'public'
	  },
	  'documentnumber',
	  Sequelize.STRING(50)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};