'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'control_answers',
		['opname'],
  { 	  indexName: 'idx__control_answers__opname'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('control_answers', 'idx__control_answers__opname');
  }
};