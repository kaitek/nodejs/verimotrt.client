'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('clients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING(50)
      },
      description: {
        type: Sequelize.STRING(255)
      },
      uuid: {
        type: Sequelize.STRING(255)
      },
      isactive: {
        type: Sequelize.BOOLEAN
      },
      ganttorder: {
        type: Sequelize.INTEGER
      },
      clientweight: {
        type: Sequelize.INTEGER
      },
      connected: {
        type: Sequelize.BOOLEAN
      },
      status: {
        type: Sequelize.STRING(255)
      },
      statustime: {
        type: Sequelize.DATE
      },
      workflow: {
        type: Sequelize.STRING(50)
      },
      info: {
        type: Sequelize.STRING(10)
      },
      watchorder: {
        type: Sequelize.INTEGER
      },
      materialpreparation: {
        type: Sequelize.BOOLEAN
      },
      ip: {
        type: Sequelize.STRING(15)
      },
      pincode: {
        type: Sequelize.INTEGER
      },
      versionapp: {
        type: Sequelize.STRING(15)
      },
      versionshell: {
        type: Sequelize.STRING(15)
      },
      computername: {
        type: Sequelize.STRING(50)
      },
      lastprodstarttime: {
        type: Sequelize.DATE
      },
      lastprodtime: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('clients');
  }
};