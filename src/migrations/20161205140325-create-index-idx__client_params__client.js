'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'client_params',
		['client'],
  { 	  indexName: 'idx__client_params__client'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('client_params', 'idx__client_params__client');
  }
};