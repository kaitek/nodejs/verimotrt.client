'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'kanban_operations',
		['client'],
  { 	  indexName: 'idx__kanban_operations__client'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('kanban_operations', 'idx__kanban_operations__client');
  }
};