'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'offtimes',
		schema: 'public'
	  },
	  'finish',
	  Sequelize.DATE
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};