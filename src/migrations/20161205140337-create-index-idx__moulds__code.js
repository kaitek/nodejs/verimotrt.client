'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'moulds',
		['code'],
  { 	  indexName: 'idx__moulds__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('moulds', 'idx__moulds__code');
  }
};