'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('client_lost_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING(50)
      },
      client: {
        type: Sequelize.STRING(50)
      },
      day: {
        type: Sequelize.DATEONLY
      },
      jobrotation: {
        type: Sequelize.STRING(50)
      },
      losttype: {
        type: Sequelize.STRING(50)
      },
      faulttype: {
        type: Sequelize.STRING(50)
      },
      faultdescription: {
        type: Sequelize.STRING(255)
      },
      faultgroupcode: {
        type: Sequelize.STRING(50)
      },
      descriptionlost: {
        type: Sequelize.STRING(255)
      },
      tasklist: {
        type: Sequelize.STRING(200)
      },
      mould: {
        type: Sequelize.STRING(50)
      },
      mouldgroup: {
        type: Sequelize.STRING(50)
      },
      opcode: {
        type: Sequelize.STRING(50)
      },
      opnumber: {
        type: Sequelize.INTEGER
      },
      opname: {
        type: Sequelize.STRING(60)
      },
      opdescription: {
        type: Sequelize.STRING(255)
      },
      employee: {
        type: Sequelize.STRING(50)
      },
      erprefnumber: {
        type: Sequelize.STRING(50)
      },
      energy: {
        type: Sequelize.DECIMAL(10,4)
      },
      taskfromerp: {
        type: Sequelize.BOOLEAN
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('client_lost_details');
  }
};