'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'moulds',
		schema: 'public'
	  },
	  'bantime',
	  Sequelize.DATE
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};