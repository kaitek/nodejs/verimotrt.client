'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      code: {
        type: Sequelize.STRING(50)
      },
      name: {
        type: Sequelize.STRING(100)
      },
      secret: {
        type: Sequelize.STRING(100)
      },
      isexpert: {
        type: Sequelize.BOOLEAN
      },
      isoperator: {
        type: Sequelize.BOOLEAN
      },
      ismaintenance: {
        type: Sequelize.BOOLEAN
      },
      ismould: {
        type: Sequelize.BOOLEAN
      },
      isquality: {
        type: Sequelize.BOOLEAN
      },
      jobrotationteam: {
        type: Sequelize.STRING(50)
      },
      lastseen: {
        type: Sequelize.DATE
      },
      strlastseen: {
        type: Sequelize.STRING(50)
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      client: {
        type: Sequelize.STRING(50)
      },
      tasklist: {
        type: Sequelize.STRING(200)
      },
      day: {
        type: Sequelize.DATEONLY
      },
      jobrotation: {
        type: Sequelize.STRING(50)
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('employees');
  }
};