'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('kanban_operations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      client: {
        type: Sequelize.STRING(50)
      },
      product: {
        type: Sequelize.STRING(60)
      },
      boxcount: {
        type: Sequelize.INTEGER
      },
      minboxcount: {
        type: Sequelize.INTEGER
      },
      maxboxcount: {
        type: Sequelize.INTEGER
      },
      currentboxcount: {
        type: Sequelize.INTEGER
      },
      packaging: {
        type: Sequelize.INTEGER
      },
      hourlydemand: {
        type: Sequelize.INTEGER
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('kanban_operations');
  }
};