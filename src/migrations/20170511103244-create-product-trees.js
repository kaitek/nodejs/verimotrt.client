'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('product_trees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      parent: {
        type: Sequelize.STRING(50)
      },
      code: {
        type: Sequelize.STRING(50)
      },
      number: {
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.STRING(255)
      },
      materialtype: {
        type: Sequelize.STRING(50)
      },
      name: {
        type: Sequelize.STRING(60)
      },
      stockcode: {
        type: Sequelize.STRING(50)
      },
      stockname: {
        type: Sequelize.STRING(60)
      },
      tpp: {
        type: Sequelize.DECIMAL(10, 4)
      },
      quantity: {
        type: Sequelize.DECIMAL(10, 4)
      },
      unit_quantity: {
        type: Sequelize.STRING(50)
      },
      opcounter: {
        type: Sequelize.INTEGER
      },
      pack: {
        type: Sequelize.STRING(50)
      },
      packcapacity: {
        type: Sequelize.DECIMAL(10, 4)
      },
      unit_packcapacity: {
        type: Sequelize.STRING(50)
      },
      oporder: {
        type: Sequelize.INTEGER
      },
      preptime: {
        type: Sequelize.INTEGER
      },
      qualitycontrolrequire: {
        type: Sequelize.BOOLEAN
      },
      task: {
        type: Sequelize.BOOLEAN
      },
      feeder: {
        type: Sequelize.STRING(5)
      },
      drainer: {
        type: Sequelize.STRING(5)
      },
      goodsplanner: {
        type: Sequelize.STRING(255)
      },
      productionmultiplier: {
        type: Sequelize.INTEGER
      },
      intervalmultiplier: {
        type: Sequelize.DECIMAL(10, 4)
      },
      delivery:{
        type: Sequelize.BOOLEAN
      },
      start: {
        type: Sequelize.DATE
      },
      finish: {
        type: Sequelize.DATE
      },
      record_id: {
        type: Sequelize.STRING(50)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('product_trees');
  }
};