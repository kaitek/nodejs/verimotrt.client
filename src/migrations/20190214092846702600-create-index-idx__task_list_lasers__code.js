'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'task_list_lasers',
		['code'],
  { 	  indexName: 'idx__task_list_lasers__code'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('task_list_lasers', 'idx__task_list_lasers__code');
  }
};