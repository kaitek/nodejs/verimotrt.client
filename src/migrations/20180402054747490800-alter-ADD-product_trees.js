'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'product_trees',
		schema: 'public'
	  },
	  'carrierdrainer',
	  Sequelize.STRING(255)
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};