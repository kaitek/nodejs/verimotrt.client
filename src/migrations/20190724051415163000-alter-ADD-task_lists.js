'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn({
		tableName: 'task_lists',
		schema: 'public'
	  },
	  'plannedfinish',
	  Sequelize.DATE
	);
  },
  down: function(queryInterface, Sequelize) {
  }
};