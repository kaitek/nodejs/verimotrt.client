'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addIndex(
		'mould_details',
		['mould','mouldgroup','opcode'],
  { 	  indexName: 'idx__mould_details__mould_mouldgroup_opcode'
  });},
  down: function(queryInterface, Sequelize) {
     queryInterface.removeIndex('mould_details', 'idx__mould_details__mould_mouldgroup_opcode');
  }
};