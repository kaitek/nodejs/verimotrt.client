const classBase=require('./base');
const io_port=require('./io_port');
const io_message=require('./io_message');
const io_MessageSendQueue=require('./io_MessageSendQueue');
const io_MessageReceiveQueue=require('./io_MessageReceiveQueue');
const net = require('net');
const processModule = require('process');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});

//Sentry.context(function () {
class io_manager extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='io_manager';
		t.version='0.1';
		t.inputs=false;
		t.outputs=false;
		t.sendqueue=false;
		t.connected=false;
		t.receivequeue=false;
		t.sock=false;
		t.serialport=false;
		t.outputPorts=[];
		t.inputPorts=[];
		t.server=false;
		t.timer_input=false;
		t.readBuff=false;
		t.wait_for_outputs=true;
		Object.assign(this, this.config);
		if(t.io_type=='PLC-USB'||t.io_type=='PLC-RS232'||t.io_type=='PLC-RS232-MB'){
			t.ENQ='05';
			t.ACK='06';
			t.NAK='15';
			t.EOT='04';
			t.ETX='03';
			t.CR='\r';
			t.LF='\n';

			t.Header='<01#';
			t.Delay=100;
		}
		let p;
		for(let i=0;i<this.outputs;i++){
			p=new io_port({type:'output',number:i+1,index:i,io_type:t.io_type});
			p.on('ioportchange',function(p,fevent){
				t.emit('ioportchange',p,fevent);
			});
			this.outputPorts.push(p);
		}
		for(let i=0;i<this.inputs;i++){
			p=new io_port({type:'input',number:i+1,index:i,io_type:t.io_type});
			p.on('ioportchange',function(p,fevent){
				t.emit('ioportchange',p,fevent);
			});
			this.inputPorts.push(p);
		}
	}
	start(){
		let t=this;
		t.emit('ioconnecting');
		//t.mjd('t.io_type:'+t.io_type);
		t.sendqueue=new io_MessageSendQueue({parent:t});
		t.receivequeue=new io_MessageReceiveQueue({parent:t});
		if(t.io_type=='IO Kart'){
			t.server=net.createServer(function(sock) {
				t.socketIp=sock.localAddress;
				t.sock=sock;
				sock.on('data', function(data) {
					t.receive.call(t, sock, data, t.messageParser);
				});
				
				sock.on('close', function() {
					t.mjd('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort + '\n');
					classBase.writeError('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort + '\n',t.hostip,'sock.on.close');
					t.connected=false;
				});
				sock.on('error', function(err){
					t.mjd('t.server.err:'+err);
					classBase.writeError('error: ' + sock.remoteAddress +' '+ sock.remotePort + err +'\n',t.hostip,'sock.on.error');
				});
			});
			t.server.listen(t.local, t.ip);
		}
		if(t.io_type=='PLC-USB'||t.io_type=='PLC-RS232'||t.io_type=='PLC-RS232-MB'){
			t._connect();	
		}
		if(t.io_type==='PLC-SOFT'){
			t.portname='COM0';
			
			setTimeout(() => {
				t.emit('ioconnected',t,t.portname);
				t.mjd('ioconnected: ' + t.portname );
			}, 100);
			setTimeout(()=>{
				t.setInputPorts(0,true);
				t.setOutputPorts('FF',true);
			},100);			
		}
	}
	_reconnect(){
		let t=this;
		if(t.io_type=='PLC-USB'||t.io_type=='PLC-RS232-MB'){
			const serial = require('serial-worker');
			t.emit('ioconnecting',t.portname);
			if(t.serialport==false){
				serial.list(function (err, ports) {
					let _find=false;
					ports.forEach(function(p) {
						if(p.path.indexOf('COM')>-1&&p.path==t.portname){
							_find=true;
						}
					});
					if(_find){
						setTimeout(function() {
							t._connect();
						}, 1000);
					}else{
						setTimeout(function() {
							t._reconnect();
						}, 1000);
					}
				});
			}else{
				console.log('port is not false');
			}
		}
		if(t.io_type=='PLC-RS232'){
			t.emit('ioconnecting',t.portname);
			if(t.serialport==false){
				let serialport_list = require('serialport').list;
				return serialport_list().then(function(ports){
					let _find=false;
					ports.forEach(function(p) {
						if(p.path.indexOf('COM')>-1&&p.path==t.portname){
							_find=true;
						}
					});
					if(_find){
						setTimeout(function() {
							t._connect();
						}, 1000);
					}else{
						setTimeout(function() {
							t._reconnect();
						}, 1000);
					}
				});
				//var SerialPort = require('serialport');
				//SerialPort.list(function (err, ports) {
				//	let _find=false;
				//	ports.forEach(function(p) {
				//		if(p.path.indexOf('COM')>-1&&p.path==t.portname){
				//			_find=true;
				//		}
				//	});
				//	if(_find){
				//		setTimeout(function() {
				//			t._connect();
				//		}, 1000);
				//	}else{
				//		setTimeout(function() {
				//			t._reconnect();
				//		}, 1000);
				//	}
				//});
			}else{
				console.log('port is not false');
			}
		}
	}
	_connect(  ){
		let t = this;
		let serial=null;
		let SerialPort=null;
		try {
			if(t.io_type=='PLC-USB'||t.io_type=='PLC-RS232-MB'){
				serial = require('serial-worker');
			}
			if(t.io_type=='PLC-RS232'){
				SerialPort = require('serialport');
			}
			let _opt=t.io_type=='PLC-RS232-MB'?{baudRate:9600,dataBits:7,parity:'even'}:t.io_type=='PLC-RS232'?{baudRate:9600,dataBits:8,parity:'odd'}:{};
			
			if(t.io_type=='PLC-USB'||t.io_type=='PLC-RS232-MB'){
				t.serialport = new serial.SerialPort(this.portname,_opt);
			}
			if(t.io_type=='PLC-RS232'){
				//t.mjd('this.portname:'+this.portname);
				t.serialport = new SerialPort(this.portname,_opt);
				//console.log(t.serialport);
			}
			t.serialport.on('open', function() {
				t.readBuff=[];
				t.sendqueue._ready=true;
				t.sendqueue.run();
				t.connected=true;
				t.setInputFilter(t.inputFilter);
				setTimeout(() => {
					t.emit('ioconnected',t,t.portname);
				}, 100);
				t.mjd('ioconnected: ' + t.portname );
				if(t.io_type=='PLC-USB'||t.io_type=='PLC-RS232-MB'){
					t.timer_input=setTimeout(function(){
						t.fn_timer_input();
					}, 500);
				}
			});
			t.serialport.on( 'data', function( chunk ) {
				if(t.io_type==='PLC-RS232'){
					let _str=chunk.toString('hex');
					//t.mjd(_str);
					if(_str.length>=2){
						for (let i = 0; i < _str.length; i += 2) {
							let _piece=_str.substring(i, i + 2);
							t.readBuff.push(_piece);
							if(_piece==='0d'||_piece==='0D'){
								let tmp=[];
								while(t.readBuff.length>0){
									tmp.push(t.readBuff.shift());
								}
								t.messageParser_RS232(tmp);
							}
						}
					}
				}else{
					t.receive_PLC.call(t, chunk, (t.io_type=='PLC-USB'?t.messageParser_USB:t.messageParser_RS232));
				}
			});
			t.serialport.on('error', function(err) {
				t.sendqueue._ready=false;
				t.connected=false;
				Sentry.captureException(err);
				console.log('Error: ', err.message);
				t.emit('ioerror',err);
				t.serialport.close();
				t.serialport.removeAllListeners('open');
				t.serialport.removeAllListeners('data');
				t.serialport.removeAllListeners('error');
				t.serialport.removeAllListeners('disconnect');
				t.serialport.removeAllListeners('close');
				clearTimeout(t.timer_input);
				t.timer_input=null;
				setTimeout(function() {
					t.serialport=false;
					t._reconnect();
				}, 1000);
			});
	
			t.serialport.on('disconnect', function (err) { 
				t.sendqueue._ready=false;
				t.connected=false;
				t.emit('iodisconnect',err);
				Sentry.captureException(err);
				console.log('Disconnected. ', err);
				clearTimeout(t.timer_input);
				t.serialport.close();
			});
	
			t.serialport.on('close', function (err) { 
				t.sendqueue._ready=false;
				t.connected=false;
				t.emit('ioclose',err);
				Sentry.captureException(err);
				console.log('Closed. Error: ', err);
				t.serialport.removeAllListeners('open');
				t.serialport.removeAllListeners('data');
				t.serialport.removeAllListeners('error');
				t.serialport.removeAllListeners('disconnect');
				t.serialport.removeAllListeners('close');
				clearTimeout(t.timer_input);
				setTimeout(function() {
					t.serialport=false;
					t._reconnect();
				}, 1000);
			});
		} catch (error) {
			Sentry.captureException(error);
		}		
	}
	setInputFilter(val){
		let t=this;
		if(t.io_type==='PLC-RS232'){
			t.inputFilter=val;
			t.serialport.write([t.inputFilter,0,13]);
		}
	}
	fn_timer_input(){
		let t=this;
		t.readInputAll();
	}
	receive(client, message, cb){
		let msg=this.prepData(message);
		if(msg.length>3)
			cb.call(this, client, msg);
	}
	receive_PLC(message,cb){
		let msg='';
		message.forEach(function(m) {
			msg+=String.fromCharCode(m);
		});
		cb.call(this, msg);
	}
	messageParser(client, message){
		let t=this;
		if(message.length>3){
			let mes;
			if(message.length==6&&!this.connected){
				this.sendqueue._ready=true;
				this.sendqueue.run();
				this.connected=true;
				this.emit('ioconnected',client);
				//classBase.writeError('ioconnected: ' + client.remoteAddress +' '+ client.remotePort + '\n',t.hostip,'sock');
				this._init(client);
			}else{
				let len=0;
				let i;
				if(message[0]=='AA'&&message[1]=='55'){
					for(i=0;i<message.length;){
						len = 5 + parseInt(message[i + 3], 16);
						if(message.length>=len){
							mes=new io_message();
							mes.data=[];
							if(message[i + 5] == '81'){
								mes.type = 'output-clear-ack';
							} 
							if(message[i + 5] == '82'){
								mes.type = 'output-set-ack';
							} 
							if(message[i + 5] == '83'){
								mes.type = 'output-toggle-ack';
							} 
							if(message[i + 5] == '84'){
								mes.type = 'output-clear-all-ack';
							} 
							if(message[i + 5] == '85'){
								mes.type = 'output-set-all-ack';
							} 
							if(message[i + 5] == '86'){
								mes.type = 'output-toggle-all-ack';
							} 
							if(message[i + 5] == '8A'){
								mes.type = 'output-read';
							} 
							if(message[i + 5] == '94'){
								mes.type = 'input-read';
							} 
							if(message[i + 5] == 'FE'){
								mes.type = 'init-0-ack';
							} 
							let crc=0;
							let j=0;
							for(j=i+2;j<i+len-1;j++){
								crc += parseInt(message[j], 16);
							}
							if(parseInt(message[j], 16)==crc%256){
								for(j=i;j<i+len;j++){
									mes.data.push(message[j]);
								}
								this.receivequeue.exec(mes);
								i += len;
							}else{
								console.log('crc hatalı '+ message);
								i += len;
							}
						}else{
							console.log('mesaj uzunluğu hatalı '+ message);
							i += len;
						}
					}
				}else{
					classBase.writeError('mesaj header hatalı: ' + message.join(', ') + '\n',t.hostip,'sock');
					console.log('mesaj header hatalı ' + message.join(', ') + ' len:' + message.length);
					i += len;
				}
			}
		}
	}
	messageParser_USB(message){
		let t=this;
		let mes;
		if(message[3]=='$'){
			let _x=message.split('');
			let _y='';
			_x.forEach(function(_x_item) {
				if(_x_item!='\r'){
					_y+=_x_item;
				}else{
					let _tmp_bcc=_y[_y.length-2]+_y[_y.length-1];
					_y=_y.slice(0,_y.length-2);
					if(t.calcBCC(_y)==_tmp_bcc){
						mes=new io_message();
						mes.data=_y;
						if(_y[4]=='R'&&_y[5]=='C'){
							if(t.sendqueue.msg.type=='input-read-all'){
								mes.type='input-read';
								t.timer_input=setTimeout(function(){
									t.fn_timer_input();
								}, 20);
							}
							if(t.sendqueue.msg.type=='output-read-all'){
								mes.type='output-read';
							}
						}
						if(_y[4]=='W'&&_y[5]=='C'){
							mes.type=t.sendqueue.msg.type+'-ack';
						}
						t.receivequeue.exec(mes);
						_y='';
					}else{
						console.log('bcc error!');
						console.log(_y+_tmp_bcc);
					}
				}
			});
		}
		if(message[3]=='!'){
			console.log('plc-write-error:'+message);
		}
	}
	messageParser_RS232(message){
		//this.mjd('messageParser_RS232');
		while(message.length>5){
			message.shift();
		}
		//this.mjd(message.join('-'));
		let t=this;
		if(t.io_type==='PLC-RS232'){
			if(t.sendqueue.waiting){
				setTimeout(() => {
					t.sendqueue.waiting=false;
				}, 10);
			}
			let fevent=false;
			if(t.wait_for_outputs){
				fevent=true;
				t.wait_for_outputs=false;
			}
			t.setInputPorts(message[0],fevent);
			t.setOutputPorts(message[2],fevent);
		}
	}
	_readAll(){
		console.log('_readAll');
		if(this.io_type!=='PLC-RS232'){
			this.readInputAll();
		}
		this.readOutputAll();
	}
	_init(){
		let mes=new io_message({
			data:[85,170,0,2,0,126]
			,type:'init-0'
		});
		this.sendqueue.exec(mes);
	}
	inputSetter(number){
		let p=this.getInputPort(number);
		//console.log('inputSetter',number,p);
		if(p!==false){
			p.setInputOnStart(1,true,'5');
		}
	}
	clearOutput(number){
		let p=this.getOutputPort(number);
		if(p!==false&&p.curval!==0){
			if(this.io_type!=='PLC-SOFT'){
				let mes=new io_message({
					data:(this.io_type=='IO Kart'?p.clearOutput():(this.io_type=='PLC-USB'||this.io_type=='PLC-RS232'||this.io_type=='PLC-RS232-MB'?this._writebit('y',p.index,false):''))
					,type:'output-clear'
					,readData:'O'+number+':0 '
					,portnumber:number
					,portval:false
				});
				this.sendqueue.exec(mes);
			}else{
				p.setCurVal(0,true,'1');
			}
		}
	}
	setOutput(number){
		let p=this.getOutputPort(number);
		if(p!==false&&p.curval!==1){
			if(this.io_type!=='PLC-SOFT'){
				let mes=new io_message({
					data:(this.io_type=='IO Kart'?p.setOutput():(this.io_type=='PLC-USB'||this.io_type=='PLC-RS232'||this.io_type=='PLC-RS232-MB'?this._writebit('y',p.index,true):''))
					,type:'output-set'
					,readData:'O'+number+':1 '
					,portnumber:number
					,portval:true
				});
				this.sendqueue.exec(mes);
			}else{
				p.setCurVal(1,true,'2');
			}
		}
	}
	toggleOutput(number){
		let p=this.getOutputPort(number);
		if(p!==false){
			if(this.io_type!=='PLC-SOFT'){
				let mes=new io_message({
					data:(this.io_type=='IO Kart'?p.toggleOutput():(this.io_type=='PLC-USB'||this.io_type=='PLC-RS232'||this.io_type=='PLC-RS232-MB'?this._writebit('y',p.index,(p.curval==1?false:true)):''))
					,type:'output-toggle'
					,readData:'O'+number+':'+p.curval+' '
					,portnumber:number
					,portval:(p.curval==1?false:true)
				});
				this.sendqueue.exec(mes);
			}else{
				p.setCurVal((p.curval==1?0:1),true,'3');
			}
		}
	}
	clearInput(number){
		let p=this.getInputPort(number);
		if(p!==false&&(p.curval!==0||this.io_type==='PLC-SOFT')){
			if(this.io_type==='PLC-SOFT'){
				p.setCurVal(0,true,'4');
			}
		}
	}
	setInput(number){
		let p=this.getInputPort(number);
		if(p!==false&&(p.curval!==1||this.io_type==='PLC-SOFT')){
			if(this.io_type==='PLC-SOFT'){
				p.setCurVal(1,true,'5');
			}
		}
	}
	clearAll(){
		if(this.io_type=='IO Kart'){
			let mes=new io_message({
				data:[85,170,0,2,0,4]//clearAll
				,type:'output-clear-all'
			});
			for(let i=0;i<this.outputs;i++){
				mes.readData+='O'+(i+1)+':0 ';
			}
			this.sendqueue.exec(mes);
		}else if(this.io_type==='PLC-SOFT'){
			let p;
			for(let i=0;i<this.outputs;i++){
				p=this.getOutputPort(i+1);
				p.setCurVal(0,true,'6');
			}
		}
	}
	setAll(){
		if(this.io_type=='IO Kart'){
			let mes=new io_message({
				data:[85,170,0,2,0,5]//setAll
				,type:'output-set-all'
			});
			for(let i=0;i<this.outputs;i++){
				mes.readData+='O'+(i+1)+':1 ';
			}
			this.sendqueue.exec(mes);
		}else if(this.io_type==='PLC-SOFT'){
			let p;
			for(let i=0;i<this.outputs;i++){
				p=this.getOutputPort(i+1);
				p.setCurVal(1,true,'7');
			}
		}
	}
	toggleAll(){
		if(this.io_type=='IO Kart'){
			let mes=new io_message({
				data:[85,170,0,2,0,6]//toggleAll
				,type:'output-toggle-all'
			});
			let p;
			for(let i=0;i<this.outputs;i++){
				p=this.getOutputPort(i+1);
				mes.readData+='O'+(i+1)+':'+(p.curval==1?0:1)+' ';
			}
			this.sendqueue.exec(mes);
		}else if(this.io_type==='PLC-SOFT'){
			let p;
			for(let i=0;i<this.outputs;i++){
				p=this.getOutputPort(i+1);
				p.setCurVal((p.curval==1?0:1),true,'8');
			}
		}
	}
	readOutputAll(){
		let _data=(this.io_type=='IO Kart'?[85,170,0,2,0,10]:(this.io_type=='PLC-USB'||this.io_type=='PLC-RS232'||this.io_type=='PLC-RS232-MB'?this._readWord('y',0):''));
		let mes=new io_message({
			data:_data
			,type:'output-read-all'
		});
		this.sendqueue.exec(mes);
	}
	readInputAll(){
		let _data=(this.io_type=='IO Kart'?[85,170,0,2,0,20]:(this.io_type=='PLC-USB'||this.io_type=='PLC-RS232'||this.io_type=='PLC-RS232-MB'?this._readWord('x',0):''));
		let mes=new io_message({
			data:_data
			,type:'input-read-all'
		});
		if(this.sendqueue){
			this.sendqueue.exec(mes);
		}
	}
	setOutputPorts(val,fevent){
		let sval='00000000'+parseInt(val, 16).toString(2);
		sval=sval.substr(-this.outputs).reverse();
		let aval=sval.split('');
		if(fevent){
			this.emit('setoutputports');
		}
		let p;
		console.log('setOutputPorts:',val,'|',sval,'|',aval);
		for(let i=0;i<this.outputs;i++){
			p=this.getOutputPort(i+1);
			p.setCurVal(parseInt(aval[i]),fevent,'9');
		}
	}
	setInputPorts(val,fevent){
		let sval='00000000'+parseInt(val, 16).toString(2);
		sval=sval.substr(-this.inputs).reverse();
		let aval=sval.split('');
		console.log('setInputPorts:',val,'|',sval,'|',aval);
		let p;
		for(let i=0;i<this.inputs;i++){
			p=this.getInputPort(i+1);
			//console.log('setInputPorts p',(i+1),'|',parseInt(p.curval),'|',parseInt(aval[i]));
			if(parseInt(p.curval)!=parseInt(aval[i])){
				p.setCurVal(parseInt(aval[i]),fevent,'10');
			}else{
				if(this.io_type=='IO Kart'&&fevent){
					p.setCurVal(parseInt(aval[i]),fevent,'11');
				}
			}
		}
	}
	prepData(message){
		let msg=[];
		for(let i=0;i<message.length;i++){
			msg.push(('00' + message[i].toString(16).toUpperCase()).substr(-2));
		}
		return msg;
	}
	calcCRC(data){
		let crc=0;
		for(let i=2;i<data.length;i++){
			crc+=data[i];
		}
		return crc%256;
	}
	getOutputPort(number){
		let p;
		for(let i=0;i<this.outputPorts.length;i++){
			p=this.outputPorts[i];
			if(p.number==number){
				return p;
			}
		}
		return false;
	}
	getInputPort(number){
		let p;
		for(let i=0;i<this.inputPorts.length;i++){
			p=this.inputPorts[i];
			if(p.number==number){
				return p;
			}
		}
		return false;
	}
	_readWord(_code, _address){
		let t=this;
		if(t.io_type=='PLC-USB'){
			let v_temp=this.Header+'RCC' + _code.toString() + ('0000'+_address).slice(-4) + ('0000'+_address).slice(-4);
			let v_senddata = v_temp.toUpperCase() + this.calcBCC(v_temp.toUpperCase()) + this.CR;
			return v_senddata;
		}
		if(t.io_type=='PLC-RS232'){
			let s='AA\r';
			let _byte=[];
			for(let i=0;i<s.length;i++){
				_byte.push(s.charCodeAt(i));
			}
			return _byte;
		}
		if(t.io_type=='PLC-RS232-MB'){
			let device='01';
			let fn=_code==='x'?'02':'01';//x-input,y-output
			let addr=(_code==='x'?'04':'05')+('00'+_address).slice(-2);
			let val='0008';
			let s=':'+device+fn+addr+val+(this.lrc_from_str(device+fn+addr+val)).toString(16).toUpperCase()+'\r\n';
			let _byte=[];
			for(let i=0;i<s.length;i++){
				_byte.push(s.charCodeAt(i));
			}
			return _byte;
		}
	}
	_writebit(_code, _address, _data){
		let t=this;
		if(t.io_type=='PLC-USB'){
			let v_cdata=_data?'1':'0';
			let v_temp=this.Header+'WCS' + _code.toString() + ('0000'+_address).slice(-4) + v_cdata;
			let v_senddata = v_temp.toUpperCase() + this.calcBCC(v_temp.toUpperCase()) + this.CR;
			return v_senddata;
		}
		
		if(t.io_type=='PLC-RS232'){
			let val=(_data?'1':'0');//set, reset:0000
			let s=(_address+1)+val+'\r';
			let _byte=[];
			for(let i=0;i<s.length;i++){
				_byte.push(s.charCodeAt(i));
			}
			return _byte;
		}
		if(t.io_type=='PLC-RS232-MB'){
			let device='01';
			let fn='05';//force single coil
			let addr='05'+('00'+_address).slice(-2);//ilk output 0500
			let val=(_data?'FF00':'0000');//set, reset:0000
			let s=':'+device+fn+addr+val+(this.lrc_from_str(device+fn+addr+val)).toString(16).toUpperCase()+'\r\n';
			let _byte=[];
			for(let i=0;i<s.length;i++){
				_byte.push(s.charCodeAt(i));
			}
			return _byte;
		}
	}
	calcBCC(data){
		let temp = data.split('');
		let bcc = temp[0].charCodeAt(0);
		for(let i=1;i<temp.length;i++){
			bcc = bcc ^ temp[i].charCodeAt(0);
		}
		return ('0'+(Number(bcc).toString(16))).slice(-2).toUpperCase();
	}
	lrc_from_buff(buffer) {
		var lrc = 0;
		for (let i = 0; i < buffer.length; i++) {
			lrc += buffer[i] & 0xFF;
		}
	
		return ((lrc ^ 0xFF) + 1) & 0xFF;
	}
	lrc_from_str(str) {
		let bytes = [];
		for (let c = 0; c < str.length; c += 2)
			bytes.push(parseInt(str.substr(c, 2), 16));
		return this.lrc_from_buff(bytes);
	}
}
module.exports=io_manager;
let _pid=false;
let _params=false;
let IO=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'init_io_manager':{
		_pid=obj.pid;
		_params=obj.data;
		io_connect();
		break;
	}
	case 'readall':{
		IO._readAll(obj.pos);
		break;
	}
	case 'output_trigger':{
		if(obj.value===1){
			IO.setOutput(obj.number);
		}else{
			IO.clearOutput(obj.number);
		}
		break;
	}
	case 'input_set':{
		IO.inputSetter(obj.number);
		break;
	}
	case 'input_trigger':{
		if(obj.value===1){
			IO.setInput(obj.number);
		}else{
			IO.clearInput(obj.number);
		}
		break;
	}
	case 'setAll':{
		IO.setAll();
		break;
	}
	case 'clearAll':{
		IO.clearAll();
		break;
	}
	case 'toggleAll':{
		IO.toggleAll();
		break;
	}
	case 'set_inputfilter':{
		IO.setInputFilter(obj.data);
		break;
	}
	default:{
		console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	//processModule.send(_pid + ': ' + e);
	Sentry.captureMessage('iomanager ' + err);
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(200);
	}, 1000);
	return;
});
let io_connect = ()=>{
	Sentry.configureScope(scope => {
		scope.setTag('ip',_params.hostip);
		scope.setTag('client_name',_params.client_name);
		scope.setTag('version',_params.versionApp);
		scope.setTag('build',_params.buildNumber);
		//scope.setUser({ ip: t.cfg_dcs_server_IP });
	});
	IO=new io_manager(_params);
	IO.on('iodatareceive', (mes)=>{
		processModule.send({event:'iodatareceive',pid:_pid,mes:mes});
	});
	IO.on('iodatasend', (mes)=>{
		processModule.send({event:'iodatasend',pid:_pid,mes:mes});
	});
	IO.on('ioconnecting', ()=>{
		processModule.send({event:'ioconnecting',pid:_pid});
	});
	IO.on('ioconnected', ()=>{
		processModule.send({event:'ioconnected',pid:_pid});
	});
	IO.on('ioerror', ()=>{
		processModule.send({event:'ioerror',pid:_pid});
	});
	IO.on('iodisconnect', ()=>{
		processModule.send({event:'iodisconnect',pid:_pid});
	});
	IO.on('ioclose', ()=>{
		processModule.send({event:'ioclose',pid:_pid});
	});
	IO.on('ioportchange', (port,fevent)=>{
		processModule.send({event:'ioportchange',pid:_pid,port:port.type+'|'+port.number+'|'+port.curval,fevent:fevent});
	});
	IO.on('sendqueuechange', (len)=>{
		processModule.send({event:'sendqueuechange',pid:_pid,length:len});
	});
	IO.on('receivequeuechange', (len)=>{
		processModule.send({event:'receivequeuechange',pid:_pid,length:len});
	});
	IO.on('setoutputports', ()=>{
		processModule.send({event:'setoutputports',pid:_pid});
	});
	IO.on('ioqueue_empty', ()=>{
		processModule.send({event:'ioqueue_empty',pid:_pid});
	});
	processModule.send({event:'io_manager_inited',pid:_pid,inputs:IO.inputPorts,outputs:IO.outputPorts,inputFilter:_params.inputFilter});
	IO.start();	
};
//});
