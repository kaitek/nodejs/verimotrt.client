const processModule = require('process');
const timezoneJS = require('timezone-js');
const tzdata = require('tzdata');
const dcs_client=require('./dcs_client');

let _pid=false;
let _dcs_client=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'start_dcs_client':{
		_pid=obj.pid;
		let _tz = timezoneJS.timezone;
		_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
		_tz.loadZoneDataFromObject(tzdata);
		_dcs_client=new dcs_client(obj.param);
		_dcs_client.addListener('dcs_inited', function(){
			processModule.send({event:'dcs_inited',pid:_pid});
		});
		_dcs_client.addListener('close-verimotrt', function(){
			processModule.send({event:'close-verimotrt',pid:_pid});
		});
		_dcs_client.addListener('reset-verimotrt', function(){
			processModule.send({event:'reset-verimotrt',pid:_pid});
		});
		_dcs_client.addListener('iodevice_manager_tcp_clear', function(){
			processModule.send({event:'iodevice_manager_tcp_clear',pid:_pid});
		});
		processModule.send({event:'started_dcs_client',pid:_pid});
		break;
	}
	case 'cn_sn':{
		break;
	}
	case 'cn_sn_setlock':{
		break;
	}
	case 'exit_viewer':{
		_dcs_client.exit_viewer();
		break;
	}
	default:{
		console.log(obj);
		break;
	}
	}
});
