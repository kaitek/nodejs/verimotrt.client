const classBase=require('./base');
const timezoneJS = require('timezone-js');
const tzdata = require('tzdata');
let _tz = timezoneJS.timezone;
_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
_tz.loadZoneDataFromObject(tzdata);
class dcs_timer extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='dcs_timer';
		t.version='0.1.0';
		t.timer=false;
		t.obj_time=false;
		t.days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		Object.assign(this, this.config);
	}
	start(_d){
		let t=this;
		let d=new timezoneJS.Date(_d);
		let gt=d.getTime();
		let _f=1000-(gt%1000);
		t.setValue(gt+_f);
		if(!t.timer){
			t.timer=setTimeout(function(){
				t.tick();
			}, _f+10);
		}
	}
	stop(){
		let t=this;
		if(t.timer){
			clearTimeout(t.timer);
			t.timer=false;
		}
	}
	setValue(__ms){
		let t=this;
		let x=new timezoneJS.Date(__ms);
		let _y=x.toString('yyyy');
		let _m=x.toString('MM');
		let _d=x.toString('dd');
		let _h=x.toString('HH');
		let _i=x.toString('mm');
		let _s=x.toString('ss');
		let _msec=x.getTime();
		let _ms=x.toString('SSS');
		t.obj_time={
			obj:x
			,ms:_msec
			,strms:_ms
			,strHour:_h
			,strMinute:_i
			,strSecond:_s
			,strTime:_h+':'+_i+':'+_s
			,strTimems:_h+':'+_i+':'+_s+'.'+_ms
			,strDate:_y+'-'+_m+'-'+_d
			,strDateTime:_y+'-'+_m+'-'+_d+' '+_h+':'+_i+':'+_s
			,strDateTimeClear:_y+_m+_d+_h+_i+_s
			,strDateTimems:_y+'-'+_m+'-'+_d+' '+_h+':'+_i+':'+_s+'.'+_ms
			,strDay: t.days[x.getDay()]//x.toLocaleString('en-US',{weekday:'long'})
		};
	}
	tick(){
		let t=this;
		t.emit('tick',{scope:t.self,objtime:t.obj_time});
		let d=new timezoneJS.Date();
		let gt=d.getTime();
		let _f=1000-(gt%1000);
		t.timer=setTimeout(function(){
			t.setValue(t.obj_time.ms+1000);
			t.tick();
		}, _f+10);
	}
	zfill(num, len) {
		return (Array(len).join('0') + num).slice(-len);
	}
}
module.exports=dcs_timer;