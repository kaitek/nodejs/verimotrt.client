const processModule = require('process');
const amqp = require('amqplib/callback_api');
const Promise = require('promise');
const Sequelize = require('sequelize');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});
let sequelize;
let conn=null;
let pubChannel = null;
let _pid=false;
let _param=false;
let _config=false;
let arr_queue_assert=[];
let arr_tmp_queue_assert=[];
let _url_amqp=false;
let _ip=false;
let _code=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'init_server':{
		_pid=obj.pid;
		_param=obj.param;
		_config=obj.config;
		_ip=obj.ip;
		_code=_config.client_name.value;
		Sentry.configureScope(scope => {
			scope.setTag('ip',_ip);
			scope.setTag('client_name',_code);
			scope.setTag('version',obj.versionApp);
			scope.setTag('build',obj.buildNumber);
		});
		_url_amqp='amqp://admin:q32wxcx@'+_config.dcs_server_IP.value;
		console.log('RabbitMQ_send Process PID:'+_pid);
		connect_amqp();
		break;
	}
	case 'holo_start':{
		if(pubChannel!==null){
			pubChannel.assertExchange('hololens', 'direct', {durable: true},function(err, ok){
				if(err){
					console.error('[AMQP-S] error-assertExchange-holo', err);
					setTimeout(function(){
						process.exit(533);
					}, 1000);
				}
				pubChannel.assertQueue('from_holo_'+_code,{durable: true},function(err, ok){
					if(err){
						console.error('[AMQP-S] error-assertQueue-holo', err);
						setTimeout(function(){
							process.exit(534);
						}, 1000);
					}
					pubChannel.bindQueue('from_holo_'+_code, 'hololens', 'from_holo_'+_code);
				});
			});
			
		}
		break;
	}
	case 'holo_finish':{
		if(pubChannel!==null){
			//pubChannel.unbindQueue('from_holo_'+_code, 'hololens', 'from_holo_'+_code,function(err, ok){
			pubChannel.deleteQueue('from_holo_'+_code);
			//});
		}
		break;
	}
	default:{
		console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(500);
	}, 1000);
	return;
});
let connect_amqp = ()=>{
	amqp.connect(_url_amqp,function(_err,_conn) {
		if (_err) {
			console.error('[AMQP-S]', _err);
			Sentry.captureException(_err);
			setTimeout(function(){
				process.exit(501);
			}, 1000);
			return;
		}
		if(conn==null){
			conn=_conn;
			conn.on('error', function(err) {
				if (err.message && err.message !== 'Connection closing') {
					console.error('[AMQP-S] conn error', err);
				}
				Sentry.captureException(err);
				setTimeout(function(){
					process.exit(502);
				}, 1000);
				return;
			});
			conn.on('close', function() {
				setTimeout(function(){
					process.exit(503);
				}, 1000);
				return;
			});
		}
		createChannel();
	});
};
let createChannel=()=> {
	conn.createChannel(function(err, ch) {
		if (closeOnErr(err)) return;
		if(pubChannel==null){
			pubChannel = ch;
			pubChannel.on('error', function(err) {
				console.error('[AMQP-S] channel error', err);
				Sentry.captureException(err);
				setTimeout(function(){
					process.exit(504);
				}, 1000);
				return;
			});
			pubChannel.on('close', function() {
				console.log('[AMQP-S] channel closed');
				setTimeout(function(){
					process.exit(505);
				}, 1000);
				return;
			});
			processModule.send({event:'cp_server_inited',pid:_pid});
			db_connect(_param);
		}
	});
};
let closeOnErr=(err)=> {
	if (!err) return false;
	console.error('[AMQP-S] error', err);
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(506);
	}, 1000);
	return true;
};
let db_connect = (obj) => {
	sequelize = new Sequelize(obj.database, obj.username, obj.password, {
		host: obj.host,
		dialect: 'postgres',
		logging: false,
		pool: {
			max: 20,
			min: 0,
			acquire: 100000,
			idle: 10000
		},
		timezone:obj.timezone
	});
	return sequelize
		.authenticate()
		.then(() => {
			return Promise.all([queue_asserter()]).then(()=>{
				process.nextTick(function(){
					setTimeout(function() {
						return Promise.all([run()]);
					}, 10);
				});
			});
		})
		.catch(err => {
			Sentry.captureException(err);
			setTimeout(function(){
				process.exit(507);
			}, 1000);
			return;
		});
};
let control_arr_tmp_queue_assert=()=>{
	if(arr_tmp_queue_assert.length>0){
		process.nextTick(function(){
			setTimeout(function() {
				return Promise.all([control_arr_tmp_queue_assert()]);
			}, 50);
		}); 
	}else{
		return true;
	}
};
let queue_asserter=()=>{
	try {
		if(pubChannel!==null){
			if(_code&&_code!==''){
				if(arr_queue_assert.indexOf(_code+'_client')===-1){
					arr_tmp_queue_assert.push(_code+'_client');
					pubChannel.assertQueue(_code+'_client',{durable: true},function(err, ok){
						if(err){
							console.error('[AMQP-S] error-assertQueue', err);
							setTimeout(function(){
								process.exit(531);
							}, 1000);
						}
						arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
					});
					arr_queue_assert.push(_code+'_client');
				}
			}else{
				if(_ip!==''){
					if(arr_queue_assert.indexOf(_ip+'_client')===-1){
						arr_tmp_queue_assert.push(_ip+'_client');
						pubChannel.assertQueue(_ip+'_client',{durable: true},function(err, ok){
							if(err){
								console.error('[AMQP-S] error-assertQueue', err);
								setTimeout(function(){
									process.exit(532);
								}, 1000);
							}
							arr_tmp_queue_assert.splice(arr_tmp_queue_assert.indexOf(ok.queue),1);
						});
						arr_queue_assert.push(_ip+'_client');
					}
				}
			}
		}else{
			setTimeout(function(){
				process.exit(508);
			}, 1000);
		}
	} catch (error) {
		setTimeout(function(){
			process.exit(529);
		}, 1000);
	}
	return Promise.all([control_arr_tmp_queue_assert()]).then(function(){
		process.nextTick(function(){
			setTimeout(function() {
				queue_asserter();
			}, 10000);
		});
	});
};
let run=()=>{
	let _promises=[];
	return Promise.all(_promises).then(()=>{
		process.nextTick(function(){
			setTimeout(function() {
				importData();
			}, 500);
		});
	}).catch(e => {
		Sentry.captureException(e);
		setTimeout(function(){
			process.exit(509);
		}, 1000);
		return;
	});
};

let importData=()=>{
	return sequelize.query('SELECT * from "_sync_messages" where queued=0 order by case when data->>\'tableName\'=\'employees\' then 0 else 1 end,id limit 100 '
		, { type: sequelize.QueryTypes.SELECT}).then(function(results) {
		let _promises=[];
		try {
			for(let i=0;i<results.length;i++){
				let result=results[i];
				if(pubChannel!==null){
					const _q=(result.type!=='watch_input_signal_hololens'?((_code!==''&&_code!==null?_code:_ip)+'_client'):'from_holo_'+_code);
					pubChannel.sendToQueue(_q, new Buffer.from(JSON.stringify(result)), {persistent: true});
					_promises.push(recordDelete(result.id));
				}
			}
		} catch (error) {
			Sentry.captureException(error);
			setTimeout(function(){
				process.exit(528);
			}, 1000);
			return;
		}
		return Promise.all(_promises).catch(e => {
			Sentry.captureException(e);
			setTimeout(function(){
				process.exit(525);
			}, 1000);
			return;
		});
	}).then(function(){
		return sequelize.query('delete from task_lists where id in (SELECT max(id)id from task_lists GROUP BY record_id having count(*)>1)').catch(e => {
			Sentry.captureException(e);
			setTimeout(function(){
				process.exit(526);
			}, 1000);
			return;
		});
	}).then(function(){
		process.nextTick(function(){
			setTimeout(function() {
				run();
			}, 100);
		});
	}).catch(e => {
		Sentry.captureException(e);
		setTimeout(function(){
			process.exit(530);
		}, 1000);
		return;
	});
};
let recordDelete = (_id,_messageId) => {
	let rep={
		id:_id
	};
	let _w='';
	if(_messageId){
		_w=' or "messageId"=:messageId';
		rep.messageId=_messageId;
	}
	let sql='DELETE from "_sync_messages" where ("id"=:id '+_w+') ';
	return sequelize.query(sql, { replacements:rep, type: sequelize.QueryTypes.DELETE}).catch(e => {
		Sentry.captureException(e);
		setTimeout(function(){
			process.exit(527);
		}, 1000);
		return;
	});
};