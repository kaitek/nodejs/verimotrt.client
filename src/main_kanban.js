const electron = require('electron');
const cp = require('child_process');
let child;
let pid;
let procOptions={stdio:'inherit',detached: true,env: 'production'};

function startApp()
{
	let onClose = function (code) {
		console.log('process exit code:' + code+ ' pid:'+pid);
		process.exit(code);
	};
	child = cp.spawn(electron, [__dirname+'/p_kanban.js',process.argv[2]],procOptions);
	pid=child.pid;
	child.on('close', onClose);
	child.on('error', function (err) {
		console.log('bs-process error '+err );
		child.kill('SIGINT');
	});
	child.on('exit', function (code) {
		console.log(`Child exited with code ${code}`);
	});
	child.on('disconnect', function () {
		console.log('Child disconnected');
	});
	return child;
}
startApp();
exports.startApp = startApp;