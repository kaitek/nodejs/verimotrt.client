const processModule = require('process');
const cp = require('child_process');
const spawn = cp.spawn;

let _pid=false;
let _conf=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'start_viewer':{
		_pid=obj.pid;
		processModule.send({event:'started_viewer',pid:_pid});
		break;
	}
	case 'dcs_inited':{
		_conf=obj.conf;
		createWindow();
		break;
	}
	case 'cn_sn_setlock':{
		break;
	}
	default:{
		console.log(obj);
		break;
	}
	}
});

let createWindow=()=>{
	const bat = spawn(_conf.path, 
		[
			'pos='+_conf.pos
			,'width='+_conf.width
			,'height='+_conf.height
			,'kiosk='+_conf.kiosk
			,'frame='+true//_conf.frame
			,'title='+_conf.title
			,'icon='+_conf.icon
			,'skipTaskbar='+_conf.skipTaskbar
			,'alwaysOnTop='+_conf.alwaysOnTop
			,'maximized='+_conf.maximized
			,'url='+_conf.url
			,'battery=false'
		], {cwd: _conf.wd});

	bat.stdout.on('data', (data) => {
		console.log(data.toString());
	});

	bat.stderr.on('data', (data) => {
		console.log(data.toString());
	});

	bat.on('exit', (code) => {
		processModule.send({event:'exit',pid:_pid,code:code});
		console.log(`viewer exited with code ${code}`);
	});
};