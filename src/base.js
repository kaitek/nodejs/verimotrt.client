const EventEmitter = require('events').EventEmitter;

const timezoneJS = require('timezone-js');
const tzdata = require('tzdata');
let _tz = timezoneJS.timezone;
_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
_tz.loadZoneDataFromObject(tzdata);
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});
String.prototype.reverse=function(){return this.split('').reverse().join('');};

function applyIf(obj,config){
	if(typeof obj!=='object')
		obj={};
	if(config)
		for(let p in config)
			if(typeof obj[p]=='undefined')
				obj[p]=config[p];
	return obj;
}
exports.applyIf = applyIf;
function writeError(_error/*,_ip,_pos,_obj*/){
	Sentry.captureException(_error);
	return ;
}
exports.writeError = writeError;

function getIp() {
	var os=require('os');
	if(os.hostname().indexOf('oguzhan')>-1){
		return '192.168.1.40';
	}else{
		var i, v, value;
		var networkInterfaces = os.networkInterfaces();
		/* jshint -W015: true */
		interfaces: for (i in networkInterfaces) {
			if(i.toLowerCase().indexOf('virtual')==-1){
				for (v in i) {
					if (networkInterfaces[i][v] && networkInterfaces[i][v].address && networkInterfaces[i][v].address.length && !networkInterfaces[i][v].internal&&networkInterfaces[i][v].family==='IPv4'&& networkInterfaces[i][v].address!=='10.10.100.100') {
						value = networkInterfaces[i][v].address;
						break interfaces;
					}
				}
			}
		}
		return value;
	}
}
exports.getIp = getIp;

class MyBase extends EventEmitter {
	constructor(config) {
		super(); 
		this.config=config;
		Object.assign(this, config);
		this.init();
	}
	init(){
		let self = this;
		self.componentClass='MyBase';
	}
	getTime(){
		this.time=new timezoneJS.Date('Europe/Istanbul');
		return this.time;
	}
	_setTime(){
		this.time=new timezoneJS.Date();
	}
	mjd(obj){
		if(typeof obj=='string')
			console.log(this.getFullLocalDateString()+' '+obj);
		else{
			console.log(this.getFullLocalDateString());
			for(let p in obj){
				console.log(p+':'+obj[p]);
			}
		}
	}
	getFirstTimeOfJobRotaion(_t,_jobrotation){
		let _time=new timezoneJS.Date(_t);
		let epoch=_time.getTime();
		try {
			let jr_start=_jobrotation.split('-')[0];
			let jr_end=_jobrotation.split('-')[1];
			if(jr_start>jr_end){
				_time=new timezoneJS.Date(epoch-(1 * 24 * 60 * 60 * 1000));
				//_time.setDate(_time.getDate()-1);
			}
			_time.setHours(jr_start.split(':')[0]);
			_time.setMinutes(jr_start.split(':')[1]);
			_time.setSeconds(0);
			_time.setMilliseconds(0);
		} catch (error) {
			console.log('err-getFirstTimeOfJobRotaion');
			console.log('_jobrotation:'+_jobrotation);
			console.log(error);
		}
		return _time.toString('yyyy-MM-dd HH:mm:ss');
	}
	getLastTimeOfJobRotaion(_t,_jobrotation){
		let _time=new timezoneJS.Date(_t);
		try {
			let jr_end=_jobrotation.split('-')[1];
			_time.setHours(jr_end.split(':')[0]);
			const _min_end=jr_end.split(':')[1];
			_time.setMinutes(_min_end=='00'?'59':parseInt(_min_end)-1);
			_time.setSeconds(59);
			_time.setMilliseconds(0);
		} catch (error) {
			console.log('err-getLastTimeOfJobRotaion');
			console.log('_t:'+_t+' _jobrotation:'+_jobrotation);
			console.log(error);
		}		
		return _time.toString('yyyy-MM-dd HH:mm:ss');
	}
	getNextJobRotationFirstTime(_t,_jobrotation){
		let _time=new timezoneJS.Date(this.getLastTimeOfJobRotaion(_t,_jobrotation));
		_time.setTime(_time.getTime()+1000);
		return _time.toString('yyyy-MM-dd HH:mm:ss');
	}
	getJobRotationDay(_jr,_t,_retcode,_t2){
		let _d=new timezoneJS.Date(_t);
		let epoch=_d.getTime();
		let sdate=_d.toString('yyyy-MM-dd');
		let itime=_d.toString('HHmmss');
		let item=false;
		let item_b=false;
		let item_e=false;
		//itime='072959';
		if(_jr.length>0){
			for(let i=0;i<_jr.length;i++){
				item=_jr[i];
				item_b=item.begin.replace(new RegExp(':', 'g'),'');
				item_e=item.end.replace(new RegExp(':', 'g'),'');
				if(item_b.length<6){
					item_b+='00';
				}
				if(item_e.length<6){
					item_e+='59';
				}
				if(item_b<item_e){
					if(itime>=item_b&&itime<=item_e){
						if(!_retcode){
							return sdate;
						}else{
							return item.code;
						}
					}
				}else{
					//23:30-07:30, gün olarak sonraki gün alınacak
					if(!_retcode){
						if(itime>=item_b&&itime>item_e){
							_d=new timezoneJS.Date(epoch+(1 * 24 * 60 * 60 * 1000));
							//_d.setDate(_d.getDate() + 1);
							sdate=_d.toString('yyyy-MM-dd');
							return sdate;
						}
						if(itime<item_b&&itime<=item_e){
							return sdate;
						}
					}else{
						if((itime>=item_b&&itime>item_e)||(itime<item_b&&itime<=item_e)){
							return item.code;
						}
					}
				}
			}
			console.log('_t:'+_t+' _t2:'+_t2);
			console.log(_jr);
		}else{
			this.mjd('getJobRotationDay-_jr.length==0');
			this.mjd(sdate);
			return sdate;
		}
	}
	getTimeString(){
		this._setTime();
		return this.time.toString('HH:mm:ss.SSS');
	}
	getFullLocalDateString(){
		this._setTime();
		return this.time.toString('yyyy-MM-dd HH:mm:ss.SSS');
	}
	secToTime(seconds) {
		let hh = Math.floor(seconds / 3600);
		let mm = Math.floor((seconds - (hh * 3600)) / 60);
		let ss = seconds - (hh * 3600) - (mm * 60);
	
		if (hh < 10) {hh = '0' + hh;}
		if (mm < 10) {mm = '0' + mm;}
		if (ss < 10) {ss = '0' + ss;}
	
		return hh + ':' + mm + ':' + ss;
	}
	convertFullLocalDateString(_time,_ms){
		if(_time==null){
			return null;
		}
		let dt = new timezoneJS.Date(_time);
		return dt.toString('yyyy-MM-dd HH:mm:ss'+(_ms==true?'.SSS':''));
	}
	convertLocalDateString(_time){
		if(_time==null){
			return null;
		}
		let dt = new timezoneJS.Date(_time);
		return dt.toString('yyyy-MM-dd');
	}
	randomIntInc(low, high) {
		return Math.floor(Math.random() * (high - low + 1) + low);
	}
	zfill(num, len) {
		return (Array(len).join('0') + num).slice(-len);
	}
	getContent(host,app,path,obj, timeout = 3000) {
		return new Promise((resolve, reject) => {
			let timer;
			const https = require('https');
			let options = {
				hostname: host,
				path: '/'+app+'/tr/'+path,
				method: 'POST',
				headers: {
					'X-Requested-With':'XMLHttpRequest'
					,'Content-Type':'application/x-www-form-urlencoded'
					//,'X-HTTP-Method-Override': 'PUT' 
				},
				rejectUnauthorized: false,
				requestCert: true,
				agent: false
			};
			const request = https.request(options, (response) => {
				// handle http errors
				if (response.statusCode < 200 || response.statusCode > 299) {
					clearTimeout(timer);
					reject(new Error('Failed to load page, status code: ' + response.statusCode));
				}
				// temporary data holder
				const body = [];
				// on every content chunk, push it to the data array
				response.on('data', (chunk) => body.push(chunk));
				// we are done, resolve promise with those joined chunks
				response.on('end', () => {
					clearTimeout(timer);
					resolve(body.join(''));
				});
			});
			// handle connection errors of the request
			request.on('error', (err) => {
				clearTimeout(timer);
				reject(err);
			});
			request.write(JSON.stringify(obj));
			request.end();
			timer = setTimeout(() => {
				reject(new Error('Zaman aşımı oluştu, tekrar deneyiniz.'));
			}, timeout);
		});
	}
	getIndex(array,key,keyValue){
		if(typeof array.length!='undefined')
			for(let i = 0, len = array.length; i < len; i++){
				if(key.indexOf('|')==-1){
					if(array[i][key]==keyValue)	return i;
				}else{
					let f=true;
					let arr_key=key.split('|');
					let arr_val=keyValue.split('|');
					for(let j=0;j<arr_key.length;j++){
						let v_row=array[i][arr_key[j]];
						let v_val=((arr_val[j]=='true'||arr_val[j]=='false')?Boolean(arr_val[j]):arr_val[j]);
						v_row=(v_row==null||v_row=='null')?null:v_row;
						v_val=(v_val==null||v_val=='null')?null:v_val;
						if(v_row!=v_val){
							f=false;
						}
					}
					if(f) return i;
				}
			}
		else
			for(let i in array)
				if(typeof array[i]!='function'){
					if(key.indexOf('|')==-1){
						if(array[i][key]==keyValue)	return i;
					}else{
						let f=true;
						let arr_key=key.split('|');
						let arr_val=keyValue.split('|');
						for(let j=0;j<arr_key.length;j++){
							if(array[i][arr_key[j]]!=arr_val[j]){
								f=false;
							}
						}
						if(f) return i;
					}
				}
		return -1;
	}
	arrRemove(arrayrecords,arrayfind,key){
		let _f=true;
		let i=0;
		while(_f&&i<arrayrecords.length){
			let _item=arrayrecords[i];
			if(arrayfind.indexOf(_item[key])===-1){
				arrayrecords.splice(i,1);
			}else{
				i++;
			}
		}
	}
	destroy(){
		let t=this;
		let _i;
		t.emit('beforedestroy',t);
		t.removeAllListeners('beforedestroy');
		if (t.relatedItems)
			for (let i = 0, l = t.relatedItems.length; i < l; i++)
				if (_i == t.relatedItems[i])
					(_i.componentClass && typeof _i.destroy == 'function') ? _i.destroy() : (typeof _i.remove == 'function' ? _i.remove() : null);
		t.emit('destroy', t);
		t.emit('afterdestroy', t);
		t.removeAllListeners('destroy');
		t.removeAllListeners('afterdestroy');
	}
	getIp() {
		var os=require('os');
		if(os.hostname().indexOf('oguzhan')>-1){
			return '192.168.1.40';
		}else{
			var i, v, value;
			var networkInterfaces = os.networkInterfaces();
			/* jshint -W015: true */
			interfaces: for (i in networkInterfaces) {
				if(i.toLowerCase().indexOf('virtual')==-1){
					for (v in i) {
						if (networkInterfaces[i][v] && networkInterfaces[i][v].address && networkInterfaces[i][v].address.length && !networkInterfaces[i][v].internal&&networkInterfaces[i][v].family==='IPv4'&& networkInterfaces[i][v].address!=='10.10.100.100') {
							value = networkInterfaces[i][v].address;
							break interfaces;
						}
					}
				}
			}
			return value;
		}
	}
}
exports.MyBase = MyBase;