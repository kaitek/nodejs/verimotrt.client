'use strict';
module.exports = function(sequelize, DataTypes) {
  var io_events = sequelize.define('io_events', {
    code: DataTypes.STRING(50),
    iotype: DataTypes.STRING(1),
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return io_events;
};