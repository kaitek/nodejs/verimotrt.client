'use strict';
module.exports = function(sequelize, DataTypes) {
  var material_units = sequelize.define('material_units', {
    code: DataTypes.STRING(50),
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return material_units;
};