'use strict';
module.exports = function(sequelize, DataTypes) {
  var material_versions = sequelize.define('material_versions', {
    mastername: DataTypes.STRING(50),
    versionname: DataTypes.STRING(10),
    fullname: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return material_versions;
};