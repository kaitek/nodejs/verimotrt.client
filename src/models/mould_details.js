'use strict';
module.exports = function(sequelize, DataTypes) {
  var mould_details = sequelize.define('mould_details', {
    mould: DataTypes.STRING(50),
    mouldgroup: DataTypes.STRING(50),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    leafmask: DataTypes.STRING(50),
    leafmaskcurrent: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return mould_details;
};