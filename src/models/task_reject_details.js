'use strict';
module.exports = function(sequelize, DataTypes) {
  var task_reject_details = sequelize.define('task_reject_details', {
    type: DataTypes.STRING(50),
    client: DataTypes.STRING(50),
    day: DataTypes.DATEONLY,
    jobrotation: DataTypes.STRING(50),
    rejecttype: DataTypes.STRING(50),
    materialunit: DataTypes.STRING(50),
    tasklist: DataTypes.STRING(200),
    time: DataTypes.DATE,
    mould: DataTypes.STRING(50),
    mouldgroup: DataTypes.STRING(50),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    opdescription: DataTypes.STRING(255),
    quantityreject: DataTypes.INTEGER,
    quantityretouch: DataTypes.INTEGER,
    quantityscrap: DataTypes.INTEGER,
    employee: DataTypes.STRING(50),
    erprefnumber: DataTypes.STRING(50),
    record_id: DataTypes.STRING(50)  ,
     isexported: DataTypes.BOOLEAN,
     description: DataTypes.STRING(255),
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return task_reject_details;
};