'use strict';
module.exports = function(sequelize, DataTypes) {
  var client_param_bases = sequelize.define('client_param_bases', {
    optiongroup: DataTypes.STRING(100),
    name: DataTypes.STRING(50),
    label: DataTypes.STRING(100),
    optiontype: DataTypes.STRING(50),
    options: DataTypes.TEXT,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return client_param_bases;
};