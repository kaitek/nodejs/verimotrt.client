'use strict';
module.exports = function(sequelize, DataTypes) {
  var job_rotations = sequelize.define('job_rotations', {
    code: DataTypes.STRING(50),
    beginval: DataTypes.STRING(8),
    endval: DataTypes.STRING(8),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return job_rotations;
};