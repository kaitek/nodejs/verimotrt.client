'use strict';
module.exports = function(sequelize, DataTypes) {
  var client_production_details = sequelize.define('client_production_details', {
    type: DataTypes.STRING(50),
    client: DataTypes.STRING(50),
    day: DataTypes.DATEONLY,
    jobrotation: DataTypes.STRING(50),
    tasklist: DataTypes.STRING(200),
    time: DataTypes.DATE,
    mould: DataTypes.STRING(50),
    mouldgroup: DataTypes.STRING(50),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    opdescription: DataTypes.STRING(255),
    leafmask: DataTypes.STRING(50),
    leafmaskcurrent: DataTypes.STRING(50),
    production: DataTypes.INTEGER,
    productioncurrent: DataTypes.INTEGER,
    employee: DataTypes.STRING(50),
    erprefnumber: DataTypes.STRING(50),
    gap: DataTypes.INTEGER,
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     energy: DataTypes.DECIMAL(10,4) ,
     taskfinishtype: DataTypes.STRING(50) ,
     taskfinishdescription: DataTypes.STRING(255) ,
     taskfromerp: DataTypes.BOOLEAN ,
     calculatedtpp: DataTypes.DECIMAL(10,
4) ,
     isexported: DataTypes.BOOLEAN ,
     opsayi: DataTypes.INTEGER ,
     refsayi: DataTypes.INTEGER ,
     losttime: DataTypes.INTEGER
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return client_production_details;
};