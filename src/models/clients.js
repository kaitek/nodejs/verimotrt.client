'use strict';
module.exports = function(sequelize, DataTypes) {
  var clients = sequelize.define('clients', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    uuid: DataTypes.STRING(255),
    isactive: DataTypes.BOOLEAN,
    ganttorder: DataTypes.INTEGER,
    clientweight: DataTypes.INTEGER,
    connected: DataTypes.BOOLEAN,
    status: DataTypes.STRING(255),
    statustime: DataTypes.DATE,
    workflow: DataTypes.STRING(50),
    info: DataTypes.STRING(10),
    watchorder: DataTypes.INTEGER,
    materialpreparation: DataTypes.BOOLEAN,
    ip: DataTypes.STRING(15),
    pincode: DataTypes.INTEGER,
    record_id: DataTypes.STRING(50)  ,
     versionapp: DataTypes.STRING(15) ,
     versionshell: DataTypes.STRING(15) ,
     computername: DataTypes.STRING(50) ,
     lastprodstarttime: DataTypes.DATE ,
     lastprodtime: DataTypes.DATE ,
     inputready: DataTypes.INTEGER
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return clients;
};