'use strict';
module.exports = function(sequelize, DataTypes) {
  var mould_types = sequelize.define('mould_types', {
    code: DataTypes.STRING(50),
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return mould_types;
};