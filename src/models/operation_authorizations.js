'use strict';
module.exports = (sequelize, DataTypes) => {
  var operation_authorizations = sequelize.define('operation_authorizations', {
    employee: DataTypes.STRING(50),
    opname: DataTypes.STRING(60),
    l1: DataTypes.INTEGER,
    l2: DataTypes.INTEGER,
    l3: DataTypes.INTEGER,
    l4: DataTypes.INTEGER,
    l5: DataTypes.INTEGER,
    time: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return operation_authorizations;
};