'use strict';
module.exports = (sequelize, DataTypes) => {
  var task_list_laser_details = sequelize.define('task_list_laser_details', {
    tasklistlaser: DataTypes.STRING(50),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    erprefnumber: DataTypes.STRING(50),
    leafmask: DataTypes.INTEGER,
    productcount: DataTypes.INTEGER,
    productdonecount: DataTypes.INTEGER,
    productdoneactivity: DataTypes.INTEGER,
    productdonejobrotation: DataTypes.INTEGER,
    scrapactivity: DataTypes.INTEGER,
    scrapjobrotation: DataTypes.INTEGER,
    scrappart: DataTypes.INTEGER,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return task_list_laser_details;
};