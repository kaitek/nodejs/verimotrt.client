'use strict';
module.exports = (sequelize, DataTypes) => {
  var task_case_controls = sequelize.define('task_case_controls', {
    code: DataTypes.STRING(50),
    opcode: DataTypes.STRING(50),
    packcapacity: DataTypes.DECIMAL(10, 4),
    quantityremaining: DataTypes.DECIMAL(10, 4),
    clients: DataTypes.STRING(255),
    record_id: DataTypes.STRING(50)  ,
     filename: DataTypes.STRING(255)
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return task_case_controls;
};