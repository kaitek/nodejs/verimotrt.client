'use strict';
module.exports = function(sequelize, DataTypes) {
  var task_finish_types = sequelize.define('task_finish_types', {
    code: DataTypes.STRING(50),
    recreate: DataTypes.BOOLEAN,
    orderfield: DataTypes.INTEGER,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return task_finish_types;
};