'use strict';
module.exports = function(sequelize, DataTypes) {
  var _sync_updatelogs = sequelize.define('_sync_updatelogs', {
    tableName: DataTypes.STRING(100),
    lastUpdate: DataTypes.DATE,
    strlastupdate: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return _sync_updatelogs;
};