'use strict';
module.exports = (sequelize, DataTypes) => {
  var task_list_lasers = sequelize.define('task_list_lasers', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    production: DataTypes.INTEGER,
    tfdescription: DataTypes.STRING(255),
    plannedstart: DataTypes.DATE,
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    client1: DataTypes.STRING(50),
    client2: DataTypes.STRING(50),
    client3: DataTypes.STRING(50),
    client4: DataTypes.STRING(50),
    client5: DataTypes.STRING(50),
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return task_list_lasers;
};