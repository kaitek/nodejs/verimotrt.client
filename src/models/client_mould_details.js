'use strict';
module.exports = (sequelize, DataTypes) => {
  var client_mould_details = sequelize.define('client_mould_details', {
    client: DataTypes.STRING(50),
    clientmouldgroup: DataTypes.STRING(50),
    mouldgroup: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return client_mould_details;
};