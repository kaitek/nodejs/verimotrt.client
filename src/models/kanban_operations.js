'use strict';
module.exports = function(sequelize, DataTypes) {
  var kanban_operations = sequelize.define('kanban_operations', {
    client: DataTypes.STRING(50),
    product: DataTypes.STRING(60),
    boxcount: DataTypes.INTEGER,
    minboxcount: DataTypes.INTEGER,
    maxboxcount: DataTypes.INTEGER,
    currentboxcount: DataTypes.INTEGER,
    packaging: DataTypes.INTEGER,
    hourlydemand: DataTypes.INTEGER,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return kanban_operations;
};