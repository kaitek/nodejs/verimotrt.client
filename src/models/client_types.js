'use strict';
module.exports = function(sequelize, DataTypes) {
  var client_types = sequelize.define('client_types', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return client_types;
};