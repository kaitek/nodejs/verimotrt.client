'use strict';
module.exports = function(sequelize, DataTypes) {
  var reject_types = sequelize.define('reject_types', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     islisted: DataTypes.BOOLEAN ,
     listorder: DataTypes.INTEGER ,
     extcode: DataTypes.STRING(50) ,
     isoeeaffect: DataTypes.BOOLEAN ,
     isproductionaffect: DataTypes.BOOLEAN
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return reject_types;
};