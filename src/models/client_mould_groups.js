'use strict';
module.exports = (sequelize, DataTypes) => {
  var client_mould_groups = sequelize.define('client_mould_groups', {
    code: DataTypes.STRING(50),
    client: DataTypes.STRING(50),
    productionmultiplier: DataTypes.INTEGER,
    intervalmultiplier: DataTypes.DECIMAL(10, 4),
    setup: DataTypes.INTEGER,
    cycletime: DataTypes.DECIMAL(10, 4),
    lotcount: DataTypes.INTEGER,
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     operatorcount: DataTypes.INTEGER ,
     isdefault: DataTypes.BOOLEAN
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return client_mould_groups;
};