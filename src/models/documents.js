'use strict';
module.exports = function(sequelize, DataTypes) {
  var documents = sequelize.define('documents', {
    type: DataTypes.STRING(50),
    path: DataTypes.STRING(255),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    currentversion: DataTypes.INTEGER,
    description: DataTypes.STRING(255),
    islot: DataTypes.BOOLEAN,
    lastupdate: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return documents;
};