'use strict';
module.exports = (sequelize, DataTypes) => {
  var case_labels = sequelize.define('case_labels', {
    erprefnumber: DataTypes.STRING(50),
    time: DataTypes.DATE,
    status: DataTypes.STRING(50),
    canceldescription: DataTypes.STRING(255),
    canceltime: DataTypes.DATE,
    cancelusername: DataTypes.STRING(100),
    synced: DataTypes.INTEGER,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return case_labels;
};