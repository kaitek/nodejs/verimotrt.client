'use strict';
module.exports = function(sequelize, DataTypes) {
  var mould_groups = sequelize.define('mould_groups', {
    code: DataTypes.STRING(50),
    mould: DataTypes.STRING(50),
    productionmultiplier: DataTypes.INTEGER,
     intervalmultiplier: DataTypes.DECIMAL(10,4),
    counter: DataTypes.INTEGER,
    setup: DataTypes.INTEGER,
    cycletime: DataTypes.DECIMAL(10, 4),
    lotcount: DataTypes.INTEGER,
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     program: DataTypes.STRING(4)
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return mould_groups;
};