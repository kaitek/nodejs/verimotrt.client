'use strict';
module.exports = (sequelize, DataTypes) => {
  var control_answers = sequelize.define('control_answers', {
    type: DataTypes.STRING(50),
    client: DataTypes.STRING(50),
    day: DataTypes.DATEONLY,
    jobrotation: DataTypes.STRING(50),
    opname: DataTypes.STRING(60),
    employee: DataTypes.STRING(50),
    time: DataTypes.DATE,
    question: DataTypes.STRING(255),
    answertype: DataTypes.STRING(50),
    valuerequire: DataTypes.STRING(100),
    valuemin: DataTypes.STRING(100),
    valuemax: DataTypes.STRING(100),
    answer: DataTypes.STRING(100),
    description: DataTypes.STRING(255),
    record_id: DataTypes.STRING(50)  ,
     erprefnumber: DataTypes.STRING(50) ,
     isexported: DataTypes.BOOLEAN ,
     documentnumber: DataTypes.STRING(50)
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return control_answers;
};