'use strict';
module.exports = (sequelize, DataTypes) => {
  var task_cases = sequelize.define('task_cases', {
    movementdetail: DataTypes.INTEGER,
    erprefnumber: DataTypes.STRING(50),
    casetype: DataTypes.STRING(5),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    stockcode: DataTypes.STRING(50),
    stockname: DataTypes.STRING(60),
     quantityused: DataTypes.DECIMAL(10,4),
    casename: DataTypes.STRING(50),
     packcapacity: DataTypes.DECIMAL(10,4),
    feeder: DataTypes.STRING(5),
    preptime: DataTypes.INTEGER,
     quantityremaining: DataTypes.DECIMAL(10,4),
     quantitydeliver: DataTypes.DECIMAL(10,4),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    finishtype: DataTypes.STRING(255),
    conveyor: DataTypes.INTEGER,
    goodsplanner: DataTypes.STRING(255),
    record_id: DataTypes.STRING(50)  ,
     casenumber: DataTypes.STRING(50) ,
     caselot: DataTypes.STRING(50) ,
     status: DataTypes.STRING(50) ,
     client: DataTypes.STRING(50) ,
     deliverytime: DataTypes.DATE
} , {
    classMethods: {
      associate: function(models) {
         // associations can be defined here
      }
    }
  });
  return task_cases;
};