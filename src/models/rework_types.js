'use strict';
module.exports = (sequelize, DataTypes) => {
  var rework_types = sequelize.define('rework_types', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return rework_types;
};