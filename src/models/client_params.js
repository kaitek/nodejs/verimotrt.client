'use strict';
module.exports = function(sequelize, DataTypes) {
  var client_params = sequelize.define('client_params', {
    client: DataTypes.STRING(50),
    name: DataTypes.STRING(50),
    value: DataTypes.STRING(50),
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return client_params;
};