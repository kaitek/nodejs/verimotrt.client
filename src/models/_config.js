'use strict';
module.exports = function(sequelize, DataTypes) {
  var _config = sequelize.define('_config', {
    paramname: DataTypes.STRING(50),
    paramlabel: DataTypes.STRING(50),
    paramval: DataTypes.STRING(50),
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return _config;
};