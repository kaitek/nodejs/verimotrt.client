'use strict';
module.exports = (sequelize, DataTypes) => {
  var offtimes = sequelize.define('offtimes', {
    clients: DataTypes.TEXT,
    losttype: DataTypes.STRING(50),
    name: DataTypes.STRING(255),
    repeattype: DataTypes.STRING(50),
    days: DataTypes.STRING(255),
    startday: DataTypes.DATEONLY,
    starttime: DataTypes.STRING(5),
    finishday: DataTypes.DATEONLY,
    finishtime: DataTypes.STRING(5),
    record_id: DataTypes.STRING(50)  ,
     start: DataTypes.DATE ,
     finish: DataTypes.DATE ,
     clearonovertime: DataTypes.BOOLEAN
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return offtimes;
};   