'use strict';
module.exports = (sequelize, DataTypes) => {
  var control_questions = sequelize.define('control_questions', {
    type: DataTypes.STRING(50),
    opname: DataTypes.STRING(60),
    question: DataTypes.STRING(255),
    answertype: DataTypes.STRING(50),
    valuerequire: DataTypes.STRING(100),
    valuemin: DataTypes.STRING(100),
    valuemax: DataTypes.STRING(100),
    record_id: DataTypes.STRING(50)  ,
     documentnumber: DataTypes.STRING(50),
	 clients: DataTypes.STRING(255) ,
     path: DataTypes.STRING(255)
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return control_questions;
};