'use strict';
module.exports = function(sequelize, DataTypes) {
  var task_lists = sequelize.define('task_lists', {
    code: DataTypes.STRING(60),
    erprefnumber: DataTypes.STRING(50),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    opdescription: DataTypes.STRING(255),
    productcount: DataTypes.INTEGER,
    productdonecount: DataTypes.INTEGER,
    deadline: DataTypes.DATEONLY,
    client: DataTypes.STRING(50),
    type: DataTypes.STRING(50),
    tpp: DataTypes.DECIMAL(10, 4),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     productdoneactivity: DataTypes.INTEGER ,
     productdonejobrotation: DataTypes.INTEGER ,
     scrapactivity: DataTypes.INTEGER ,
     scrapjobrotation: DataTypes.INTEGER ,
     scrappart: DataTypes.INTEGER,
     taskfromerp: DataTypes.BOOLEAN ,
     tfddescription: DataTypes.STRING(255) ,
     plannedstart: DataTypes.DATE ,
     resourceid: DataTypes.STRING(50) ,
     plannedfinish: DataTypes.DATE ,
     duration: DataTypes.INTEGER ,
     offtime: DataTypes.INTEGER ,
     totaltime: DataTypes.INTEGER     ,
     idx: DataTypes.STRING(60) ,
     status: DataTypes.STRING(50) ,
     mouldaddress: DataTypes.STRING(50)
} , {
      classMethods: {
        associate: function(models) {
          // associations can be defined here
        }
      }
    });
    return task_lists;
  };