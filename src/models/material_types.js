'use strict';
module.exports = function(sequelize, DataTypes) {
  var material_types = sequelize.define('material_types', {
    code: DataTypes.STRING(50),
    value: DataTypes.STRING(5),
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return material_types;
};