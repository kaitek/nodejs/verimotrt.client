'use strict';
module.exports = function(sequelize, DataTypes) {
  var lost_types = sequelize.define('lost_types', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    isemployeerequired: DataTypes.BOOLEAN,
    isexpertemployeerequired: DataTypes.BOOLEAN,
    time: DataTypes.INTEGER,
    isoperationnumberrequired: DataTypes.BOOLEAN,
    ioevent: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     closenoemployee: DataTypes.BOOLEAN ,
     islisted: DataTypes.BOOLEAN ,
     listorder: DataTypes.INTEGER ,
     extcode: DataTypes.STRING(50) ,
     isclient: DataTypes.BOOLEAN ,
     isemployee: DataTypes.BOOLEAN ,
     isshortlost: DataTypes.BOOLEAN ,
     issourcelookup: DataTypes.BOOLEAN ,
     authorizeloststart: DataTypes.STRING(50) ,
     authorizelostfinish: DataTypes.STRING(50) ,
     isdescriptionrequired: DataTypes.BOOLEAN ,
     isopenallports: DataTypes.BOOLEAN
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return lost_types;
};