'use strict';
module.exports = function(sequelize, DataTypes) {
  var moulds = sequelize.define('moulds', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    counter: DataTypes.INTEGER,
    mouldtype: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)   ,
     serialnumber: DataTypes.STRING(50) ,
     client1: DataTypes.STRING(50) ,
     client2: DataTypes.STRING(50) ,
     client3: DataTypes.STRING(50) ,
     client4: DataTypes.STRING(50) ,
     client5: DataTypes.STRING(50) ,
     bantime: DataTypes.DATE
} , {
  classMethods: {
    associate: function(models) {
      // associations can be defined here
    }
  }
});
return moulds;
};  