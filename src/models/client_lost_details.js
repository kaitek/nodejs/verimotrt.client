'use strict';
module.exports = function(sequelize, DataTypes) {
  var client_lost_details = sequelize.define('client_lost_details', {
    type: DataTypes.STRING(50),
    client: DataTypes.STRING(50),
    day: DataTypes.DATEONLY,
    jobrotation: DataTypes.STRING(50),
    losttype: DataTypes.STRING(50),
    faulttype: DataTypes.STRING(50),
    faultdescription: DataTypes.STRING(255),
    faultgroupcode: DataTypes.STRING(50),
    descriptionlost: DataTypes.STRING(255),
    tasklist: DataTypes.STRING(200),
    mould: DataTypes.STRING(50),
    mouldgroup: DataTypes.STRING(50),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    opdescription: DataTypes.STRING(255),
    employee: DataTypes.STRING(50),
    erprefnumber: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     energy: DataTypes.DECIMAL(10,4) ,
     taskfromerp: DataTypes.BOOLEAN ,
     faultdevice: DataTypes.STRING(50) ,
     sourceclient: DataTypes.STRING(50) ,
     sourcedescriptionlost: DataTypes.STRING(255) ,
     sourcelosttype: DataTypes.STRING(50) ,
     isexported: DataTypes.BOOLEAN ,
     opsayi: DataTypes.INTEGER ,
     refsayi: DataTypes.INTEGER
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return client_lost_details;
};