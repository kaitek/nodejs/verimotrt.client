'use strict';
module.exports = (sequelize, DataTypes) => {
  var overtimes = sequelize.define('overtimes', {
    clients: DataTypes.TEXT,
    name: DataTypes.STRING(255),
    repeattype: DataTypes.STRING(50),
    days: DataTypes.STRING(255),
    startday: DataTypes.DATEONLY,
    starttime: DataTypes.STRING(5),
    finishday: DataTypes.DATEONLY,
    finishtime: DataTypes.STRING(5),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return overtimes;
};