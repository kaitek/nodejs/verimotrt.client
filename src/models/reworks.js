'use strict';
module.exports = (sequelize, DataTypes) => {
  var reworks = sequelize.define('reworks', {
    client: DataTypes.STRING(50),
    reworktype: DataTypes.STRING(50),
    code: DataTypes.STRING(60),
    opcode: DataTypes.STRING(50),
    opnumber: DataTypes.INTEGER,
    opname: DataTypes.STRING(60),
    opdescription: DataTypes.STRING(255),
    tpp: DataTypes.DECIMAL(10, 4),
    islisted: DataTypes.BOOLEAN,
    listorder: DataTypes.INTEGER,
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     ismandatory: DataTypes.BOOLEAN
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return reworks;
};