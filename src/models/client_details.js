'use strict';
module.exports = function(sequelize, DataTypes) {
  var client_details = sequelize.define('client_details', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    clienttype: DataTypes.STRING(50),
    client: DataTypes.STRING(50),
    iotype: DataTypes.STRING(1),
    portnumber: DataTypes.INTEGER,
    ioevent: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     triggertype: DataTypes.STRING(50)
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return client_details;
};