'use strict';
module.exports = function(sequelize, DataTypes) {
  var fault_types = sequelize.define('fault_types', {
    code: DataTypes.STRING(50),
    description: DataTypes.STRING(255),
    client: DataTypes.STRING(50),
    faultgroupcode: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return fault_types;
};