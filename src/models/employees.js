'use strict';
module.exports = function(sequelize, DataTypes) {
  var employees = sequelize.define('employees', {
    code: DataTypes.STRING(50),
    name: DataTypes.STRING(100),
    secret: DataTypes.STRING(100),
    isexpert: DataTypes.BOOLEAN,
    isoperator: DataTypes.BOOLEAN,
    ismaintenance: DataTypes.BOOLEAN,
    ismould: DataTypes.BOOLEAN,
    isquality: DataTypes.BOOLEAN,
    jobrotationteam: DataTypes.STRING(50),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    client: DataTypes.STRING(50),
    tasklist: DataTypes.STRING(200),
    day: DataTypes.DATEONLY,
    jobrotation: DataTypes.STRING(50),
    record_id: DataTypes.STRING(50)  ,
     lastseen: DataTypes.DATE ,
     strlastseen: DataTypes.STRING(50) ,
     teamleader: DataTypes.STRING(50)
} , {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return employees;
};