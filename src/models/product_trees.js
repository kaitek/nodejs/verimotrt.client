'use strict';
module.exports = function(sequelize, DataTypes) {
  var product_trees = sequelize.define('product_trees', {
    parent: DataTypes.STRING(50),
    code: DataTypes.STRING(50),
    number: DataTypes.INTEGER,
    description: DataTypes.STRING(255),
    materialtype: DataTypes.STRING(50),
    name: DataTypes.STRING(60),
    stockcode: DataTypes.STRING(50),
    stockname: DataTypes.STRING(60),
    tpp: DataTypes.DECIMAL(10, 4),
    quantity: DataTypes.DECIMAL(10, 4),
    unit_quantity: DataTypes.STRING(50),
    opcounter: DataTypes.INTEGER,
    pack: DataTypes.STRING(50),
    packcapacity: DataTypes.DECIMAL(10, 4),
    unit_packcapacity: DataTypes.STRING(50),
    oporder: DataTypes.INTEGER,
    preptime: DataTypes.INTEGER,
    qualitycontrolrequire: DataTypes.BOOLEAN,
    task: DataTypes.BOOLEAN,
    feeder: DataTypes.STRING(5),
    drainer: DataTypes.STRING(5),
    goodsplanner: DataTypes.STRING(255),
    productionmultiplier: DataTypes.INTEGER,
     intervalmultiplier: DataTypes.DECIMAL(10,4),
    start: DataTypes.DATE,
    finish: DataTypes.DATE,
    record_id: DataTypes.STRING(50)  ,
     delivery: DataTypes.BOOLEAN ,
     carrierfeeder: DataTypes.STRING(255) ,
     carrierdrainer: DataTypes.STRING(255) ,
     locationsource: DataTypes.STRING(50) ,
     locationdestination: DataTypes.STRING(50) ,
     empcount: DataTypes.INTEGER ,
     isdefault: DataTypes.BOOLEAN ,
     client: DataTypes.STRING(50) ,
     notificationtime: DataTypes.INTEGER
} , {
  classMethods: {
    associate: function(models) {
      // associations can be defined here
    }
  }
});
return product_trees;
};