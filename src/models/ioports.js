'use strict';
module.exports = function(sequelize, DataTypes) {
  var IOPorts = sequelize.define('IOPorts', {
    number: DataTypes.INTEGER,
    type: DataTypes.STRING(1),
    value: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return IOPorts;
};