const electron = require('electron');
const classBase=require('./base');
const spawn = require('child_process').spawn;
let _ip=classBase.getIp();
let child;
let pid;
let objproductionEnv = Object.create(process.env);
let productionEnv = typeof objproductionEnv.NODE_ENV=='undefined'?'development':objproductionEnv.NODE_ENV;
let procOptions={stdio:'inherit',detached: true,env: productionEnv};
function killApp()
{
	if (child) {
		console.log('killApp-2.pid:'+pid+' ch.pid:'+child.pid);
	}
}
exports.killApp = killApp;

function startApp()
{
	let onClose = function (code) {
		console.log('process exit code:' + code+ ' pid:'+pid);
		startApp();
	};
	child = spawn(electron, [__dirname+'/main.js'],procOptions);
	pid=child.pid;
	child.on('close', onClose);
	child.on('error', function (err) {
		console.log('bs-process error '+err );
		classBase.writeError(err,_ip,'bootstrap-child-error');
		child.kill('SIGINT');
	});
	child.on('exit', function (code) {
		console.log(`Child exited with code ${code}`);
	});
	child.on('message', function (code) {
		console.log(`Child message ${code}`);
	});
	child.on('disconnect', function () {
		console.log('Child disconnected');
	});
	return child;
}
exports.startApp = startApp;