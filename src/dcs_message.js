const classBase=require('./base');

const { v4: uuidv4 } = require('uuid');
class dcs_message extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t=this;
		t.componentClass='dcs_message';
		t.version='0.1';
		t.id=0;
		t.messageId=uuidv4();
		t.tryCount=0;
		t.type='';
		t.ack=false;
		t.data=false;
		t.queued=0;
		Object.assign(this, this.config);
	}
	getdata(){
		return {messageId:this.messageId ,tryCount:this.tryCount ,type:this.type ,ack:this.ack ,data:this.data ,queued:this.queued};
	}
}
module.exports=dcs_message;