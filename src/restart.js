var join = require('path').join;
var electron = require('electron');

var main = join(__dirname, 'src/main.js');
var watch = join(__dirname, 'src');

require('spawn-auto-restart')({
  proc: {
    command: electron,
    args: 'src/main.js'
  },
  watch: 'src'
});