const processModule = require('process');
const socketIOClient = require('socket.io-client');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});
let _config=false;
let _pid=false;
let _io=false;
let _params=false;
let _timer_ping=false;
let _ping_counter=0;
//Sentry.context(function () {
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'init_socket_io_client':{
		_pid=obj.pid;
		_config=obj.param;
		_params=obj.data;
		Sentry.configureScope(scope => {
			scope.setTag('ip',obj.ip);
			scope.setTag('client_name',_config.client_name.value);
			scope.setTag('version',obj.versionApp);
			scope.setTag('build',obj.buildNumber);
			//scope.setUser({ ip: t.cfg_dcs_server_IP });
		});
		io_connect();
		break;
	}
	case 'cn_sn':{
		_io.emit('cn_sn',obj.data);
		break;
	}
	case 'cn_sn_setlock':{
		_io.emit('cn_sn_setlock',obj.id,obj.status);
		break;
	}
	default:{
		console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	//processModule.send(_pid + ': ' + e);
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(101);
	}, 1000);
	return;
});
let io_connect = () => {
	processModule.send({event:'socket_io_client_inited',pid:_pid});
	_io=socketIOClient('http://'+_config.dcs_server_IP.value+':'+_config.dcs_server_PORT.value);
	_io._IP=_config.dcs_server_IP.value;
	_io._PORT=_config.dcs_server_PORT.value;
	_io.dcsClientId=_params.dcsClientId;
	_io.id=0;
	_io.code='';
	_io.on('connect_error',function(/*err*/){
		console.log('socket_io_connect_error');
		//Sentry.captureMessage('socket_io_client connect_error ' + err);
		clearInterval(_timer_ping);
		_timer_ping=false;
		processModule.send({event:'disconnect'});
		//Sentry.captureException(err);
		//processModule.send({event:'connect_error',pid:_pid});
		setTimeout(function(){
			process.exit(102);
		}, 1000);
		return;
	});
	_io.on('connect_timeout',function(/*err*/){
		console.log('socket_io_connect_timeout');
		//Sentry.captureMessage('socket_io_client connect_timeout ' + err);
		clearInterval(_timer_ping);
		_timer_ping=false;
		processModule.send({event:'disconnect'});
		//Sentry.captureException(err);
		//processModule.send({event:'connect_timeout',pid:_pid});
		setTimeout(function(){
			process.exit(102);
		}, 1000);
		return;
	});
	_io.on('reconnect_error', function () {
		console.log('socket_io_reconnect_error');
		//Sentry.captureMessage('socket_io_client reconnect_error ');
		clearInterval(_timer_ping);
		_timer_ping=false;
		processModule.send({event:'disconnect'});
		//Sentry.captureException(err);
		//processModule.send({event:'reconnect_error',pid:_pid});
		setTimeout(function(){
			process.exit(102);
		}, 1000);
		return;
	});
	_io.on('reconnect_failed', function () {
		console.log('socket_io_reconnect_failed');
		//Sentry.captureMessage('socket_io_client reconnect_failed ');
		clearInterval(_timer_ping);
		_timer_ping=false;
		processModule.send({event:'disconnect'});
		//Sentry.captureException(err);
		//processModule.send({event:'reconnect_failed',pid:_pid});
		setTimeout(function(){
			process.exit(102);
		}, 1000);
		return;
	});
	_io.on('error',function(err){
		console.log('socket_io_error');
		//Sentry.captureMessage('socket_io_client io_error ' + err);
		Sentry.captureException(err);
		//processModule.send({event:'error',pid:_pid});
		setTimeout(function(){
			process.exit(103);
		}, 1000);
		return;
	});
	_io.on('connect', function(){
		processModule.send({event:'connect',pid:_pid});
		_io.emit('cn_sn',{event:'connect',versionApp:_params.versionApp,buildNumber:_params.buildNumber,versionShell:_params.versionShell,dcsClientId:_params.dcsClientId,client_name:_params.client_name,computerName:_params.computerName,workflow:_params.workflow});
		if(_timer_ping!==false){
			clearInterval(_timer_ping);
			_timer_ping=false;
		}
		if(_timer_ping===false){
			_timer_ping=setInterval(() => {
				_ping();
			}, 10000);
		}
	});
	_io.on('sn_cn',function(obj){
		_ping_counter=0;
		if(obj.event!=='pong'){
			processModule.send({event:'data',pid:_pid,params:obj});
		//}else{
		//	console.log('socket_io__ping_responded');
		}		
	});
	_io.on('disconnect', function(){
		//Sentry.captureMessage('socket_io_client disconnect');
		clearInterval(_timer_ping);
		_timer_ping=false;
		processModule.send({event:'disconnect'});
		//processModule.send({event:'disconnect',pid:_pid});
		setTimeout(function(){
			process.exit(104);
		}, 1000);
		return;
	});
};
let _ping=()=>{
	try {
		if(_ping_counter<=2){
			_ping_counter++;
			///console.log('socket_io__ping:'+_ping_counter);
			_io.emit('cn_sn',{event:'ping',client_name:_params.client_name});
		}else{
			console.log('socket_io__destroy:'+_ping_counter);
			clearInterval(_timer_ping);
			_timer_ping=false;
			//Sentry.captureMessage('socket_io_client ping timeout');
			//processModule.send({event:'disconnect-ping',pid:_pid});
			setTimeout(function(){
				process.exit(105);
			}, 1000);
			return;
		}
	} catch (error) {
		clearInterval(_timer_ping);
		setTimeout(function(){
			process.exit(105);
		}, 1000);
	}
};
//});