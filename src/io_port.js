const classBase=require('./base');

class io_port extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='io_port';
		t.version='0.1';
		t.type=false;
		t.number=false;
		t.index=false;
		t.curval=false;
		Object.assign(this, this.config);
		t.curval=0;
	}
	setInputOnStart(val,fevent,pos){
		if(isNaN(val)||isNaN(parseInt(val))){
			console.log('setInputOnStart-NaN:'+pos);
		}
		//console.log('setInputOnStart:'+parseInt(this.curval),'|',parseInt(val),'|',fevent);
		if((parseInt(this.curval)!=parseInt(val))||fevent==true){
			this.curval=parseInt(val);
		}
	}
	setCurVal(val,fevent,pos){
		if(isNaN(val)||isNaN(parseInt(val))){
			console.log('setCurVal-NaN:'+pos);
		}
		//console.log('setCurVal:'+parseInt(this.curval),'|',parseInt(val),'|',fevent);
		if((parseInt(this.curval)!=parseInt(val))||fevent==true){
			this.curval=parseInt(val);
			this.emit('ioportchange',this,fevent);
		}
	}
	toggleCurVal(){
		if(this.curval==1){
			this.setCurVal(0);
		}else{
			this.setCurVal(1);
		}
	}
	clearOutput(){
		if(this.type=='input') return false;
		return [85,170,0,3,0,1,this.number];
	}
	setOutput(){
		if(this.type=='input') return false;
		return [85,170,0,3,0,2,this.number];
	}
	toggleOutput(){
		if(this.type=='input') return false;
		return [85,170,0,3,0,3,this.number];
	}
}
module.exports=io_port;