const processModule = require('process');
const amqp = require('amqplib/callback_api');
const Promise = require('promise');
const Sequelize = require('sequelize');
const timezoneJS = require('timezone-js');
const tzdata = require('tzdata');
let _tz = timezoneJS.timezone;
_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
_tz.loadZoneDataFromObject(tzdata);
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});
let sequelize;
let conn=null;
let pubChannel = null;
let _pid=false;
let _param=false;
let _config=false;
let arr_queue_assert=[];
let _url_amqp=false;
let _ip=false;
let _code=false;
let _messages=[];
//Sentry.context(function () {
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'init_server':{
		_pid=obj.pid;
		_param=obj.param;
		_config=obj.config;
		_ip=obj.ip;
		_code=_config.client_name.value;
		Sentry.configureScope(scope => {
			scope.setTag('ip',_ip);
			scope.setTag('client_name',_code);
			scope.setTag('version',obj.versionApp);
			scope.setTag('build',obj.buildNumber);
			//scope.setUser({ ip: t.cfg_dcs_server_IP });
		});
		_url_amqp='amqp://admin:q32wxcx@'+_config.dcs_server_IP.value;
		console.log('RabbitMQ_receive Process PID:'+_pid);
		connect_amqp();
		break;
	}
	default:{
		console.log('message:',obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(400);
	}, 1000);
	return;
});
let connect_amqp = ()=>{
	amqp.connect(_url_amqp,function(_err,_conn) {
		if (_err) {
			//Sentry.captureMessage('[AMQP-R] connect ' + _err);
			console.error('[AMQP-R]', _err);
			Sentry.captureException(_err);
			setTimeout(function(){
				process.exit(401);
			}, 1000);
			return;
		}
		if(conn==null){
			conn=_conn;
			conn.on('error', function(err) {
				if (err.message && err.message !== 'Connection closing') {
					console.error('[AMQP-R] conn error', err);
				}
				Sentry.captureException(err);
				setTimeout(function(){
					process.exit(402);
				}, 1000);
				return;
			});
			conn.on('close', function() {
				setTimeout(function(){
					process.exit(403);
				}, 1000);
				return;
			});
		}
		createChannel();
	});
};
let createChannel=()=> {
	conn.createChannel(function(err, ch) {
		if (closeOnErr(err)) return;
		if(pubChannel==null){
			pubChannel = ch;
			pubChannel.on('error', function(err) {
				console.error('[AMQP-R] channel error', err);
				Sentry.captureException(err);
				setTimeout(function(){
					process.exit(404);
				}, 1000);
				return;
			});
			pubChannel.on('close', function() {
				console.log('[AMQP-R] channel closed');
				setTimeout(function(){
					process.exit(405);
				}, 1000);
				return;
			});
			processModule.send({event:'cp_server_inited',pid:_pid});
			db_connect(_param);
		}
	});
};
let closeOnErr=(err)=> {
	if (!err) return false;
	//Sentry.captureMessage('[AMQP-R] error' + err);
	console.error('[AMQP-R] error', err);
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(406);
	}, 1000);
	return true;
};
let db_connect = (obj) => {
	sequelize = new Sequelize(obj.database, obj.username, obj.password, {
		host: obj.host,
		dialect: 'postgres',
		logging: false,
		//operatorsAliases: false,
		pool: {
			max: 20,
			min: 0,
			acquire: 100000,
			idle: 10000
		},
		timezone:obj.timezone
	});
	return sequelize
		.authenticate()
		.then(() => {
			return Promise.all([queue_asserter()]).then(()=>{
				process.nextTick(function(){
					setTimeout(function() {
						return Promise.all([run()]);
					}, 10);
				});
			});
		})
		.catch(err => {
			//Sentry.captureMessage('[AMQP-R-db] ' + err);
			Sentry.captureException(err);
			setTimeout(function(){
				process.exit(407);
			}, 1000);
			return;
		});
};
let queue_asserter=()=>{
	try {
		if(pubChannel!==null){
			if(arr_queue_assert.length==0){
				if(_code&&_code!==''){
					if(arr_queue_assert.indexOf(_code+'_server')===-1){
						pubChannel.assertQueue(_code+'_server',{durable: true},function(err, ok){
							if(err){
								//Sentry.captureMessage('[AMQP-R-assert] ' + err);
								console.error('[AMQP-R] error-assertQueue', err);
								setTimeout(function(){
									process.exit(431);
								}, 1000);
							}
							arr_queue_assert.push(_code+'_server');
							return pubChannel.consume(ok.queue, function(msg) {
								if (msg !== null) {
									_messages.push(msg);
								}
							});
						});
					}
				}else{
					if(_ip!==''){
						if(arr_queue_assert.indexOf(_ip+'_server')===-1){
							pubChannel.assertQueue(_ip+'_server',{durable: true},function(err, ok){
								if(err){
									//Sentry.captureMessage('[AMQP-R-assert] ' + err);
									console.error('[AMQP-R] error-assertQueue', err);
									setTimeout(function(){
										process.exit(432);
									}, 1000);
								}
								arr_queue_assert.push(_ip+'_server');
								return pubChannel.consume(ok.queue, function(msg) {
									if (msg !== null) {
										_messages.push(msg);
									}
								});
							});
						}
					}
				}
			}
		}else{
			//Sentry.captureMessage('channel==null');
			setTimeout(function(){
				process.exit(408);
			}, 1000);
		}
	} catch (error) {
		//Sentry.captureMessage('catch_queue_asserter');
		setTimeout(function(){
			process.exit(429);
		}, 1000);
	}
	return null;
};
let run=()=>{
	let _promises=[];
	if(arr_queue_assert.length>0){
		let _tmp=[];
		let i=0;
		while(_messages.length>0&&i++<50){
			_tmp.push(_messages.shift());
		}
		while(_tmp.length>0){
			_promises.push(recordSave(_tmp.shift()));
		}
	}
	return Promise.all(_promises).then(()=>{
		process.nextTick(function(){
			setTimeout(function() {
				run();
			}, 500);
		});
	}).catch(e => {
		//Sentry.captureMessage('[AMQP-R-run] ' + e);
		Sentry.captureException(e);
		setTimeout(function(){
			process.exit(409);
		}, 1000);
		return;
	});
};

let recordSave = (msg) => {
	let data=JSON.parse(msg.content);
	//console.log(data);
	let obj_time_=new timezoneJS.Date('Europe/Istanbul');
	let str_time=obj_time_.toString('yyyy-MM-dd HH:mm:ss');
	if(typeof data.messagetype!=='undefined'&&typeof data.type==='undefined'){
		data.type=data.messagetype;
	}
	switch (data.type) {
	case 'sync_message':{
		let _data=data.data.data;
		let _table=data.data.tableName;
		let _procType=data.data.procType;
		let _record_id=_data.record_id;
		let sql='';
		if(_table==='fault_interventions'){
			pubChannel.ack(msg);
			return;
		}
		if(_procType=='insert'){
			if(_table==='__sync_updates'){
				pubChannel.ack(msg);
				return;
			}
			if(_table!=='_sync_updatelogs'){
				if(_table!=='client_details'){
					return sequelize.query('SELECT * FROM "'+_table+'" WHERE "record_id"=:record_id'
						, { replacements:{record_id:_record_id}, type: sequelize.QueryTypes.SELECT})
						.then((results) => {
							if(results.length>0){
								pubChannel.ack(msg);
							}else{
								if(_data.id){
									delete _data.id;
								}
								if(_data.isallowna){
									delete _data.isallowna;
								}
								let query = ['INSERT INTO'];
								query.push('"'+_table+'"');
								query.push('(');
								let afn=[];
								let afv=[];
								let orep={};
								let val;
								for(let prop in _data){
									if(prop!=='create_user_id'&&prop!=='update_user_id'){
										afn.push('"'+prop+'"');
										afv.push(':'+prop);
										val=_data[(prop=='updated_at'?'updatedAt':(prop=='created_at'?'createdAt':prop))];
										orep[prop]=val==='null'?null:val;
									}
								}
								query.push(afn.join(', '));
								query.push(') VALUES (');
								query.push(afv.join(', '));
								query.push(')');
								sql=query.join(' ');
								//console.log(sql);
								//console.log(val);
								return sequelize.query(sql
									, { replacements: orep, type: sequelize.QueryTypes.INSERT })
									.then(() => {
										pubChannel.ack(msg);
										return afterRecordSave(data);
									})
									.catch(e => {
										Sentry.addBreadcrumb({
											category: 'rs_sync_message_insert',
											message: 'sync_message_insert ',
											level: 'info',
											data:JSON.parse(JSON.stringify({sql:sql,val:val}))
										});
										Sentry.captureException(e);
										console.log('4');
										console.error(e.stack);
										setTimeout(function(){
											process.exit(410);
										}, 1000);
										return;
									});
							}
						})
						.catch(e => {
							pubChannel.ack(msg);
							Sentry.captureException(e);
							console.log('5-'+_table);
							setTimeout(function(){
								process.exit(411);
							}, 1000);
							return;
						});
				}else{
					return sequelize.query('SELECT * FROM "'+_table+'" WHERE "client"=:client and start=:start'
						, { replacements:{client:_data.client,start:_data.start}, type: sequelize.QueryTypes.SELECT})
						.then((results) => {
							if(results.length>0){
								pubChannel.ack(msg);
							}else{
								if(_data.id){
									delete _data.id;
								}
								if(_data.isallowna){
									delete _data.isallowna;
								}
								let query = ['INSERT INTO'];
								query.push('"'+_table+'"');
								query.push('(');
								let afn=[];
								let afv=[];
								let orep={};
								let val;
								for(let prop in _data){
									if(prop!=='create_user_id'&&prop!=='update_user_id'){
										afn.push('"'+prop+'"');
										afv.push(':'+prop);
										val=_data[(prop=='updated_at'?'updatedAt':(prop=='created_at'?'createdAt':prop))];
										orep[prop]=val==='null'?null:val;
									}
								}
								query.push(afn.join(', '));
								query.push(') VALUES (');
								query.push(afv.join(', '));
								query.push(')');
								sql=query.join(' ');
								//console.log(sql);
								//console.log(val);
								return sequelize.query(sql
									, { replacements: orep, type: sequelize.QueryTypes.INSERT })
									.then(() => {
										pubChannel.ack(msg);
										return afterRecordSave(data);
									})
									.catch(e => {
										Sentry.addBreadcrumb({
											category: 'rs_sync_message_insert',
											message: 'sync_message_insert ',
											level: 'info',
											data:JSON.parse(JSON.stringify({sql:sql,val:val}))
										});
										Sentry.captureException(e);
										console.log('4');
										console.error(e.stack);
										setTimeout(function(){
											process.exit(410);
										}, 1000);
										return;
									});
							}
						})
						.catch(e => {
							pubChannel.ack(msg);
							Sentry.captureException(e);
							console.log('5-'+_table);
							setTimeout(function(){
								process.exit(411);
							}, 1000);
							return;
						});
				}
			}else{
				return sequelize.query('SELECT * FROM "_sync_updatelogs" WHERE "tableName"=:tableName ORDER BY "strlastupdate" DESC'
					, { replacements:{tableName:_data.tableName}, type: sequelize.QueryTypes.SELECT}).then(results => {
					if(results.length>0){
						return sequelize.query('UPDATE "_sync_updatelogs" set "strlastupdate"=:strlastupdate where "tableName"=:tableName'
							, { replacements:{tableName:_data.tableName,lastUpdate:(_data.lastUpdate?_data.lastUpdate:_data.updatedAt),strlastupdate:(_data.strlastupdate?_data.strlastupdate:_data.updated)}, type: sequelize.QueryTypes.UPDATE}).then(() => {
							pubChannel.ack(msg);
							return null;
						});
					}else{
						return sequelize.query('INSERT INTO "_sync_updatelogs" ("tableName","lastUpdate","createdAt","updatedAt","strlastupdate") VALUES (:tableName,:lastUpdate,:createdAt,:updatedAt,:strlastupdate)'
							, { replacements:{tableName:_data.tableName,lastUpdate:(_data.lastUpdate?_data.lastUpdate:_data.updatedAt),createdAt:_data.updated,updatedAt:_data.updated,strlastupdate:(_data.strlastupdate?_data.strlastupdate:_data.updated)}, type: sequelize.QueryTypes.INSERT}).then(() => {
							pubChannel.ack(msg);
							return null;
						}).catch(e => {
							Sentry.captureException(e);
							console.log('6');
							setTimeout(function(){
								process.exit(412);
							}, 1000);
							return;
						});
					}
					
				});
				
			}
		}
		if(_procType=='update'){
			if(!_data.record_id&&_table!=='client_details'&&_table!=='client_params'){
				console.log('sync_message-update:',_table);
				console.log(_data);
				pubChannel.ack(msg);
				return;
			}
			if(_table!=='client_details'&&_table!=='client_params'){
				return sequelize.query('SELECT * FROM "'+_table+'" WHERE "record_id"=:record_id'
					, { replacements:{record_id:_record_id}, type: sequelize.QueryTypes.SELECT})
					.then((results) => {
						if(results.length==0){
							//if(_data.updated&&(_table=='task_lists'||_table=='operation_authorizations')){
							if(_data.updated){
								if(_data.id){
									delete _data.id;
								}
								if(_data.isallowna){
									delete _data.isallowna;
								}
								let query = ['INSERT INTO'];
								query.push('"'+_table+'"');
								query.push('(');
								let afn=[];
								let afv=[];
								let orep={};
								let val;
								for(let prop in _data){
									if(prop!=='create_user_id'&&prop!=='update_user_id'){
										if(prop!=='updated'){
											afn.push('"'+prop+'"');
											afv.push(':'+prop);
											val=_data[(prop=='updated_at'?'updatedAt':(prop=='created_at'?'createdAt':prop))];
											orep[prop]=val==='null'?null:val;
										}else{
											val=_data[prop];
											afn.push('"updatedAt"');
											afv.push(':upd_1');
											orep['upd_1']=val;
											afn.push('"createdAt"');
											afv.push(':cre_1');
											orep['cre_1']=val;
										}
									}
								}
								query.push(afn.join(', '));
								query.push(') VALUES (');
								query.push(afv.join(', '));
								query.push(')');
								sql=query.join(' ');
								//console.log(sql);
								//console.log(val);
								return sequelize.query(sql
									, { replacements: orep, type: sequelize.QueryTypes.INSERT })
									.then(() => {
										pubChannel.ack(msg);
										return afterRecordSave(data);
									})
									.catch(e => {
										Sentry.captureException(e);
										console.log('44');
										//console.log(sql);
										//console.log(orep);
										setTimeout(function(){
											process.exit(413);
										}, 1000);
										return;
									});
							}else{
								pubChannel.ack(msg);
							}
						}else{
							if(_data.isallowna){
								delete _data.isallowna;
							}
							let query = ['UPDATE'];
							query.push('"'+_table+'"');
							query.push('SET');
							let set = [];
							let orep={};
							for(let key in _data){
								if(key!=='record_id'&&key!=='create_user_id'&&key!=='update_user_id'){
									set.push('"' +((key=='updated_at'||key=='updated')?'updatedAt':((key=='created_at'||key=='created')?'createdAt':key))+'"' + '=:'+key+' '); 
									if(key==='taskinfo'&&(_data[key]!=='null'||_data[key]!==null)){
										console.log('taskinfo-1:',_data[key]);
										console.log('taskinfo-2:',JSON.stringify(_data[key]));
										//console.log(JSON.stringify(_data[key]).replaceAll('"','\"'));
										//orep[key]=(_data[key]==='null'?null:JSON.stringify(_data[key]).replaceAll('"','\\"'));
										orep[key]=(_data[key]==='null'?null:(typeof _data[key]==='object'?JSON.stringify(_data[key]):_data[key]));
									}else{
										orep[key]=(_data[key]==='null'?null:_data[key]);
									}
								}
							}
							orep.record_id=_data['record_id'];
							query.push(set.join(', '));
							query.push('WHERE "record_id" = :record_id' );
							sql=query.join(' ');
							//if(_table==='task_lists'){
							//	console.log(sql);
							//	console.log(orep);
							//}
							return sequelize.query(sql, { replacements:orep, type: sequelize.QueryTypes.UPDATE})
								.then(() => {
									pubChannel.ack(msg);
									return afterRecordSave(data);
								})
								.catch(e => {
									Sentry.addBreadcrumb({
										category: 'rs_sync_message_update',
										message: 'sync_message_update ',
										level: 'info',
										data:JSON.parse(JSON.stringify({sql:sql,orep:orep}))
									});
									Sentry.captureException(e);
									console.log('7',sql,orep);
									setTimeout(function(){
										process.exit(414);
									}, 1000);
									return;
								});
						}
					});
			}else{
				if(_table==='client_details'){
					return sequelize.query('SELECT * FROM "'+_table+'" WHERE "client"=:client and start=:start'
						, { replacements:{client:_data.client,start:_data.start}, type: sequelize.QueryTypes.SELECT})
						.then((results) => {
							if(results.length==0){
								if(_data.updated){
									if(_data.id){
										delete _data.id;
									}
									let query = ['INSERT INTO'];
									query.push('"'+_table+'"');
									query.push('(');
									let afn=[];
									let afv=[];
									let orep={};
									let val;
									for(let prop in _data){
										if(prop!=='create_user_id'&&prop!=='update_user_id'){
											if(prop!=='updated'){
												afn.push('"'+prop+'"');
												afv.push(':'+prop);
												val=_data[(prop=='updated_at'?'updatedAt':(prop=='created_at'?'createdAt':prop))];
												orep[prop]=val==='null'?null:val;
											}else{
												val=_data[prop];
												afn.push('"updatedAt"');
												afv.push(':upd_1');
												orep['upd_1']=val;
												afn.push('"createdAt"');
												afv.push(':cre_1');
												orep['cre_1']=val;
											}
										}
									}
									query.push(afn.join(', '));
									query.push(') VALUES (');
									query.push(afv.join(', '));
									query.push(')');
									sql=query.join(' ');
									//console.log(sql);
									//console.log(val);
									return sequelize.query(sql
										, { replacements: orep, type: sequelize.QueryTypes.INSERT })
										.then(() => {
											pubChannel.ack(msg);
											return afterRecordSave(data);
										})
										.catch(e => {
											Sentry.captureException(e);
											console.log('44');
											//console.log(sql);
											//console.log(orep);
											setTimeout(function(){
												process.exit(413);
											}, 1000);
											return;
										});
								}else{
									pubChannel.ack(msg);
								}
							}else{
								let query = ['UPDATE'];
								query.push('"'+_table+'"');
								query.push('SET');
								let set = [];
								let orep={};
								for(let key in _data){
									if(key!=='record_id'&&key!=='create_user_id'&&key!=='update_user_id'){
										set.push('"' +((key=='updated_at'||key=='updated')?'updatedAt':key)+'"' + '=:'+key+' '); 
										orep[key]=(_data[key]==='null'?null:_data[key]);
									}
								}
								orep.record_id=_data['record_id'];
								query.push(set.join(', '));
								query.push('WHERE "client"=:client and start=:start' );
								sql=query.join(' ');
								//console.log(sql);
								//console.log(orep);
								return sequelize.query(sql, { replacements:orep, type: sequelize.QueryTypes.UPDATE})
									.then(() => {
										pubChannel.ack(msg);
										return afterRecordSave(data);
									})
									.catch(e => {
										Sentry.addBreadcrumb({
											category: 'rs_sync_message_update',
											message: 'sync_message_update ',
											level: 'info',
											data:JSON.parse(JSON.stringify({sql:sql,orep:orep}))
										});
										Sentry.captureException(e);
										console.log('77');
										setTimeout(function(){
											process.exit(414);
										}, 1000);
										return;
									});
							}
						});
				}
				if(_table==='client_params'){
					return sequelize.query('UPDATE client_params set "value"=:value where name=:name', { replacements:{value:_data.value,name:_data.name}, type: sequelize.QueryTypes.UPDATE})
						.then(() => {
							pubChannel.ack(msg);
							return afterRecordSave(data);
						});
				}
			}
		}
		if(_procType=='delete'){
			console.log('sync_message-delete from '+_table+' where record_id=',_record_id);
			return sequelize.query('DELETE FROM "'+_table+'" WHERE "record_id"=:record_id'
				, { replacements:{record_id:_record_id}, type: sequelize.QueryTypes.DELETE})
				.then(() => {
					pubChannel.ack(msg);
					return afterRecordSave(data);
				})
				.catch(e => {
					Sentry.addBreadcrumb({
						category: 'rs_sync_message_delete',
						message: 'sync_message_delete ',
						level: 'info',
						data:JSON.parse(JSON.stringify({_table:_table,record_id:_record_id}))
					});
					Sentry.captureException(e);
					console.log('8');
					setTimeout(function(){
						process.exit(415);
					}, 1000);
					return;
				});
		}
		break;
	}
	case 'mh_label_inserted':{
		let _x=data.data;
		let sql='insert into case_labels (erprefnumber,time,status,record_id,"createdAt","updatedAt") values (:erprefnumber,:time,:status,:record_id,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)';
		return sequelize.query(sql, { replacements:{erprefnumber:_x.erprefnumber,time:str_time,status:_x.status,record_id:_x.record_id}, type: sequelize.QueryTypes.INSERT})
			.then(()=>{
				pubChannel.ack(msg);
				return null;
			})
			.catch(e => {
				Sentry.addBreadcrumb({
					category: 'rs_mh_label_inserted',
					message: 'rs_mh_label_inserted ',
					level: 'info',
					data:JSON.parse(JSON.stringify({erprefnumber:_x.erprefnumber}))
				});
				Sentry.captureException(e);
				//console.log('100');
				setTimeout(function(){
					process.exit(416);
				}, 1000);
				return;
			});
	}
	case 'mh_label_updated':{
		let _x=data.data;
		return sequelize.query('select * from case_labels where erprefnumber=:erprefnumber', 
			{ replacements:{erprefnumber:_x.erprefnumber}, type: sequelize.QueryTypes.SELECT}).then((results)=>{
			if(results.length>0){
				return sequelize.query('update case_labels set status=:status,canceldescription=:canceldescription,canceltime=:canceltime,cancelusername=:cancelusername,"updatedAt"=CURRENT_TIMESTAMP(0) where erprefnumber=:erprefnumber', 
					{ replacements:{status:_x.status,erprefnumber:_x.erprefnumber,canceldescription:_x.canceldescription,canceltime:_x.canceltime,cancelusername:_x.cancelusername}, type: sequelize.QueryTypes.UPDATE}).then(()=>{
					let sql='update task_cases set status=:status where movementdetail is not null and erprefnumber=:erprefnumber and status in (\'WAIT\',\'WORK\')';
					return sequelize.query(sql, { replacements:{status:_x.status,erprefnumber:_x.erprefnumber}, type: sequelize.QueryTypes.UPDATE});
				});
			}else{
				let sql='insert into case_labels (erprefnumber,time,status,record_id,"createdAt","updatedAt") values (:erprefnumber,:time,:status,:record_id,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)';
				return sequelize.query(sql, { replacements:{erprefnumber:_x.erprefnumber,time:str_time,status:_x.status,record_id:_x.record_id}, type: sequelize.QueryTypes.INSERT});
			}
		}).then(()=>{
			pubChannel.ack(msg);
			return null;
		}).catch(e => {
			Sentry.addBreadcrumb({
				category: 'rs_mh_label_updated',
				message: 'rs_mh_label_updated ',
				level: 'info',
				data:JSON.parse(JSON.stringify({erprefnumber:_x.erprefnumber}))
			});
			Sentry.captureException(e);
			//console.log('101');
			setTimeout(function(){
				process.exit(417);
			}, 1000);
			return;
		});
	}
	case 'mh_label_deleted':{
		let _x=data.data;
		return sequelize.query('delete from case_labels where erprefnumber=:erprefnumber', { replacements:{erprefnumber:_x.erprefnumber}, type: sequelize.QueryTypes.DELETE}).then(()=>{
			let sql='update task_cases set status=:status where movementdetail is not null and erprefnumber=:erprefnumber and status in (\'WAIT\',\'WORK\')';
			return sequelize.query(sql, { replacements:{status:_x.status,erprefnumber:_x.erprefnumber}, type: sequelize.QueryTypes.UPDATE});
		}).then(()=>{
			pubChannel.ack(msg);
			return null;
		}).catch(e => {
			Sentry.addBreadcrumb({
				category: 'rs_mh_label_deleted',
				message: 'rs_mh_label_deleted ',
				level: 'info',
				data:JSON.parse(JSON.stringify({erprefnumber:_x.erprefnumber}))
			});
			Sentry.captureException(e);
			//console.log('102');
			setTimeout(function(){
				process.exit(417);
			}, 1000);
			return;
		});
	}
	case 'mh_waitmaterial_set':{
		pubChannel.ack(msg);
		processModule.send({event:'mh_waitmaterial_set',pid:_pid,data:data.data});
		break;
	}
	case 'mh_waitmaterial_clear':{
		pubChannel.ack(msg);
		processModule.send({event:'mh_waitmaterial_clear',pid:_pid,data:data.data});
		break;
	}
	case 'mh_case_movement_target_time':{
		pubChannel.ack(msg);
		processModule.send({event:'mh_case_movement_target_time',pid:_pid,data:data.data});
		break;
	}
	case 'remote_quality_confirm':{
		pubChannel.ack(msg);
		processModule.send({event:'remote_quality_confirm',pid:_pid,data:data.data});
		break;
	}
	case 'remote_quality_rejection':{
		pubChannel.ack(msg);
		processModule.send({event:'remote_quality_rejection',pid:_pid,data:data.data});
		break;
	}
	case 'query_null_record':{
		let _x=data.data;
		return sequelize.query('select finish from '+_x.tableName+' where finish is not null and record_id=:record_id',{replacements:{record_id:_x.record_id}, type: sequelize.QueryTypes.SELECT}).then(results => {
			if(results.length>0){
				if(pubChannel!==null){
					let data={
						type:'res_query_null_record',
						tableName:_x.tableName,
						record_id:_x.record_id,
						finish:convertFullLocalDateString(results[0].finish)
					};
					pubChannel.sendToQueue((_x.client+'_client'), new Buffer.from(JSON.stringify(data)), {persistent: true});
				}
			}
			return null;
		}).then(()=>{
			pubChannel.ack(msg);
			return null;
		}).catch(e => {
			Sentry.addBreadcrumb({
				category: 'rs_query_null_record',
				message: 'rs_query_null_record ',
				level: 'info',
				data:JSON.parse(JSON.stringify({tableName:_x.tableName,record_id:_x.record_id}))
			});
			Sentry.captureException(e);
			//console.log('103');
			setTimeout(function(){
				process.exit(418);
			}, 1000);
			return;
		});
	}
	case 'update_taskinfo':{
		pubChannel.ack(msg);
		processModule.send({event:'update_taskinfo',pid:_pid,data:data.data});
		break;
	}
	}
};
let afterRecordSave = (mes) => {
	let _table=mes.data.tableName;
	let _procType=mes.data.procType;
	let _data=mes.data.data;
	let _lut=mes.data.data.updated?mes.data.data.updated:mes.data.data.updatedAt;
	if(_table==='client_params'){
		processModule.send({event:'client_params_updated',pid:_pid});
	}
	if(_procType=='insert'&&_table==='task_cases'&&_data.movementdetail!==null&&parseInt(_data.movementdetail)>0){
		processModule.send({event:'task_cases_inserted',pid:_pid,data:_data});
	}
	if(_table!='_sync_updatelogs'){
		return sequelize.query('SELECT * FROM "_sync_updatelogs" WHERE "tableName"=:tableName ORDER BY "strlastupdate" DESC'
			, { replacements:{tableName:_table}, type: sequelize.QueryTypes.SELECT})
			.then(results => {
				if(results.length>0){
					let _promises=[];
					for(let i=0;i<results.length;i++){
						let result=results[i];
						if(_lut>result.strlastupdate){
							_promises.push(sequelize.query('UPDATE "_sync_updatelogs" set "strlastupdate"=:strlastupdate where "tableName"=:tableName'
								, { replacements:{tableName:_table,strlastupdate:_lut}, type: sequelize.QueryTypes.UPDATE}));
						}
					}
					return Promise.all(_promises)
						.catch(e => {
							Sentry.addBreadcrumb({
								category: 'ars_update',
								message: 'ars_update ',
								level: 'info',
								data:JSON.parse(JSON.stringify({tableName:_table,strlastupdate:_lut}))
							});
							Sentry.captureException(e);
							console.log('11');
							setTimeout(function(){
								process.exit(419);
							}, 1000);
							return;
						});
				}else{//açılışta tüm log tablosu kayıtları açılıyor, buraya girmemesi gerekiyor
					return sequelize.query('INSERT INTO "_sync_updatelogs"("tableName","lastUpdate","createdAt","updatedAt","strlastupdate") VALUES (:tableName,:lastUpdate,:createdAt,:updatedAt,:strlastupdate)'
						, { replacements:{tableName:_table,lastUpdate:_lut,createdAt:_lut,updatedAt:_lut,strlastupdate:_lut}, type: sequelize.QueryTypes.INSERT}).then(() => {
					}).catch(e => {
						Sentry.addBreadcrumb({
							category: 'ars_insert',
							message: 'ars_insert ',
							level: 'info',
							data:JSON.parse(JSON.stringify({tableName:_table,strlastupdate:_lut}))
						});
						Sentry.captureException(e);
						console.log('9');
						setTimeout(function(){
							process.exit(420);
						}, 1000);
						return;
					});
				}
			})
			.catch(e => {
				Sentry.captureException(e);
				console.log('10');
				setTimeout(function(){
					process.exit(421);
				}, 1000);
				return;
			});
	}
	return;
};

let convertFullLocalDateString=(_time,_ms)=>{
	var dt = new timezoneJS.Date(_time);
	return dt.toString('yyyy-MM-dd HH:mm:ss'+(_ms==true?'.SSS':''));
};
//});