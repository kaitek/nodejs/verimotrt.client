const classBase=require('./base');
const timezoneJS = require('timezone-js');
const tzdata = require('tzdata');
let _tz = timezoneJS.timezone;
_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
_tz.loadZoneDataFromObject(tzdata);
const dcs_message=require('./dcs_message');
const dcs_timer=require('./dcs_timer');
const Promise = require('promise');
const os = require('os');
const crypto = require('crypto');
const dir = require('node-dir');
const http=require('http');
const url=require('url');
const fs=require('fs');
const path = require('path');
const isPortAvailable = require('is-port-available');
const { v4: uuidv4 } = require('uuid');
const processModule = require('process');
const Sentry = require('@sentry/node');

var Sequelize = require('sequelize');
const Op = Sequelize.Op;

Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});
const cp = require('child_process');
const models = require('./models');
class dcs_client extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='dcs_client';
		t.version='0.1.0';
		t.inited=false;
		t.rmc=0;
		t.smc=0;
		t.db=false;
		t.socket_io_client=false;
		t.updatePrefix='verimotRT-Client_';
		t.isDevMachine=false;
		t.isLocked=false;
		t.started=false;
		t.jobrotations=false;
		t.obj_jobrotation=false;
		t.offtimes=false;
		t.overtimes=false;
		t.dcsClientId=false;
		t.objConfig=false;
		t.objParam=false;
		t.wait_for_client_connect=false;
		t.wait_for_ioqueue_empty_resetapp=false;
		t.is_foctory_reset=false;
		t.is_kanban_exists=false;
		t.arr_kanban_over=false;
		t.is_time_synced=false;
		t.tickTimer=false;
		t.tick_lost_status_change=false;
		t.workflow=false;
		t.rabbitmq_send=false;
		t.rabbitmq_receive=false;
		t._arr_offtimes_losttype=false;
		t.waitmaterial=false;
		t.lastrun_controlOfftimes=0;
		t.securitypins={
			server:false
			,client:true
			,system:false
			,prefix:71
			,suffix:39
		};
		t.proc={
			update_downloader:false
		};
		t._flag_employee_jobrotation=false;
		Object.assign(this, this.config);
		if(!t.ip){
			t.ip=classBase.getIp();
		}
		t.db=models;
		t.jobrotations=[];
		t.offtimes=[];
		t.overtimes=[];
		t.__basepath=__dirname.split('\\').slice(0, -1).join('\\');
		t.dcsClientId=t.getMacId();
		t.tmp_queue=[];
		t.sync_tables=[];
		t.arr_sync_updatelogs=[];
		t.arr_kanban_over=[];
		t.obj_jobrotation={day:false,code:false};
		t.objConfig={//cf=controlfunction
			client_name:{label:'Cihaz Adı',value:'',type:'string',cf:{lenmin:3,lenmax:50,forced:true}}
			,client_info:{label:'Kısa İsim',value:'',type:'string',cf:{lenmax:10}}
			,workflow:{label:'Cihaz Tipi',value:'',type:'combo',data:t.getWorkFlows(),cf:{forced:true}}
			,isSetupRequire:{label:'isSetupRequire',value:'true',type:'checkbox'}
			,dcs_server_IP:{label:'Sunucu IP',value:'',type:'ip',inputCntCls:'col-md-3',cf:{forced:true}}
			,dcs_server_PORT:{label:'Sunucu Port',value:'8800',type:'number',inputCntCls:'col-md-2',inputCls:'text-right',cf:{min:1,max:65535,forced:true}}
			,mainapp_IO_TYPE:{label:'IO Tipi',value:'',type:'combo',data:t.getIoTypes(),cf:{forced:false}}
			,mainapp_IO_IP:{label:'IO IP',value:'',type:'ip',labelCls:'col-md-5',inputCntCls:'col-md-3',cf:{forced:false}}
			,mainapp_IO_PORT:{label:'IO Port',value:'',type:'number',labelCls:'col-md-5',inputCntCls:'col-md-2',inputCls:'text-right',cf:{min:1,max:65535,forced:false}}
			,mainapp_IO_COM_PORT:{label:'Com Port',value:'',type:'combo',labelCls:'col-md-5',inputCntCls:'col-md-3',inputCls:'text-right',data:null,cf:{forced:false}}
			,mainapp_IO_INPUTS:{label:'Input Sayısı',value:'',type:'number',inputCntCls:'col-md-2',inputCls:'text-right',cf:{min:1,max:8,forced:false}}
			,mainapp_IO_OUTPUTS:{label:'Output Sayısı',value:'',type:'number',inputCntCls:'col-md-2',inputCls:'text-right',cf:{min:1,max:8,forced:false}}
		};
		t.objParam={
			app_autoUpdate:{
				label:'Otomatik Güncelleme'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,app_empIdentify:{
				label:'Operatör tanımlama şekli'
				,type:'combo'
				,data:[{code:'Klavye'},{code:'RF ID'},{code:'Barkod'}]
				,value:'Klavye'
				,cf:{forced:true}
			}
			,app_empSecret:{
				label:'Personel şifre istenmesi'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,app_showOee:{
				label:'OEE verisi gösterilmesi'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,app_showTempo:{
				label:'Verim değeri gösterilmesi'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,app_showTpp:{
				label:'Çevrim süresi gösterilmesi'
				,type:'checkbox'
				,data:false
				,value:(t.ip&&t.ip.indexOf('172.16')>-1?true:false)
				,cf:{forced:true}
			}
			,app_showCounter:{
				label:'Duruşa kalan süre sayacının gösterilmesi'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,app_inputFilter:{
				label:'PLC Giriş Filtre (5-255)'
				,type:'number'
				,data:false
				,value:50
				,cf:{min:5,max:255}
			}
			,app_btnLabel:{
				label:'Etiket Yazdırma'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,app_showControlAnswers:{
				label:'Kontrol Sorularının Cevaplarını Görüntüleme'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,app_showSOP:{
				label:'Doküman Görüntüleme'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,app_showSOPOnJoin:{
				label:'Göreve Katılmada Doküman Görüntüleme'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,app_showTaskPlanEmployees:{
				label:'Personel İş Planı'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,app_mixedRealityDataSend:{
				label:'Hololens Entegrasyon'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,app_caseVerify:{
				label:'Kasa Doğrulama'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,app_case_stok_before_work:{
				label:'Hammadde kasa bilgisi üretim öncesi istensin'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,process_wsSetup:{
				label:'İş başlatma sonrası ayar süreci'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,process_wsQuality:{
				label:'İş başlatma sonrası kalite süreci'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,process_wfSetup:{
				label:'İş bitirme sonrası ayar süreci'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,process_selectfirstmould:{
				label:'Çoklu kalıp varsa ilkini seç'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,process_productionQualityControl:{
				label:'Üretim sırasında kalite kontrol'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,process_remoteQualityConfirmation:{
				label:'Uzaktan Kalite Onay Süreci'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,process_wsSetupControlQuestions:{
				label:'Setup sonrası kontrol soruları tek tek sorulsun'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_selection:{
				label:'Görev başlatma şekli'
				,type:'combo'
				,data:[{code:'İş Seçerek'},{code:'Kalıp Seçerek'}]
				,value:'İş Seçerek'
				,cf:{forced:true}
			}
			,task_listingOtherClients:{
				label:'Farklı cihazlara ait işlerin listelenmesi'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_finishQuantityReach:{
				label:'İş emri miktarına ulaşıldığında işin sonlandırılması'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,task_finishRequireExpert:{
				label:'İş emri bitirme için yetkili sicil istensin'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,task_addFromUser:{
				label:'Elle görev ekleme'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,task_addFromList:{
				label:'Elle eklenen görev listeden seçilsin'
				,type:'checkbox'
				,data:false
				,value:(t.ip&&t.ip.indexOf('172.16')>-1?false:true)
				,cf:{forced:true}
			}
			,task_delivery:{
				label:'Tesellüm'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_manual_delivery:{
				label:'Manuel Tesellüm'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_materialpreparation:{
				label:'Malzeme Besleme'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_OperationAuthorization:{
				label:'Operasyonel Yetkinlik'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_OpAuth_notDefinedOperation:{
				label:'Yetkinlik tanımı olmayan operasyonda gözetimli üretim yapsın'
				,type:'checkbox'
				,data:false
				,value:true
				,cf:{forced:true}
			}
			,task_selectGroupPlan:{
				label:'Planda gruplu işler tek seferde seçilsin'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_outOfSystem:{
				label:'Sistem Dışı Çalışma'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_reworkType:{
				label:'Görev Listesi'
				,type:'combo'
				,data:[{code:'Üretim Planı'},{code:'Yeniden İşlem'}]
				,value:'Yeniden İşlem'
				,cf:{forced:true}
			}
			,task_finishnoemployee:{
				label:'Kimse kalmayınca iş oturumunu kapat'
				,type:'checkbox'
				,data:false
				,value:(t.ip&&t.ip.indexOf('10.10')>-1?true:false)
				,cf:{forced:true}
			}
			,task_closetaskcases:{
				label:'İşi bitirince kasaları kapat'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_delivery_half_case_completion:{
				label:'Bitmemiş kasa tesellümünde kasayı kapasiteye tamamla'
				,type:'checkbox'
				,data:false
				,value:(t.ip&&t.ip.indexOf('10.10')>-1?false:true)
				,cf:{forced:true}
			}
			,task_manual_time_control:{
				label:'El işçiliği süre kontrolü'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_production_case_control:{
				label:'Üretim kasa bildirimi'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
			,task_production_case_stock_control:{
				label:'Üretim kasa bildirimi stok kontrolü'
				,type:'checkbox'
				,data:false
				,value:false
				,cf:{forced:true}
			}
		};
		t.json_config=false;
		try {
			fs.accessSync(__dirname+'/../config.json');
			t.json_config = JSON.parse(fs.readFileSync(__dirname+'/../config.json', 'utf-8'));
		} catch (error) {
			//
			console.log('catch-json_config');
			console.error(error.toString());
		}
		t.serverHost=t.json_config.server_url.split('/')[2];
		t.serverAppName=t.json_config.server_url.split('/')[3];
		t.securitypins.isDevMachine=t.isDevMachine;
		t.err=false;
		t.tickTimer=new dcs_timer({self:t});
		t.tickTimer.on('tick',t.el_dcs_timer_tick);
		let __d=new timezoneJS.Date('Europe/Istanbul');
		t.tickTimer.start(t.convertFullLocalDateString(__d.getTime(),true));
		t._arr_tables=['client_details','client_lost_details','client_params','client_production_details','operation_authorizations','task_cases','task_reject_details'];
		try {
			fs.accessSync(t.__basepath+'/version.txt');
		} catch (error) {
			fs.writeFileSync(t.__basepath+'/version.txt', '1');
		}
		try {
			fs.accessSync(t.__basepath+'/update');
		} catch (error) {
			fs.mkdirSync(t.__basepath+'/update');
		}
		try{
			let package_json = JSON.parse(fs.readFileSync(__dirname+'/package.json', 'utf8'));
			t.npm_version=package_json.version;
			let content = fs.readFileSync(t.__basepath+'/version.txt');
			t.buildNumber=parseInt(content);
			t.versionApp=t.npm_version+'.'+t.buildNumber;
			t.beforeStart();
		}catch(err){
			t.mjd('start-err:'+err.stack);
		}
	}
	beforeStart(){
		let t=this;
		let p = t.__basepath+'/update';
		let files = dir.files(p, {sync:true});
		if(files){
			files.sort();
			files = files.filter(function (file) {
				return file.match(/.zip$/)!==null;
			});
			if(files.length>0){
				t.mjd('Güncellemeler kontrol ediliyor.');
				function execShellCommand(cmd) {
					const exec = require('child_process').exec;
					return new Promise((resolve, reject) => {
						exec(cmd, (error, stdout, stderr) => {
							if (error) {
								reject(error);
							}
							console.log(stdout);
							console.log(stderr);
							resolve(stdout? stdout : stderr);
						});
					});
				};
				function extractor (__path){
					const extract = require('extract-zip');
					const fsPromises = require('fs').promises;
					return new Promise((resolve, reject) => {
						return extract(__path, { dir: t.__basepath+'/' })
						.then(function (){
							return fsPromises.unlink(__path).then(function(){
								console.log('file_update_and_delete',__path);
								resolve();
							});
						}).catch(function(err) {
							return fsPromises.unlink(__path).then(function(){
								console.log('file_delete',__path);
								reject();
							});
						});
					});
				};
				let __arr_cur_version=t.npm_version.split('.');
				__arr_cur_version.push(t.buildNumber);
				let __file=files[0];
				try {
					let __arr_file_version=__file.split('verimotRT-Client_')[1].split('.zip')[0].split('.');
					if(__arr_file_version[0]>__arr_cur_version[0]
						||(__arr_file_version[0]==__arr_cur_version[0]&&__arr_file_version[1]>__arr_cur_version[1])
						||(__arr_file_version[0]==__arr_cur_version[0]&&__arr_file_version[1]==__arr_cur_version[1]&&__arr_file_version[2]>__arr_cur_version[2])
						||(__arr_file_version[0]==__arr_cur_version[0]&&__arr_file_version[1]==__arr_cur_version[1]&&__arr_file_version[2]==__arr_cur_version[2]&&__arr_file_version[3]>__arr_cur_version[3])
					){
						//güncelleme gerekiyor
						console.log('güncelleme dosyası-',__file);
						return Promise.all([extractor(__file)]).then(function (){
							if(files.length>1){
								return t.beforeStart();
							}else{
								return execShellCommand('cd '+t.__basepath+'\\src && node_modules\\.bin\\sequelize.cmd db:migrate  ').then(function (){
									t.resetApp();
								});
							}
						});
					}else{
						console.log('güncelleme gerekmiyor-',__file);
						fs.unlinkSync(__file);
						return t.beforeStart();
					}
				} catch (error) {
					Sentry.captureException(error);
					Sentry.captureMessage('Güncelleme dosyası işlenemedi.'+__file);
					fs.unlinkSync(__file);
					return t.beforeStart();
				}
			}else{
				t.start();
			}
		}else{
			t.start();
		}
	}
	start(){
		let t=this;
		let config=t.db._config;
		t.db.sequelize.transaction(/*{isolationLevel: t.db.sequelize.Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED},*/function (_tr) {
			let sul_add_record=function(_table){
				return t.db._sync_updatelogs.findOne({attributes: { exclude: ['createdAt'] },where:{tableName:_table } }/*, {transaction: _tr}*/).then(function(results_sul) {
					if(results_sul){
						return null;
					}else{
						return t.db._sync_updatelogs.create({tableName:_table,strlastupdate:'2000-01-01 00:00:00'}/*, {transaction: _tr}*/);
					}
				});
			};
			let _promise=[];
			Object.keys(t.db.sequelize.models).forEach(function(key) {
				if((key.indexOf('_')==-1||key.indexOf('_')>0)&&key!='IOPorts'&&key!='operation_authorizations'){
					_promise.push(sul_add_record(key));
				}
			});
			return Promise.all(_promise).then(function(){
				return config.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{paramname:'client_name' } }, {transaction: _tr}).then(function(result){
					_promise=[];
					let addrecord=function(_tr,_table,_data){
						return t.db[_table].create(_data, {transaction: _tr});
					};
					if(result){
						_promise.push(t.readConfigs());
					}else{
						t.objConfig.client_name.value='';
						t.objConfig.isSetupRequire.value=true;
						t.objConfig.workflow.value='';
						t.objConfig.dcs_server_IP.value='';
						t.objConfig.dcs_server_PORT.value=8800;
						t.objConfig.mainapp_IO_TYPE.value='IO Kart';
						t.objConfig.mainapp_IO_IP.value='10.10.100.100';
						t.objConfig.mainapp_IO_PORT.value='8899';
						t.objConfig.mainapp_IO_INPUTS.value=8;
						t.objConfig.mainapp_IO_OUTPUTS.value=8;
						t.objConfig.mainapp_IO_COM_PORT.value='';
						Object.keys(t.objConfig).forEach(function(key) {
							let obj=t.objConfig[key];
							_promise.push(addrecord(_tr,'_config',{paramname:key,paramlabel:obj.label,paramval:obj.value}));
						});
					}
					return Promise.all(_promise).then(function(){
						let _sql="delete from client_details where id in (SELECT min(id) id from client_details GROUP BY code,description,clienttype,client,iotype,portnumber,ioevent,start HAVING count(*)>1)";
						return t.db.sequelize.query(_sql,{type: t.db.sequelize.QueryTypes.DELETE}); 
					});
				});
			}).then(function(){
				if(t.objConfig.isSetupRequire.value!==true){
					return config.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{paramname:'isSetupRequire' } }, {transaction: _tr}).then(function(result){
						if(result){
							t.objConfig.isSetupRequire.value=true;
						}else{
							t.objConfig.isSetupRequire.value=false;
						}
						return null;
					});
				}else
					return null;
			}).then(function(){
				if(t.objConfig.isSetupRequire.value==true){
					return null;
				}else{
					return Promise.all([t.readParams('p1')]);
				}
			}).then(function(){
				if(t.objConfig.mainapp_IO_TYPE.value=='PLC-SOFT'){
					return null;
				}else{
					return t.getComPorts().then(function(res){
						console.log(res);
						t.objConfig.mainapp_IO_COM_PORT.data=res;
						// if(res.length>0){
						// 	if(t.objConfig.mainapp_IO_TYPE.value!=='IO Kart'){
						// 		//birden fazla com port varsa seçili için işlem yap
						// 		if(t.objConfig.client_name.value!=='03.06.0009'
						// 			&&t.objConfig.client_name.value!=='03.08.0002'
						// 			&&t.objConfig.client_name.value!=='03.08.0007'
						// 			&&t.objConfig.client_name.value!=='03.08.0020'
						// 			&&t.objConfig.client_name.value!=='03.08.0023'
						// 			&&t.objConfig.client_name.value!=='03.08.0021'
						// 			&&t.objConfig.client_name.value!=='03.08.0065'
						// 			&&t.objConfig.client_name.value!=='03.08.0067'
						// 			&&t.objConfig.client_name.value!=='Bliss 2'
						// 			&&t.objConfig.client_name.value!=='P1010'
						// 			&&t.objConfig.client_name.value!=='P1013'
						// 			&&t.objConfig.client_name.value!=='K491'
						// 		){
						//			if(res[res.length-1].code!==t.objConfig.mainapp_IO_COM_PORT.value){
						//				console.info(res[res.length-1].code);
						//				t.objConfig.mainapp_IO_COM_PORT.value=res[res.length-1].code;
						//				return t.db._config
						//					.update({
						//						paramval:t.objConfig.mainapp_IO_COM_PORT.value
						//					}, {
						//						where: { paramname:'mainapp_IO_COM_PORT' }
						//					});
						//			}
						// 		}
						// 	}
						// }
						return null;
					});
				}
			}).then(function(){
				if(t.buildNumber<26){
					let sql='update product_trees set isdefault=true where materialtype in (\'H\',\'K\') and isdefault is null';
					return t.db.sequelize.query(sql,{type: t.db.sequelize.QueryTypes.UPDATE});
				}
				return null;
			}).then(function(){
				if(t.netApp){
					return Promise.all([t.serverHTTP()]);
				}else{
					return null;
				}
			}).then(function(){
				if(t.objConfig.isSetupRequire.value==true){
					return null;
				}else{
					t.p_update_downloader();
					return null;
				}
			}).then(function(){
				let fe1=fs.existsSync('./src/node_modules/socket.io/node_modules/socket.io-parser');
				if(fe1){
					fs.rmdirSync('./src/node_modules/socket.io/node_modules/socket.io-parser', { recursive: true });
				}
				let fe2=fs.existsSync('./src/node_modules/socket.io-client/node_modules/socket.io-parser');
				if(fe2){
					fs.rmdirSync('./src/node_modules/socket.io-client/node_modules/socket.io-parser', { recursive: true });
					t._resetNow();
				}
				return;
			}).then(function(){
				_promise=[];
				let sql = 'SELECT * '+
					'FROM information_schema.tables '+
					'WHERE table_schema = :schema AND table_name = :name ';
					return t.db.sequelize.query(sql, { replacements:{schema:'public',name:'__status'}, type: t.db.sequelize.QueryTypes.SELECT}).then(result =>{
						if(result&&result.length===0){
							sql = 'CREATE SEQUENCE IF NOT EXISTS __status_id_seq;'+
							'CREATE TABLE IF NOT EXISTS public.__status ( '+
							'	"id" int4 NOT NULL DEFAULT nextval(\'__status_id_seq\'::regclass) '+
							'	,"data" json '+
							');'+
							'ALTER TABLE public.__status ADD CONSTRAINT __status_pkey PRIMARY KEY ("id");';
							return t.db.sequelize.query(sql);
						}
						return null;
					});
			});
		}).then(function () {
			Sentry.configureScope(scope => {
				scope.setTag('ip',t.ip);
				scope.setTag('client_name',t.objConfig.client_name.value);
				scope.setTag('version',t.versionApp);
				scope.setTag('build',t.buildNumber);
				scope.setTag('nodejs',process.versions.node);
				//scope.setUser({ ip: t.cfg_dcs_server_IP });
			});
			Sentry.addBreadcrumb({
				category: "program_start",
				message: "Device " + t.objConfig.client_name.value+ ' IP: '+t.ip,
				level: Sentry.Severity.Info,
			});
			Sentry.captureMessage('Device Start');
			t.started=true;

			t.mjd('commit-init');
			t.emit('dcs_inited');
			while(t.tmp_queue.length>0){
				t.db._sync_messages.create(t.tmp_queue.shift().getdata());
			}
			return null;
		}).catch(function (err) {
			t.mjd('rollback-init');
			console.log(err);
			t.tmp_queue=[];
			return null;
		});
	}
	exit_viewer(){
		let t=this;
		if(t.json_config.tcp_io){
			setTimeout(() => {
				t.workflow.iodevice_manager_tcp.send({
					event: 'clearAll'
				});
			}, 10);
			setTimeout(() => {
				t.emit('iodevice_manager_tcp_clear');
			},1000);
		}else{
			t.emit('iodevice_manager_tcp_clear');
		}
	}
	serverHTTP(){
		let t=this;
		let onRequest=function(req,res){
			try {
				let uri = url.parse(req.url).pathname;
				let x = uri.replace('/','').split('&');
				if(x.length>1){
					//
				}else{
					let filename;
					if (uri == '/'){
						uri = t.netElectron?'/index1.html':'/index.html';
						filename = path.join(t.__basepath.split('\\').join('/')+'/src/html/', unescape(uri));
					}else{
						filename = path.join(t.__basepath.split('\\').join('/'), unescape(uri));
					}
					let stats;
					let extname = path.extname(filename);
					let contentType = 'text/html';
					switch (extname) {
					case '.zip':
						contentType = 'application/zip';
						break;
					case '.js':
						contentType = 'text/javascript';
						break;
					case '.css':
						contentType = 'text/css';
						break;
					case '.txt':
						contentType = 'text/plain';
						break;
					}
					try {
						stats = fs.lstatSync(filename);
					} catch (e) {
						res.writeHead(404, {'Content-Type': 'text/plain'});
						res.write('404 Not Found\n');
						res.end();
						t.mjd('onrequest-1:'+e.stack);
						return null;
					}
					if (stats.isFile()) {
						let fileStream = fs.createReadStream(filename);
						fileStream.on('error', function (/*error*/) {
							res.writeHead(404, { 'Content-Type': 'text/plain'});
							res.end('file not found');
						});
						fileStream.on('open', function() {
							res.writeHead(200, {'Content-Type': contentType});
							if(extname=='.zip'||extname=='.txt'){
								res.writeHead(200,{/*'Content-Length': stats.size.toString(),*/'Content-Disposition': 'attachment; filename='+path.basename(uri)});
							}
						});
						fileStream.on('end', function() {
						});
						fileStream.pipe(res).on('close', function() {
							res.end();
						});
						req.on('close',function(/*err*/){
							fileStream.close();
						});
					} else if (stats.isDirectory()) {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('Index of '+uri+'\n');
						res.write('show index?\n');
						res.end();
					} else {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('200 Internal server error\n');
						res.end();
					}
					
				}
			} catch(e) {
				res.writeHead(200);
				res.end();
				t.mjd('onrequest-2:'+e.stack);
			}
		};
		t.server_netApp=http.createServer(onRequest);
		let _io = require('socket.io')(t.server_netApp);
		_io.on('connection', function (client){
			let clientIp = client.request.connection.remoteAddress.replace('::ffff','').replace(':','');
			t.ipc=client;
			t.runinitserver();
			client.ip=clientIp;
			client.on('close-verimotrt', function(){
				t.emit('close-verimotrt');
			});
			client.on('disconnect', function(){
				t.mjd('device disconnected');
				t._resetNow();
			});
		});
		t.server_netApp.timeout = 20000;
		return isPortAvailable(3000).then( status =>{
			if(status){
				t.server_netApp.listen(3000,function(){
					t.runinit(1);
					t.mjd('httpserver started');
				});
			}else{
				t.emit('port_in_use',{port:3000});
				if(processModule){
					processModule.send({event:'port_in_use',port:3000});
				}
			}
		});
	}
	serverKanban(){
		let t=this;
		let onRequest=function(req,res){
			try {
				let uri = url.parse(req.url).pathname;
				let x = uri.replace('/','').split('&');
				if(x.length>1){
					//
				}else{
					let filename;
					if (uri == '/'){
						uri = '/kanban.html';
						filename = path.join(t.__basepath.split('\\').join('/')+'/src/html/', unescape(uri));
					}else{
						filename = path.join(t.__basepath.split('\\').join('/'), unescape(uri));
					}
					let stats;
					let extname = path.extname(filename);
					let contentType = 'text/html';
					switch (extname) {
					case '.zip':
						contentType = 'application/zip';
						break;
					case '.js':
						contentType = 'text/javascript';
						break;
					case '.css':
						contentType = 'text/css';
						break;
					case '.txt':
						contentType = 'text/plain';
						break;
					}
					try {
						stats = fs.lstatSync(filename); 
					} catch (e) {
						res.writeHead(404, {'Content-Type': 'text/plain'});
						res.write('404 Not Found\n');
						res.end();
						t.mjd('onrequest-1:'+e.stack);
						return null;
					}
					if (stats.isFile()) {
						let fileStream = fs.createReadStream(filename);
						fileStream.on('error', function (/*error*/) {
							res.writeHead(404, { 'Content-Type': 'text/plain'});
							res.end('file not found');
						});
						fileStream.on('open', function() {
							res.writeHead(200, {'Content-Type': contentType});
							if(extname=='.zip'||extname=='.txt'){
								res.writeHead(200,{/*'Content-Length': stats.size.toString(),*/'Content-Disposition': 'attachment; filename='+path.basename(uri)});
							}
						});
						fileStream.on('end', function() {
							//
						});
						fileStream.pipe(res).on('close', function() {
							res.end();
						});
						req.on('close',function(/*err*/){
							fileStream.close();
						});
					} else if (stats.isDirectory()) {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('Index of '+uri+'\n');
						res.write('show index?\n');
						res.end();
					} else {
						res.writeHead(200, {'Content-Type': 'text/plain'});
						res.write('200 Internal server error\n');
						res.end();
					}
				}
			} catch(e) {
				res.writeHead(200);
				res.end();
				t.mjd('onrequest-2:'+e.stack);
			}
		};
		t.server_kanban=http.createServer(onRequest);
		let _io = require('socket.io')(t.server_kanban);
		_io.on('connection', function (client){
			let clientIp = client.request.connection.remoteAddress.replace('::ffff','').replace(':','');
			t.kanban_client=client;
			client.ip=clientIp;
			client.on('disconnect', function(){
				
			});
			client.on('js_cn_workflow',function(e, arg){
				if(t.netApp){
					arg=e;
				}
				let _event=arg.event;
				switch(_event) {
				case 'kanban_operations|get':{
					console.log('kanban_operations|get:'+arg.client);
					if(t.workflow){
						t.workflow.eventListener({event:'kanban_operations|get',serverKanban:true,client:arg.client});
					}
					break;
				}
				default:
					t.mjd('js_cn_workflow event yazilmamis!event:'+_event+' fn:serverKanban');
					break;
				}
			});
		});
		t.server_kanban.timeout = 20000;
		return isPortAvailable(3001).then( status =>{
			if(status){
				t.server_kanban.listen(3001);
			}else{
				t.emit('port_in_use',{port:3001});
				if(processModule){
					processModule.send({event:'port_in_use',port:3001});
				}				
			}
		});
	}
	p_update_downloader(){
		let t=this;
		let onMessage = function(obj) {
			try {
				switch (obj.event) {
				case 'update_downloader_started':{
					t.mjd('update_downloader Process PID:'+obj.pid);				
					break;
				}
				case 'update_downloader_complete':{
					if(t.objParam.app_autoUpdate.value===true){
						t.winmessend('cn_js_dcsclient',{event:'update',version:obj.version});
						if(fs.existsSync(obj.path)){
							fs.unlink(obj.path,function(){
								t.resetApp();
							});
						}else{
							t.resetApp();
						}
					}
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				console.log('Error:p_update_downloader-onMessage');
				console.log(error);
			}
		};
		let onError_ud = function(e) {
			console.log('Error_server_update_downloader');
			console.log(e.stack);
		};
		let onClose_ud = function(/*e*/){
			t.mjd('kill-onClose_update_downloader');
		};
		let onDisconnect_ud = function() {
			console.log('kill-onDisconnect_update_downloader');
			let b_run=t.proc.update_downloader&&t.proc.update_downloader.pid?this.pid===t.proc.update_downloader.pid:false;
			if(b_run){
				try {
					this.kill();
				} catch (error) {
					//classBase.writeError(error,t.ip,'');
				}
			}
			t.proc.update_downloader=false;
			setTimeout(() => {
				t.p_update_downloader();
			}, 1000);
		};
		if(t.isDevMachine){
			t.proc.update_downloader=cp.fork(path.join(__dirname, '/update_downloader.js'), [], {execArgv: ['--inspect=7540']});
		}else{
			t.proc.update_downloader=cp.fork(path.join(__dirname, '/update_downloader.js'), []);
		}
		t.proc.update_downloader.on('message',onMessage);
		t.proc.update_downloader.on('error',onError_ud);
		t.proc.update_downloader.on('error',onClose_ud);
		t.proc.update_downloader.on('disconnect',onDisconnect_ud);
		t.proc.update_downloader.send({
			event: 'start_update_downloader',
			pid: t.proc.update_downloader.pid,
			__basepath: t.__basepath
		});
	}
	readConfigs(/*fn_cb*/){
		let t=this;
		return t.db._config.findAll({}).then(function(results) {
			if(results.length>0){
				for (let i = 0; i < results.length; i++) {
					let res=results[i];
					let _val=res.paramval=='true'?true:res.paramval=='false'?false:res.paramval;
					if(typeof t.objConfig[res.paramname]!='undefined'){
						t.objConfig[res.paramname].value=_val;
						if((t.ip==='192.168.1.40'||t.ip==='172.16.1.146'/*||t.ip==='172.16.40.191'*/)&&res.paramname==='mainapp_IO_TYPE'){
							t.objConfig[res.paramname].value='PLC-SOFT';
						}else{
							t.objConfig[res.paramname].value=_val;
						}
						//t.objConfig[res.paramname].value=_val;
					}
				}
			}
			return null;
		});
	}
	readParams(pos){
		let t=this;
		//t.mjd(pos);
		let __promises=[];
		let _time=t.tickTimer.obj_time.strDateTime;
		let __obj_time=t.tickTimer.obj_time;
		if(t.is_time_synced){
		//	t.mjd(__obj_time);
			let _obj_pid={
				update_downloader:t.proc.update_downloader.pid
				,socket_io_client:t.socket_io_client.pid
				,rabbitmq_send:t.rabbitmq_send.pid
				,rabbitmq_receive:t.rabbitmq_receive.pid
			};
			t.mjd(_obj_pid);
		}
		let v_client=t.objConfig.client_name.value;
		Object.keys(t.objParam).forEach(function(key) {
			t.objParam[key].is_db_exists=false;
		});
		let _sql_cp='delete from client_params where id in (SELECT max(id)id from client_params GROUP BY name HAVING count(*)>1)';
		return t.db.sequelize.query(_sql_cp,{ type: t.db.sequelize.QueryTypes.DELETE})
			.then(function(){
				return t.db.client_params.findAll({}).then(function(results) {
					__promises=[];
					if(results.length>0){
						for (let i = 0; i < results.length; i++) {
							let res=results[i];
							let _val=res.value=='true'?true:res.value=='false'?false:res.value;
							if(typeof t.objParam[res.name]!='undefined'){
								t.objParam[res.name].value=_val;
								t.objParam[res.name].is_db_exists=true;
							}
						}
					}
					if(pos=='p1'){
						Object.keys(t.objParam).forEach(function(key) {
							if(t.objParam[key].is_db_exists==false){
								if(key==='task_OpAuth_notDefinedOperation'){
									if( (t.objConfig.dcs_server_IP.value==='172.16.1.143'&&t.objConfig.dcs_server_PORT.value==='8900')
									||(t.objConfig.dcs_server_IP.value==='172.16.1.144'&&t.objConfig.dcs_server_PORT.value==='8800') ){
										if(t.objParam[key].value===true){
											t.objParam[key].value=false;
											t.objParam.task_OpAuth_notDefinedOperation.value===false;
										}
									}
								}
								__promises.push(t.createRecord(null,'client_params',_time,{client:v_client,name:key,value:t.objParam[key].value,record_id:uuidv4()}));
							}else{
								if(key==='task_OpAuth_notDefinedOperation'){
									if( (t.objConfig.dcs_server_IP.value==='172.16.1.143'&&t.objConfig.dcs_server_PORT.value==='8900')
									||(t.objConfig.dcs_server_IP.value==='172.16.1.144'&&t.objConfig.dcs_server_PORT.value==='8800') ){
										if(t.objParam[key].value===true){
											t.objParam[key].value=false;
											t.objParam.task_OpAuth_notDefinedOperation.value===false;
											__promises.push(t.updateRecord(null,'client_params',_time,{name:key},{client:t.objConfig.client_name.value,name:key,value:t.objParam[key].value}));
										}
									}
								}
								if(key==='task_production_case_stock_control'){
									if( t.objConfig.dcs_server_IP.value==='172.16.1.143' || t.objConfig.dcs_server_IP.value==='172.16.1.144' ){
										if(t.objParam[key].value===true){
											t.objParam[key].value=false;
											t.objParam.task_production_case_stock_control.value===false;
											__promises.push(t.updateRecord(null,'client_params',_time,{name:key},{client:t.objConfig.client_name.value,name:key,value:t.objParam[key].value}));
										}
									}
								}
								//if(key==='app_case_stok_before_work'){
								//	if( t.objConfig.dcs_server_IP.value==='10.10.0.10' ){
								//		if(t.objParam[key].value===true){
								//			t.objParam[key].value=false;
								//			t.objParam.app_case_stok_before_work.value===false;
								//			__promises.push(t.updateRecord(null,'client_params',_time,{name:key},{client:t.objConfig.client_name.value,name:key,value:t.objParam[key].value}));
								//		}
								//	}
								//}
								//if(key==='task_production_case_control'){
								//	if( t.objConfig.dcs_server_IP.value==='10.10.0.10' ){
								//		if(t.objParam[key].value===true){
								//			t.objParam[key].value=false;
								//			t.objParam.task_production_case_control.value===false;
								//			__promises.push(t.updateRecord(null,'client_params',_time,{name:key},{client:t.objConfig.client_name.value,name:key,value:t.objParam[key].value}));
								//		}
								//	}
								//}
								//if(key==='task_production_case_stock_control'){
								//	if( t.objConfig.dcs_server_IP.value==='10.10.0.10' ){
								//		if(t.objParam[key].value===true){
								//			t.objParam[key].value=false;
								//			t.objParam.task_production_case_stock_control.value===false;
								//			__promises.push(t.updateRecord(null,'client_params',_time,{name:key},{client:t.objConfig.client_name.value,name:key,value:t.objParam[key].value}));
								//		}
								//	}
								//}
							}
						});
					}
					if(__promises.length>0){
						return Promise.all(__promises);
					}else{
						return null;
					}
				}).then(function(){
					__promises=[];
					__promises.push(t.findAll('job_rotations',{'code':'code','begin':'beginval','end':'endval'},{[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:_time}}]},[],function(items){
						let _arr_jr_code=[];
						for (let i = 0; i < items.length; i++) {
							let res=items[i];
							_arr_jr_code.push(res.code);
							let _idx=t.getIndex(t.jobrotations,'code',res.code);
							if(_idx===-1){
								t.jobrotations.push({code:res.code,begin:res.begin+':00',end:res.end+':59'});
							}else{
								Object.assign(t.jobrotations[_idx], res);
							}
						}
						t.arrRemove(t.jobrotations,_arr_jr_code,'code');
					}));
					if(t.is_time_synced&&(t.objConfig.workflow.value!=='El İşçiliği')){
						let _sql_off='SELECT * '+
						' FROM offtimes  '+
						' where clients ilike :client  '+
						' and ( '+
						'	(repeattype=\'day\' and :time between COALESCE(start,:time) and COALESCE(finish,:time))  '+
						'	or '+
						'	(repeattype=\'none\' and start is not null and finish is not null and :time between start and COALESCE(finish,:time) and startday=:day) '+
						' ) '+
						' order by repeattype,startday,starttime';
						__promises.push(t.getAllRecords(_sql_off,{client:'%'+v_client+'%',time:_time,day:__obj_time.strDate},function(items){
							let _i=t.offtimes.length;
							while(_i-->0){
								t.offtimes[_i].modified=false;
								if(t.offtimes[_i].splitted==true){
									t.offtimes.splice(_i,1);
								}
							}
							t._arr_offtimes_losttype=[];
							for (let i = 0; i < items.length; i++) {
								let res=items[i];
								if(t._arr_offtimes_losttype.indexOf(res.losttype)==-1){
									t._arr_offtimes_losttype.push(res.losttype);
								}
								if(res.repeattype==='day'){
									if(res.days){
										if(res.days.indexOf(__obj_time.strDay)>-1){
											let _idx=t.getIndex(t.offtimes,'record_id',res.record_id);
											res.modified=true;
											if(_idx===-1){
												t.offtimes.push(res);
											}else{
												Object.assign(t.offtimes[_idx], res);
											}
										}
									}
								}else{
									if(t.convertFullLocalDateString(res.start)<=__obj_time.strDateTime
										&&t.convertFullLocalDateString(res.finish)>=__obj_time.strDateTime
									){
										let _idx=t.getIndex(t.offtimes,'record_id',res.record_id);
										res.modified=true;
										if(_idx===-1){
											t.offtimes.push(res);
										}else{
											Object.assign(t.offtimes[_idx], res);
										}
									}
								}
							}
							//modified=false olan kayıtlar siliniyor
							_i=t.offtimes.length;
							while(_i-->0){
								if(t.offtimes[_i].modified==false){
									t.offtimes.splice(_i,1);
								}
							}
						}));
						let _sql_over='SELECT * '+
						' FROM overtimes  '+
						' where clients ilike :client  '+
						' and ( '+
						'	(repeattype=\'day\' and :time between COALESCE(start,:time) and COALESCE(finish,:time))  '+
						'	or '+
						'	(repeattype=\'none\' and start is not null and finish is not null and :time between start and COALESCE(finish,:time) and startday=:day) '+
						' ) '+
						' order by repeattype,startday,start,starttime';
						__promises.push(t.getAllRecords(_sql_over,{client:'%'+v_client+'%',time:_time,day:__obj_time.strDate},function(items){
							for(let i=0;i<t.overtimes.length;i++){
								t.overtimes[i].modified=false;
							}
							for (let i = 0; i < items.length; i++) {
								let res=items[i];
								if(res.repeattype==='day'){
									if(res.days.indexOf(__obj_time.strDay)>-1){
										let _idx=t.getIndex(t.overtimes,'record_id',res.record_id);
										res.modified=true;
										if(_idx===-1){
											t.overtimes.push(res);
										}else{
											Object.assign(t.overtimes[_idx], res);
										}
									}
								}else{
									if(t.convertFullLocalDateString(res.start)<=__obj_time.strDateTime
										&&t.convertFullLocalDateString(res.finish)>=__obj_time.strDateTime
									){
										let _idx=t.getIndex(t.overtimes,'record_id',res.record_id);
										res.modified=true;
										if(_idx===-1){
											t.overtimes.push(res);
										}else{
											Object.assign(t.overtimes[_idx], res);
										}
									}
								}
							}
							//modified=false olan kayıtlar siliniyor
							let _i=t.overtimes.length;
							while(_i-->0){
								if(t.overtimes[_i].modified==false){
									t.overtimes.splice(_i,1);
								}
							}
						}));
					}
					return Promise.all(__promises).then(function(){
						if(t.is_time_synced&&(t.objConfig.workflow.value!=='El İşçiliği')){
							let _overtimes=t.overtimes;
							if(_overtimes.length>0){
								let _offtimes=t.offtimes;
								let _arr_offtime_add=[];
								for(let i=0;i<_offtimes.length;i++){
									let item_off=_offtimes[i];
									if(item_off.clearonovertime==true&&(item_off.finish==null||(item_off.finish!=null&&t.convertFullLocalDateString(item_off.finish)>_time))){
										let vi_start_off=parseInt(item_off.starttime.split(':').join(''));
										let vi_finish_off=parseInt(item_off.finishtime.split(':').join(''));
										for(let j=0;j<_overtimes.length;j++){
											let item_over=_overtimes[j];
											let vi_start_over=parseInt(item_over.starttime.split(':').join(''));
											let vi_finish_over=parseInt(item_over.finishtime.split(':').join(''));
											if(vi_start_off>=vi_start_over){
												//offtime öncesinden başlayan bir fazla mesai
												if(vi_finish_off>vi_finish_over){
													//offtime başlangıcını ötele
													item_off.starttime=item_over.finishtime;
												}else if(vi_finish_off<=vi_finish_over){
													//offtime kaydını sil
													item_off.modified=false;
												}
											}else{
												if(vi_finish_off>vi_finish_over){
													//parçala
													let _x={};
													Object.assign(_x, item_off);
													item_off.finishtime=item_over.starttime;
													_x.starttime=item_over.finishtime;
													_arr_offtime_add.push(_x);
												}else{
													//offtime bitişini geri çek
													item_off.finishtime=item_over.starttime;
												}
											}
										}
									}
								}
								for(let i=0;i<_arr_offtime_add.length;i++){
									t.offtimes.push(_arr_offtime_add[i]);
								}
								//modified=false olan kayıtlar siliniyor
								let _i=t.offtimes.length;
								while(_i-->0){
									if(typeof t.offtimes[_i].modified!=='undefined'&&t.offtimes[_i].modified==false){
										t.offtimes.splice(_i,1);
									}
								}
							}else{
								//t.mjd('readParams');
							}
						}
						return null;
					});
				}).then(function(){
					if(!t.is_kanban_exists){
						return t.db.kanban_operations.findOne({attributes: { exclude: ['createdAt'] },where:{client:v_client } }).then(function(results_ko) {
							if(results_ko){
								t.is_kanban_exists=true;
								return Promise.all([t.serverKanban()]);
							}
							return null;
						});
					}else{
						let sql='SELECT client,product,boxcount,currentboxcount from kanban_operations where currentboxcount>boxcount order by product';
						return t.db.sequelize.query(sql, {type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
							let _promises=[];
							for(let i=0;i<results.length;i++){
								let row=results[i];
								let _idx=t.getIndex(t.arr_kanban_over,'product',row.product);
								if(_idx==-1){
									row.time=_time;
									let mes=new dcs_message({
										type:'kanban_info'
										,ack:true
										,data:row
									});
									_promises.push(t.db._sync_messages.create(mes.getdata()));
									t.arr_kanban_over.push(row);
									mes=undefined;
								}
							}
							if(_promises.length>0){
								return Promise.all(_promises);
							}else{
								return null;
							}
						});
					}
				});
			});
	}
	getWorkFlows(){
		return [
			{code:'El İşçiliği'}//*
			,{code:'Değişken Üretim Aparatlı'/*pres-plastik enjeksiyon */}
			,{code:'Sabit Üretim Aparatlı'/*robot*/}
			,{code:'Üretim Aparatsız'/*sabit punta*/}
			,{code:'Lazer Kesim'}//
			,{code:'Kataforez'}//
			,{code:'CNC'} //
		];
	}
	getIoTypes(){
		return [{code:'IO Kart'},{code:'PLC-USB'/*panasonic mewtocol*/},{code:'PLC-RS232'/*panasonic embedded*/},{code:'PLC-RS232-MB'/*delta*/}];
	}
	getComPorts(){
		let _p=[];
		try {
			const nodeMajorVersion=parseInt(process.versions.node.split('.')[0]);
			let serialport_list = (this.ip==='192.168.1.40'||nodeMajorVersion>=12)?require('serialport').list:Promise.denodeify(require('serialport').list);
			return serialport_list().then(function(ports){
				console.log(ports);
				ports.forEach(function(p) {
					if(p.path.indexOf('COM')>-1){
						_p.push({code:p.path});
					}
				});
				//console.log(_p);
				let _s=function (array, key) {
					return array.sort(function(a, b) {
						var x = a[key]; var y = b[key];
						return ((x < y) ? -1 : ((x > y) ? 1 : 0));
					});
				};
				return _s(_p,'code');
			});
		} catch (error) {
			console.log(error);
			return new Promise(function(resolve) { resolve([]);});
		}
	}
	runinit(p){
		let t=this;
		t.mjd('start client v:'+t.versionApp+' wf:'+t.objConfig.workflow.value+' issetuprequire:'+t.objConfig.isSetupRequire.value+' p:'+p+' node:'+process.versions.node);
		if(!t.server){
			let onRequest=(req,res)=>{
				try {
					let uri = url.parse(req.url).pathname;
					let x = uri.replace('/','').split('&');
					if(x.length>1){
						//
					}else{
						let filename;
						if (uri == '/'){
							uri = '/index.html';
							filename = path.join(t.__basepath.split('\\').join('/')+'/src/html/', unescape(uri));
						}else{
							filename = path.join(t.__basepath.split('\\').join('/'), unescape(uri));
						}
						let stats;
						let extname = path.extname(filename);
						let contentType = 'text/html';
						switch (extname) {
						case '.zip':
							contentType = 'application/zip';
							break;
						case '.js':
							contentType = 'text/javascript';
							break;
						case '.css':
							contentType = 'text/css';
							break;
						case '.txt':
							contentType = 'text/plain';
							break;
						}
						try {
							stats = fs.lstatSync(filename); // throws if path doesn't exist
						} catch (e) {
							res.writeHead(404, {'Content-Type': 'text/plain'});
							res.write('404 Not Found\n');
							res.end();
							t.mjd('onrequest-1:'+e.stack);
							return null;
						}
						if (stats.isFile()) {
							let fileStream = fs.createReadStream(filename);
							fileStream.on('error', function (/*error*/) {
								res.writeHead(404, { 'Content-Type': 'text/plain'});
								res.end('file not found');
							});
							fileStream.on('open', function() {
								res.writeHead(200, {'Content-Type': contentType});
								if(extname=='.zip'||extname=='.txt'){
									res.writeHead(200,{/*'Content-Length': stats.size.toString(),*/'Content-Disposition': 'attachment; filename='+path.basename(uri)});
								}
							});
							fileStream.on('end', function() {
								//
							});
							fileStream.pipe(res).on('close', function() {
								res.end();
							});
							req.on('close',function(/*err*/){
								fileStream.close();
							});
						} else if (stats.isDirectory()) {
							res.writeHead(200, {'Content-Type': 'text/plain'});
							res.write('Index of '+uri+'\n');
							res.write('show index?\n');
							res.end();
						} else {
							res.writeHead(200, {'Content-Type': 'text/plain'});
							res.write('200 Internal server error\n');
							res.end();
						}
						
					}
				} catch(e) {
					res.writeHead(200);
					res.end();
					t.mjd('onrequest-2:'+e.stack);
				}
			};
			t.server=http.createServer(onRequest);
			t.server.timeout = 20000;
			isPortAvailable(parseInt(t.objConfig.dcs_server_PORT.value)+1).then( status =>{
				if(status){
					t.server.listen(parseInt(t.objConfig.dcs_server_PORT.value)+1);
				}else{
					t.emit('port_in_use',{port:parseInt(t.objConfig.dcs_server_PORT.value)+1});
					if(processModule){
						processModule.send({event:'port_in_use',port:parseInt(t.objConfig.dcs_server_PORT.value)+1});
					}
				}
			});
		}
		if(p==2){
			t.runinitserver();
		}	
		t.addListener('dcs_updatereceived', t.el_dcs_updatereceived);
	}
	runinitserver(){
		let t=this;
		if(t.ipc){
			t.ipc.on('js_cn_dcsclient',function(e, arg){
				if(t.netApp){
					arg=e;
				}
				let _event=arg.event;
				switch(_event) {
				case 'init':{
					if(!t.versionShell){
						t.version_node=process.versions.node;
					}
					t.winmessend('cn_js_dcsclient',{
						event:_event
						,config_json:t.json_config
						,cfg:t.objConfig
						,params:t.objParam
						,pins:t.securitypins
						,screensize:t.screensize
						,versionApp:t.versionApp
						,buildNumber:t.buildNumber
						,ip:t.ip
						,is_kanban_exists:t.is_kanban_exists
						,isDevMachine:t.isDevMachine
						,version_node:t.version_node
						,server_url:(t.json_config.server_url?t.json_config.server_url:'')
					});
					//e.sender.send('cn_js_dcsclient',{event:_event,cfg:t.objConfig,params:t.objParam,pins:t.securitypins,screensize:t.screensize,versionApp:t.versionApp,buildNumber:t.buildNumber,ip:t.ip,is_kanban_exists:t.is_kanban_exists,isDevMachine:t.isDevMachine});
					break;
				}
				case 'setuser':{
					//console.log('isEdge:'+arg.isEdge);
					break;
				}
				case 'resetApp':{
					t.resetApp();
					break;
				}
				case 'setup_step_1':{
					let _time=t.tickTimer.obj_time.strDateTime;
					let saverecord=function(_item,_val){
						return t.db._config.findOne({where:{paramname:_item}}).then(function(result){
							if(result){
								return result.update({
									paramval: _val
									, updatedAt: _time
								});
							}
						});
					};
					let _promises=[];
					_promises.push(saverecord('dcs_server_IP',arg.dcs_server_IP));
					_promises.push(saverecord('dcs_server_PORT',arg.dcs_server_PORT));
					Promise.all(_promises).then(function(){
						t.objConfig.dcs_server_IP.value=arg.dcs_server_IP;
						t.objConfig.dcs_server_PORT.value=arg.dcs_server_PORT;
						t.db.clients.truncate({}).then(function(){
							return t.db._sync_updatelogs
								.update({
									strlastupdate: '2000-01-01 00:00:00'
								}, {
									where: { tableName:'clients' }
								}).then(function (/*result*/) {
									if(t.socket_io_client==false){
										t.p_socket_io_client();
									}
									if(t.rabbitmq_send===false){
										t.p_rabbitmq_send();
									}
									if(t.rabbitmq_receive===false){
										t.p_rabbitmq_receive();
									}
								});
						});
					});
					break;
				}
				case 'clients|get':{
					t.db.clients.findAll({where:{code:{[Op.ne]:''} },order: [['code','ASC']]}).then(function(results) {
						let _res=[];
						if(results.length>0){
							for (let i = 0; i < results.length; i++) {
								let res=results[i];
								_res.push({id:res.id,code:res.code,info:res.info,uuid:res.uuid,workflow:(res.workflow==null?'':res.workflow),ip:res.ip,isthis:(t.dcsClientId==res.uuid)
									,rowclass:(res.uuid==t.dcsClientId?'success':res.uuid!=null?'danger':'')
								});
							}
						}
						return t.winmessend('cn_js_dcsclient',{event:_event,result:'ok',message:'',clients:_res});
					});
					break;
				}
				case 'setup_step_3':{
					let _promises=[];
					let _time=t.tickTimer.obj_time.strDateTime;
					t.db.sequelize.transaction(function (_tr) {
						let saverecord=function(_item,_val){
							return t.db._config.findOne({where:{paramname:_item}},{transaction: _tr}).then(function(result){
								if(result){
									return result.update({
										paramval: _val
										, updatedAt: _time
									}, {transaction: _tr});
								}else{
									return t.db._config.create({ paramname: _item, paramlabel:t.objConfig[_item].label, paramval: _val}, {transaction: _tr});
								}
							});
						};
						let cretaenewclient=function(_name,_info,_type){
							let _obj_insert_client={
								code: _name
								,info: _info
								,uuid: t.dcsClientId
								,ip:t.ip
								,isactive:true
								,connected:true
								,workflow:_type
								,status:'lost:İŞ YOK|work:-|emp:0|wfp:-|tpp:0'
								,statustime:_time
								,materialpreparation:false
								,pincode:t.randomIntInc(1000,9999)
								,record_id:uuidv4()
								,updatedAt:_time };
							return t.db.clients.findOne({attributes: { exclude: ['createdAt'] },where:{uuid:t.dcsClientId, [Op.or]: [{code:_name}] }}, {transaction: _tr}).then(function(result_c) {
								if(result_c){
									if(result_c.code==''){
										throw new Error('Bu cihaz için bir tanım var!<br>'+t.dcsClientId);
									}else{
										throw new Error('Bu cihaz <b>'+result_c.code+'</b> olarak tanımlı!');
									}
								}else{
									return null;
								}
							}).then(function() {
								return t.db.clients.create(_obj_insert_client, {transaction: _tr}).then(function(){
									return t.db._sync_updatelogs
										.update({
											strlastupdate: _time
										}, {
											where: { tableName:'clients' },
											transaction: _tr/*,
											returning: true,
											plain: true*/
										}).then(function (/*result*/) {
											_obj_insert_client.create_user_id=0;
											_obj_insert_client.createdAt=_time;
											_obj_insert_client.update_user_id=0;
											_obj_insert_client.updatedAt=_time;
											let mes=new dcs_message({
												type:'sync_message'
												,ack:true
												,data:{tableName:'clients',procType:'insert'
													,data:_obj_insert_client,id:null}
											});
											t.tmp_queue.push(mes);
											mes=undefined;
										});
								});
							});
						};
						_promises.push(saverecord('client_name',arg.data.client_name));
						_promises.push(saverecord('client_info',arg.data.client_info));
						_promises.push(saverecord('workflow',arg.data.workflow));
						_promises.push(saverecord('mainapp_IO_TYPE',arg.data.mainapp_IO_TYPE));
						_promises.push(saverecord('mainapp_IO_IP',arg.data.mainapp_IO_IP));
						_promises.push(saverecord('mainapp_IO_PORT',arg.data.mainapp_IO_PORT));
						_promises.push(saverecord('mainapp_IO_COM_PORT',arg.data.mainapp_IO_COM_PORT));
						_promises.push(saverecord('mainapp_IO_INPUTS',arg.data.mainapp_IO_INPUTS));
						_promises.push(saverecord('mainapp_IO_OUTPUTS',arg.data.mainapp_IO_OUTPUTS));
						return Promise.all(_promises).then(function(){
							_promises=[];
							let _obj_where={
								uuid: t.dcsClientId
							};
							if(t.objConfig.isSetupRequire.value==true){//makine verileri temizlenmiş veya factory reset yapılmış
								//bu durumda client insert yapılacak, client uygunluğu addclient.btnsave.click içinde kontrol ediliyor
								return t.db.clients.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }, {transaction: _tr}).then(function(result_c){
									if(result_c){
										let _obj_update_client={
											code: arg.data.client_name
											,info: arg.data.client_info
											,uuid: t.dcsClientId
											,ip:t.ip
											,isactive:true
											,connected:true
											,workflow:arg.data.workflow
											,status:'lost:İŞ YOK|work:-|emp:0|wfp:-|tpp:0'
											,statustime:_time
											,materialpreparation:false
											,pincode:t.randomIntInc(1000,9999)
											,record_id:result_c.record_id
											,update_user_id:0
											,updatedAt:_time };
										return Promise.all([t.updateRecord(_tr,'clients',_time,_obj_where,_obj_update_client)]).then(function(){
											_promises=[];
											return t.db._config.destroy({
												where: { paramname: 'isSetupRequire' }
											}, {transaction: _tr});
										});
									}else{
										return Promise.all([cretaenewclient(arg.data.client_name,arg.data.client_info,arg.data.workflow)]).then(function(){
											_promises=[];
											return t.db._config.destroy({
												where: { paramname: 'isSetupRequire' }
											}, {transaction: _tr});
										});
									}
								});
							}
						});
					}).then(function () {
						let _promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						if(t.objConfig.isSetupRequire.value==true){//bu değer yukarıda silindiği için aşağıda değeri gelmeyecek
							t.objConfig.isSetupRequire.value=false;
						}
						return Promise.all(_promises).then(function(){
							setTimeout(function() {
								t.resetApp();
							}, 3000);
						});
					}).catch(function (err) {
						console.log('rollback');
						console.log(err);
						classBase.writeError(err,t.ip,'catch-dcs_client-2');
						setTimeout(function() {
							t.resetApp();
						}, 3000);
						t.tmp_queue=[];
						return null;
					});
					break;
				}
				case 'connectserver':{
					if(t.inited==false){
						t.wait_for_client_connect=true;
						setTimeout(() => {
							if(t.socket_io_client==false){
								t.p_socket_io_client();
							}
							if(t.rabbitmq_send===false){
								t.p_rabbitmq_send();
							}
							if(t.rabbitmq_receive===false){
								t.p_rabbitmq_receive();
							}
						}, 1000);
					}
					break;
				}
				case 'openIOSetupscreen':{
					t.db.client_types.findAll({order: [['code','ASC']]}).then(function(results_ct) {
						let res=[];
						if(results_ct.length>0){
							for (let i = 0; i < results_ct.length; i++) {
								let result=results_ct[i];
								res.push({id:result.id
									,code:result.code
									,description:result.description
								});
							}
						}
						return res;
					}).then(function(_ct){
						t.db.io_events.findAll({order: [['code','ASC']]}).then(function(results_ioev){
							let res=[];
							if(results_ioev.length>0){
								for (let i = 0; i < results_ioev.length; i++) {
									let result=results_ioev[i];
									res.push({id:result.id
										,code:result.code
										,iotype:result.iotype
									});
								}
							}
							return {client_types:_ct,io_events:res};
						}).then(function(_obj){
							t.db.client_details.findAll({where:{client:t.objConfig.client_name.value,finish:{[Op.eq]:null} },order: [ ['iotype', 'ASC'],['portnumber', 'ASC'],['code', 'ASC'] ]}).then(function(results_cd) {
								let res=[];
								if(results_cd.length>0){
									for (let i = 0; i < results_cd.length; i++) {
										let result=results_cd[i];
										res.push({id:result.id
											,idx:i
											,record_id:result.record_id
											,start:result.start
											,code:result.code
											,description:result.description
											,clienttype:result.clienttype
											,client:result.client
											,iotype:result.iotype
											,portnumber:result.portnumber
											,ioevent:result.ioevent});
									}
								}
								t.winmessend('cn_js_dcsclient',{event:_event,client_details:res,client_types:_obj.client_types,io_events:_obj.io_events});
								return null;
							});
						});
					});
					break;
				}
				case 'client_details|add':{
					let _tablename=_event.split('|')[0];
					let _time=t.tickTimer.obj_time.strDateTime;
					let _obj_insert=arg.data;
					_obj_insert.client=t.objConfig.client_name.value;
					_obj_insert.start=_time;
					_obj_insert.finish=null;
					_obj_insert.updatedAt=_time;
					if(_obj_insert.iotype===''){
						_obj_insert.iotype=null;
					}
					if(_obj_insert.portnumber===''){
						_obj_insert.portnumber=null;
					}
					if(_obj_insert.ioevent===''){
						_obj_insert.ioevent=null;
					}
					_obj_insert.record_id=uuidv4();
					t.db.sequelize.transaction(function (_tr) {
						return t.db[_tablename].create(_obj_insert, {transaction: _tr}).then(function(){
							return t.db._sync_updatelogs
								.update({
									strlastupdate: _time
								}, {
									where: { tableName:_tablename },
									transaction: _tr/*,
									returning: true,
									plain: true*/
								}).then(function (/*result*/) {
									_obj_insert.create_user_id=0;
									_obj_insert.createdAt=_time;
									_obj_insert.update_user_id=0;
									_obj_insert.updatedAt=_time;
									let mes=new dcs_message({
										type:'sync_message'
										,ack:true
										,data:{tableName:_tablename,procType:'insert'
											,data:_obj_insert,id:null}
									});
									t.tmp_queue.push(mes);
									mes=undefined;
									return null;
								});
						});
					}).then(function () {
						let _promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						return Promise.all(_promises).then(function(){
							return t.db[_tablename].findAll({where:{client:t.objConfig.client_name.value,finish:{[Op.eq]:null} },order: [ ['iotype', 'ASC'],['portnumber', 'ASC'],['code', 'ASC'] ]}).then(function(results_cd) {
								let res=[];
								if(results_cd.length>0){
									for (let i = 0; i < results_cd.length; i++) {
										let result=results_cd[i];
										res.push({id:result.id
											,idx:i
											,record_id:result.record_id
											,start:result.start
											,code:result.code
											,description:result.description
											,clienttype:result.clienttype
											,client:result.client
											,iotype:result.iotype
											,portnumber:result.portnumber
											,ioevent:result.ioevent});
									}
								}
								t.winmessend('cn_js_dcsclient',{event:_event,result:'ok',message:'',client_details:res});
								return null;
							});
						});
					}).catch(function (err) {
						console.log('rollback');
						console.log(err);
						classBase.writeError(err,t.ip,'catch-dcs_client-3');
						t.winmessend('cn_js_dcsclient',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						t.tmp_queue=[];
						return null;
					});
					break;
				}
				case 'client_details|edit':{
					let _tablename=_event.split('|')[0];
					let _time=t.tickTimer.obj_time.strDateTime;
					let _obj_update=arg.data;
					if(_obj_update.iotype===''){
						_obj_update.iotype=null;
					}
					if(_obj_update.portnumber===''||_obj_update.portnumber==='null'){
						_obj_update.portnumber=null;
					}
					if(_obj_update.ioevent===''){
						_obj_update.ioevent=null;
					}
					_obj_update.client=t.objConfig.client_name.value;
					if(_obj_update.start){
						_obj_update.start=t.convertFullLocalDateString(_obj_update.start);
					}
					if(_obj_update.record_id===''||_obj_update.record_id==='null'||_obj_update.record_id===null){
						_obj_update.record_id=uuidv4();
					}
					t.db.sequelize.transaction(function (_tr) {
						return t.db[_tablename].findOne({attributes: { exclude: ['createdAt'] },where:{client:_obj_update.client,start:_obj_update.start } }, {transaction: _tr}).then(function(result){
							if(result){
								let _p=t.workflow.iodevice_manager.outputPorts[result.portnumber-1];
								if ( _p && _p.curval == 1 && result.portnumber != _obj_update.portnumber ) {
									//eski port değerini temizle
									t.workflow.iodevice_manager.send({
										event: 'output_trigger',
										pid: t.workflow.iodevice_manager.pid,
										value: 0,
										number: result.portnumber
									});
									if ( result.ioevent == _obj_update.ioevent ) {
										t.workflow.iodevice_manager.send({
											event: 'output_trigger',
											pid: t.workflow.iodevice_manager.pid,
											value: 1,
											number: _obj_update.portnumber
										});
									}
								}
								return result.update(_obj_update, {transaction: _tr});
							}else{
								throw new Error('Kayıt bulunamadı!');
							}
						}).then(function(){
							return t.db._sync_updatelogs
								.update({
									strlastupdate: _time
								}, {
									where: { tableName:_tablename },
									transaction: _tr/*,
									returning: true,
									plain: true*/
								}).then(function (/*result*/) {
									_obj_update.update_user_id=0;
									_obj_update.updatedAt=_time;
									let mes=new dcs_message({
										type:'sync_message'
										,ack:true
										,data:{tableName:_tablename,procType:'update'
											,data:_obj_update,id:null}
									});
									t.tmp_queue.push(mes);
									mes=undefined;
									return null;
								});
						});
					}).then(function () {
						let _promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						return Promise.all(_promises).then(function(){
							return t.db[_tablename].findAll({where:{client:t.objConfig.client_name.value,finish:{[Op.eq]:null} },order: [ ['iotype', 'ASC'],['portnumber', 'ASC'],['code', 'ASC'] ]}).then(function(results_cd) {
								let res=[];
								if(results_cd.length>0){
									for (let i = 0; i < results_cd.length; i++) {
										let result=results_cd[i];
										res.push({id:result.id
											,idx:i
											,record_id:result.record_id
											,start:result.start
											,code:result.code
											,description:result.description
											,clienttype:result.clienttype
											,client:result.client
											,iotype:result.iotype
											,portnumber:result.portnumber
											,ioevent:result.ioevent});
									}
								}
								t.winmessend('cn_js_dcsclient',{event:_event,result:'ok',message:'',client_details:res});
								return null;
							});
						});
					}).catch(function (err) {
						console.log('rollback');
						console.log(err);
						classBase.writeError(err,t.ip,'catch-dcs_client-4');
						t.winmessend('cn_js_dcsclient',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						t.tmp_queue=[];
						return null;
					});
					break;
				}
				case 'client_details|delete':{
					let _tablename=_event.split('|')[0];
					let _time=t.tickTimer.obj_time.strDateTime;
					let _data=arg.data;
					t.db.sequelize.transaction(function (_tr) {
						return t.db[_tablename].findOne({attributes: { exclude: ['createdAt'] },where:{id:_data.id } }, {transaction: _tr}).then(function(result){
							if(result){
								if(result.iotype==='O'){
									t.workflow.iodevice_manager.send({
										event: 'output_trigger',
										pid: t.workflow.iodevice_manager.pid,
										value: 0,
										number: result.portnumber
									});
								}
								return null;
							}else{
								throw new Error('Kayıt bulunamadı!');
							}
						}).then(function(){
							return t.db[_tablename].destroy({
								where: {id:_data.id }
							}).then(function (/*result*/) {
								let mes=new dcs_message({
									type:'sync_message'
									,ack:true
									,data:{tableName:_tablename,procType:'delete'
										,data:{ 
											client:_data.client
											,record_id:_data.record_id
											,start:t.convertFullLocalDateString(_data.start)
											,finish:_time
										,update_user_id:0,updatedAt:_time },id:null}
								});
								t.tmp_queue.push(mes);
								mes=undefined;
								return null;
							});
						});
					}).then(function () {
						let _promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						return Promise.all(_promises).then(function(){
							return t.db[_tablename].findAll({where:{client:t.objConfig.client_name.value,finish:{[Op.eq]:null} },order: [ ['iotype', 'ASC'],['portnumber', 'ASC'],['code', 'ASC'] ]}).then(function(results_cd) {
								let res=[];
								if(results_cd.length>0){
									for (let i = 0; i < results_cd.length; i++) {
										let result=results_cd[i];
										res.push({id:result.id
											,idx:i
											,record_id:result.record_id
											,start:result.start
											,code:result.code
											,description:result.description
											,clienttype:result.clienttype
											,client:result.client
											,iotype:result.iotype
											,portnumber:result.portnumber
											,ioevent:result.ioevent});
									}
								}
								t.winmessend('cn_js_dcsclient',{event:_event,result:'ok',message:'',client_details:res});
								return null;
							});
						});
					}).catch(function (err) {
						console.log('rollback');
						console.log(err);
						classBase.writeError(err,t.ip,'catch-dcs_client-5');
						t.winmessend('cn_js_dcsclient',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						t.tmp_queue=[];
						return null;
					});
					break;
				}
				case 'client_params|save':{
					let _promises=[];
					let _reset=false;
					t.db.sequelize.transaction(function (_tr) {
						Object.keys(arg.data).forEach(function(key) {
							let _val=arg.data[key];
							if(_val!=t.objParam[key].value){
								t.objParam[key].value=_val;
								_promises.push(t.updateRecord(_tr,'client_params',t.tickTimer.obj_time.strDateTime,{name:key},{client:t.objConfig.client_name.value,name:key,value:_val}));
							}
						});
						if(/*t.objConfig.mainapp_IO_TYPE=='PLC-RS232'&&*/t.workflow){
							t.workflow.emit('send_plc_inputfilter', arg.data.app_inputFilter);
						}
						return Promise.all(_promises);
					}).then(function () {
						_promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						return Promise.all(_promises).then(function(){
							t.winmessend('cn_js_dcsclient',{event:_event,result:'ok',message:'',params:t.objParam,app_reset:_reset});
							return null;
						});
					}).catch(function (err) {
						console.log('rollback');
						console.log(err);
						classBase.writeError(err,t.ip,'catch-client_params|save');
						t.winmessend('cn_js_dcsclient',{event:_event,result:'err',message:err.toString().replace('Error: ',''),app_reset:true});
						t.tmp_queue=[];
						return null;
					});
					break;
				}
				case 'openIOTestscreen':{
					let p;
					let outputs=[];
					let inputs=[];
					if(t.workflow.iodevice_manager){
						if(t.workflow.iodevice_manager.inputPorts){
							for(let i=0;i<t.workflow.iodevice_manager.inputPorts.length;i++){
								p=t.workflow.iodevice_manager.inputPorts[i];
								inputs.push(p.curval);
							}
						}

						if(t.workflow.iodevice_manager.outputPorts){
							for(let i=0;i<t.workflow.iodevice_manager.outputPorts.length;i++){
								p=t.workflow.iodevice_manager.outputPorts[i];
								outputs.push(p.curval);
							}
						}
					}
					t.winmessend('cn_js_dcsclient',{event:_event,input:inputs,output:outputs,pins:t.securitypins});
					break;
				}
				case 'device_refresh':{
					t.deviceRefresh(function(/*obj*/){
						if(t.workflow&&t.workflow.iodevice_manager&&t.workflow.iodevice_manager.connected){
							t.wait_for_ioqueue_empty_resetapp=true;
						}
						setTimeout(function() {
							t.resetApp();
						}, 3000);
					});
					break;
				}
				case 'device_factoryreset':{
					t.deviceFactoryReset(function(/*obj*/){
						if(t.workflow&&t.workflow.iodevice_manager&&t.workflow.iodevice_manager.connected){
							t.wait_for_ioqueue_empty_resetapp=true;
						}
						t.is_foctory_reset=true;
						setTimeout(function() {
							t.resetApp();
						}, 3000);
					});
					break;
				}
				default:
					t.mjd('js_cn_dcsclient event yazilmamis!event:'+_event+' fn:runinit');
					break;
				}
			});
			t.ipc.on('js_cn_workflow',function(e, arg){
				if(t.netApp){
					arg=e;
				}
				if(t.workflow){
					t.workflow.emit('js_cn_workflow', t.workflow, arg);
				}
			});
		}
	}
	prep_sync_data(){
		let t=this;
		t.arr_sync_updatelogs=[];
		let f_cd=false;
		return t.db.sequelize.query("select * from client_details where finish is null", { type: t.db.sequelize.QueryTypes.SELECT}).then(function(results_cd){
			if(results_cd.length===0){
				f_cd=true;
			}
			let sql='SELECT * FROM "_sync_updatelogs" ORDER BY case when ("tableName"=\'job_rotations\') then 1 when ("tableName"=\'clients\') then 2 when ("tableName"=\'client_details\') then 3 when ("tableName"=\'client_types\') then 4 when ("tableName"=\'io_events\') then 5 when ("tableName"=\'employees\') then 6 else 7 end,"tableName" asc';
			return t.db.sequelize.query(sql, { type: t.db.sequelize.QueryTypes.SELECT}).then(function(results_sul){
				for (let i = 0; i < results_sul.length; i++) {
					let result=results_sul[i];
					if(result.tableName!=='operation_authorizations'){
						if(result.tableName!=='client_details'){
							t.arr_sync_updatelogs.push({
								tableName:result.tableName
								,strlastupdate: result.strlastupdate
							});
						}else{
							t.arr_sync_updatelogs.push({
								tableName:result.tableName
								,strlastupdate: (f_cd?'2000-01-01 00:00:00':result.strlastupdate)
							});
						}
					}
				}
				return null;
			});
		});
	}
	p_socket_io_client(){
		let t=this;
		let onMessage = function(obj) {
			try {
				switch (obj.event) {
				case 'socket_io_client_inited':{
					t.mjd('Server socket_client PID:'+obj.pid);
					break;
				}
				case 'connect_error':{
					//t.socket_io_client.kill();
					break;
				}
				case 'error':{
					//t.socket_io_client.kill();
					break;
				}
				case 'connect':{
					t.socket_io_client.connected=true;
					break;
				}
				case 'data':{
					switch(obj.params.event) {
					case 'connect':{
						t.mjd('sn_cn-connect rtc:'+obj.params.rtc);
						if(t.tickTimer!=false){
							t.tickTimer.stop();
							t.tickTimer.start(obj.params.rtc);
							t.is_time_synced=true;
						}
						return Promise.all([t.prep_sync_data(),t.readParams('p2')]).then(function(){
							if(t.objConfig.isSetupRequire.value===true){
								t.socket_io_client.send({event:'cn_sn',data:{event:'setuser',ip:t.ip,versionApp:t.versionApp,buildNumber:t.buildNumber,dcsClientId:t.dcsClientId,client_name:t.objConfig.client_name.value,sync_data:t.arr_sync_updatelogs}});
							}else{
								if(t.wait_for_client_connect==true){
									t.wait_for_client_connect=false;
									t.winmessend('cn_js_dcsclient',{event:'connectserver'});
								}
								t.socket_io_client.send({event:'cn_sn',data:{event:'setuser',ip:t.ip,versionApp:t.versionApp,buildNumber:t.buildNumber,dcsClientId:t.dcsClientId,client_name:t.objConfig.client_name.value,sync_data:t.arr_sync_updatelogs,params:t.objParam}});
								t.winmessend('cn_js_dcsclient',{event:'dcsserver_connected'});
								t.inited=true;
								if(!t.workflow){
									try {
										let dcs_client_workflow=require('./dcs_client_workflow');
										t.workflow=new dcs_client_workflow({self:t,workflow:t.objConfig.workflow.value,is_kanban_exists:t.is_kanban_exists});
									} catch (error) {
										console.log(error);
									}
								}
							}
						});
					}
					case 'setuser':{
						t.securitypins.system=obj.params.pin;
						t.securitypins.server=obj.params.pincode;
						t.securitypins.client=false;
						t.socket_io_client.id=obj.params.id;
						t.socket_io_client.code=obj.params.code;
						break;
					}
					case 'sync_time':{
						if(t.tickTimer!=false){
							let d=new timezoneJS.Date(obj.params.rtc);
							let gt=d.getTime();
							if(gt>t.tickTimer.ms){
								if((gt-t.tickTimer.ms)>(60*1000)){
									Sentry.captureMessage('Zaman ileri alındı. Cihaz zamanı:'+t.tickTimer.strDateTime+' Sunucu zamanı:'+obj.params.rtc);
								}
							}else{
								if((t.tickTimer.ms-gt)>(60*1000)){
									Sentry.captureMessage('Zaman geri alındı. Cihaz zamanı:'+t.tickTimer.strDateTime+' Sunucu zamanı:'+obj.params.rtc);
								}
							}
							t.tickTimer.stop();
							t.tickTimer.start(obj.params.rtc);
							t.is_time_synced=true;
						}
						break;
					}
					case 'routecb':{
						break;
					}
					case 'req_watch_info':{
						if(t.workflow){
							t.workflow.eventListener({event:'req_watch_info'});
						}
						break;
					}
					case 'update_available':{
						let options = {
							basepath: t.__basepath,
							host: t.objConfig.dcs_server_IP.value,
							port: t.objConfig.dcs_server_PORT.value,
							path: obj.params.path,
							hash: obj.params.hash,
							version: obj.params.version
						};
						console.log(options);
						t.proc.update_downloader.send({
							event: 'update_download',
							options: options
						});
						break;
					}
					case 'res_control_delivery_package_info':{
						if(t.workflow){
							t.workflow.eventListener(obj.params);
						}
						break;
					}
					case 'res_case_delivery_manual':{
						if(t.workflow){
							t.workflow.eventListener(obj.params);
						}
						break;
					}
					case 'res_refresh_delivery_package_info':{
						if(t.workflow){
							t.workflow.eventListener(obj.params);
						}
						break;
					}
					case 'calculated_oee':{
						if(t.workflow){
							t.workflow.eventListener(obj.params);
						}
						break;
					}
					case 'query_losttype':{
						if(t.workflow){
							t.workflow.eventListener(obj.params);
						}
						break;
					}
					case 'activity_control_questions_response':{
						if(t.workflow){
							t.workflow.set_activity_control_questions(obj.params.data);
						}
						break;
					} 
					//case 'documents_response':{
					//	if(t.workflow){
					//		t.workflow.set_sop_documents(obj.params);
					//	}
					//	break;
					//}
					case 'res|case_movements|get':{
						t.workflow.eventListener(obj.params);
						break;
					}
					case 'res|task_plan_employees|get':{
						t.workflow.eventListener(obj.params);
						break;
					}
					case 'penetrations_response':{
						if(t.workflow){
							t.workflow.set_penetrations_response(obj.params);
						}
						break;
					}
					case 'imported_api_call':{
						if(t.workflow){
							t.workflow.imported_api_call(obj.params);
						}
					}
					}
					break;
				}
				case 'disconnect':{
					//t.socket_io_client.kill();
					t.socket_io_client.connected=false;
					break;
				}
				case 'disconnect-ping':{
					//t.socket_io_client.kill();
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				console.log('Error:p_socket_io_client-onMessage');
				console.log(error);
			}
		};
		let onError = function(e) {
			console.log('Error_server');
			console.log(e.stack);
		};
		let onClose = function(/*e*/){
			t.mjd('kill-onClose_socket_io_client');
			t.socket_io_client=false;
			try {
				this.kill();
				t.f_p_socket_io_client=true;
				setTimeout(() => {
					t.p_socket_io_client();
				}, 1000);
			} catch (error) {
				Sentry.captureException(error);
				t.f_p_socket_io_client=true;
				setTimeout(() => {
					t.p_socket_io_client();
				}, 1000);
			}
		};
		let onDisconnect = function() {
			console.log('kill-onDisconnect-socket_io_client');
		};
		
		if(t.isDevMachine){
			t.socket_io_client=cp.fork(path.join(__dirname, '/socket_io_client.js'), [], {execArgv: ['--inspect=7640']});
		}else{
			t.socket_io_client=cp.fork(path.join(__dirname, '/socket_io_client.js'), []);
		}
		t.f_p_socket_io_client=false;
		t.socket_io_client.on('message',onMessage);
		t.socket_io_client.on('error',onError);
		t.socket_io_client.on('close',onClose);
		t.socket_io_client.on('disconnect',onDisconnect);
		t.socket_io_client.send({
			event: 'init_socket_io_client',
			param: t.objConfig,
			pid: t.socket_io_client.pid,
			ip:t.ip,
			data: {versionApp:t.versionApp,buildNumber:t.buildNumber,versionShell:t.versionShell,dcsClientId:t.dcsClientId,client_name:t.objConfig.client_name.value,computerName:t.computerName,workflow:t.objConfig.workflow.value}
		});
	}
	p_rabbitmq_send(){
		let t=this;
		let onMessage_rabbitmq = function(obj) {
			try {
				switch (obj.event) {
				case 'cp_server_inited':{
					t.mjd('rabbitmq_send Process PID:'+obj.pid);
					break;
				}
				case 'set_clients':{
					try {
						t.clients=obj.data;
						t.proc.socket_server_device.send({
							event: 'set_clients',
							data: obj.data
						});
					} catch (error) {
						//
					}
					break;
				}
				case 'error':{
					t.mjd('rabbitmq_send error:'+obj.msg);
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				t.mjd('Error:p_rabbitmq_send-onMessage');
				console.log(error);
			}
		};
		let onError_rabbitmq = function(e) {
			t.mjd('Error_p_rabbitmq_send');
			console.log(e.stack);
			classBase.writeError(e,t.ip,'onError_rabbitmq_send');
			onDisconnect_rabbitmq(e);
		};
		let onClose_rabbitmq = function(/*e*/){
			t.mjd('kill-onClose_rabbitmq_send');
			t.rabbitmq_send=false;
			try {
				this.kill();
				t.f_p_rabbitmq_send=true;
				setTimeout(() => {
					t.p_rabbitmq_send();
				}, 1000);
			} catch (error) {
				Sentry.captureException(error);
				t.f_p_rabbitmq_send=true;
				setTimeout(() => {
					t.p_rabbitmq_send();
				}, 1000);
			}
		};
		let onDisconnect_rabbitmq = function() {
			t.mjd('kill-onDisconnect_p_rabbitmq_send');
		};
		if(t.isDevMachine){
			t.rabbitmq_send=cp.fork(path.join(__dirname, '/rabbitmq_send.js'), [], {execArgv: ['--inspect=7340']});
		}else{
			t.rabbitmq_send=cp.fork(path.join(__dirname, '/rabbitmq_send.js'), []);
		}
		t.f_p_rabbitmq_send=false;
		t.rabbitmq_send.on('message',onMessage_rabbitmq);
		t.rabbitmq_send.on('error',onError_rabbitmq);
		t.rabbitmq_send.on('close',onClose_rabbitmq);
		t.rabbitmq_send.on('disconnect',onDisconnect_rabbitmq);
		t.rabbitmq_send.send({
			versionApp:t.versionApp,
			buildNumber:t.buildNumber,
			event: 'init_server',
			param: t.db.sequelize.options,
			config: t.objConfig,
			ip:t.ip,
			pid: t.rabbitmq_send.pid
		});
	}
	p_rabbitmq_receive(){
		let t=this;
		let onMessage_rabbitmq = function(obj) {
			try {
				switch (obj.event) {
				case 'cp_server_inited':{
					t.mjd('rabbitmq_receive Process PID:'+obj.pid);
					break;
				}
				case 'set_clients':{
					try {
						t.clients=obj.data;
						t.proc.socket_server_device.send({
							event: 'set_clients',
							data: obj.data
						});
					} catch (error) {
						//
					}
					break;
				}
				case 'client_params_updated':{
					return t.db.client_params.findAll({}).then(function(results) {
						if(results.length>0){
							for (let i = 0; i < results.length; i++) {
								let res=results[i];
								let _val=res.value=='true'?true:res.value=='false'?false:res.value;
								if(typeof t.objParam[res.name]!='undefined'){
									t.objParam[res.name].value=_val;
									t.objParam[res.name].is_db_exists=true;
								}
							}
						}
						return null;
					});
				}
				case 'mh_waitmaterial_set':{
					t.waitmaterial=obj.data;
					t.winmessend('cn_js_workflow',{event:'mh_waitmaterial_set',stockcode:obj.data.stockcode,erprefnumber:obj.data.erprefnumber});
					break;
				}
				case 'mh_waitmaterial_clear':{
					t.waitmaterial=false;
					t.winmessend('cn_js_workflow',{event:'mh_waitmaterial_clear',stockcode:obj.data.stockcode,erprefnumber:obj.data.erprefnumber});
					break;
				}
				case 'remote_quality_confirm':{
					t.workflow.remote_quality_confirm(obj.data);
					break;
				}
				case 'remote_quality_rejection':{
					t.workflow.remote_quality_rejection(obj.data);
					break;
				}
				case 'mh_case_movement_target_time':{
					if(t.workflow){
						t.workflow.mh_case_movement_target_time(obj.data);
					}
					break;
				}
				case 'task_cases_inserted':{
					t.workflow.task_cases_inserted(obj.data);
					break;
				}
				case 'update_taskinfo':{
					t.workflow.update_taskinfo(obj.data);
					break;
				}
				case 'error':{
					t.mjd('rabbitmq_receive error:'+obj.msg);
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				t.mjd('Error:p_rabbitmq_receive-onMessage');
				console.log(error);
			}
		};
		let onError_rabbitmq = function(e) {
			t.mjd('Error_p_rabbitmq_receive');
			console.log(e.stack);
			classBase.writeError(e,t.ip,'onError_rabbitmq_receive');
			onDisconnect_rabbitmq(e);
		};
		let onClose_rabbitmq = function(/*e*/){
			t.mjd('kill-onClose_rabbitmq_receive');
			t.rabbitmq_receive=false;
			try {
				this.kill();
				t.f_p_rabbitmq_receive=true;
				setTimeout(() => {
					t.p_rabbitmq_receive();
				}, 1000);
			} catch (error) {
				Sentry.captureException(error);
				t.f_p_rabbitmq_receive=true;
				setTimeout(() => {
					t.p_rabbitmq_receive();
				}, 1000);
			}
		};
		let onDisconnect_rabbitmq = function() {
			t.mjd('kill-onDisconnect_p_rabbitmq_receive');
		};
		if(t.isDevMachine){
			t.rabbitmq_receive=cp.fork(path.join(__dirname, '/rabbitmq_receive.js'), [], {execArgv: ['--inspect=7240']});
		}else{
			t.rabbitmq_receive=cp.fork(path.join(__dirname, '/rabbitmq_receive.js'), []);
		}
		t.f_p_rabbitmq_receive=false;
		t.rabbitmq_receive.on('message',onMessage_rabbitmq);
		t.rabbitmq_receive.on('error',onError_rabbitmq);
		t.rabbitmq_receive.on('close',onClose_rabbitmq);
		t.rabbitmq_receive.on('disconnect',onDisconnect_rabbitmq);
		t.rabbitmq_receive.send({
			versionApp:t.versionApp,
			buildNumber:t.buildNumber,
			event: 'init_server',
			param: t.db.sequelize.options,
			config: t.objConfig,
			ip:t.ip,
			pid: t.rabbitmq_receive.pid
		});
	}
	el_dcs_updatereceived(/*sc*/){
		console.log('el_dcs_updatereceived');
	}
	el_dcs_timer_tick(_e){
		let t=_e.scope;
		let _obj=_e.objtime;
		let _jrday;
		let _jrcode;
		let _promises=[];
		let _status='';
		if(t.workflow&&t.workflow.wait_for_first_controljobrotation){
			return null;
		}
		if(t.jobrotations&&t.workflow&&t.jobrotations.length>0){
			if((t.obj_jobrotation.day==false&&t.obj_jobrotation.code==false)||(!!t.workflow&&(t.workflow.inited_wf==0) ) ){
				_jrday=t.getJobRotationDay(t.jobrotations,_obj.strDateTime,false,_obj.strDateTime);
				_jrcode=t.getJobRotationDay(t.jobrotations,_obj.strDateTime,true,_obj.strDateTime);
				_status='init';
				_promises.push(t.workflow.controljobrotations(_jrday,_jrcode,false));
			}else{
				if(_obj.strSecond=='00'){
					_jrday=t.getJobRotationDay(t.jobrotations,_obj.strDateTime,false,_obj.strDateTime);
					_jrcode=t.getJobRotationDay(t.jobrotations,_obj.strDateTime,true,_obj.strDateTime);
					if(_jrday!==false&&_jrcode!==false&&(t.obj_jobrotation.day!=_jrday||t.obj_jobrotation.code!=_jrcode)){
						t.obj_jobrotation.day=_jrday;
						t.obj_jobrotation.code=_jrcode;
						_status='jr_change';
						_promises.push(t.workflow.controljobrotations(_jrday,_jrcode,true));
					}
				}
			}
		}
		if(_promises.length>0){
			return Promise.all(_promises).then(function(){
				if(_status==='init'){
					//vardiya değişimini anlamak için değer ataması controljobrotations sonrasında yapılıyor
					t.obj_jobrotation.day=_jrday;
					t.obj_jobrotation.code=_jrcode;
				}
				return t.fn_cb_tick_promise(_obj);
			});
		}else{
			return t.fn_cb_tick_promise(_obj);
		}
	}
	fn_cb_tick_promise(_obj){
		let t=this;
		let _promises=[];
		let _connected=false;
		if(_obj.strSecond=='30'){
			let drecord=function(_id){
				return t.db.sequelize.models._sync_updatelogs.destroy({ where: { id: _id } }).catch(function (err) {
					console.log(err);
					return null;
				});
			};
			let select_sm=function(){
				let _prom=[];
				return t.db.sequelize.query('SELECT "tableName" from "_sync_updatelogs" GROUP BY "tableName" HAVING count(*)>1', { type: t.db.sequelize.QueryTypes.SELECT })
					.then(function(_res){
						if(_res.length>0){
							for (let i = 0; i < _res.length; i++) {
								let result=_res[i];
								let sql='SELECT * from "_sync_updatelogs" where "tableName"=\''+result.tableName+'\' order by strlastupdate asc';
								return t.db.sequelize.query(sql, { type: t.db.sequelize.QueryTypes.SELECT }).then(function(res_del){
									if(res_del.length>0){
										for (let j = 0; j < res_del.length-1; j++) {//son kayıt kalsın diye length-1
											let row=res_del[j];
											_prom.push(drecord(row.id));
										}
									}
									return null;
								});
							}
						}
						return null;
					}).then(function(){
						if(_prom.length>0){
							return Promise.all(_prom);
						}else{
							return null;
						}
					});
			};
			if((t.ip!=='192.168.1.40'&&parseInt(_obj.strMinute)%5==0)||t.ip=='192.168.1.40'){
				_promises.push(t.readParams('p3'));
			}
			_promises.push(select_sm());
		}
		if(_obj.strSecond=='00'){
			if(t.inited){
				if(!t.f_p_rabbitmq_send&&!t.rabbitmq_send){
					t.p_rabbitmq_send();
				}
				if(!t.f_p_rabbitmq_receive&&!t.rabbitmq_receive){
					t.p_rabbitmq_receive();
				}
				if(!t.f_p_socket_io_client&&!t.socket_io_client){
					t.p_socket_io_client();
				}
			}
		}
		if(parseInt(_obj.strMinute)%5==0&&_obj.strSecond=='15'){
			if(t.obj_jobrotation.day!==false&&t.obj_jobrotation.code!==false&&t.objConfig.workflow.value!=='El İşçiliği'&&t.objConfig.workflow.value!=='Kataforez'){
				let tn='';
				if(t.workflow){
					for(let i=0;i<t.workflow.processVariables.productions.length;i++){
						tn+=(tn==''?'':',')+t.workflow.processVariables.productions[i].opname;
					}
				}
				let _obj_c_p_d={
					type:'c_s_ping'
					,client:t.objConfig.client_name.value
					,day:t.obj_jobrotation.day
					,jobrotation:t.obj_jobrotation.code
					,time:_obj.strDateTime
					,tasklist:'port:0'
					,production:0
					,productioncurrent:0
					,opdescription:tn.substr(0,250)
					,gap:0
					,start:_obj.strDateTime
					,finish:null
					,record_id:uuidv4()
				};
				_promises.push(t.createRecord(null,'client_production_details',_obj.strDateTime,_obj_c_p_d));
			}
		}
		if(parseInt(_obj.strMinute)%25===0){
			if(!t._flag_employee_jobrotation&&t.is_time_synced){
				t._flag_employee_jobrotation=true;
				_promises.push(t.cleanemployeerecords(_obj.strHour+':'+_obj.strMinute));
			}
		}else{
			t._flag_employee_jobrotation=false;
		}
		if(t.socket_io_client){
			_connected=t.socket_io_client.connected;
		}
		if(_connected){
			if(_obj.strSecond=='00'){
				t.socket_io_client.send({event:'cn_sn',data:{event:'sync_time'}});	
			}
			if((_obj.strMinute%2==0||t.isDevMachine)&&_obj.strSecond=='00'){
				let p = t.__basepath+'/update';
				let _v=t.versionApp;
				dir.files(p, function(err, files) {
					if (err) throw err;
					// sort ascending
					files.sort();
					// sort descending
					files = files.filter(function (file) {
						return file.match(/.zip$/)!==null;
					});
					files.reverse();
					
					if(files.length>0){
						let _file=files[0].split('\\');
						_file=_file[_file.length-1];
						_v=_file.replace(t.updatePrefix,'').replace('.zip','');
					}
					if(t.socket_io_client){
						t.socket_io_client.send({event:'cn_sn',data:{event:'update_check',versionApp:_v}});	
					}
				});
			}
		}
		let obj_window_timer={event:'dcs_timer_tick',time:_obj,jobrotation:t.obj_jobrotation,connected:_connected};
		if(t.workflow){
			//t.mjd(_obj.strTimems);
			if(t.is_time_synced
				&&parseInt(_obj.strMinute)%5==0
				&&_obj.strSecond=='15'
				&&(t.workflow.processVariables.status_lost=='İŞ YOK'
					||t.workflow.processVariables.status_lost=='GÖREVDE KİMSE YOK'
					||t.workflow.processVariables.status_lost=='YEMEK MOLASI'
					||t.workflow.processVariables.status_lost=='ÇAY MOLASI')
			){
				_promises.push(t.cleanDeviceTables());
			}
			obj_window_timer.outputports=[];
			if(t.workflow.iodevice_manager&&t.workflow.iodevice_manager.outputPorts){
				for(let _pi=0;_pi<t.workflow.iodevice_manager.outputPorts.length;_pi++){
					let _p=t.workflow.iodevice_manager.outputPorts[_pi];
					obj_window_timer.outputports.push('output|'+_p.number+'|'+_p.curval);
				}
			}
			if(parseInt(_obj.strSecond)>5){
				if(t.workflow.workflow!=='Lazer Kesim'&&t.objConfig.workflow.value!=='Kataforez'&&(t.workflow.processVariables.status_lost==='-'||t.workflow.processVariables.status_lost===''||(t.workflow.processVariables.status_lost==='SETUP-AYAR'&&t.workflow.processVariables.status_workflowprocess==='SETUP'&&t.workflow.processVariables.setup>0))
					&&t.workflow.processVariables.status_work!=='-'&&t.workflow.device_inited){
					obj_window_timer.setuptimer=-10000000;
					if(t.workflow.processVariables.status_lost==='SETUP-AYAR'&&t.workflow.processVariables.status_workflowprocess==='SETUP'&&t.workflow.processVariables.setup>0){
						let v_pd_gap=Math.floor((_obj.ms-new timezoneJS.Date(t.workflow.processVariables.statustime).getTime())/1000);
						obj_window_timer.setup=Math.floor(t.workflow.processVariables.setup/60);
						if(v_pd_gap<=t.workflow.processVariables.setup){
							obj_window_timer.setuptimer=t.workflow.processVariables.setup-v_pd_gap;
							obj_window_timer.setuptimerstr=t.secToTime(t.workflow.processVariables.setup-v_pd_gap);
						}else{
							obj_window_timer.setuptimer=v_pd_gap;
							obj_window_timer.setuptimerstr=t.secToTime(v_pd_gap);
						}
					}else if(t.workflow.processVariables.lastprodstarttime!==null/*&&t.workflow.arr_inputready.length===0*/){
						let v_pd_gap=Math.floor((_obj.ms-new timezoneJS.Date(t.workflow.processVariables.lastprodstarttime).getTime())/1000)-t.workflow.scrap_timer_add;
						//console.log(v_pd_gap);
						obj_window_timer.v_pd_gap=v_pd_gap;
						obj_window_timer.cpd_time=t.workflow.cpd_time;
						if(t.workflow.cpd_time!==false){
							let v_cpd_gap=Math.floor((_obj.ms-new timezoneJS.Date(t.workflow.cpd_time).getTime())/1000);
							if(v_cpd_gap>0){
								obj_window_timer.gap=v_cpd_gap;
								//obj_window_timer.tempo=Math.round(100*(100-(((v_cpd_gap-t.workflow.cpd_pctime-t.workflow.cld_mlosttime)*100)/v_cpd_gap)),2)/100;
								obj_window_timer.tempo=(t.workflow.cpd_pctime/(v_cpd_gap-t.workflow.cld_mlosttime))*100;
								if(obj_window_timer.tempo>10){
									obj_window_timer.tempo=Math.floor(obj_window_timer.tempo);
									if(obj_window_timer.tempo>100){
										obj_window_timer.tempo=100;
									}
								}
							}else{
								obj_window_timer.tempo=100;
							}
						}else{
							obj_window_timer.tempo=0;
						}
						let _tpp=t.workflow.processVariables.status_tpp;
						if(_tpp>0&&t.workflow.bool_init==true){
							if(t.workflow.processVariables.inputready>0&&t.workflow.arr_inputready.length===0){
								_tpp=t.workflow.processVariables.inputready;
							}
							if(t.workflow.arr_inputready.length>0){
								obj_window_timer.countdowntimer=-1;//stop count down
							}else{
								if(v_pd_gap<=parseInt('0'+_tpp)){
									t.tick_lost_status_change=false;
									if(t.workflow.arr_inputready.length===0){
										obj_window_timer.countdowntimer=_tpp-v_pd_gap;
										obj_window_timer.countdowntimerstr=t.secToTime(_tpp-v_pd_gap);
									}else{
										obj_window_timer.countdowntimer=-1;//stop count down
									}
								}else{
									if(!t.workflow.show_delivery){
										obj_window_timer.countdowntimer=0;//change status
										if(!t.tick_lost_status_change){
											t.tick_lost_status_change=true;
											_promises.push(t.workflow.eventListener({event:'work_lost'}));
										}
									}else{
										console.log('t.workflow.show_delivery-work_lost');
									}
								}
							}
						}
						obj_window_timer.loststarttime=t.workflow.processVariables.lastprodstarttime;
						obj_window_timer.losttime=v_pd_gap;
						obj_window_timer.losttimestr=t.secToTime(v_pd_gap);
						obj_window_timer.tpp=_tpp;
					}else{
						obj_window_timer.countdowntimer=-1;//stop count down
						t.tick_lost_status_change=false;
					}
				}else{
					obj_window_timer.countdowntimer=-1;//stop count down
					t.tick_lost_status_change=false;
				}
			}else if(parseInt(_obj.strSecond)==0&&t.is_time_synced&&(t.objConfig.workflow.value!=='El İşçiliği'&&t.objConfig.workflow.value!=='CNC')){
				let epoch=t.tickTimer.obj_time.obj.getTime();
				if(epoch-t.lastrun_controlOfftimes>4000){
					t.lastrun_controlOfftimes=epoch;
					if(t.offtimes.length>0){
						_promises.push(t.workflow.controlOfftimes(t.obj_jobrotation.day,t.obj_jobrotation.code,_obj,t._arr_offtimes_losttype));
					}
				}
			}
			if(t.json_config.restarts&&t.ip!=='192.168.1.40'){
				for(let i=0;i<t.json_config.restarts.length;i++){
					let row=t.json_config.restarts[i];
					if(parseInt(row.h)==parseInt(_obj.strHour)&&parseInt(row.m)==parseInt(_obj.strMinute)&&parseInt(_obj.strSecond)>=0&&parseInt(_obj.strSecond)<5){
						if(t.workflow.processVariables.status_lost==''||t.workflow.processVariables.status_lost=='İŞ YOK'||t.workflow.processVariables.status_lost=='GÖREVDE KİMSE YOK'||t.workflow.processVariables.status_lost=='YEMEK MOLASI'||t.workflow.processVariables.status_lost=='ÇAY MOLASI'){
							t._resetNow();
						}
					}
				}
			}
		}
		t.winmessend('cn_js_dcsclient',obj_window_timer);
		if(_promises.length>0){
			return Promise.all(_promises);
		}else{
			return null;
		}
	}
	cleanemployeerecords(_time){
		let t=this;
		let sql='SELECT id,code,beginval,endval,cast(case when beginval>endval and cast(current_time as varchar(5))>endval then current_date + INTERVAL \'1 day\' else current_date end as varchar(10)) "day"   '+
		' from job_rotations  '+
		' where (finish is null or finish<now()) '+
		'	and ( '+
		'	(endval>beginval and :time between beginval and endval) '+
		'	or (endval<beginval and (:time>=beginval or :time<endval) ) '+
		' )';
		return t.db.sequelize.query(sql, { replacements:{time:_time}, type: t.db.sequelize.QueryTypes.SELECT}).then(result =>{
			if(result&&result.length>0){
				if(result[0].code){
					return t.db.sequelize.query('UPDATE "employees" set client=null,day=null,jobrotation=null '+
					'where client is not null and day is not null and jobrotation is not null and (jobrotation<>:jobrotation) '
					, { replacements:{jobrotation:result[0].code},type: t.db.sequelize.QueryTypes.UPDATE});
				}
				return null;
			}
		});
	}
	cleanDeviceTables(){
		let t=this;
		let epoch=t.tickTimer.obj_time.obj.getTime();
		let _d1=new timezoneJS.Date((epoch-(1 * 24 * 60 * 60 * 1000)));
		let _d10=new timezoneJS.Date((epoch-(10 * 24 * 60 * 60 * 1000)));
		let _d30=new timezoneJS.Date((epoch-(30 * 24 * 60 * 60 * 1000)));
		let d1=_d1.toString('yyyy-MM-dd');
		let d10=_d10.toString('yyyy-MM-dd');
		let d30=_d30.toString('yyyy-MM-dd');
		let _obj_where_trd={
			day:{[Op.lte]:d10}
		};
		let _obj_where_tc={
			start:{[Op.lte]:d30}
			,status:{[Op.in]:['CLOSE','CLOSED','WAIT_FOR_INFO_SEND','WAIT_FOR_INFO_SEND_RETOUCH','WAIT_FOR_DELIVERY']}
		};
		let _obj_where_cl={
			time:{[Op.lte]:d30}
			,status:{[Op.in]:['CLOSE','CLOSED','WAIT_FOR_INFO_SEND','WAIT_FOR_INFO_SEND_RETOUCH','WAIT_FOR_DELIVERY']}
		};

		let deleterecords=function(_sql,_rep){
			return t.db.sequelize.query(_sql,{replacements:_rep, type: t.db.sequelize.QueryTypes.DELETE});
		};
		let updaterecords=function(_sql,_rep){
			return t.db.sequelize.query(_sql,{replacements:_rep, type: t.db.sequelize.QueryTypes.UPDATE});
		};
		let deleterecord=function(_table,_obj_where){
			if(t.db[_table]&&typeof t.db[_table].destroy=='function'){
				return t.db[_table].destroy({
					where: _obj_where
				});
			}else{
				return null;
			}
		};
		let cleantable=function(_table,_obj_where){
			let __promises=[];
			return t.db[_table].findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where,limit: 50 }).then(function(results){
				if(results){
					for (let i = 0; i < results.length; i++) {
						let result=results[i];
						__promises.push(deleterecord(_table,{record_id:result.record_id}));
					}
				}
				return null;
			}).then(function(){
				if(__promises.length>0){
					return Promise.all(__promises);
				}else{
					return null;
				}
			});
		};
		let cleanrepeated=function(_table){
			let sql='delete from '+_table+' where id in (SELECT max(id)id from '+_table+' GROUP BY record_id HAVING count(*)>1)';
			return t.db.sequelize.query(sql, {type: t.db.sequelize.QueryTypes.DELETE});
		};
		let _promises=[];
		let _sql_cld='delete from client_lost_details where day<:d10 and finish is not null';
		_promises.push(deleterecords(_sql_cld,{d10:d10}));
		let _sql_cpd='delete from client_production_details where (day<:d1 and type like \'c_s%\') or (day<:d10 and type not like \'c_s%\' and finish is not null)';
		_promises.push(deleterecords(_sql_cpd,{d1:d1,d10:d10}));
		if(t.objConfig.dcs_server_IP.value!=='10.0.0.101'&&t.objConfig.dcs_server_IP.value!=='10.0.0.237'){
			let _sql_tl_0='update task_lists set finish=CURRENT_TIMESTAMP where finish is null and deadline<:deadline';
			_promises.push(updaterecords(_sql_tl_0,{deadline:d30}));
			let _sql_tl_ld='delete from task_list_laser_details where tasklistlaser in (SELECT code from task_list_lasers where (plannedstart<:d30 and finish is not null))';
			_promises.push(deleterecords(_sql_tl_ld,{d30:d30}));
			let _sql_tl_l='delete from task_list_lasers where (plannedstart<:d30 and finish is not null)';
			_promises.push(deleterecords(_sql_tl_l,{d30:d30}));
		}
		let _sql_pt='delete from product_trees pt where pt.finish is not null and pt.finish<:d10';
		_promises.push(deleterecords(_sql_pt,{d10:d10}));
		let _sql_ca='delete from control_answers ca where ca.day<:d10';
		_promises.push(deleterecords(_sql_ca,{d10:d10}));
		let _sql_tcc='delete from task_case_controls where quantityremaining=0 ';
		_promises.push(deleterecords(_sql_tcc,{}));
		let _sql_tc='update task_cases '+
		'	set status=\'CLOSED\'  '+
		'	from (SELECT tc.id  '+
		'		from task_cases tc  '+
		'		left join task_lists tl on tl.erprefnumber=tc.erprefnumber '+	
		'		where tc.start is not null and tc.start<:start and tc.status in (\'WORK\',\'WAIT\',\'WAIT_FOR_DELIVERY\') and tl.id is null ) tc '+
		'where tc.id=task_cases.id';
		_promises.push(updaterecords(_sql_tc,{start:d10}));
		_promises.push(cleantable('task_reject_details',_obj_where_trd));
		//let _sql_tl='delete from task_lists where finish is not null and deadline<:deadline and (type=\'time\' or (type=\'quantity\' and productdonecount>productcount))';
		if(t.objConfig.dcs_server_IP.value!=='10.0.0.101'&&t.objConfig.dcs_server_IP.value!=='10.0.0.237'){
			let _sql_tl='delete from task_lists where finish is not null and deadline<:deadline';
			_promises.push(deleterecords(_sql_tl,{deadline:d30}));
		}
		_promises.push(cleantable('case_labels',_obj_where_cl));
		_promises.push(cleantable('task_cases',_obj_where_tc));

		_promises.push(cleanrepeated('client_types'));
		_promises.push(cleanrepeated('control_questions'));
		_promises.push(cleanrepeated('fault_types'));
		_promises.push(cleanrepeated('lost_types'));
		_promises.push(cleanrepeated('mould_types'));
		_promises.push(cleanrepeated('reject_types'));
		_promises.push(cleanrepeated('rework_types'));
		_promises.push(cleanrepeated('task_finish_types'));

		return Promise.all(_promises).catch(function (err) {
			t.mjd('rollback-cleanDeviceTables');
			console.log(err);
			classBase.writeError(err,t.ip,'catch-cleanDeviceTables');
			return null;
		});
	}
	setWindow(obj){
		this.windows=obj;
		this.runinit(2);
	}
	winmessend(_event,_obj){
		let t=((this.windows||this.netApp)?this:this.self);
		if(typeof t=='undefined'){
			return null;
		}
		try {
			if(t.windows&&t.windows.main&&t.windows.main&&t.windows.main.win&&t.windows.main.win.webContents&&typeof t.windows.main.win.webContents.send=='function'&&!t.netElectron){
				t.windows.main.win.webContents.send(_event,_obj);
			}else if(t.netApp){
				if(t.ipc&&typeof _obj.serverKanban=='undefined'){
					t.ipc.emit(_event,_obj);
				}else{
					if(t.kanban_client){
						t.kanban_client.emit(_event,_obj);
					}
				}
			}else{
				if(_obj.event){
					if(_obj.event!='dcs_timer_tick'){
						console.log('err-winmessend');
						console.log(_obj);
					}
				}else{
					t.mjd(_obj.event);
					t.mjd('ERROR!!!-winmessend.else');
				}
			}
		} catch (error) {
			if(error.toString().indexOf('Object has been destroyed')==-1){
				console.log('winmessend');
				console.log(error);
			}
		}
	}
	walkSync(sc,dir, filelist) {
		let files = fs.readdirSync(dir);
		filelist = filelist || [];
		files.forEach(function(file) {
			let curPath = dir + '/' + file;
			if (fs.statSync(curPath).isDirectory()) {
				filelist = sc.walkSync(sc,curPath, filelist);
				filelist.push(curPath);
			}
			else {
				filelist.push(curPath);
			}
		});
		return filelist;
	}
	deleteFolderRecursive(sc,path) {
		if( fs.existsSync(path) ) {
			fs.readdirSync(path).forEach(function(file/*,index*/){
				let curPath = path + '/' + file;
				if(fs.lstatSync(curPath).isDirectory()) { // recurse
					sc.deleteFolderRecursive(curPath);
				} else { // delete file
					fs.unlinkSync(curPath);
				}
			});
			fs.rmdirSync(path);
		}
	}
	getMacId(networkInterfaces, fallback) {
		let i, v, value;
		networkInterfaces = networkInterfaces || os.networkInterfaces() || {};
		if (!fallback) {
			/* jshint -W015: true */
			interfaces: for (i in networkInterfaces) {
				for (v in i) {
					if (networkInterfaces[i][v] && networkInterfaces[i][v].address && networkInterfaces[i][v].address.length && !networkInterfaces[i][v].internal&&networkInterfaces[i][v].family==='IPv4'&& networkInterfaces[i][v].address!=='10.10.100.100') {
						value = networkInterfaces[i][v].mac;
						break interfaces;
					}
				}
			}
		}
		return crypto.createHash('md5')
			.update(fallback || value || os.hostname(), 'utf8')
			.digest('hex');
	}
	_rebootNow(){
		const spawn = require('child_process').spawn;
		spawn('shutdown', ['/r','/t','0']);
	}
	_resetNow(){
		let t=this;
		try{
			if(t.windows){
				if(t.windows.main&&t.windows.main.win&&typeof t.windows.main.win.close=='function'){
					t.windows.main.win.close();
				}
				if(t.windows.w1&&t.windows.w1.win&&typeof t.windows.w1.win.close=='function'){
					t.windows.w1.win.close();
				}
			}else if(t.netApp){
				t.emit('reset-verimotrt');
			}
		}catch(err){
			this.mjd('_resetNow-err:'+err.stack);
		}
	}
	resetApp(){
		this.mjd('resetApp');
		let t=this;
		if(t.objConfig.workflow.value!='El İşçiliği'&&t.objConfig.workflow.value!=='Kataforez'){
			if(t.wait_for_ioqueue_empty_resetapp==true&&t.objConfig.mainapp_IO_TYPE.value==='IO Kart'){
				setTimeout(function() {
					t.resetApp();
				}, 1000);
			}else{
				t._resetNow();
			}
		}else{
			t._resetNow();
		}
	}
	deviceRefresh(fn_cb){
		let t=this;
		let _time=t.tickTimer.obj_time.strDateTime;
		let _promises=[];
		t.db.sequelize.transaction(function (_tr) {
			if(t.workflow){
				if(t.workflow.processVariables.status_lost!=='-'){
					let _obj_where_t_l_d={
						start:{[Op.ne]:null}
						,finish:{[Op.eq]:null}
					};
					_promises.push(t.workflow.cjr_client_lost_details(_tr,_obj_where_t_l_d,_time));
				}
			}
			_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:t.objConfig.client_name.value,finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:_time}));
			return Promise.all(_promises).then(function(){
				_promises=[];
				if(t.workflow){
					t.workflow.processVariables.status_employee=0;
					if(t.workflow.processVariables.status_work!=='-'){
						let _obj_where_c_p_d={
							client:t.objConfig.client_name.value
							,start:{[Op.ne]:null}
							,finish:{[Op.eq]:null}
						};
						return t.db.client_production_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where_c_p_d }).then(function(res_cpd){
							if(res_cpd){
								for(let i=0;i<res_cpd.length;i++){
									if(res_cpd[i].type=='c_p'){
										_promises.push(t.updateRecord(_tr,'task_lists',_time,{code:res_cpd[i].tasklist},{productdoneactivity:0,scrapactivity:0}));
									}
									_promises.push(t.workflow.cjr_client_production_details(_tr,{record_id:res_cpd[i].record_id},_time));
								}
							}
							if(_promises.length>0){
								return Promise.all(_promises);
							}else{
								return null;
							}
						});
					}else{
						return null;
					}
				}else{
					return null;
				}
			}).then(function(){
				_promises=[];
				_promises.push(t.findAll('clients',false,{uuid:t.dcsClientId},[],function(items){
					for (let i = 0; i < items.length; i++) {
						let res=items[i];
						let mes=new dcs_message({
							type:'sync_message'
							,ack:true
							,data:{tableName:'clients',procType:'delete'
								,data:res,id:null}
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					}
				}));
				return Promise.all(_promises).then(function(){
					_promises=[];
					let sql='select setval(\'_configs_id_seq\', 1);';
					return t.db.sequelize.query(sql, {type: t.db.sequelize.QueryTypes.SELECT,transaction: _tr}).then(function(){
						return t.db._config.truncate({},{transaction: _tr});
					});
				}).then(function(){
					let saverecord=function(_item,_val){
						return t.db._config.findOne({where:{paramname:_item}},{transaction: _tr}).then(function(result){
							if(result){
								return result.update({
									paramval: _val
									, updatedAt: _time
								}, {transaction: _tr});
							}else{
								return t.db._config.create({ paramname: _item, paramlabel:t.objConfig[_item].label, paramval: _val}, {transaction: _tr});
							}
						});
					};
					_promises.push(saverecord('client_name',''));
					_promises.push(saverecord('workflow',''));
					_promises.push(saverecord('dcs_server_IP',''));
					_promises.push(saverecord('dcs_server_PORT',8800));
					_promises.push(saverecord('mainapp_IO_TYPE','IO Kart'));
					_promises.push(saverecord('mainapp_IO_IP','10.10.100.100'));
					_promises.push(saverecord('mainapp_IO_PORT','8899'));
					_promises.push(saverecord('mainapp_IO_COM_PORT',''));
					_promises.push(saverecord('mainapp_IO_INPUTS',8));
					_promises.push(saverecord('mainapp_IO_OUTPUTS',8));
					_promises.push(saverecord('isSetupRequire',true));
					return Promise.all(_promises).then(function(){
						let updateIO=function(_id){
							return t.db.IOPorts.findOne({where:{id:_id}},{transaction: _tr}).then(function(result){
								if(result){
									if(result.type=='O'){
										if(parseInt(result.value)==1){
											if(t.workflow){
												t.workflow.emit('setoutput', result.number, 0);
											}
										}
									}
									return result.update({
										value: 0
									}, {transaction: _tr});
								}else{
									return null;
								}
							});
						};
						_promises=[];
						return t.db.IOPorts.findAll({}).then(function(results) {
							if(results.length>0){
								for (let i = 0; i < results.length; i++) {
									let item=results[i];
									_promises.push(updateIO(item.id));
								}
							}
							if(_promises.length>0){
								return Promise.all(_promises);
							}else{
								return null;
							}
						});
					});
				});
			});
		}).then(function () {
			let _promises=[];
			if(t.workflow){
				while(t.workflow.tmp_queue.length>0){
					_promises.push(t.db._sync_messages.create(t.workflow.tmp_queue.shift().getdata()));
				}
			}
			while(t.tmp_queue.length>0){
				_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
			}
			return Promise.all(_promises); 
		}).then(function () {
			console.log('commit-deviceRefresh');
			fn_cb({result:'ok',message:'Ayarlar kaydedildi.'/*,cfg:t.objConfig*/});
			return null;
		}).catch(function (err) {
			t.mjd('rollback-deviceRefresh');
			console.log(err);
			classBase.writeError(err,t.ip,'catch-dcs_client-15');
			fn_cb({result:'err',message:err.toString().replace('Error: ','')/*,cfg:t.objConfig*/});
			t.tmp_queue=[];
			return null;
		});
	}
	deviceFactoryReset(fn_cb){
		let t=this;
		t.deviceRefresh(function(obj){
			if(obj.result=='ok'){
				t.db.sequelize.transaction(function (_tr) {
					let cleantable=function(_table){
						if(t.db[_table]&&typeof t.db[_table].destroy=='function'){
							let sql='select setval(\''+_table+'_id_seq\', 1);';
							return t.db.sequelize.query(sql, {type: t.db.sequelize.QueryTypes.SELECT,transaction: _tr}).then(function(){
								return t.db[_table].truncate({},{transaction: _tr});
							});
						}else{
							return null;
						}
					};
					let _promises=[];
					let _exclude=['_config','IOPorts','_sync_messages'];
					Object.keys(t.db.sequelize.models).forEach(function(key) {
						if(_exclude.indexOf(key)==-1){
							_promises.push(cleantable(key));
						}
					});
					return Promise.all(_promises);
				}).then(function () {
					let _promises=[];
					if(t.workflow){
						while(t.workflow.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.workflow.tmp_queue.shift().getdata()));
						}
					}
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					return Promise.all(_promises); 
				}).then(function () {
					console.log('commit-deviceFactoryReset');
					fn_cb({result:'ok',message:'Ayarlar kaydedildi.',cfg:t.objConfig});
					return null;
				}).catch(function (err) {
					console.log('rollback');
					console.log(err);
					classBase.writeError(err,t.ip,'catch-dcs_client-16');
					fn_cb({result:'err',message:err.toString().replace('Error: ',''),cfg:t.objConfig});
					t.tmp_queue=[];
					return null;
				});
			}else{
				fn_cb(obj);
			}
		});
	}
	getLastElementOfArray(_arr){
		return _arr.length>0?_arr[_arr.length-1]:[];
	}

	copyAllRecords(_tr,_table,_time,_obj_where,_obj_new_record_override,_obj_base_record_update,_base_record_update){
		let t=this.db?this:this.self;
		let __promises=[];
		return t.db[_table].findAll({attributes: { exclude: ['id','createdAt','updatedAt'] },where:_obj_where }).then(function(results){
			if(results){
				for (let i = 0; i < results.length; i++) {
					const result=results[i];
					let _tmp_data={};
					let _row_jr_day;
					let _row_jr_code;
					for(let _a in result.dataValues){
						switch (_a) {
						case 'id':
							break;
						case 'day':
							_tmp_data[_a]=t.obj_jobrotation.day;
							break;
						case 'jobrotation':
							_tmp_data[_a]=t.obj_jobrotation.code;
							break;
						case 'start':
							_row_jr_day=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(result[_a]),false,result[_a]);
							_row_jr_code=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(result[_a]),true,result[_a]);
							_tmp_data[_a]=_time;
							break;
						case 'record_id':
							_tmp_data[_a]=uuidv4();
							break;
						default:
							_tmp_data[_a]=result[_a];
							break;
						}
					}
					for(let _b in _obj_new_record_override){
						_tmp_data[_b]=_obj_new_record_override[_b];
					}
					__promises.push(t.createRecord(_tr,_table,_time,_tmp_data));
					if(_base_record_update){
						if(_obj_base_record_update.finish){
							if(t.processVariables._jrday!=_row_jr_day||t.processVariables._jrcode!=_row_jr_code){
								_obj_base_record_update.finish=t.getLastTimeOfJobRotaion(t.convertLocalDateString(_row_jr_day),_row_jr_code);
							}
						}
						__promises.push(t.updateRecord(_tr,_table,_time,{record_id:result.record_id},_obj_base_record_update));
					}
				}
			}
			if(__promises.length>0){
				return Promise.all(__promises);
			}else{
				return null;
			}
		});
	}
	createRecord(_tr,_table,_time,_data){
		let t=this.db?this:this.self;
		let _s=typeof this._arr_tables!='undefined'?this:this.self;
		let __data=classBase.applyIf({}, _data);
		__data.create_user_id=0;
		__data.createdAt=_time;
		__data.update_user_id=0;
		__data.updatedAt=_time;
		let mes=new dcs_message({
			type:'sync_message'
			,ack:true
			,data:{tableName:_table,procType:'insert'
				,data:__data,id:null}
		});
		t.tmp_queue.push(mes);
		mes=undefined;
		return t.db[_table].create(_data, {transaction: _tr}).then(function(){
			if(_s._arr_tables.indexOf(_table)==-1){
				return t.db._sync_updatelogs
					.update({
						strlastupdate:_time
					}, {
						where: { tableName:_table },
						transaction: _tr/*,
						returning: true,
						plain: true*/
					}).then(function (/*result*/) {
						return null;
					});
			}else{
				return null;
			}
		}).catch(function (err) {
			Sentry.addBreadcrumb({
				category: 'createRecord',
				message: 'createRecord-err',
				level: 'info',
				data:JSON.parse(JSON.stringify({table:_table,data:_data}))
			});
			Sentry.captureMessage('createRecord ' + err.sql + err.stack);
			Sentry.captureMessage(JSON.stringify({table:_table,data:_data}));
			Sentry.captureException(err);
		});
	}
	deleteRecord(_tablename,_data){
		let t=this.db?this:this.self;
		return t.db[_tablename].destroy({
			where: {id:_data.id }
		}).then(function (/*result*/) {
			let mes=new dcs_message({
				type:'sync_message'
				,ack:true
				,data:{tableName:_tablename,procType:'delete'
					,data:_data,id:null}
			});
			t.tmp_queue.push(mes);
			mes=undefined;
			return null;
		});
	}
	replaceRecord(_table,_time,_filterField,_filterValue,_data){
		let t=this.db?this:this.self;
		let __data=classBase.applyIf({}, _data);
		__data.create_user_id=0;
		__data.createdAt=_time;
		__data.update_user_id=0;
		__data.updatedAt=_time;
		let mes=new dcs_message({
			type:'sync_message'
			,ack:true
			,data:{tableName:_table,fieldName:_filterField,fieldValue:_filterValue,procType:'replace'
				,data:__data,id:null}
		});
		t.tmp_queue.push(mes);
		mes=undefined;
	}
	findAll(_table,_obj_record,_obj_where,_arr_order,fn_cb){
		let t=this.db?this:this.self;
		return t.db[_table].findAll({attributes: { exclude: ['createdAt','updatedAt'] }, where: _obj_where, order: _arr_order}).then(function(results) {
			let res=[];
			if(results.length>0){
				for (let i = 0; i < results.length; i++) {
					let result=results[i];
					let _tmp={};
					if(_obj_record!==false&&_obj_record!==null){
						for(let a in _obj_record){
							_tmp[a]=result[_obj_record[a]];
						}
					}else{
						for(let a in result.dataValues){
							_tmp[a]=result[a];
						}
					}
					res.push(_tmp);
				}
			}
			if(typeof fn_cb=='function'){
				return fn_cb(res);
			}else{
				return res;
			}
		});
	}
	updateAllRecords(_tr,_table,_time,_obj_where,_obj_update){
		let t=this.db?this:this.self;
		let __time=_time;
		let __promises=[];
		return t.db[_table].findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }).then(function(results){
			if(results){
				for (let i = 0; i < results.length; i++) {
					let result=results[i];
					__promises.push(t.updateRecord(_tr,_table,__time,{record_id:result.record_id},_obj_update));
				}
			}
			if(__promises.length>0){
				return Promise.all(__promises);
			}else{
				return null;
			}
		});
	}
	updateRecord(_tr,_table,_time,_obj_where,_obj_update){
		let t=this.db?this:this.self;
		let __time=_time;
		let _s=typeof this._arr_tables!='undefined'?this:this.self;
		return t.db[_table].findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }, {transaction: _tr}).then(function(result){
			if(result){
				let __obj_update=classBase.applyIf({}, _obj_update);
				__obj_update.record_id=result.record_id;
				__obj_update.update_user_id=0;
				__obj_update.updatedAt=__time;
				if(_table=='task_lists'){
					//if(typeof _obj_update.clients!='undefined'){
					//	delete _obj_update.clients;
					//}
					if(typeof _obj_update.taskinfo!='undefined'){
						delete _obj_update.taskinfo;
					}
				}
				let mes=new dcs_message({
					type:'sync_message'
					,ack:true
					,data:{tableName:_table,procType:'update'
						,data:__obj_update,id:null}
				});
				t.tmp_queue.push(mes);
				if(_table=='task_lists'){
					if(typeof _obj_update.clients!='undefined'){
						delete _obj_update.clients;
					}
				}
				return result.update(_obj_update,{transaction:_tr}).then(function(){
					if(_s._arr_tables.indexOf(_table)==-1){
						return t.db._sync_updatelogs
							.update({
								updatedAt:__time
								,strlastupdate:__time
							}, {
								where: { tableName:_table,strlastupdate:{[Op.lt]:__time} },
								transaction: _tr/*,
								returning: true,
								plain: true*/
							}).then(function (/*result*/) {
								return null;
							});
					}else{
						return null;
					}
				}).catch(function (err) {
					let _arr_join=['Girilen ıskarta'];
					let _f_join=false;
					for(let _i_join=0;_i_join<_arr_join.length;_i_join++){
						if(!_f_join&&err.toString().indexOf(_arr_join[_i_join])>-1){
							_f_join=true;
						}
					}
					if(!_f_join){
						Sentry.addBreadcrumb({
							category: 'updateRecord',
							message: 'updateRecord-err',
							level: 'info',
							data:JSON.parse(JSON.stringify({table:_table,where:_obj_where,data:_obj_update}))
						});
						Sentry.captureMessage('updateRecord ' + err);
						Sentry.captureMessage(JSON.stringify({table:_table,where:_obj_where,data:_obj_update}));
						Sentry.captureException(err);
					}
				});
			}else{
				console.log(_table+' kayıt bulunamadı!!!');
				console.log(_obj_where);
				return null;
			}
		});
	}
	getAllRecords(_sql,_rep,fn_cb){
		let t=this.db?this:this.self;
		return t.db.sequelize.query(_sql, {replacements:_rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
			if(typeof fn_cb=='function'){
				return fn_cb(results);
			}else{
				return results;
			}
		});
	}
}
module.exports=dcs_client;