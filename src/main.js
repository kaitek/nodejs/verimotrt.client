'use strict';
const electron = require('electron');
const powerSaveBlocker = electron.powerSaveBlocker;
const path = require('path');
const fs = require('fs');
const os = require('os');
const url = require('url');
const timezoneJS = require('timezone-js');
const tzdata = require('tzdata');
const classBase = require('./base');
const dcs_client = require('./dcs_client');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipc = electron.ipcMain;
let _tz = timezoneJS.timezone;
_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
_tz.loadZoneDataFromObject(tzdata);
let _ip = classBase.getIp();
let _arr_ip = ['192.168.1.40', '192.168.1.10', '172.16.1.143', '172.16.1.146'];
let $1 = {
	activeWindow: null,
	ip: _ip,
	isDevMachine: (_arr_ip.indexOf(_ip) > -1),
	dcs_client: false,
	windows: {}
};
if (fs.existsSync(path.join(os.homedir(), '\\AppData\\Roaming\\Electron\\DevTools Extensions'))) {
	fs.unlinkSync(path.join(os.homedir(), '\\AppData\\Roaming\\Electron\\DevTools Extensions'));
}
powerSaveBlocker.start('prevent-display-sleep');
ipc.on('errorInWindow', function (event, arg) {
	classBase.writeError(arg.msg, _ip, 'main-ipc-errorInWindow');
});

app.on('gpu-process-crashed', function () {
	console.log('GPU process crashed');
});

app.on('crashed', function () {
	console.log('app.Crashed');
});

process.on('unhandledRejection', function (err) {
	console.log('main-process-unhandledRejection');
	console.log(err);
	let _arr_handle = ['pool is draining'];
	let _f_handle = false;
	try {
		for (let _i_handle = 0; _i_handle < _arr_handle.length; _i_handle++) {
			if (!_f_handle && err.toString().indexOf(_arr_handle[_i_handle]) > -1) {
				_f_handle = true;
			}
		}
		if (!_f_handle) {
			classBase.writeError(err, _ip, 'main-process-unhandledRejection');
		}
	} catch (error) {
		/** */
	}

});

process.on('uncaughtException', function (err) {
	console.log('main-uncaughtException' + err.toString() + '\n' + err.stack);
	try {
		classBase.writeError(err, _ip, 'main-process-uncaughtException');
		if (err.errno === 'EADDRINUSE' || err.errno === 'EADDRNOTAVAIL') {
			//do nothing
		} else {
			if ($1.windows.main && $1.windows.main.win && typeof $1.windows.main.win.close == 'function') {
				$1.windows.main.win.close();
			}
		}
	} catch (error) {
		/** */
	}
});

function createWindow() {
	let electronScreen = electron.screen;
	$1.displays = electronScreen.getAllDisplays();
	let externalDisplay = null;
	for (let i in $1.displays) {
		if ($1.displays[i].bounds.x != 0 || $1.displays[i].bounds.y != 0) {
			externalDisplay = $1.displays[i];
			break;
		}
	}
	let _w = $1.displays[0].bounds.width;
	let _h = $1.displays[0].bounds.height;
	if ($1.ip == '192.168.1.40' || $1.ip == '172.16.1.146') {
		_w = 1600;
		_h = 900;
	}
	$1.dcs_client.screensize = {
		width: _w,
		height: _h
	};
	let _conf = {
		x: 0,
		y: 0,
		width: _w,
		height: _h,
		kiosk: true,
		frame: false,
		title: 'Verimot RT',
		icon: path.join(__dirname, '../favicon.ico'),
		skipTaskbar: true,
		alwaysOnTop: true,
		show: false,
		autoHideMenuBar: true,
		webPreferences: {
			nodeIntegration: true
		}
	};
	if (!$1.dcs_client.isDevMachine) {
		if (externalDisplay) {
			$1.windows.main = {
				name: 'main',
				win: null,
				loc: '/html/main.html',
				conf: _conf
			};
		} else {
			$1.windows.main = {
				name: 'main',
				win: null,
				loc: '/html/main.html',
				conf: _conf
			};
		}
	} else {
		_conf.kiosk = false;
		_conf.frame = true;
		_conf.alwaysOnTop = false;
		_conf.skipTaskbar = false;
		$1.windows.main = {
			name: 'main',
			win: null,
			loc: '/html/main.html',
			conf: _conf
		};
	}
	$1.windows.main.win = new BrowserWindow($1.windows.main.conf);
	if ($1.ip !== '192.168.1.40' && $1.ip !== '172.16.1.146') {
		$1.windows.main.win.maximize();
	}
	//$1.windows.main.win.setFullScreen(true);
	$1.windows.main.win.webContents.on('did-finish-load', () => {
		$1.windows.main.win.show();
	});
	$1.windows.main.win.webContents.on('crashed', function (err /*, killed*/ ) {
		console.log('crashed');
		classBase.writeError(err, _ip, 'main-webContents-crashed');
		if ($1.windows.main && $1.windows.main.win && typeof $1.windows.main.win.close == 'function') {
			$1.windows.main.win.close();
		}
	});
	$1.windows.main.win.webContents.on('destroyed', function () {
		console.log('destroyed');
	});
	$1.windows.main.win.webContents.on('plugin-crashed', function (err, name, version) {
		console.log('plugin-crashed');
		console.log(err);
		console.log(name);
		console.log(version);
		classBase.writeError(err, _ip, 'main-webContents-plugin-crashed');
		if ($1.windows.main && $1.windows.main.win && typeof $1.windows.main.win.close == 'function') {
			$1.windows.main.win.close();
		}
	});
	$1.windows.main.win.on('unresponsive', function () {
		console.log('unresponsive');
		if ($1.windows.main && $1.windows.main.win && typeof $1.windows.main.win.close == 'function') {
			$1.windows.main.win.close();
		}
	});

	$1.windows.main.win.on('crashed', function (err) {
		console.log('Main window crashed');
		classBase.writeError(err, _ip, 'main-window-crashed');
		if ($1.windows.main && $1.windows.main.win && typeof $1.windows.main.win.close == 'function') {
			$1.windows.main.win.close();
		}
	});

	$1.windows.main.win.on('gpu-process-crashed', function (err) {
		console.log('Main Window GPU process crashed');
		classBase.writeError(err, _ip, 'main-window-gpu-crashed');
		if ($1.windows.main && $1.windows.main.win && typeof $1.windows.main.win.close == 'function') {
			$1.windows.main.win.close();
		}
	});

	$1.activeWindow = $1.windows.main;
	if (!$1.dcs_client.isDevMachine) {
		$1.windows.main.win.setMenu(null);
	} else {
		if ($1.ip == '192.168.1.40' || $1.ip == '172.16.1.146') {
			try {
				$1.windows.main.win.webContents.openDevTools();
			} catch (error) {
				console.log(error);
			}
		}
	}
	ipc.on('changewindow', function (e, arg) {
		if ($1.activeWindow.name != 'main') {
			$1.windows[$1.activeWindow.name].win.close();
			$1.windows[$1.activeWindow.name].win = null;
		}
		if ($1.windows[arg].win === null) {
			$1.windows[arg].win = new BrowserWindow($1.windows[arg].conf);
			$1.windows[arg].win.loadURL($1.windows[arg].loc);
		}
		$1.activeWindow = $1.windows[arg];
	});

	onWindowCreated();
}

function onWindowCreated() {
	$1.dcs_client.setWindow($1.windows);
	$1.windows.main.win.loadURL(url.format({
		pathname: path.join(__dirname, $1.windows.main.loc),
		protocol: 'file'
	}));
}

$1.dcs_client = new dcs_client({
	ipc: ipc,
	ip: $1.ip,
	isDevMachine: $1.isDevMachine,
	versionShell: app.getVersion(),
	computerName: process.env.COMPUTERNAME || process.env.USERDOMAIN
});
$1.dcs_client.addListener('dcs_inited', function () {
	createWindow();
});

$1.dcs_client.addListener('port_in_use', function () {
	app.quit();
});

app.on('window-all-closed', function () {
	console.log('window-all-closed');
	if (process.platform !== 'darwin') {
		app.relaunch();
		app.quit();
	}
});

ipc.on('close-verimotrt', ( /*event, arg*/ ) => {
	app.quit();
});