const classBase=require('./base');

class io_message extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='io_message';
		t.version='0.1';
		t.tryCount=0;
		t.data=false;
		t.type='';
		t.time='';
		t.rawData=false;
		t.readData='';
		t.portnumber=false;
		t.portval=false;
		Object.assign(this, this.config);
	}
}
module.exports=io_message;