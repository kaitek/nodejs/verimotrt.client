const classBase=require('./base');

class io_MessageReceiveQueue extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='io_MessageReceiveQueue';
		t.version='0.1';
		t.timeout=50;
		t.queue=[];
		t._ready=true;
		t.parent=false;
		Object.assign(this, this.config);
	}
	exec(mes){
		this.queue.push(mes);
		this.parent.emit('receivequeuechange',this.queue.length);
		this.process();
	}
	process(){
		let t=this;
		if (t.queue.length === 0) return;
		if (!t._ready) return;
		//t.mjd('RQP ' + ' c:' +t.queue.length+ '\n');
		t.receive(t.queue.shift());
		t.parent.emit('receivequeuechange',t.queue.length);
		t._ready = false;
		process.nextTick(function(){
			setTimeout(function() {
				t._ready = true;
				t.process();
			}, t.timeout);
		});
	}
	receive(mes,callback){
		this.parent.rmc+=1;
		if(typeof mes=='undefined') return;
		
		mes.rawData=mes.data;
		mes.readData='';
		if(this.parent.sendqueue.waiting){
			let senmes=this.parent.sendqueue.msg;
			if(mes.type=='output-clear-ack'){
				if(senmes.type=='output-clear'){
					if(this.parent.io_type=='IO Kart'){
						if(mes.data[6] == senmes.data[6] && senmes.data[5] == 1 && mes.data[7] == 0){
							this.parent.sendqueue.waiting=false;
							let p=this.parent.getOutputPort(mes.data[6]);
							p.setCurVal(0);
							mes.readData+='O'+(mes.data[6])+':'+p.curval+' ';
						}
					}
					if(this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232-MB'){
						this.parent.sendqueue.waiting=false;
						let p=this.parent.getOutputPort(senmes.portnumber);
						p.setCurVal(0);
						mes.readData+='O'+(senmes.portnumber)+':'+p.curval+' ';
					}
				}
			}
			if(mes.type=='output-set-ack'){
				if(senmes.type=='output-set'){
					if(this.parent.io_type=='IO Kart'){
						if(mes.data[6] == senmes.data[6] && senmes.data[5] == 2 && mes.data[7] == 1){
							this.parent.sendqueue.waiting=false;
							let p=this.parent.getOutputPort(mes.data[6]);
							p.setCurVal(1);
							mes.readData+='O'+(mes.data[6])+':'+p.curval+' ';
						}
					}
					if(this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232-MB'){
						this.parent.sendqueue.waiting=false;
						let p=this.parent.getOutputPort(senmes.portnumber);
						p.setCurVal(1);
						mes.readData+='O'+(senmes.portnumber)+':'+p.curval+' ';
					}
				}
			}
			if(mes.type=='output-toggle-ack'){
				if(senmes.type=='output-toggle'){
					if(this.parent.io_type=='IO Kart'){
						if(mes.data[6] == senmes.data[6] && senmes.data[5] == 3){
							this.parent.sendqueue.waiting=false;
							let p=this.parent.getOutputPort(mes.data[6]);
							p.toggleCurVal();
							mes.readData+='O'+(mes.data[6])+':'+p.curval+' ';
						}
					}
					if(this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232-MB'){
						this.parent.sendqueue.waiting=false;
						let p=this.parent.getOutputPort(senmes.portnumber);
						p.toggleCurVal();
						mes.readData+='O'+(senmes.portnumber)+':'+p.curval+' ';
					}
				}
			}
			if(mes.type=='output-clear-all-ack'){
				if(senmes.type=='output-clear-all'){
					if(senmes.data[5] == 4 && mes.data[6] == 0){
						this.parent.sendqueue.waiting=false;
						let p;
						for(let i=0;i<this.parent.outputs;i++){
							p=this.parent.getOutputPort(i+1);
							p.setCurVal(0);
							mes.readData+='O'+(i+1)+':'+p.curval+' ';
						}
					}
				}
			}
			if(mes.type=='output-set-all-ack'){
				if(senmes.type=='output-set-all'){
					if(senmes.data[5] == 5 && mes.data[6] == 1){
						this.parent.sendqueue.waiting=false;
						let p;
						for(let i=0;i<this.parent.outputs;i++){
							p=this.parent.getOutputPort(i+1);
							p.setCurVal(1);
							mes.readData+='O'+(i+1)+':'+p.curval+' ';
						}
					}
				}
			}
			if(mes.type=='output-toggle-all-ack'){
				if(senmes.type=='output-toggle-all'){
					if(senmes.data[5] == 6){
						this.parent.sendqueue.waiting=false;
						let p;
						for(let i=0;i<this.parent.outputs;i++){
							p=this.parent.getOutputPort(i+1);
							p.toggleCurVal();
							mes.readData+='O'+(i+1)+':'+p.curval+' ';
						}
					}
				}
			}
			if(mes.type=='output-read'){
				if(senmes.type=='output-read-all'){
					this.parent.sendqueue.waiting=false;
					if(this.parent.io_type=='IO Kart'){
						this.parent.setOutputPorts(mes.data[6],true);
					}
					if(this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232-MB'){
						this.parent.setOutputPorts(mes.data[8]+mes.data[9]+mes.data[6]+mes.data[7],true);
					}
					let p;
					for(let i=0;i<this.parent.outputs;i++){
						p=this.parent.getOutputPort(i+1);
						mes.readData+='O'+(i+1)+':'+p.curval+' ';
					}
				}
			}
			if(mes.type=='input-read'){
				if(senmes.type=='input-read-all'){
					this.parent.sendqueue.waiting=false;
					if(this.parent.io_type=='IO Kart'){
						this.parent.setInputPorts(mes.data[6],true);
					}
					if(this.parent.io_type=='PLC-USB'){
						this.parent.setInputPorts(mes.data[8]+mes.data[9]+mes.data[6]+mes.data[7],true);
					}
					if(this.parent.io_type=='PLC-RS232-MB'){
						this.parent.setInputPorts(mes.data[8]+mes.data[9]+mes.data[6]+mes.data[7],true);
					}
					let p;
					for(let i=0;i<this.parent.inputs;i++){
						p=this.parent.getInputPort(i+1);
						mes.readData+='I'+(i+1)+':'+p.curval+' ';
					}
				}
			}
			if(mes.type=='init-0-ack'){
				if(senmes.type=='init-0'){
					this.parent.sendqueue.waiting=false;
				}
			}
			if(this.parent.sendqueue&&this.parent.sendqueue.queue.length==0){
				if(this.parent){
					this.parent.emit('ioqueue_empty');
				}
			}
		}else{
			if(mes.type=='input-read'){
				if(this.parent.io_type=='IO Kart'){
					this.parent.setInputPorts(mes.data[6],false);
				}
				if(this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232-MB'){
					this.parent.setInputPorts(mes.data[8]+mes.data[9]+mes.data[6]+mes.data[7],false);
				}
				let p;
				for(let i=0;i<this.parent.inputs;i++){
					p=this.parent.getInputPort(i+1);
					mes.readData+='I'+(i+1)+':'+p.curval+' ';
				}
			}
			if(mes.type=='output-read'){
				if(this.parent.io_type=='IO Kart'){
					this.parent.setOutputPorts(mes.data[6],false);
				}
				if(this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232-MB'){
					this.parent.setOutputPorts(mes.data[8]+mes.data[9]+mes.data[6]+mes.data[7],false);
				}
				let p;
				for(let i=0;i<this.parent.outputs;i++){
					p=this.parent.getOutputPort(i+1);
					mes.readData+='O'+(i+1)+':'+p.curval+' ';
				}
			}
		}
		this.parent.emit('iodatareceive',mes);
		if (callback) callback();
	}
}
module.exports=io_MessageReceiveQueue;