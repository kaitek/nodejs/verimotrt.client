//admin start
//https://www.maketecheasier.com/standard-users-run-program-admin-rights/
const classBase=require('./base');
let dcs_client=require('./dcs_client');
let dcs_message=require('./dcs_message');
const Promise = require('promise');
const { v4: uuidv4 } = require('uuid');
const timezoneJS = require('timezone-js');
const tzdata = require('tzdata');
let _tz = timezoneJS.timezone;
_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
_tz.loadZoneDataFromObject(tzdata);
const cp = require('child_process');
const fs=require('fs');
const path = require('path');
const arraySort = require('array-sort');
const Sentry = require('@sentry/node');

var Sequelize = require('sequelize');
const { rootCertificates } = require('tls');
const Op = Sequelize.Op;

Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});
class dcs_client_workflow extends dcs_client {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='dcs_client_workflow';
		t.version='0.1.0';
		t.iodevice_manager=false;
		t.tmp_queue=false;
		t.processVariables=false;
		t.ports=false;
		t.cpd_time=false;
		t.cpd_ptime=false;
		t.cpd_pctime=false;
		t.cld_mlosttime=false;
		t.inited_wf=0;
		t.device_inited=false;
		t.bool_init_io=false;
		t.bool_init=false;
		t.bool_control_device_delivery=false;
		t.activity_control_questions=false;
		t.all_documents=false;
		t.sop_documents=false;
		t.client_control_document=false;
		Object.assign(this, this.config);
		t.scrap_timer_add=0;
		t.wait_for_io_first_connect=true;
		t.wait_for_first_controljobrotation=true;
		t.timer_delivery_control=false;
		t.timer_manual_delivery_control=false;
		t.bool_operationAuthorization_supervision=false;
		t.db=t.self.db;
		t.ipc=t.self.ipc;
		t.ip=t.self.ip;
		t.hammaddeeksiklikleri=[];
		t.obj_jobrotation=t.self.obj_jobrotation;
		t.tickTimer=t.self.tickTimer;
		t.objConfig=t.self.objConfig;
		t.objParam=t.self.objParam;
		t.securitypins=t.self.securitypins;
		t.arr_cpd_exclude=['id','time','leafmask','leafmaskcurrent','production','productioncurrent','gap','createdAt','updatedAt','energy','taskfinishtype','taskfinishdescription'];
		t.processVariables={};
		t.processVariables.operators=[];
		t.processVariables.productions=[];
		t.processVariables.status_lost='-';
		t.processVariables.status_pre_lost='-';
		t.processVariables.status_employee=0;
		t.processVariables.status_tpp=0;
		t.processVariables.status_work='-';
		t.processVariables.status_workflowprocess='-';
		t.processVariables.emp_identify='code';
		t.processVariables.lastprodstarttime=null;
		t.processVariables.lastprodtime=null;
		t.faultcodes=['ARIZA KALIP-APARAT','ARIZA MAKİNE ELEKTRİK','ARIZA MAKİNE MEKANİK','ARIZA ÜRETİM'];
		t.ports={
			inputs:[]
			,outputs:[]
		};
		t.last_state={client:'',time:'',state:'',losttype:'',tasks:[]};
		t.holo_status=false;
		t.lost_recursive=false;
		t.show_scrap_before_finish=false;
		t.show_delivery=false;
		t.tmp_queue=[];
		t.arr_inputready=[];
		t.status='';
		t.timer_missing_material=false;
		t.is_task_selected=false;
		t.str_missing_material='';
		t.obj_missing_material=false;
		t.case_movement_target_time=false;
		t.timer_injection=false;
		let find_io=function(){
			if(t.objConfig.mainapp_IO_TYPE.value!=''&&t.objConfig.workflow.value!='El İşçiliği'&&t.objConfig.workflow.value!='Kataforez'){
				return t.db.IOPorts.findAll({}).then(function(results) {
					if(results.length==0){
						for (let i = 1; i <= t.objConfig.mainapp_IO_INPUTS.value; i++) {
							t.db.IOPorts.create({ number:i, type: 'I', value:0 });
						}
						for (let j = 1; j <= t.objConfig.mainapp_IO_OUTPUTS.value; j++) {
							t.db.IOPorts.create({ number:j, type: 'O', value:0 });
						}
					}
					return null;
				}).then(function(){
					return Promise.all([t.setIOEvents()]);
				}).then(function(){
					let _promises=[];
					if(t.self.json_config.tcp_io){
						_promises.push(t.p_io_manager_tcp());
					}
					_promises.push(t._init_p_io_manager());
					return Promise.all(_promises);
				});
			}
			return null;
		};
		t.arr_inputs_for_input_signal_after_lock=[];
		for (let i = 1; i <= t.objConfig.mainapp_IO_INPUTS.value; i++) {
			t.arr_inputs_for_input_signal_after_lock.push({ number:i, value:false });
		}
		t.on('error',function(err){
			console.log('workflow-error:'+err.message);
		});
		let _l=function(e,arg){
			if(!arg&&(t.netApp||t.self.netApp)){
				arg=e;
			}
			t.eventListener(arg);
		};
		t.on('js_cn_workflow',_l);
		t.on('beforedestroy',function(){
			t.mon('js_cn_workflow',_l);
		});
		t.on('send_plc_inputfilter',function(_val){
			if(t.iodevice_manager){
				t.iodevice_manager.send({
					event: 'set_inputfilter',
					pid: t.iodevice_manager.pid,
					data: parseInt(_val)
				});
			}
		});
		//console.log('__dirname:'+__dirname);
		Sentry.configureScope(scope => {
			scope.setTag('ip',t.ip);
			scope.setTag('client_name',t.objConfig.client_name.value);
			scope.setTag('version',t.self.versionApp);
			scope.setTag('build',t.self.buildNumber);
			//scope.setUser({ ip: t.ip });
		});
		//try {
		//	doSomething(a[0]);
		//} catch(e) {
		//	Sentry.captureException(e);
		//}
		return Promise.all([find_io()]).then(function () {
			t.json_status={
				fault:{faulttype:false,faultdescription:false,faultgroupcode:false,faultdevice:false,fault_unique_id:false,pos:'init',time:false,operators:[]}
				,operationAuthorization:{data:[],nextControl:null}
				,prelosttype:false
			};
			try {
				fs.accessSync(__dirname+'/../status.json');
				let tmp_json_data = JSON.parse(fs.readFileSync(__dirname+'/../status.json', 'utf-8'));
				let _flag_write=false;
				if(tmp_json_data.fault){
					if(!tmp_json_data.fault.operators){
						tmp_json_data.fault.operators=[];
						_flag_write=true;
					}
					t.json_status.fault = tmp_json_data.fault;
					if(tmp_json_data.operationAuthorization){
						t.json_status.operationAuthorization=tmp_json_data.operationAuthorization;
					}else{
						t.json_status.operationAuthorization={data:[],nextControl:null};
						_flag_write=true;
					}
				}else{
					if(!tmp_json_data.operators){
						tmp_json_data.operators=[];
					}
					t.json_status.fault = tmp_json_data;
					t.json_status.operationAuthorization={data:[],nextControl:null};
					_flag_write=true;
				}
				if(!tmp_json_data.prelosttype){
					tmp_json_data.prelosttype=false;
					t.json_status.prelosttype=false;
					_flag_write=true;
				//}else{
				//	tmp_json_data.prelosttype=false;
				//	t.json_status.prelosttype=false;
				//	_flag_write=true;
				}
				if(_flag_write){
					//üzerine yaz
					try {
						fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
					} catch (error) {
						Sentry.captureException(error);
					}
				}
				tmp_json_data=null;			
			} catch (error) {
				Sentry.captureException(error);
				fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
			}
			//return Promise.all([t.getDbValues('init')]).then(function(){
			if(t.self.isDevMachine&&(t.ip=='172.16.1.143'||t.ip=='172.16.1.146')){
				setTimeout(() => {
					t.winmessend('cn_js_dcsclient',{event:'ioconnected'});
					t.winmessend('cn_js_workflow',{event:'ioconnected'});
				}, 1000);
			}
			return null;
			//});
		});
	}
	_init_p_io_manager(){
		let t=this;
		return t.p_io_manager();
	}
	p_io_manager(){
		let t=this;
		if(!t.iodevice_manager){
			let onMessage = function(obj) {
				try {
					switch (obj.event) {
					case 'io_manager_inited':{
						t.iodevice_manager.inputPorts=obj.inputs;
						t.iodevice_manager.outputPorts=obj.outputs;
						t.mjd('Client IO Device Manager PID:'+obj.pid);
						break;
					}
					case 'ioconnecting':{
						t.iodevice_manager.connected=false;
						t.el_ioconnecting();
						break;
					}
					case 'ioconnected':{
						t.iodevice_manager.connected=true;
						t.el_ioconnected();
						let mes=new dcs_message({
							type:'device_io_info'
							,ack:true
							,data:{
								type:'ioconnected'
							}
						});
						return Promise.all([t.db._sync_messages.create(mes.getdata())]);
						//break;
					}
					case 'ioerror':{
						let mes=new dcs_message({
							type:'device_io_info'
							,ack:true
							,data:{
								type:'ioerror'
							}
						});
						return Promise.all([t.db._sync_messages.create(mes.getdata())]);
						//break;
					}
					case 'iodisconnect':{
						let mes=new dcs_message({
							type:'device_io_info'
							,ack:true
							,data:{
								type:'iodisconnect'
							}
						});
						return Promise.all([t.db._sync_messages.create(mes.getdata())]);
						//break;
					}
					case 'ioclose':{
						let mes=new dcs_message({
							type:'device_io_info'
							,ack:true
							,data:{
								type:'ioclose'
							}
						});
						return Promise.all([t.db._sync_messages.create(mes.getdata())]);
						//break;
					}
					case 'iodatareceive':{
						t.el_iodatareceive(obj.mes);
						break;
					}
					case 'iodatasend':{
						t.el_iodatasend(obj.mes);
						break;
					}
					case 'ioportchange':{
						t.el_ioportchange(obj.port,obj.fevent);
						break;
					}
					case 'sendqueuechange':{
						t.el_sendqueuechange(obj.length);
						break;
					}
					case 'receivequeuechange':{
						t.el_receivequeuechange(obj.length);
						break;
					}
					case 'setoutputports':{
						t.el_setoutputports();
						break;
					}
					case 'ioqueue_empty':{
						t.el_ioqueue_empty();
						break;
					}
					default:{
						break;
					}
					}
				} catch (error) {
					console.log('Error:p_io_manager-onMessage');
					console.log(error);
				}
			};
			let onError_im = function(e) {
				console.log('Error_p_io_manager');
				console.log(e.stack);
			};
			let onClose_im = function(e){
				t.mjd('kill-onClose_iodevice_manager');
				if(e>0){
					Sentry.captureMessage('[onClose_im-p_io_manager] ' + e);
				}
			};
			let onDisconnect_im = function() {
				console.log('kill-onDisconnect-iodevice_manager');
				let b_run=t.iodevice_manager&&t.iodevice_manager.pid?this.pid===t.iodevice_manager.pid:false;
				if(b_run){
					try {
						this.kill();
					} catch (error) {
						//classBase.writeError(error,t.ip,'');
					}
				}
				t.iodevice_manager=false;
				t.removeAllListeners('setoutput');
				t.removeAllListeners('light_red');
				t.removeAllListeners('light_green');
				t.removeAllListeners('light_yellow');
				t.removeAllListeners('light_blue');
				t.removeAllListeners('light_white');
				t.removeAllListeners('buzzer');
				t.ipc.removeAllListeners('dcs_io_set');
				t.ipc.removeAllListeners('output-set-all');
				t.ipc.removeAllListeners('output-clear-all');
				t.ipc.removeAllListeners('output-toggle-all');
				setTimeout(() => {
					t.p_io_manager();
				}, 1000);
			};
			
			if(t.self.isDevMachine){
				t.iodevice_manager=cp.fork(path.join(__dirname, '/io_manager.js'), [], {execArgv: ['--inspect=7440']});
			}else{
				t.iodevice_manager=cp.fork(path.join(__dirname, '/io_manager.js'), []);
			}
			t.iodevice_manager.on('message',onMessage);
			t.iodevice_manager.on('error',onError_im);
			t.iodevice_manager.on('close',onClose_im);
			t.iodevice_manager.on('disconnect',onDisconnect_im);
			//
			// output port event listener tanımları başlangıcı
			//
			t.addListener('setoutput', function (_number, _value) {
				if(t.iodevice_manager&&t.iodevice_manager.connected){
					t.iodevice_manager.send({
						event: 'output_trigger',
						pid: t.iodevice_manager.pid,
						value: _value,
						number: _number
					});
				}
			});
			t.addListener('light_red', function(_value){
				let t=this;
				t.setoutputport('light_red', _value);
			});
			t.addListener('light_green', function(_value){
				let t=this;
				t.setoutputport('light_green', _value);
			});
			t.addListener('light_yellow', function(_value){
				let t=this;
				t.setoutputport('light_yellow', _value);
			});
			t.addListener('light_blue', function(_value){
				let t=this;
				t.setoutputport('light_blue', _value);
			});
			t.addListener('light_white', function(_value){
				let t=this;
				t.setoutputport('light_white', _value);
			});
			t.addListener('buzzer', function(_value){
				let t=this;
				t.setoutputport('buzzer', _value);
			});
			//
			// output port event listener tanımları bitişi
			//
			t.iodevice_manager.send({
				event: 'init_io_manager',
				pid: t.iodevice_manager.pid,
				data: {
					io_type:t.objConfig.mainapp_IO_TYPE.value
					,ip:t.objConfig.mainapp_IO_IP.value
					,local:t.objConfig.mainapp_IO_PORT.value
					,portname:t.objConfig.mainapp_IO_COM_PORT.value
					,rmc:0
					,smc:0
					,inputs:t.objConfig.mainapp_IO_INPUTS.value
					,outputs:t.objConfig.mainapp_IO_OUTPUTS.value
					,hostip:t.ip
					,inputFilter:parseInt(t.objParam.app_inputFilter.value)
					,client_name:t.objConfig.client_name.value
					,versionApp:t.self.versionApp
					,buildNumber:t.self.buildNumber
				}
			});
			t.ipc.on('dcs_io_set',function(e, arg){
				if(!arg&&(t.netApp||t.self.netApp)){
					arg=e;
				}
				if(arg.type=='O'){
					t.iodevice_manager.send({
						event: 'output_trigger',
						pid: t.iodevice_manager.pid,
						value: (arg.value===1?0:1),
						number: arg.number
					});
				}
				if(arg.type=='I'&&(t.ip==='192.168.1.40'||t.ip==='172.16.1.146')){
					t.iodevice_manager.send({
						event: 'input_trigger',
						pid: t.iodevice_manager.pid,
						value: (arg.value===1?0:1),
						number: arg.number
					});
				}		
			});
			t.ipc.on('output-set-all',function(){
				t.iodevice_manager.send({
					event: 'setAll',
					pid: t.iodevice_manager.pid
				});
			});
			t.ipc.on('output-clear-all',function(){
				t.iodevice_manager.send({
					event: 'clearAll',
					pid: t.iodevice_manager.pid
				});
			});
			t.ipc.on('output-toggle-all',function(){
				t.iodevice_manager.send({
					event: 'toggleAll',
					pid: t.iodevice_manager.pid
				});
			});
		}else{
			t.iodevice_manager.send({
				event: 'readall',
				pid: t.iodevice_manager.pid,
				pos: '4'
			});
		}
	}
	p_io_manager_tcp(){
		let t=this;
		let onMessage = function(obj) {
			try {
				switch (obj.event) {
				case 'io_manager_tcp_inited':{
					t.mjd('IO Device Manager TCP PID:'+obj.pid);
					break;
				}
				case 'ioconnecting':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'ioconnected':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'ioerror':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'iodisconnect':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'ioclose':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'iodatareceive':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'iodatasend':{
					t.mjd(obj.event+'_tcp');
					//console.log(obj.mes);
					break;
				}
				case 'ioportchange':{
					console.log(obj.event+'_tcp');
					t.mjd(obj);
					break;
				}
				case 'sendqueuechange':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'receivequeuechange':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'setoutputports':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				case 'ioqueue_empty':{
					t.mjd(obj.event+'_tcp');
					break;
				}
				default:{
					break;
				}
				}
			} catch (error) {
				console.log('Error:p_io_manager_tcp-onMessage');
				console.log(error);
			}
		};
		let onError_im = function(e) {
			console.log('Error_p_io_manager_tcp');
			console.log(e.stack);
		};
		let onClose_im = function(e){
			t.mjd('kill-onClose_iodevice_manager_tcp');
			if(e>0){
				Sentry.captureMessage('[onClose_im-p_io_manager_tcp] ' + e);
			}
		};
		let onDisconnect_im = function() {
			console.log('kill-onDisconnect-iodevice_manager_tcp');
			try {
				this.kill();
			} catch (error) {
				//classBase.writeError(error,t.ip,'');
			}
			setTimeout(() => {
				t.p_io_manager_tcp();
			}, 1000);
		};
		
		//if(t.self.isDevMachine){
		//	t.iodevice_manager_tcp=cp.fork(path.join(__dirname, '/io_manager_tcp.js'), [], {execArgv: ['--inspect=7440']});
		//}else{
		t.iodevice_manager_tcp=cp.fork(path.join(__dirname, '/io_manager_tcp.js'), []);
		//}
		t.iodevice_manager_tcp.on('message',onMessage);
		t.iodevice_manager_tcp.on('error',onError_im);
		t.iodevice_manager_tcp.on('close',onClose_im);
		t.iodevice_manager_tcp.on('disconnect',onDisconnect_im);
		t.iodevice_manager_tcp.send({
			event: 'init_io_manager_tcp',
			pid: t.iodevice_manager_tcp.pid,
			data: {
				io_type:'IO Kart'
				,ip:t.self.json_config.tcp_io.ip
				,local:t.self.json_config.tcp_io.port
				,portname:''
				,rmc:0
				,smc:0
				,inputs:t.self.json_config.tcp_io.inputs
				,outputs:t.self.json_config.tcp_io.outputs
				,hostip:t.ip
				,client_name:t.objConfig.client_name.value
				,versionApp:t.self.versionApp
				,buildNumber:t.self.buildNumber
			}
		});
	}
	getInput(_number, _value){
		let t=this;
		//let v_client=t.objConfig.client_name.value;
		let time=t.tickTimer.obj_time.strDateTime;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		//gelen input verisini iş akışına göre değerlendirilecek
		//	pres,plastik enjeksiyon veya sabit punta ise ve değer 1 ise üretim sinyali
		//	robot ise, geldiği portta client detail tablosunda tanımlı fikstür için üretim sinyali
		//anlamına gelecek.
		if(v_workflow!=='El İşçiliği'){
			//if(v_workflow=='Üretim Aparatsız'){
			//@@todo:port değeri tanım ekranına eklendiğinde burada düşen-yükselen kenar tipine göre tetik yapılacak
			console.log('getInput number:',_number,' value:',_value);
			if(_value==1){
				for(let i=0;i<t.ports.inputs.length;i++){
					let item=t.ports.inputs[i];
					if(item.portnumber==_number){
						if(item.ioevent==='input'){
							if(v_workflow!=='CNC'){
								if(t.objConfig.dcs_server_IP.value==='10.0.0.101'
									&&(v_client==='ENJ01'||v_client==='ENJ02')){
										let _item_=item;
										t.timer_injection=setTimeout(() => {
											t.timer_injection=false;
											t.processInputSignal(_item_);
										}, 15000);
								}else{
									t.processInputSignal(item);
								}
							}else{
								t.processVariables.lastprodstarttime=time;
								t.processVariables.lastprodtime=time;
								t.processVariables.inputStatus=true;
								t.arr_inputready.push(1);
								t.eventListener({event:'req_watch_info'});
							}
						}
						if(item.ioevent==='input_ready'&&t.processVariables.inputready>0){
							//@@todo:client update
							t.processVariables.lastprodstarttime=time;
							t.arr_inputready.push(t.processVariables.inputready);
							console.log('arr_inputready.push:'+t.arr_inputready.length);
						}
						if((item.ioevent==='input_ready_clear'||item.ioevent==='clear_ready')&&t.processVariables.inputready>0){
							if(t.arr_inputready.length>0){
								t.arr_inputready.shift();
								t.processVariables.lastprodstarttime=time;
								console.log('arr_inputready.clear:'+t.arr_inputready.length);
							}
						}
						if(item.ioevent==='input_error'){
							t.eventListener({event:'device-lost-start',description:'Tezgahtan alınan hata bildirimi nedeniyle yaşanan kayıp.',selectedRow:{code:'KAYNAK ÜNİTESİ ARIZASI'},op_transfer:true,status:t.generateClientStatus()});
							setTimeout(function() {
								t.eventListener({event:'device_init'});
								//setTimeout(function(){
								//	t.winmessend('cn_js_workflow',{event:'show_message',result:'ok',title:'Lütfen Dikkat',text:'Dişi çeliğe (kaplamalı olan) hava tutun, kuru bezle silin, talaş kalmadığından emin olun.',type:'info',showConfirmButton:true,event_cb:'device-lost-finish'});
								//}, 500);
							}, 500);
						}
						if(item.ioevent==='input_nok'){
							//kameradan parça nok sinyali
							let idx=t.getIndex(t.processVariables.productions,'mould',item.code);
							if(idx!==-1){
								t.processVariables.lastprodstarttime=null;
								t.setAllOutputs();
								t.winmessend('cn_js_workflow',{event:'input_nok',result:'ok',message:'',tasklist:t.processVariables.productions[idx].tasklist});
							}
						}
						break;
					}
				}
			}else{
				for(let i=0;i<t.ports.inputs.length;i++){
					let item=t.ports.inputs[i];
					if(item.portnumber==_number){
						if(item.ioevent==='input'){
							if(v_workflow==='CNC'){
								t.processVariables.lastprodstarttime=time;
								t.processVariables.lastprodtime=time;
								t.processVariables.inputStatus=false;
								if(t.arr_inputready.length>0){
									t.arr_inputready.shift();
								}
								t.eventListener({event:'req_watch_info'});
								//C0001 ise üretim bitiminde görevde kimse yok yap
								let flag_emp=false;
								for(let i=0;i<t.processVariables.operators.length;i++){
									let op=t.processVariables.operators[i];
									if(flag_emp===false&&op.code==='C0001'){
										flag_emp=op;
									}
								}
								if(flag_emp!==false){
									return Promise.all([t.employeeLeave(null,{code:flag_emp.code},time,false)]);
								}
							}else{
								if(t.objConfig.dcs_server_IP.value==='10.0.0.101'
									&&(v_client==='ENJ01'||v_client==='ENJ02')){
									if(t.timer_injection!=false){
										clearTimeout(t.timer_injection);
										t.timer_injection=false;
									}
								}
							}
						}
						if(item.ioevent==='input_error'){
							t.winmessend('cn_js_workflow',{event:'input_error_clear',result:'ok',message:''});
						}
					}
				}
				
			}
			//}
		}
		//deneme için yazıldı
		//sc.emit('setoutput', sc, _number, _value);
		//sc.emit('light_red', sc, _value);
		//sc.emit('light_green', sc, _value);
		//sc.emit('light_yellow', sc, _value);
		//sc.emit('buzzer', sc, _value);
	}
	getDeviceInitValues(/*pos*/){
		let t=this;
		let _time=t.tickTimer.obj_time.strDateTime;
		let _jrday=t.getJobRotationDay(t.self.jobrotations,_time,false,_time);
		let _jrcode=t.getJobRotationDay(t.self.jobrotations,_time,true,_time);
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		
		t._lbl_cls='panel-danger';
		t._lbl_status='';
		t._state_start=_time;
		let _promises=[];
		return Promise.all([t.getClientStatus()]).then(function(_res){
			t._state_start=t.getLastElementOfArray(_res);
			if(t.processVariables.status_lost=='-'&&t.processVariables.status_work!='-'){
				t._lbl_cls=(t.processVariables.status_work=='OP-OUT-SYSTEM'?'panel-default':'panel-success');
				t._lbl_status= (t.processVariables.status_work=='OP-OUT-SYSTEM'?'Sistem Dışı ':'')+'Üretim Başlangıç : ' + t._state_start;
			}else{
				let _kalipsok='';
				if(	t.objParam.process_wfSetup.value===true
				&&t.processVariables.status_lost==='USTA BEKLEME'
				&&t.processVariables.status_workflowprocess==='SETUP-WORK-FINISH'
				){
					_kalipsok=' KALIP SÖKME ';
				}
				t._lbl_status= t.processVariables.status_lost+_kalipsok+'. Başlangıç : ' + t._state_start;
			}
			if( (t.objConfig.dcs_server_IP.value==='10.0.0.101'&&t.processVariables.status_lost==='DENEMELER')
				||(t.objConfig.dcs_server_IP.value==='192.168.1.40'&&t.processVariables.status_lost==='DENEME ÜRETİM')
			){
				const _obj_where_cld={
					type:'l_c'
					,client:v_client
					,finish:{[Op.eq]:null}
				};
				return t.db.client_lost_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] }
					,where:_obj_where_cld}).then(function(res_cld){
					if(res_cld&&res_cld.length>0){
						for (let i = 0; i < res_cld.length; i++) {
							let _cld_item=res_cld[i];
							if(_cld_item.mould!==null&&_cld_item.mould!==''){
								t._lbl_status= t.processVariables.status_lost+'('+_cld_item.mould+')'+'. Başlangıç : ' + t._state_start;
							}
						}
					}
					return null;
				});
			}
			return null;
		}).then(function(){
			const _obj_where_cpd={
				type:{[Op.in]: ['c_p', 'c_p_unconfirm']}
				,client:v_client
				,finish:{[Op.eq]:null}
			};
			return t.db.client_production_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] }
				,where:_obj_where_cpd}).then(function(res_cpd){
				let _obj_tmp={
					moulds:[]
					,mould_groups:[]
					,task_lists:[]
				};
				if(res_cpd&&res_cpd.length>0){
					for (let i = 0; i < res_cpd.length; i++) {
						let _cpd_item=res_cpd[i];
						if(_cpd_item.mould!==null){
							if(_obj_tmp.moulds.indexOf(_cpd_item.mould)===-1){
								_obj_tmp.moulds.push(_cpd_item.mould);
							}
						}
						if(_cpd_item.mouldgroup!==null){
							if(_obj_tmp.mould_groups.indexOf(_cpd_item.mouldgroup)===-1){
								_obj_tmp.mould_groups.push(_cpd_item.mouldgroup);
							}
						}
						if(_cpd_item.tasklist!==null){
							if(_obj_tmp.task_lists.indexOf(_cpd_item.tasklist)===-1){
								_obj_tmp.task_lists.push(_cpd_item.tasklist);
							}
						}
					}
					return _obj_tmp;
				}else{
					const _obj_where_cld={
						type:'l_c_t'
						,client:v_client
						,finish:{[Op.eq]:null}
					};
					return t.db.client_lost_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] }
						,where:_obj_where_cld}).then(function(res_cld){
						if(res_cld&&res_cld.length>0){
							for (let i = 0; i < res_cld.length; i++) {
								let _cld_item=res_cld[i];
								if(_cld_item.mould!==null){
									if(_obj_tmp.moulds.indexOf(_cld_item.mould)===-1){
										_obj_tmp.moulds.push(_cld_item.mould);
									}
								}
								if(_cld_item.mouldgroup!==null){
									if(_obj_tmp.mould_groups.indexOf(_cld_item.mouldgroup)===-1){
										_obj_tmp.mould_groups.push(_cld_item.mouldgroup);
									}
								}
								if(_cld_item.tasklist!==null){
									if(_obj_tmp.task_lists.indexOf(_cld_item.tasklist)===-1){
										_obj_tmp.task_lists.push(_cld_item.tasklist);
									}
								}
							}
						}
						return _obj_tmp;
					});
				}
			}).then(function(_ret_arr){
				if(_ret_arr.task_lists.length>0){
					if(v_workflow==='Üretim Aparatsız'||v_workflow==='CNC'){
						let sql='select tl.*,\'01\' leafmask,\'01\' leafmaskcurrent ' + 
							'		,tl.productcount-tl.productdonecount remaining,tl.code tasklist,tl.taskfromerp,pt_d.delivery,pt_p.preptime ' +
							' 	,round( COALESCE(tl.tpp,(pt_d.tpp*COALESCE(pt_d.productionmultiplier,100))),2)t_p_p,tl.client clients '+
							'		,COALESCE(pt_d.notificationtime,2) notificationtime '+
							' from task_lists tl  ' +
							' left join product_trees pt_d on pt_d.materialtype=\'O\' and pt_d.finish is null and pt_d.name=tl.opname ' + 
							// (( (t.ip&&(t.ip.indexOf('10.10')>-1) ))?' and pt_d.client=:client2 ':'') +
							' left join product_trees pt_p on pt_p.materialtype=\'K\' and pt_p.finish is null and pt_p.name=tl.opname ' +
							' where tl.finish is null and tl.client ilike :client and tl.code in (:code) ' +
							' order by tl.opname';
						let rep={
							client: '%'+v_client+'%'
							,code: _ret_arr.task_lists
						};
						//if(t.ip&&(t.ip.indexOf('10.10')>-1) ){
						//	rep.client2=v_client;
						//}
						return t.db.sequelize.query(sql, {replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
							for(let i=0;i<results.length;i++){
								let row=results[i];
								let _idx=t.getIndex(t.processVariables.productions,'code',row.code);
								if(_idx==-1){
									if(row.leafmaskcurrent.length>2&&typeof row.step==='undefined'){
										row.step=0;
									}
									t.processVariables.productions.push(row);
								}else{
									Object.assign(t.processVariables.productions[_idx],row);
								}
							}
							return null;
						});
					}
					if(v_workflow==='Değişken Üretim Aparatlı'||v_workflow==='Sabit Üretim Aparatlı'||v_workflow==='Kataforez'){
						let rep={
							materialtype:'O'
							,type: ['c_p', 'c_p_unconfirm']
							,client:v_client
							//,day:_jrday
							//,jobrotation:_jrcode
						};
						let sql='SELECT tl.*,COALESCE (cpd.leafmask, \'01\') leafmask,COALESCE (cpd.leafmaskcurrent, \'01\') leafmaskcurrent ' +
						'		,tl.productcount - tl.productdonecount remaining,coalesce(cpd.mould,\'\')mould,cpd.mouldgroup,cpd.tasklist,tl.client clients ' +
						'		,COALESCE(pt_d.delivery,false)delivery,pt_p.preptime,COALESCE(mg.setup,1800)setup,mg.program ' +
						'		,round(COALESCE(cpd.calculatedtpp, COALESCE(tl.tpp ,COALESCE(mg.productionmultiplier,100)*mg.cycletime/100) ),2) t_p_p ' +
						'		,COALESCE(pt_d.notificationtime,2)notificationtime '+
						' from client_production_details cpd ' +
						' join task_lists tl on tl.code=cpd.tasklist ' +
						' left JOIN mould_groups mg ON mg.finish IS NULL AND mg.mould = cpd.mould AND mg.code = cpd.mouldgroup ' +
						' LEFT JOIN product_trees pt_d ON pt_d.materialtype = :materialtype AND pt_d.finish IS NULL AND pt_d.name = tl.opname ' +
						//(( (t.ip&&(t.ip.indexOf('10.10')>-1) ))?' and pt_d.client=:client ':'') +
						' left join product_trees pt_p on pt_p.materialtype=\'K\' and pt_p.finish is null and pt_p.name=tl.opname ' +
						' where cpd.finish is null and tl.finish is null and cpd.type in (:type) and cpd.client=:client ';
						// eklenmesi halinde pazar tatili sonrası tüm tezgahlar iş yok konumuna geçiyor
						///*and cpd.day=:day and cpd.jobrotation=:jobrotation*/
						
						return t.db.sequelize.query(sql, {replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
							let _tmp_m_mg='';
							if(v_workflow==='Değişken Üretim Aparatlı'){
								t.processVariables.setup=0;
							}
							for(let i=0;i<results.length;i++){
								let row=results[i];
								let _idx=-1;
								if(v_workflow==='Değişken Üretim Aparatlı'){							
									if(_tmp_m_mg.indexOf()==-1){
										_tmp_m_mg+='-'+row.mould+'|'+row.mouldgroup;
										t.processVariables.setup+=row.setup>0?row.setup:0;
									}
									_idx=t.getIndex(t.processVariables.productions,'code',row.code);
								}else{
									_idx=t.getIndex(t.processVariables.productions,'code|mould|mouldgroup',row.code+'|'+row.mould+'|'+row.mouldgroup);
								}
								if(_idx==-1){
									if(row.leafmaskcurrent.length>2&&typeof row.step==='undefined'){
										row.step=0;
									}
									t.processVariables.productions.push(row);
								}else{
									Object.assign(t.processVariables.productions[_idx],row);
								}
							}
							return null;
						});
					}
					if(v_workflow==='Lazer Kesim'){
						let sql='SELECT tll.tasklistlaser,tll.opcode,tll.opnumber,tll.opname,tll.erprefnumber '+
							'	,tll.leafmask,tll.productcount,tll.productdonecount,tll.productdoneactivity '+
							'	,tll.productdonejobrotation,tll.scrappart,tll.scrapactivity,tll.scrapjobrotation '+
							'	,tll.record_id,cast(pt.tpp as integer) t_p_p,pt_p.preptime '+
							'	,tll.leafmask leafmaskcurrent,tll.productcount-tll.productdonecount remaining '+
							'	,tll.tasklistlaser code,tll.tasklistlaser tasklist '+
							' from task_list_laser_details tll '+
							' left join product_trees pt on pt.name=tll.opname and pt.materialtype=\'O\' and pt.finish is null '+
							//(( (t.ip&&(t.ip.indexOf('10.10')>-1) ))?' and pt.client=:client ':'') +
							' left join product_trees pt_p on pt_p.materialtype=\'K\' and pt_p.finish is null and pt_p.name=tll.opname ' +
							' where tll.tasklistlaser in (:code) '+
							' order by tll.opname ';
							let _rep={code: _ret_arr.task_lists};
							//if(t.ip&&(t.ip.indexOf('10.10')>-1) ){
							//	_rep.client=v_client;
							//}
						return t.db.sequelize.query(sql,{ replacements: _rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
							for(let i=0;i<results.length;i++){
								let row=results[i];
								let _idx=-1;
								_idx=t.getIndex(t.processVariables.productions,'tasklistlaser|opname|erprefnumber',row.tasklistlaser+'|'+row.opname+'|'+row.erprefnumber);
								if(_idx==-1){
									t.processVariables.productions.push(row);
								}else{
									Object.assign(t.processVariables.productions[_idx],row);
								}
							}
						});
					}
				}
			});
		}).then(function(){
			_promises=[];
			_promises.push(t.findAll('employees',{'name':'name','code':'code','isexpert':'isexpert','isoperator':'isoperator','ismaintenance':'ismaintenance','ismould':'ismould','isquality':'isquality'},{client:v_client,finish:{[Op.eq]:null}},[['name', 'ASC']],false));
			return Promise.all(_promises).then(function(results){
				_promises=[];
				const _arr_op=t.getLastElementOfArray(results);
				//t.mjd('getDeviceInitValues');
				for(let i=0;i<_arr_op.length;i++){
					if(t.processVariables.status_lost==='GÖREVDE KİMSE YOK'){
						_promises.push(t.updateRecord(null, 'employees',_time,{code:_arr_op[i].code},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.tickTimer.obj_time.strDateTime}));
					}else{
						if(t.getIndex(t.processVariables.operators,'code',_arr_op[i].code)==-1){
							t.processVariables.operators.push(_arr_op[i]);
						}
					}
				}
				t.processVariables.status_employee=t.processVariables.operators.length;
				return Promise.all(_promises);
			});
		}).then(function(){
			_promises=[];
			let _tmp_opnames=[];
			for(let i=0;i<t.processVariables.productions.length;i++){
				let prod=t.processVariables.productions[i];
				let tmp_opname=prod.opname.replace('/','_');
				if(_tmp_opnames.indexOf(tmp_opname)==-1){
					_tmp_opnames.push(tmp_opname);
				}
			}
			if(_tmp_opnames.length>0){
				if(t.objParam.app_showSOP.value===true){
					_promises.push(t.get_all_documents(_tmp_opnames));
				}
				if(t.objParam.process_wsSetup.value===true&&t.client_control_document===false){
					if(t.processVariables.status_lost==='SETUP-AYAR'&&t.processVariables.status_workflowprocess==='SETUP'){
						_promises.push(t.get_client_control_document(v_client,_tmp_opnames));
					}
				}
			}
			return Promise.all(_promises);
		}).then(function(){
			_promises=[];
			//cihaz durumunun çalışmaya uygun olmadığı durumlarda cihaz kendini iş yok konumuna alacak
			if((t.processVariables.productions.length===0&&t.processVariables.status_lost!=='İŞ YOK'&&t.processVariables.status_work!='-')){
				//üretilen parça bilgisi yok,kayıp iş yok değil,durum üretim->İŞ YOK
				t._lbl_cls='panel-danger';
				t.setStatus('İŞ YOK');
				t.processVariables.status_work='-';
				t.processVariables.status_workflowprocess='-';
				t.processVariables.status_tpp=0;
				if(t.processVariables.status_employee>0){
					//personeller boşa çıkarılacak
					_promises.push(t.updateAllRecords(null,'employees',_time,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
				}
				t.processVariables.status_employee=0;
				t.processVariables.operators=[];
				_promises.push(t.cjr_client_production_details(null,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null},type:{[Op.in]: ['c_p', 'e_p','c_p_unconfirm','e_p_unconfirm']}},_time));
				_promises.push(t.cjr_client_lost_details(null,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
				let _tmp_data={
					type:'l_c'
					,client:v_client
					,day:_jrday
					,jobrotation:_jrcode
					,losttype:'İŞ YOK'
					,start:_time
					,record_id:uuidv4()
				};
				_promises.push(t.createRecord(null,'client_lost_details',_time,_tmp_data));
				t._lbl_status= t.processVariables.status_lost+'. Başlangıç : ' + _time;
				t.processVariables.lastprodstarttime=null;
				//t.processVariables.lastprodtime=null;
				t.cpd_time=false;
				let obj_client_update={statustime:_time,status:t.generateClientStatus(),lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime};
				_promises.push(t.updateRecord(null,'clients',_time,{code:v_client},obj_client_update));
				t.setAllOutputs();
				return Promise.all(_promises);
			}else if(t.processVariables.operators.length==0&&t.processVariables.status_employee==0&&t.processVariables.productions.length>0&&t.processVariables.status_lost=='-'&&t.processVariables.status_work!='-'&&t.processVariables.status_workflowprocess=='-'){
				//operatör yok,üretilen parça bilgisi var,kayıp yok,durum üretim->GÖREVDE KİMSE YOK
				t._lbl_cls='panel-danger';
				t.setStatus('GÖREVDE KİMSE YOK');
				_promises.push(t.updateAllRecords(null, 'employees',_time,{client:v_client},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.tickTimer.obj_time.strDateTime}));
				t.processVariables.status_employee=0;
				t.processVariables.operators=[];
				_promises.push(t.cjr_client_production_details(null,{type:'e_p',client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
				_promises.push(t.cjr_client_lost_details(null,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
				let _tmp_data={
					type:'l_c'
					,client:v_client
					,day:_jrday
					,jobrotation:_jrcode
					,losttype:'GÖREVDE KİMSE YOK'
					,start:_time
					,record_id:uuidv4()
					,sourcedescriptionlost:'getDeviceInitValues-1'
				};
				_promises.push(t.createRecord(null,'client_lost_details',_time,_tmp_data));
				let _tmp_prod=[];
				for(let i=0;i<t.processVariables.productions.length;i++){
					let prod=t.processVariables.productions[i];
					let _str=prod.mould+'|'+prod.mouldgroup+'|'+prod.opname;
					if(_tmp_prod.indexOf(_str)===-1){
						_tmp_prod.push(_str);
						let _tmp_data_lct={
							type: 'l_c_t'
							,client: v_client
							,day: _jrday
							,jobrotation: _jrcode
							,losttype: 'GÖREVDE KİMSE YOK'
							,descriptionlost: null
							,tasklist: prod.tasklist
							,mould: prod.mould
							,mouldgroup: prod.mouldgroup
							,opcode: prod.opcode
							,opnumber: prod.opnumber
							,opname: prod.opname
							,opdescription: prod.opdescription
							,erprefnumber: prod.erprefnumber
							,taskfromerp:prod.taskfromerp
							,start: _time
							,finish: null
							,record_id: uuidv4()
							,energy: null
							,sourcedescriptionlost:'getDeviceInitValues-2'
						};
						_promises.push(t.createRecord(null,'client_lost_details',_time,_tmp_data_lct));
					}
				}
				t._lbl_status= t.processVariables.status_lost+'. Başlangıç : ' + _time;
				t.processVariables.lastprodstarttime=null;
				t.cpd_time=false;
				let obj_client_update={statustime:_time,status:t.generateClientStatus(),lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime};
				_promises.push(t.updateRecord(null,'clients',_time,{code:v_client},obj_client_update));
				t.setAllOutputs();
				if(t.objParam.task_production_case_stock_control.value===true){
					if(t.processVariables.productions.length>0){
						let erprefnumbers=[];
						for(let i=0;i<t.processVariables.productions.length;i++){
							const row=t.processVariables.productions[i];
							if(erprefnumbers.indexOf(row.erprefnumber)===-1){
								erprefnumbers.push(row.erprefnumber);
							}
							if(row.productdonecount<row.productcount){
								_promises.push(t.kontrol_kasa(row.erprefnumber,row.opname));
							}
						}
					}
				}
				return Promise.all(_promises);
			}else{
				_promises=[];
				if(t.inited_wf>0){
					if(t.objParam.task_production_case_stock_control.value===true
						&&t.processVariables.status_workflowprocess=='-'
						&&t.processVariables.status_work!=='-'
						&&(t.processVariables.status_lost==='-'||t.processVariables.status_lost==='MALZEME KALMADI')
						&&t.objParam.app_case_stok_before_work.value===true
						){
						t.hammaddeeksiklikleri=[];
						if(t.processVariables.productions.length>0){
							let erprefnumbers=[];
							for(let i=0;i<t.processVariables.productions.length;i++){
								const row=t.processVariables.productions[i];
								if(erprefnumbers.indexOf(row.erprefnumber)===-1){
									erprefnumbers.push(row.erprefnumber);
								}
								if((row.productdonecount<row.productcount||(t.objConfig.dcs_server_IP.value==='10.10.0.10'||t.objConfig.dcs_server_IP.value==='192.168.1.40'))&&row.delivery===true){
									let arr_opname=row.opname.split('-');
									if(arr_opname.length>1){
										arr_opname.pop();
									}
									let tmp_code=arr_opname.join('-');
									_promises.push(t.kontrol_hammadde(row.erprefnumber,tmp_code));
									_promises.push(t.kontrol_kasa(row.erprefnumber,row.opname));
								}
							}
							if(erprefnumbers.length>0){
								let kontrol_kasa=function(_obj_where){
									let deleterecords=function(_sql,_rep){
										return t.db.sequelize.query(_sql,{replacements:_rep, type: t.db.sequelize.QueryTypes.DELETE});
									};
									return t.db.task_cases.findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }).then(function(res_tc){
										if(res_tc.length>0){
											let __promises__=[];
											for(let i=0;i<res_tc.length;i++){
												__promises__.push(deleterecords('delete from task_cases where id=:id',{id:res_tc[i].id}));
											}
											return Promise.all(__promises__);
										}
										return;
									});
								};
								_promises.push(kontrol_kasa({erprefnumber:{[Op.notIn]: erprefnumbers} }))
							}
						}
					}
					_promises.push(t.setDeviceOutputs(/*_tr*/));
				}
				return Promise.all(_promises);
			}
		}).then(function(){
			if(v_workflow!=='El İşçiliği'){
				if(t.device_inited){
					const _state=(t.processVariables.status_lost==='-'?'ÜRETİM':t.processVariables.status_lost);
					if(_state!==t.last_state.losttype){
						t.last_state.time=_time;
						t.last_state.client=v_client;
						if(t.last_state.tasks.length>0){
							if(_state==='İŞ YOK'){
								//önceki iş için bekliyor-tamamlandı bilgisi gönderilecek
								for(let i=0;i<t.last_state.tasks.length;i++){
									const task=t.last_state.tasks[i];
									if(task.productcount>task.productdonecount){
										//bekliyor
										let mes=new dcs_message({
											type:'device_state_change'
											,ack:true
											,data:{client:t.last_state.client,time:t.last_state.time,state:'Bekliyor',losttype:t.last_state.losttype,tasks:[task]}
										});
										t.tmp_queue.push(mes);
										mes=undefined;
										// son parça kontrolü de olsun denirse buraya mesaj eklenecek
									}else{
										//tamamlandı
										let mes=new dcs_message({
											type:'device_state_change'
											,ack:true
											,data:{client:t.last_state.client,time:t.last_state.time,state:'Tamamlandı',losttype:t.last_state.losttype,tasks:[task]}
										});
										t.tmp_queue.push(mes);
										mes=undefined;
									}
								}
								t.last_state.tasks=[];
							}else{
								if(_state!=='KALIP SÖKME'){
									t.last_state.tasks=[];
									for(let i=0;i<t.processVariables.productions.length;i++){
										const product=t.processVariables.productions[i];
										t.last_state.tasks.push({erprefnumber:product.erprefnumber,opname:product.opname,productcount:product.productcount,productdonecount:product.productdonecount});
									}
								}
							}
						}else{
							t.last_state.tasks=[];
							for(let i=0;i<t.processVariables.productions.length;i++){
								const product=t.processVariables.productions[i];
								t.last_state.tasks.push({erprefnumber:product.erprefnumber,opname:product.opname,productcount:product.productcount,productdonecount:product.productdonecount});
							}
							// işe ilk başladığında devam ediyor olarak mesaj gidecek, duruş bitimi sonrası gitmeyecek
							for(let i=0;i<t.last_state.tasks.length;i++){
								const task=t.last_state.tasks[i];
								let mes=new dcs_message({
									type:'device_state_change'
									,ack:true
									,data:{client:t.last_state.client,time:t.last_state.time,state:'Devam Ediyor',losttype:_state,tasks:[task]}
								});
								t.tmp_queue.push(mes);
								mes=undefined;
							}
						}
						if(t.last_state.state!==''){
							//önceki durum boş değil
							if(t.last_state.state==='Devam Ediyor'){
								t.last_state.state='Duruş Başladı';
							}else{
								if(_state==='ÜRETİM'){
									t.last_state.state='Duruş Bitti';
									let mes=new dcs_message({
										type:'device_state_change'
										,ack:true
										,data:{client:t.last_state.client,time:t.last_state.time,state:t.last_state.state,losttype:t.last_state.losttype,tasks:t.last_state.tasks}
									});
									t.tmp_queue.push(mes);
									mes=undefined;
									// console.warn('getDeviceInitValues-device_state_change',t.last_state);
									t.last_state.state='Devam Ediyor';
								}else{
									t.last_state.state='Duruş Bitti';
									let mes=new dcs_message({
										type:'device_state_change'
										,ack:true
										,data:{client:t.last_state.client,time:t.last_state.time,state:t.last_state.state,losttype:t.last_state.losttype,tasks:(t.last_state.losttype==='İŞ YOK'?[]:t.last_state.tasks)}
									});
									t.tmp_queue.push(mes);
									mes=undefined;
									if(t.last_state.losttype==='SETUP-AYAR'){
										t.last_state.state='Setup Yapıldı';
										let mes=new dcs_message({
											type:'device_state_change'
											,ack:true
											,data:{client:t.last_state.client,time:t.last_state.time,state:t.last_state.state,losttype:t.last_state.losttype,tasks:(t.last_state.losttype==='İŞ YOK'?[]:t.last_state.tasks)}
										});
										t.tmp_queue.push(mes);
										mes=undefined;
									}
									// console.warn('getDeviceInitValues-device_state_change',t.last_state);
									t.last_state.state='Duruş Başladı';
								}
							}
						}else{
							//önceki durum boş
							if(_state==='ÜRETİM'){
								t.last_state.state='Devam Ediyor';
							}else{
								t.last_state.state='Duruş Başladı';
							}
						}
						t.last_state.losttype=_state;
						// işe ilk başladığında devam ediyor olarak mesaj gidecek, duruş bitimi sonrası gitmeyecek
						if(t.last_state.state!=='Devam Ediyor'){
							let mes=new dcs_message({
								type:'device_state_change'
								,ack:true
								,data:t.last_state
							});
							t.tmp_queue.push(mes);
							mes=undefined;
						}
						// console.warn('getDeviceInitValues-device_state_change',t.last_state);
						_promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						return Promise.all(_promises).then(function(){
							const sql = 'update __status set data=:data where id=1';
							return t.db.sequelize.query(sql
								, { replacements: {data:JSON.stringify(t.last_state)}, type: t.db.sequelize.QueryTypes.UPDATE });
						});
					}
				}else{
					if(t.last_state.client===''||t.last_state.losttype===''){
						t.last_state.client=v_client;
						let sql='select * from __status';
						return t.db.sequelize.query(sql, { type: t.db.sequelize.QueryTypes.SELECT }).then(function(result){
							if(result&&result.length>0){
								t.last_state=result[0].data;
							}else{
								sql = 'insert into __status (id,data) values (1,:data)';
								return t.db.sequelize.query(sql
									, { replacements: {data:JSON.stringify(t.last_state)}, type: t.db.sequelize.QueryTypes.INSERT });
							}
						});
					}
				}
			}
			return;
		});
	}
	setIOEvents(){
		let t=this;
		const v_client=t.objConfig.client_name.value;
		const v_workflow=t.objConfig.workflow.value;
		let _promises=[];
		//seq-finish-sample
		return t.db.client_details.findAll({where:{client:v_client,[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:t.tickTimer.obj_time.strDateTime}}] },order: [ ['iotype', 'ASC'],['portnumber', 'ASC'],['code', 'ASC'] ]}).then(function(results_cd) {
			if(results_cd.length>0){
				t.ports.inputs=[];
				t.ports.outputs=[];
				let _arr_cd_record_id=[];
				for (let i = 0; i < results_cd.length; i++) {
					let result=results_cd[i];
					_arr_cd_record_id.push(result.record_id);
					if(result.iotype=='I'){
						t.ports.inputs.push(result.dataValues);
					}
					if(result.iotype=='O'){
						t.ports.outputs.push(result.dataValues);
					}
				}
				if(v_workflow==='Sabit Üretim Aparatlı'){
					// olmayanları temizle
					t.arrRemove(t.ports.inputs,_arr_cd_record_id,'record_id');
					t.arrRemove(t.ports.outputs,_arr_cd_record_id,'record_id');
					let get_mould_details=function(v_mould_group){
						return t.db.mould_details.findAll({where:{mould:v_mould_group.mould,mouldgroup:v_mould_group.code,[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:t.tickTimer.obj_time.strDateTime}}]},order: [ ['opname', 'ASC'] ]}).then(function(results_md) {
							if(results_md.length>0){
								let _arr_md_record_id=[];
								for(let i=0;i<results_md.length;i++){
									let _item_md=results_md[i].dataValues;
									_arr_md_record_id.push(_item_md.record_id);
									let _idx=-1;
									_idx=t.getIndex(v_mould_group.mould_details,'record_id',_item_md.record_id);
									if(_idx===-1){
										v_mould_group.mould_details.push(_item_md);
									}else{
										Object.assign(v_mould_group.mould_details[_idx], _item_md);
									}
								}
								// olmayanları temizle
								t.arrRemove(v_mould_group.mould_details,_arr_md_record_id,'record_id');
							}
							return null;
						});
					};
					let get_mould_groups=function(v_mould){
						let __promises=[];
						return t.db.mould_groups.findAll({where:{mould:v_mould.code,[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:t.tickTimer.obj_time.strDateTime}}]},order: [ ['code', 'ASC'] ]}).then(function(results_mg) {
							if(results_mg.length>0){
								let _arr_mg_record_id=[];
								for(let i=0;i<results_mg.length;i++){
									let _item_mg=results_mg[i].dataValues;
									_arr_mg_record_id.push(_item_mg.record_id);
									let _idx=-1;
									_idx=t.getIndex(v_mould.mould_groups,'record_id',_item_mg.record_id);
									if(_idx===-1){
										_item_mg.mould_details=[];
										v_mould.mould_groups.push(_item_mg);
										__promises.push(get_mould_details(_item_mg));
									}else{
										Object.assign(v_mould.mould_groups[_idx], _item_mg);
										__promises.push(get_mould_details(v_mould.mould_groups[_idx]));
									}
								}
								// olmayanları temizle
								t.arrRemove(v_mould.mould_groups,_arr_mg_record_id,'record_id');
								return null;
							}
							return null;
						}).then(function(){
							if(__promises.length>0){
								return Promise.all(__promises);
							}else{
								return null;
							}
						});
					};
					for (let i = 0; i < t.ports.inputs.length; i++) {
						// input port mould_groups ve mould_details yüklemeleri
						let _mould=t.ports.inputs[i];
						if(typeof _mould.mould_groups==='undefined'){
							_mould.mould_groups=[];
						}
						_promises.push(get_mould_groups(_mould));
					}
					return Promise.all(_promises);
				}
			}
			return null;
		});
	}
	el_iodatareceive(data){
		let t=this;
		try{
			t.winmessend('cn_js_workflow',{event:'iodatareceive',data:data});
		}catch(err){
			this.mjd('el_iodatareceive-err:'+err.stack);
		}
		
	}
	el_iodatasend(data){
		let t=this;
		try{
			//t.mjd(data);
			t.winmessend('cn_js_workflow',{event:'iodatasend',data:data});
		}catch(err){
			this.mjd('el_iodatasend-err:'+err.stack);
		}
	}
	el_ioconnecting(){
		let t=this;
		t.mjd('el_ioconnecting');
	}	
	el_ioconnected(){
		let t=this;
		try{
			t.winmessend('cn_js_dcsclient',{event:'ioconnected'});
			t.winmessend('cn_js_workflow',{event:'ioconnected'});
			if(t.wait_for_io_first_connect){
				//ilk açıldığındaki IO bağlantısı
				t.wait_for_io_first_connect=false;
				t.wait_for_first_controljobrotation=true;
			}else{
				//cihaz açıkken bağlantının gelmesi durumu
			}
		}catch(err){
			this.mjd('el_ioconnected-err:'+err.stack);
		}
	}
	el_ioportchange(str_data,fevent){
		let t=this;
		let _tmp=str_data.split('|');
		let _type=_tmp[0];
		let _number=parseInt(_tmp[1]);
		let _curval=parseInt(_tmp[2]);
		if(isNaN(_curval)){
			t.mjd('p:'+_number+' type:'+_type+' val:'+_curval+' fevent:'+fevent+' str_data:'+str_data);
			Sentry.addBreadcrumb({
				category: 'el_ioportchange',
				message: 'el_ioportchange_nan',
				level: 'info',
				data:JSON.parse(JSON.stringify(str_data))
			});
			return;
		}
		if(_type==='input'){
			//console.log('p:',_number,' type:',_type,' val:',_curval,' fevent:',fevent,' str_data:',str_data);
			t.iodevice_manager.inputPorts[_number-1].curval=_curval;
			//console.log('el_ioportchange',t.iodevice_manager.inputPorts[_number-1]);
		}else{
			t.iodevice_manager.outputPorts[_number-1].curval=_curval;
		}
		try{
			//değişiklik verisi veritabanına yazılacak
			t.db.IOPorts.update(
				{ value: _curval, updatedAt: t.tickTimer.obj_time.strDateTime },
				{ where: { type: (_type=='output'?'O':'I'),number:_number,value:{[Op.ne]:_curval} }} 
			);
			if(!fevent){
				//cihazdan gelen input değeri için trigger burada olacak, çıkış portları trigger için algoritmada setOutput,clearOutput öncesinde olacak
				if(_type=='input'){
					//t.emit('getinput',_number,_curval);
					t.getInput(_number,_curval);
				}
			}else{
				if(_type=='input'){
					if(t.objConfig.mainapp_IO_TYPE.value=='PLC-SOFT'||t.objConfig.mainapp_IO_TYPE.value=='PLC-USB'||t.objConfig.mainapp_IO_TYPE.value=='PLC-RS232'||t.objConfig.mainapp_IO_TYPE.value=='PLC-RS232-MB'){
						t.getInput(_number,_curval);
					}
				}else{
					if(t.iodevice_manager.connected===true){
						t.iodevice_manager.send({
							event: 'output_trigger',
							pid: t.iodevice_manager.pid,
							value: _curval,
							number: _number
						});
					}else{
						console.log('t.iodevice_manager.connected:',t.iodevice_manager.connected);
					}
				}
			}
			t.winmessend('cn_js_workflow',{event:'ioportchange',data:_type+'|'+_number+'|'+_curval});
		}catch(err){
			this.mjd('el_ioportchange-err:'+err.stack);
		}
	}
	el_sendqueuechange(length){
		let t=this;
		try{
			t.winmessend('cn_js_workflow',{event:'sendqueuechange',len:length});
		}catch(err){
			this.mjd('el_sendqueuechange-err:'+err.stack);
		}
	}
	el_receivequeuechange(length){
		let t=this;
		try{
			t.winmessend('cn_js_workflow',{event:'receivequeuechange',len:length});
		}catch(err){
			this.mjd('el_receivequeuechange-err:'+err.stack);
		}
	}
	el_setoutputports(){
		let t=this;
		if(t.wait_for_first_controljobrotation){
			t.eventListener({event:'device_init'});
		}
	}
	el_ioqueue_empty(){
		let t=this;
		if(t.self.wait_for_ioqueue_empty_resetapp==true){
			t.self.wait_for_ioqueue_empty_resetapp=false;
		}
	}
	setoutputport(_key, _value){
		let t=this;
		let i = t.getIndex(t.ports.outputs, 'ioevent', _key);
		if (i > -1){
			const _number=t.ports.outputs[i].portnumber;
			console.log('setoutput_0:',_number, _value);
			t.emit('setoutput', _number, _value);
		}
	}
	cjr_client_lost_details(_tr,_obj_where,_time,_obj_update){
		let t=this;
		let _promises=[];
		return t.db.client_lost_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }).then(function(res_cld){
			if(res_cld){
				let obj_where;
				let obj_update;
				let _row_jr_day;
				let _row_jr_code;
				for(let i=0;i<res_cld.length;i++){
					obj_where={
						record_id:res_cld[i].record_id
					};
					_row_jr_day=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(res_cld[i].start),false,res_cld[i].start);
					_row_jr_code=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(res_cld[i].start),true,res_cld[i].start);
					obj_update={
						finish:_time
					};
					if(t.processVariables._jrday!=_row_jr_day||t.processVariables._jrcode!=_row_jr_code){
						obj_update.finish=t.getLastTimeOfJobRotaion(t.convertLocalDateString(_row_jr_day),_row_jr_code);
						if(obj_update.finish>_time){
							obj_update.finish=_time;
						}
					}
					if(_obj_update){
						if(typeof _obj_update.descriptionlost==='string'&&i==0){
							if(res_cld[i].descriptionlost!==null){
								_obj_update.descriptionlost=(res_cld[i].descriptionlost+' '+_obj_update.descriptionlost).substr(0,250);
							}else{
								_obj_update.descriptionlost=(_obj_update.descriptionlost).substr(0,250);
							}
						}
						Object.assign(obj_update, _obj_update);
					}
					_promises.push(t.updateRecord(_tr,'client_lost_details',_time,obj_where,obj_update));
				}
			}
			if(_promises.length>0){
				return Promise.all(_promises);
			}else{
				return null;
			}
		});
	}
	cjr_client_production_details(_tr,_obj_where,_time,_obj_update){
		let t=this;
		let _promises=[];
		if(!_obj_where.type){
			_obj_where.type={[Op.notIn]: ['c_s', 'c_s_ping']};
		}
		return t.db.client_production_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }).then(function(res_cpd){
			if(res_cpd){
				let obj_where;
				let obj_update;
				let _row_jr_day;
				let _row_jr_code;
				for(let i=0;i<res_cpd.length;i++){
					obj_where={
						record_id:res_cpd[i].record_id
					};
					_row_jr_day=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(res_cpd[i].start),false,res_cpd[i].start);
					_row_jr_code=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(res_cpd[i].start),true,res_cpd[i].start);
					let row_finish_time=_time;
					if(t.processVariables._jrday!=_row_jr_day||t.processVariables._jrcode!=_row_jr_code){
						row_finish_time=t.getLastTimeOfJobRotaion(t.convertLocalDateString(_row_jr_day),_row_jr_code);
						if(row_finish_time>_time){
							row_finish_time=_time;
						}
					}
					let v_pd_gap=Math.ceil((new timezoneJS.Date(row_finish_time).getTime()-res_cpd[i].start.getTime())/1000);
					obj_update={
						finish:row_finish_time
						,gap:v_pd_gap>0?v_pd_gap:0
					};
					if(_obj_update){
						Object.assign(obj_update, _obj_update);
					}
					_promises.push(t.updateRecord(_tr,'client_production_details',_time,obj_where,obj_update));
				}
			}
			if(_promises.length>0){
				return Promise.all(_promises);
			}else{
				return null;
			}
		});
	}
	setStatus(_status){
		let t=this;
		t.processVariables.status_pre_lost=t.processVariables.status_lost;
		t.processVariables.status_lost=_status;
	}
	getClientStatus(){
		let t=this;
		let _promises=[];
		const v_client=t.objConfig.client_name.value;
		let _state_start=t.tickTimer.obj_time.strDateTime;
		return t.db.clients.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{code:v_client} }/*, {transaction: _tr}*/).then(function(res_c){
			if(res_c){
				t.processVariables.inputready=res_c.inputready!==null||res_c.inputready!==''?parseInt(res_c.inputready):null;
				if(res_c.status==null&&t.processVariables.status_work==='-'&&t.processVariables.status_lost==='-'){
					t.processVariables.status_work='-';
					t.processVariables.status_employee=0;
					t.processVariables.operators=[];
					t.processVariables.productions=[];
					t.processVariables.status_workflowprocess='-';
					t.processVariables.status_tpp=0;
					t.processVariables.lastprodstarttime=null;
					//t.processVariables.lastprodtime=null;
					t.processVariables.statustime=_state_start;
					t.setStatus('İŞ YOK');
					t.cpd_time=false;
					let _jrday=t.getJobRotationDay(t.self.jobrotations,_state_start,false,_state_start);
					let _jrcode=t.getJobRotationDay(t.self.jobrotations,_state_start,true,_state_start);
					let create_lost_record_not_exists=function(_tr){
						let __promises=[];
						let _obj_where_clt_rne={
							type:'l_c'
							,client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,losttype:t.processVariables.status_lost
							,start:{[Op.ne]:null}
							,finish:{[Op.eq]:null}
						};
						return t.db.client_lost_details.findOne({where:_obj_where_clt_rne},{transaction: _tr}).then(function(result){
							if(result){
								return null;
							}else{
								let _tmp_data_lc={
									type:'l_c'
									,client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,losttype:t.processVariables.status_lost
									,start:_state_start
									,record_id:uuidv4()
								};
								__promises.push(t.create_lost_record(_tr,t.tickTimer.obj_time.strDateTime,_tmp_data_lc));
								return null;
							}
						}).then(function(){
							if(__promises.length>0){
								return Promise.all(__promises);
							}else{
								return null;
							}
						});
					};
					_promises.push(create_lost_record_not_exists(null));
					_promises.push(t.updateRecord(null,'clients',t.tickTimer.obj_time.strDateTime,{record_id:res_c.record_id},{status:t.generateClientStatus()}));
					return Promise.all([create_lost_record_not_exists(null)]);
				}else{
					_state_start=t.convertFullLocalDateString(res_c.statustime);
					t.processVariables.statustime=_state_start;
					const _tmp_arr_status=res_c.status.split('|');
					t.processVariables.status_work=_tmp_arr_status[1].split(':')[1];
					t.processVariables.status_employee=_tmp_arr_status[2].split(':')[1];
					if(t.processVariables.status_employee<0){
						t.processVariables.status_employee=0;
						t.processVariables.operators=[];
					}
					if(_tmp_arr_status.length>=4){
						t.processVariables.status_workflowprocess=_tmp_arr_status[3].split(':')[1];
					}else{
						t.processVariables.status_workflowprocess='-';
					}
					if(_tmp_arr_status.length>=5){
						t.processVariables.status_tpp=_tmp_arr_status[4].split(':')[1];
					}else{
						t.processVariables.status_tpp=0;
					}
					if(t.processVariables.status_lost!=_tmp_arr_status[0].split(':')[1]){
						t.processVariables.status_lost=_tmp_arr_status[0].split(':')[1];
						t.setStatus(_tmp_arr_status[0].split(':')[1]);
					}
					t.processVariables.lastprodstarttime=res_c.lastprodstarttime!=null?t.convertFullLocalDateString(res_c.lastprodstarttime):null;
					t.processVariables.lastprodtime=res_c.lastprodtime!=null?t.convertFullLocalDateString(res_c.lastprodtime):null;
					return null;
				}
			}
		}).then(function(){
			return _state_start;
		});
	}
	generateClientStatus(){
		let t=this;
		return 'lost:'+t.processVariables.status_lost+'|work:'+t.processVariables.status_work+'|emp:'+t.processVariables.status_employee+'|wfp:'+t.processVariables.status_workflowprocess+'|tpp:'+t.processVariables.status_tpp;
	}
	controljobrotations(__day,__jobrotation,__change){
		let t=this;
		if(t.inited_wf==1){
			console.log('return-cjr');
			return null;
		}
		if(t.inited_wf==0){
			t.inited_wf=1;
		}
		//t.mjd('cjr:'+__change);
		const _time=t.tickTimer.obj_time.strDateTime;
		const v_client=t.objConfig.client_name.value;
		const v_workflow=t.objConfig.workflow.value;
		t.obj_jobrotation.day=__day;
		t.obj_jobrotation.code=__jobrotation;
		t.processVariables._time=_time;
		t.processVariables._jrday=__day;
		t.processVariables._jrcode=__jobrotation;
		t.processVariables.v_client=v_client;
		t.status=t.status==''?'controljobrotations':t.status;
		let _st=false;
		let _jrday=false;
		let _jrcode=false;
		let _promises=[];
		let obj_client_update={};
		return t.db.sequelize.transaction(function (_tr) {
			if(v_workflow==='El İşçiliği'){
				return t.db.employees.findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:{client:v_client,[Op.or]:[{day: {[Op.ne]: __day}},{jobrotation: {[Op.ne]: __jobrotation}}]} }).then(function(res_employees){
					if(res_employees){
						for (let i = 0; i < res_employees.length; i++) {
							let _emp=res_employees[i];
							_promises.push(t.employeeLeave(_tr,{code:_emp.code},_time,__change));
						}
					}
					if(_promises.length>0){
						return Promise.all(_promises);
					}else{
						return null;
					}
				});
			}else{
				_promises=[];
				let _tmp_status_lost=t.processVariables.status_lost;
				if(t.processVariables.statustime){
					_st=t.convertFullLocalDateString(t.processVariables.statustime);
					_jrday=t.getJobRotationDay(t.self.jobrotations,_st,false,_st);
					_jrcode=t.getJobRotationDay(t.self.jobrotations,_st,true,_st);
				}
				if(__day!=_jrday||__jobrotation!=_jrcode||__change){
					__change=true;
					t.processVariables.lastprodstarttime=null;
					obj_client_update.statustime=_time;
				}
				if(t.processVariables.status_lost==='URETIM SONU ISKARTA BEKLEME'){
					_tmp_status_lost='İŞ YOK';
					t.cpd_time=false;
					obj_client_update.statustime=_time;
					t.processVariables.lastprodstarttime=null;
					//t.processVariables.lastprodtime=null;
					t.processVariables.status_work='-';
					t.processVariables.status_employee=0;
					t.processVariables.status_tpp=0;
					t.processVariables.operators=[];
					_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
					_promises.push(t.cjr_client_production_details(_tr,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null},type:{[Op.in]: ['c_p', 'e_p','c_p_unconfirm','e_p_unconfirm']} },_time));
					_promises.push(t.cjr_client_lost_details(_tr,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
					let _tmp_data={
						type:'l_c'
						,client:v_client
						,day:_jrday
						,jobrotation:_jrcode
						,losttype:_tmp_status_lost
						,start:_time
						,record_id:uuidv4()
					};
					_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
				}
				if(__change){
					let ltjr=t.getLastTimeOfJobRotaion(_jrday,_jrcode);
					if(v_workflow!=='CNC'){
						for (let i = 0; i < t.processVariables.operators.length; i++) {
							const _emp=t.processVariables.operators[i];
							_promises.push(t.employeeLeave(_tr,{code:_emp.code},ltjr,__change));
						}
					}
					for(let i=0;i<t.processVariables.productions.length;i++){
						let item=t.processVariables.productions[i];
						item.productdoneactivity=0;
						item.scrapactivity=0;
						item.productdonejobrotation=0;
						item.scrapjobrotation=0;
						let _obj_update_tasklist={productdoneactivity:0,scrapactivity:0,productdonejobrotation:0,scrapjobrotation:0};
						_promises.push(t.updateRecord(_tr,'task_lists',_time,{code:item.tasklist},_obj_update_tasklist));
					}
					t.json_status.fault.operators=[];
					t.json_status.operationAuthorization.data=[];
					t.json_status.operationAuthorization.nextControl=null;
					try {
						fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
					} catch (error) {
						Sentry.captureException(error);
					}
					_promises.push(t.setDeviceOutputs());
				}
				return Promise.all(_promises).then(function(){
					return Promise.all([t.controlDeviceStatus(_tr,_time,'controljobrotations',__change)]);
				});
			}
		}).then(function () {
			_promises=[];
			while(t.tmp_queue.length>0){
				_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
			}
			if(t.objParam.task_materialpreparation.value===true&&(t.processVariables.status_lost=='MALZEME BEKLEME'||t.processVariables.status_lost=='PLANLI TAŞIMA BEKLEME'||t.processVariables.status_lost=='ALT PARÇA YOK')&&t.timer_missing_material==false){
				_promises.push(t.control_missing_materials('delivered',1));
			}
			return Promise.all(_promises).then(function(){
				t.status='';
				if((__change)||(t.processVariables.status_pre_lost==='URETIM SONU ISKARTA BEKLEME')){
					t.initApp();
				}
				return null;
			});	
		}).catch(function (err) {
			t.status='';
			classBase.writeError(err,t.ip,'catch_workflow_controljobrotations');
			console.log(err);
			t.tmp_queue=[];
			return null;
		});
	}
	controlOfftimes(__day,__jobrotation,__obj_time,_arr_offtimes_losttype){
		let t=this;
		const _time=t.tickTimer.obj_time.strDateTime;
		const v_client=t.objConfig.client_name.value;
		const v_workflow=t.objConfig.workflow.value;
		const vi_time=parseInt(__obj_time.strHour+__obj_time.strMinute);
		if(t.faultcodes.indexOf(t.processVariables.status_lost)>-1||v_workflow==='CNC'){
			return;
		}
		//t.mjd('controlOfftimes');
		let _f=false;
		let _trigger_start=false;
		let _trigger_finish=false;
		let _trigger_screen_change=false;
		let _promises=[];
		let molabitir=function(_tr,_losttype){
			let _promises=[];
			_trigger_finish=true;
			if(t.processVariables.status_lost===_losttype){
				_trigger_screen_change=true;
				_promises.push(t.cjr_client_lost_details(_tr,{client:v_client,losttype: _losttype,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
				let _c_lost_type=t.json_status.prelosttype;
				if(_c_lost_type===_losttype){
					//otomatik mola öncesinde elle mola başlatıldı ise
					if(t.processVariables.productions.length>0){
						if(t.processVariables.status_workflowprocess==='-'){
							if(t.processVariables.operators.length>0){
								_c_lost_type='-';
							}else{
								_c_lost_type='GÖREVDE KİMSE YOK';
							}
						}else{
							if(t.objParam.process_wsQuality.value===true){
								if(t.processVariables.status_workflowprocess==='QUALITY'){
									if(t.objParam.process_remoteQualityConfirmation.value===true){
										_c_lost_type='KALİTE ONAY';
									}else{
										if(t.processVariables.operators.length>0){
											//görevdekiler arasında kalite personeli var mı
											let _flag_required_operator_exists=false;
											for(let i=0;i<t.processVariables.operators.length;i++){
												let _op_item=t.processVariables.operators[i];
												if(!_flag_required_operator_exists){
													if(_op_item.isquality){
														_flag_required_operator_exists=true;
													}
												}
											}
											if(_flag_required_operator_exists){
												_c_lost_type='KALİTE ONAY';
											}else{
												_c_lost_type='KALİTE BEKLEME';
											}
										}else{
											_c_lost_type='KALİTE BEKLEME';
										}
									}
								}
							}
							if(t.objParam.process_wsSetup.value===true){
								if(t.processVariables.status_workflowprocess==='SETUP'){
									if(t.processVariables.operators.length>0){
										//görevdekiler arasında usta personel var mı
										let _flag_required_operator_exists=false;
										for(let i=0;i<t.processVariables.operators.length;i++){
											let _op_item=t.processVariables.operators[i];
											if(!_flag_required_operator_exists){
												if(_op_item.isexpert){
													_flag_required_operator_exists=true;
												}
											}
										}
										if(_flag_required_operator_exists){
											_c_lost_type='SETUP-AYAR';
										}else{
											_c_lost_type='USTA BEKLEME';
										}
									}else{
										_c_lost_type='USTA BEKLEME';
									}
								}
							}
						}
					}else{
						_c_lost_type='İŞ YOK';
					}
				}
				if(_c_lost_type!=='-'&&_c_lost_type!==false){
					t.lost_recursive=false;
					let _faulttype=t.json_status.fault.faulttype;
					let _faultdescription=t.json_status.fault.faultdescription;
					let _faultgroupcode=t.json_status.fault.faultgroupcode;
					let _faultdevice=t.json_status.fault.faultdevice;
					let _tasklist=t.json_status.fault.fault_unique_id;
					_promises.push(t.create_lost_records(_tr,_time,{losttype:_c_lost_type,descriptionlost:null,faulttype:_faulttype,faultdescription:_faultdescription,faultgroupcode:_faultgroupcode,faultdevice:_faultdevice,tasklist:_tasklist},_time,t.processVariables.productions,'controlOfftimes-1'));
				}
				t.setStatus(_c_lost_type);
				t.processVariables.lastprodstarttime=_time;
				_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:_time}));
				t.json_status.prelosttype=false;
				return Promise.all(_promises);
			}
			t.json_status.prelosttype=false;
			return null;
		};
		let _offtimes=t.self.offtimes;
		return t.db.sequelize.transaction(function (_tr) {
			for(let i=0;i<_offtimes.length;i++){
				let item=_offtimes[i];
				if(!_f
					&&(item.finish==null||(item.finish!=null&&t.convertFullLocalDateString(item.finish)>__obj_time.strDateTime))
					&&(item.repeattype!=='day'||(item.repeattype==='day'&&item.days.indexOf(__obj_time.strDay)>-1))
				){
					let vi_start=parseInt(item.starttime.split(':').join(''));
					let vi_finish=parseInt(item.finishtime.split(':').join(''));
					if(vi_time>=vi_start && vi_time<=vi_finish){
						//t.mjd(item);
						if(t.json_status.prelosttype==false){//mola kaybı elle bitirildiğinde bir daha açmaması için
							//cihazın o anki durum kontrolü
							if(t.processVariables.status_lost!=='-'){
								if(t.processVariables.status_lost==='-'&&t.processVariables.operators.length>0){
									_promises.push(t.cjr_client_lost_details(_tr,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{losttype:'MOLA ÖNCESİ KAYIP',descriptionlost:item.losttype}));
								}else{
									_promises.push(t.cjr_client_lost_details(_tr,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
								}
							}
							t.json_status.prelosttype=t.processVariables.status_lost;
							t.lost_recursive=false;
							_promises.push(t.create_lost_records(_tr,_time,{losttype: item.losttype,descriptionlost:item.name},_time,t.processVariables.productions,'controlOfftimes-2'));
							t.setStatus(item.losttype);
							t.processVariables.lastprodstarttime=null;
							_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null}));
							_f=true;
							_trigger_start=true;
							_trigger_screen_change=true;
						}else{//mola süresi bitince t.json_status.prelosttype=false yapılmalı
							if(vi_time==vi_finish){
								_promises.push(molabitir(_tr,item.losttype));
							}
							_f=true;
						}
					}
				}
			}
			//t.mjd('controlOfftimes _f:'+_f);
			//console.log(_arr_offtimes_losttype);
			if(!_f){
				if(t.json_status.prelosttype===false){
					if(_arr_offtimes_losttype.indexOf(t.processVariables.status_lost)>-1){
						t.json_status.prelosttype=t.processVariables.status_lost;
					}
				}
				if(t.json_status.prelosttype!==false){
					_promises.push(molabitir(_tr,t.json_status.prelosttype));
				}
			}
			return Promise.all(_promises).then(function(){
				try {
					fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
				} catch (error) {
					Sentry.captureException(error);
				}
				return null;
			});
		}).then(function () {
			_promises=[];
			while(t.tmp_queue.length>0){
				_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
			}
			if(_trigger_finish&&t.objParam.task_materialpreparation.value===true&&(t.processVariables.status_lost=='MALZEME BEKLEME'||t.processVariables.status_lost=='PLANLI TAŞIMA BEKLEME'||t.processVariables.status_lost=='ALT PARÇA YOK')&&t.timer_missing_material==false){
				_promises.push(t.control_missing_materials('delivered',2));
			}
			if(_trigger_start&&t.objParam.task_materialpreparation.value===true&&(t.processVariables.status_lost=='MALZEME BEKLEME'||t.processVariables.status_lost=='PLANLI TAŞIMA BEKLEME'||t.processVariables.status_lost=='ALT PARÇA YOK')&&t.timer_missing_material!==false){
				clearTimeout(t.timer_missing_material);
				t.timer_missing_material=false;
			}
			return Promise.all(_promises).then(function(){
				t.status='';
				if(_trigger_screen_change&&v_workflow!=='El İşçiliği'){
					t.initApp();
				}
				return null;
			});	
		}).catch(function (err) {
			t.status='';
			classBase.writeError(err,t.ip,'catch_workflow_controlOfftimes');
			console.log(err);
			t.tmp_queue=[];
			return null;
		});
	}
	controlDeviceStatus(_tr,_time,_type,_change_jr){
		let t=this;
		let _promises=[];
		let v_client=t.objConfig.client_name.value;
		let _obj_upd_client={status:t.generateClientStatus()};
		t.tmp__type=_type;
		if((_change_jr&&_type==='controljobrotations')||_type!=='controljobrotations'){
			_obj_upd_client.statustime=_time;
		}
		if(_type==='join'&&t.processVariables.status_employee==1&&t.faultcodes.indexOf(t.processVariables.status_lost)==-1){
			t.processVariables.lastprodstarttime=_time;
			_obj_upd_client.lastprodstarttime=_time;
		}
		if(_type==='device-employee-leave'||_type==='controljobrotations'){
			if(t.processVariables.status_lost=='GÖREVDE KİMSE YOK'){
				_obj_upd_client.lastprodstarttime=null;
			}else if(t.processVariables.status_lost=='İŞ YOK'){
				_obj_upd_client.lastprodstarttime=null;
				//_obj_upd_client.lastprodtime=null;
				t.processVariables.productions=[];
			}
		}
		_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},_obj_upd_client));
		if(t.processVariables.status_work!=='-'&&_type==='controljobrotations'&&_change_jr===true){
			_promises.push(t.cjr_client_production_details(_tr,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null},type:{[Op.in]: ['c_p', 'e_p','c_p_unconfirm','e_p_unconfirm']}},_time));
		}
		let c_l_r=false;
		if(_type==='controljobrotations'&&_change_jr===true){
			c_l_r=true;
			const tmptime=t.getFirstTimeOfJobRotaion(t.processVariables._jrday,t.processVariables._jrcode);
			if(tmptime < _time && t.processVariables.statustime < tmptime){
				t.processVariables.statustime=tmptime;
			}else{
				t.processVariables.statustime=_time;
			}
			_promises.push(t.cjr_client_lost_details(_tr,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
		}
		if(_type==='device-employee-leave'){
			if(t.processVariables.lost_close_on_no_employee){
				c_l_r=true;
			}
			if(t.processVariables.status_pre_lost==='-'){
				if(t.processVariables.status_employee===0){
					c_l_r=true;
				}
			}else if(t.processVariables.status_employee==0){
				c_l_r=true;
			}
			if(c_l_r){
				_promises.push(t.cjr_client_lost_details(_tr,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
			}
		}
		if(c_l_r){
			t.processVariables.lost_close_on_no_employee=false;
			let _faulttype=t.json_status.fault.faulttype;
			let _faultdescription=t.json_status.fault.faultdescription;
			let _faultgroupcode=t.json_status.fault.faultgroupcode;
			let _faultdevice=t.json_status.fault.faultdevice;
			let _tasklist=t.json_status.fault.fault_unique_id;
			t.json_status.fault.operators=[];
			return Promise.all(_promises).then(function(){
				_promises=[];
				let s_time=t.processVariables.statustime;
				if(t.processVariables.status_pre_lost==='-'){//üretim devam ederken kapanmış ve sonra açılmış
					t.processVariables.statustime=s_time;
				}
				//let _s_jr_day=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(s_time),false,s_time);
				//let _s_jr_code=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(s_time),true,s_time);
				//let _jrday=t.obj_jobrotation.day;
				//let _jrcode=t.obj_jobrotation.code;
				//if(_jrday!=_s_jr_day||_jrcode!=_s_jr_code){
				//	t.processVariables.statustime=t.getNextJobRotationFirstTime(_s_jr_day,_s_jr_code);
				//	if(t.processVariables.status_pre_lost==='-'){//üretim devam ederken kapanmış ve sonra açılmış
				//		t.processVariables.statustime=s_time;
				//	}
				//}
				t.lost_recursive=false;
				_promises.push(t.create_lost_records(_tr,_time,{losttype:t.processVariables.status_lost,descriptionlost:null,faulttype:_faulttype,faultdescription:_faultdescription,faultgroupcode:_faultgroupcode,faultdevice:_faultdevice,tasklist:_tasklist},s_time,t.processVariables.productions,t.tmp__type+'-controlDeviceStatus'));
				return Promise.all(_promises);
			});
		}else{
			return Promise.all(_promises);
		}
	}
	isEmployeeFreeManual(_tr,_obj_where,_controlLastSeen){
		//@@todo:parametredeki tanıma göre operatörün birden fazla cihazda çalışabilmesi sağlanacak
		let t=this;
		return t.db.employees.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }, {transaction: _tr}).then(function(eresult){
			if(eresult){
				if(eresult.finish===null){
					//başka yerde görevde olup olmadığı kontrol edilecek
					if(eresult.isexpert||eresult.ismaintenance||eresult.ismould||eresult.isquality){
						return eresult;
					}else if(eresult.client===null){
						//operatör boşta
						if(_controlLastSeen){
							//sistemde son görülme zamanı kontrol ediliyor
							let _ls=eresult.strlastseen;
							let _ftjr=t.getFirstTimeOfJobRotaion(t.obj_jobrotation.day,t.obj_jobrotation.code);
							if(_ls==null||(_ls<_ftjr)){
								_ls=_ftjr;
							}
							let v_ls_gap=Math.floor((t.tickTimer.obj_time.ms-new timezoneJS.Date(_ls).getTime())/1000);
							if(v_ls_gap>300){
								throw new Error('outofsystem|'+eresult.code+'|'+t.convertFullLocalDateString(_ls));
							}
						}
						return eresult;
					}else{
						//çalışılan istasyon-görev var
						let _m='';
						_m='<b>'+eresult.client+'</b> istasyonundaki işlemden ayrılmalısınız.';
						throw new Error(_m);
					}
				}else{
					throw new Error('Operatör işten ayrılmış.');
				}
			}else{
				//sistemde tanımlı bir operatör verisi değil
				t.mjd(_obj_where);
				throw new Error('Operatör bulunamadı.');
			}
		});
	}
	isEmployeeFree(_tr,emp,_controlLastSeen,_authemployee){
		let t=this;
		//if(emp.isexpert||emp.ismaintenance||emp.ismould||emp.isquality){//bakım personeli için de görev kontrolü yapılacak
		if((emp.isexpert||emp.ismould||emp.isquality)&&!(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40')){
			return emp;
		}else if(emp.client===null){
			//operatör boşta
			if(t.objConfig.dcs_server_IP.value==='10.0.0.101'&&(emp.code==='0400'||emp.code==='0523'||emp.ismaintenance)){
				//oskim 0400 sicili için neredeydin sormasın talebi için eklendi
				return emp;
			}
			if(_controlLastSeen){
				//sistemde son görülme zamanı kontrol ediliyor
				let _ls=emp.strlastseen;
				let _ftjr=t.getFirstTimeOfJobRotaion(t.obj_jobrotation.day,t.obj_jobrotation.code);
				if(_ls==null||(_ls<_ftjr)){
					_ls=_ftjr;
				}
				let v_ls_gap=Math.floor((t.tickTimer.obj_time.ms-new timezoneJS.Date(_ls).getTime())/1000);
				if(v_ls_gap>300){
					throw new Error('outofsystem|'+emp.code+'|'+t.convertFullLocalDateString(_ls));
				}
			}
			for(let j=0;j<t.processVariables.operators.length;j++){
				let op=t.processVariables.operators[j];
				if(op.code===emp.code){
					throw new Error('Zaten burada çalışmaktasınız!');
				}
			}
			if(emp.isexpert||emp.ismaintenance||emp.ismould||emp.isquality){
				return emp;
			}else{
				//if(t.ip.indexOf('10.10')>-1){
				if(t.ip&&t.ip.indexOf('10.10')>-1){
					let _start=t.convertFullLocalDateString(emp.start);
					if(_start<'2016-01-01 00:00:00'){
						return emp;
					}
				}
				if(t.objParam.task_OperationAuthorization.value===true){
					return Promise.all([t.operationAuthorization(_tr,emp,_authemployee)]).then(function(){
						try {
							fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
						} catch (error) {
							Sentry.captureException(error);
						}
						return null;
					});
				}else{
					try {
						fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
					} catch (error) {
						Sentry.captureException(error);
					}
					return null;
				}
			}
			//return emp;
		}else{
			//çalışılan istasyon-görev var
			let _m='';
			_m='<b>'+emp.client+'</b> istasyonundaki işlemden ayrılmalısınız.';
			throw new Error(_m);
		}
	}
	isEmployeeWork(_tr,_obj_where){
		let t=this;
		return t.db.employees.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }, {transaction: _tr}).then(function(result){
			if(result){
				let _res=result.dataValues;
				//başka yerde görevde olup olmadığı kontrol edilecek
				if(_res.isexpert||t.objConfig.workflow.value==='El İşçiliği'){
					return _res;
				}else if(_res.tasklist==null&&_res.client==null){
					//operatör boşta
					//throw new Error('Operatörün çalışma bilgisi bulunamadı.');
					t.mjd('Operatörün çalışma bilgisi bulunamadı.');
					return _res;
				}else{
					return _res;
				}
			}else{
				//sistemde tanımlı bir operatör verisi değil
				throw new Error('Operatör bulunamadı.');
			}
		});
	}
	operationAuthorization(_tr,_employee,_authemployee){
		let t=this;
		if(typeof _authemployee=='undefined'){
			_authemployee=null;
		}
		const _time=t.tickTimer.obj_time.strDateTime;
		const v_client=t.objConfig.client_name.value;
		if(t.processVariables.productions.length>0){
			let _tmp=[];
			for(let i=0;i<t.processVariables.productions.length;i++){
				let row=t.processVariables.productions[i];
				if(_tmp.indexOf(row.opname)==-1){
					_tmp.push(row.opname);
				}
			}
			let strSystemOut=(t.processVariables.status_work=='OP-OUT-SYSTEM'?' Sistem Dışı Çalışma':'');
			//online kontrol
			let obj={
				employee:_employee.code,
				opname:_tmp
			};
			const _timeout=t.self.isDevMachine?10000:8000;
			return t.getContent(t.self.serverHost,t.self.serverAppName,'OperationAuthorizationEmployee/checkEmployeeRight',obj,_timeout)
				.then((data) => {
					let json = JSON.parse(data);
					let results=json.records;
					if(results.length>0){
						for (let i = 0; i < results.length; i++) {
							let row=results[i];
							if(row.id==null||row.l1==0){
								//yetki tanımlı değilse bile seviye 1 gibi davranacak
								//json_status kontrol edilecek, orada var ise bir daha istenmeyecek
								let idx=t.getIndex(t.json_status.operationAuthorization.data,'employee|opname|level',_employee.code+'|'+row.name+'|'+1);
								if(idx===-1){
									let mes=new dcs_message({
										type:'operation_authorization_device_log'
										,ack:true
										,data:{
											time:_time
											,employee:_employee.code
											,client:v_client
											,opname:row.name
											,authemployee:null
											,type:'Sistem '+(t.objParam.task_OperationAuthorization.value===true?'Açık':'Kapalı')+' Yetki Tanımsız'+strSystemOut
										}
									});
									if(t.objParam.task_OperationAuthorization.value===false||t.objParam.task_OpAuth_notDefinedOperation.value===true){
										t.json_status.operationAuthorization.data.push({employee:_employee.code,opname:row.name,level:1});
										if(t.json_status.operationAuthorization.nextControl==null&&t.objParam.task_OperationAuthorization.value===true){
											let _d=new timezoneJS.Date(t.tickTimer.obj_time.strDateTime);
											_d.setMinutes(_d.getMinutes()+((t.ip==='192.168.1.40'||t.ip==='172.16.1.146')?5:(v_client.split('03.08').length>1?120:60)));
											t.json_status.operationAuthorization.nextControl=_d.toString();
										}
									}
									return t.db._sync_messages.create(mes.getdata()).then(()=>{
										if(t.objParam.task_OperationAuthorization.value===true&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
											if(t.objParam.task_OpAuth_notDefinedOperation.value===false){
												throw new Error('operationAuthorization|'+_employee.code+'|'+row.name+' Operasyonunda çalışmak için yetkinliğiniz yok!');
											}else{
												throw new Error('operationAuthorization|'+_employee.code+'|'+row.name+' Operasyonunda çalışmak için yetkinliğiniz yetersiz-Seviye 1!');
											}
										}
										mes=undefined;
										return null;
									});
								}
							}else{
								if(row.l2==0){
									//json_status kontrol edilecek, orada var ise bir daha istenmeyecek
									let idx=t.getIndex(t.json_status.operationAuthorization.data,'employee|opname|level',_employee.code+'|'+row.name+'|'+1);
									if(idx===-1){
										let mes=new dcs_message({
											type:'operation_authorization_device_log'
											,ack:true
											,data:{
												time:_time
												,employee:_employee.code
												,client:v_client
												,opname:row.name
												,authemployee:null
												,type:'Sistem '+(t.objParam.task_OperationAuthorization.value===true?'Açık':'Kapalı')+' Yetki '+(row.l1==1?'Yetersiz Seviye 1':'Tanımsız')+strSystemOut
											}
										});
										t.json_status.operationAuthorization.data.push({employee:_employee.code,opname:row.name,level:1});
										if(t.json_status.operationAuthorization.nextControl==null&&t.objParam.task_OperationAuthorization.value===true){
											let _d=new timezoneJS.Date(t.tickTimer.obj_time.strDateTime);
											_d.setMinutes(_d.getMinutes()+((t.ip==='192.168.1.40'||t.ip==='172.16.1.146')?5:(v_client.split('03.08').length>1?120:60)));
											t.json_status.operationAuthorization.nextControl=_d.toString();
										}
										return t.db._sync_messages.create(mes.getdata()).then(()=>{
											if(t.objParam.task_OperationAuthorization.value===true&&_authemployee==null&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
												throw new Error('operationAuthorization|'+_employee.code+'|'+row.name+' Operasyonunda çalışmak için yetkinliğiniz '+(row.l1==1?'yetersiz-Seviye 1':'yok')+'!');
											}
											mes=undefined;
											return null;
										});
									}
								}else{
									//json_status kontrol edilecek, orada var ise bir daha istenmeyecek
									if(row.l3==0){
										let idx=t.getIndex(t.json_status.operationAuthorization.data,'employee|opname|level',_employee.code+'|'+row.name+'|'+2);
										if(idx===-1){
											let mes=new dcs_message({
												type:'operation_authorization_device_log'
												,ack:true
												,data:{
													time:_time
													,employee:_employee.code
													,client:v_client
													,opname:row.name
													,authemployee:_authemployee
													,type:'Sistem '+(t.objParam.task_OperationAuthorization.value===true?'Açık':'Kapalı')+' Yetki Yetersiz Seviye 2'+strSystemOut
												}
											});
											t.json_status.operationAuthorization.data.push({employee:_employee.code,opname:row.name,level:2});
											return t.db._sync_messages.create(mes.getdata()).then(()=>{
												if(t.objParam.task_OperationAuthorization.value===true&&_authemployee==null&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
													throw new Error('operationAuthorization|'+_employee.code+'|'+row.name+' Operasyonunda çalışmak için yetkinliğiniz yetersiz-Seviye 2!');
												}
												mes=undefined;
												return null;
											});
										}
									}
								}
							}
						}
						return _employee;
					}else{
						let mes=new dcs_message({
							type:'operation_authorization_device_log'
							,ack:true
							,data:{
								time:_time
								,employee:_employee.code
								,client:v_client
								,opname:_tmp.join(',').substr(0,250)
								,authemployee:null
								,type:'Sistem '+(t.objParam.task_OperationAuthorization.value===true?'Açık':'Kapalı')+' Yetki Tanımsız'
							}
						});
						return t.db._sync_messages.create(mes.getdata()).then(()=>{
							mes=undefined;
							if(t.objParam.task_OperationAuthorization.value===true){
								if(_tmp.length>1){
									throw new Error('operationAuthorization|'+_employee.code+'|'+_tmp.join(',')+' Operasyonlarında çalışmak için yetkinliğiniz yok!');
								}else{
									throw new Error('operationAuthorization|'+_employee.code+'|'+_tmp.join(',')+' Operasyonunda çalışmak için yetkinliğiniz yok!');
								}
							}else{
								return _employee;
							}
						});
					}
				})
				.catch((err) => {
					throw new Error(err.message);
				});
		}else{
			return _employee;
		}
	}
	kontrol_kasa(_erpref,_opname){
		let t=this;
		let v_client=t.objConfig.client_name.value;
		const _time=t.tickTimer.obj_time.strDateTime;
		let _promises=[];
		if(t.objParam.task_production_case_stock_control.value===true&&(t.objConfig.dcs_server_IP.value==='10.10.0.10'||t.objConfig.dcs_server_IP.value==='192.168.1.40')){
			let sql=`select * from (
				select pt.code,pt.number,pt.name,pt.stockcode,pt.stockname,pt.packcapacity,0 requirement,pt.delivery,pt.pack
					,:client client,:erprefnumber erprefnumber
					,case when exists(select *
						from task_cases tc 
						where tc.movementdetail is null and tc.status='TCC' and tc.casetype='O' and tc.opname=pt.name
							and tc.client=:client and tc.erprefnumber=:erprefnumber) then true else false end caseexists
				from product_trees pt 
				where pt.materialtype='K' and pt.finish is null and pt.name=:name
			)ptc
			where ptc.caseexists=false
			order by ptc.stockcode`;
			let obj_requirements={
				client:v_client,
				erprefnumber:_erpref,
				name:_opname
			};
			return t.db.sequelize.query(sql, { replacements: obj_requirements, type: t.db.sequelize.QueryTypes.SELECT }).then(function(res_requirements){
				for(let i=0;i<res_requirements.length;i++){
					let _tmp_data_tc_o={
						client:res_requirements[i].client
						,movementdetail:null
						,erprefnumber:res_requirements[i].erprefnumber
						,casetype:'O'
						,opcode:res_requirements[i].code
						,opnumber:res_requirements[i].number
						,opname:res_requirements[i].name
						,stockcode:res_requirements[i]['stockcode']
						,stockname:res_requirements[i]['stockname']
						,quantityused:parseFloat(1)
						,casename:res_requirements[i]['pack']
						,casenumber:1
						,caselot:''
						,packcapacity:res_requirements[i]['packcapacity']
						,preptime:10
						,quantityremaining:0
						,quantitydeliver:0
						,conveyor:0
						,status:'TCC'
						,start:_time
						,record_id:uuidv4()
					};
					_promises.push(t.createRecord(null,'task_cases',_time,_tmp_data_tc_o));
				}
				return Promise.all(_promises);
			});
		}
	}
	kontrol_hammadde=function(_erpref,_opcode){
		let t=this;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		let arr_ret=[];
		if(t.objParam.task_production_case_stock_control.value===true&&(t.objConfig.dcs_server_IP.value==='10.10.0.10'||t.objConfig.dcs_server_IP.value==='192.168.1.40')){
			let sql=`select * from (
				select pt.code,pt.number,pt.name,pt.stockcode,pt.stockname,pt.quantity,0 requirement
					,:client client,:erprefnumber erprefnumber
					,(select COALESCE(sum(tc.quantityremaining),0) 
					from task_cases tc 
					where tc.movementdetail is null and tc.status='TCC' and tc.casetype='I' and tc.opcode=pt.code and tc.finish is null
						and tc.stockcode=pt.stockcode and tc.client=:client and tc.erprefnumber=:erprefnumber) quantityremaining
				from product_trees pt 
				where pt.materialtype='H' and pt.finish is null and pt.name<>pt.stockcode and pt.isdefault=true and pt.code=:code
			)ptc
			where ptc.quantityremaining=0
			order by ptc.stockcode`;
			let obj_requirements={
				client:v_client,
				erprefnumber:_erpref,
				code:_opcode
			};
			return t.db.sequelize.query(sql, { replacements: obj_requirements, type: t.db.sequelize.QueryTypes.SELECT }).then(function(res_requirements){
				for(let i=0;i<res_requirements.length;i++){
					t.hammaddeeksiklikleri.push(res_requirements[i]);
				}
				if(t.hammaddeeksiklikleri.length>0){
					setTimeout(() => {
						if(v_workflow!=='El İşçiliği'){
							t.eventListener({event:'device-lost-start',description:'Malzeme bitti.',selectedRow:{code:'MALZEME KALMADI'},op_transfer:true,status:t.generateClientStatus()});
						}
						setTimeout(() => {
							t.winmessend('cn_js_workflow',{event:'device-work|req_requirements',result:'ok',message:'',data:t.hammaddeeksiklikleri});
						}, 750);
					}, 2000);
				}
				return true;
			});
		}
		return arr_ret;
	}
	employeeJoin(_tr,_obj_where,_time,_label,_controlLastSeen,_authemployee){
		let t=this;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		let v_employee=0;
		let _promises=[];
		let kontrol_isg=function(v_client,_jrday,_jrcode,v_employee,v_erprefnumber,v_opname){
			if((t.processVariables.status_work==='OP'||(t.processVariables.status_work!=='-'&&v_workflow==='Lazer Kesim'))
				&&(t.processVariables.status_lost==='-'||t.processVariables.status_lost==='GÖREVDE KİMSE YOK'
				||t.processVariables.status_lost==='ÇAY MOLASI'||t.processVariables.status_lost==='YEMEK MOLASI') ){
				let obj_tmp={
					client:v_client
					,client2: '%'+v_client+'%'
					,day:_jrday
					,jobrotation:_jrcode
					,employee:v_employee
					,erprefnumber:v_erprefnumber
					,type:'GÖREVE KATILMA'
					,typeQuality:'KALİTE ONAY'
					,typeAutocontrol:'OTONOM KONTROL'
					,opname:v_opname
				};
				let sql_cq=`SELECT 'control_questions_isg' qtype,cq.id,cq.type,cq.opname,cq.question,cq.answertype,cq.valuerequire,cq.valuemax,cq.valuemin,cq.record_id,cq.documentnumber,cq.clients  
						,case cq.answertype when 'Evet-Hayır' then case when (cq.valuerequire='1' or cq.valuerequire='Evet') then 'Evet' else 'Hayır' end else cq.valuerequire end valuerequire 
				 		,case cq.answertype when 'Evet-Hayır' then 'radio' else 'text' end cm_row_type,cq.path 
				 		,case cq.answertype when 'Rakam Değer' then 'numpadex' when 'Evet-Hayır' then 'radio' else 'tr_TR' end cm_row_layout 
				 	from control_questions cq  
				 	left join control_answers ca on ca.type=cq.type and ca.question=cq.question and ca.answertype=cq.answertype 
						and ca.client=:client and ca.day=:day and ca.jobrotation=:jobrotation and ca.employee=:employee and ca.erprefnumber in (:erprefnumber) 
					left join control_answers cao on cao.type=cq.type and cao.question=cq.question and cao.answertype=cq.answertype 
						and cao.client=:client and cao.day=:day and cao.jobrotation=:jobrotation
				 	where (ca.id is null and ( (cq.type=:type and cq.clients like :client2) or (cq.type=:typeQuality and cq.opname in (:opname)) ) )
					 	or (cao.id is null and (cq.type=:typeAutocontrol and cq.clients like :client2)) 
				 	GROUP BY cq.id,cq.type,cq.opname,cq.question,cq.answertype,cq.valuerequire,cq.valuemax,cq.valuemin,cq.record_id,cq.documentnumber,cq.clients 
				 	order by case cq.type when 'control_questions_isg' then 0 else 1 end,cq.type,cq.question `;
				return t.db.sequelize.query(sql_cq, { replacements: obj_tmp, type: t.db.sequelize.QueryTypes.SELECT }).then(function(res_questions){
					return res_questions;
				});
			}
			return [];
		};
		return Promise.all([t.employeeAuthentication(_tr,_obj_where,_label)]).then(function(_emp){
			_promises=[];
			if(typeof _emp=='object'&&_emp.length>0){
				_emp=t.getLastElementOfArray(_emp);
				v_employee=_emp.code;
			}
			let _tmp=[];
			let _tmp_opname=[];
			for(let i=0;i<t.processVariables.productions.length;i++){
				let row=t.processVariables.productions[i];
				if(_tmp.indexOf(row.erprefnumber)==-1){
					_tmp.push(row.erprefnumber);
				}
				if(_tmp_opname.indexOf(row.opname)==-1){
					_tmp_opname.push(row.opname);
				}
			}
			return Promise.all([kontrol_isg(v_client,_jrday,_jrcode,v_employee,_tmp,_tmp_opname)]).then(function(res_questions){
				let result_questions=res_questions[0];
				//console.log(result_questions);
				_promises=[];
				if(result_questions.length>0){
					return {event:'device-employee-join|isg_questions',data:result_questions};
				}else{
					if(t.faultcodes.indexOf(t.processVariables.status_lost)>-1&&(((t.processVariables.status_lost=='ARIZA MAKİNE ELEKTRİK'||t.processVariables.status_lost=='ARIZA MAKİNE MEKANİK')&&_emp.ismaintenance==true)||(t.processVariables.status_lost=='ARIZA KALIP-APARAT'&&_emp.ismould==true))){
						let _obj_cld_update={
							type:'f_e_s'
							,client:v_client
							,day:_jrday
							,employee:v_employee
							,start:_time
							,faulttype:t.processVariables.status_lost
							,tasklist:t.json_status.fault.fault_unique_id?t.json_status.fault.fault_unique_id:''
							,taskcode:t.json_status.fault.taskcode?t.json_status.fault.taskcode:''
						};
						if(t.json_status.fault.operators){
							t.json_status.fault.operators.push({employee:v_employee,start:_time,finish:null});
						}
						t.json_status.fault.pos='f_e_s';
						t.json_status.fault.time=_time;
						_promises.push(t.find_fault_master_record(_tr,'employee_fault_start',_obj_cld_update));
						try {
							fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
						} catch (error) {
							Sentry.captureException(error);
						}
					}
					_promises.push(t.isEmployeeFree(_tr,_emp,_controlLastSeen,_authemployee));
					return Promise.all(_promises).then(function(){
						_promises=[];
						if(t.getIndex(t.processVariables.operators,'code',_emp.code)==-1){
							t.processVariables.operators.push(_emp);
							t.processVariables.status_employee++;
						}
						_promises.push(t.updateRecord(_tr,'employees',t.processVariables._time,{code:v_employee},{client:t.processVariables.v_client,day:t.processVariables._jrday,jobrotation:t.processVariables._jrcode,strlastseen:t.tickTimer.obj_time.strDateTime}));
						return Promise.all(_promises);
					}).then(function(){
						_promises=[];
						if(t.processVariables.status_workflowprocess!=='-'){
							let _cur_losttype=t.processVariables.status_lost;
							let c_obj_where={
								type:{[Op.in]: ['l_c', 'l_c_t']}
								,client:t.processVariables.v_client
								,start:{[Op.ne]:null}
								,finish:{[Op.eq]:null}
							};
							if(t.processVariables.status_workflowprocess==='SETUP'){
								if(t.processVariables.status_lost==='USTA BEKLEME'){
									_cur_losttype='SETUP-AYAR';
									//makine için kayıp başlatılması, eski kayıtların kapatılması
									_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,c_obj_where,{losttype:_cur_losttype},{finish:_time},true));
								}
							}else if(t.processVariables.status_workflowprocess==='QUALITY'){
								if(t.processVariables.status_lost==='KALİTE BEKLEME'){
									//mevcut duruşu kapat, kalite onay kaybı aç(makine,kalite personeli)
									_cur_losttype='KALİTE ONAY';
									//makine için kayıp başlatılması, eski kayıtların kapatılması
									_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,c_obj_where,{losttype:_cur_losttype},{finish:_time},true));
								}
							}else if(t.processVariables.status_workflowprocess==='SETUP-WORK-FINISH'){
								if(t.processVariables.status_lost==='USTA BEKLEME'){
									_cur_losttype='KALIP SÖKME';
									_promises.push(t.get_production_finish_data(_tr,_time));
									//makine için kayıp başlatılması, eski kayıtların kapatılması
									_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,c_obj_where,{losttype:_cur_losttype},{finish:_time},true));
								}
							}
							//personel için kalite onay kayıp kayıtlarının açılması
							c_obj_where.type={[Op.in]: ['l_c']};
							_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,c_obj_where,{type: 'l_e',employee:v_employee,losttype:_cur_losttype},{},false));
							c_obj_where.type={[Op.in]: ['l_c_t']};
							_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,c_obj_where,{type: 'l_e_t',employee:v_employee,losttype:_cur_losttype},{},false));
							t.setStatus(_cur_losttype);
						}else{
							if(t.processVariables.status_work!=='-'){
								let _tmp_opnames=[];
								for(let i=0;i<t.processVariables.productions.length;i++){
									let prod=t.processVariables.productions[i];
									let tmp_opname=prod.opname.replace('/','_');
									if(_tmp_opnames.indexOf(tmp_opname)==-1){
										_tmp_opnames.push(tmp_opname);
									}
									let _tmp_data_emp={
										type: 'e_p'+(t.processVariables.status_work=='OP-OUT-SYSTEM'?'_unconfirm':'')
										,client: v_client
										,day: _jrday
										,jobrotation: _jrcode
										,tasklist: prod.tasklist
										,time : _time
										,mould: prod.mould
										,mouldgroup: prod.mouldgroup
										,opcode: prod.opcode
										,opnumber: prod.opnumber
										,opname: prod.opname
										,opdescription: prod.opdescription
										,leafmask: (v_workflow=='Üretim Aparatsız'||prod.leafmask==null?'01':prod.leafmask)
										,leafmaskcurrent: (v_workflow=='Üretim Aparatsız'||prod.leafmaskcurrent==null?'01':prod.leafmaskcurrent)
										,production: 0
										,productioncurrent: 0
										,employee: v_employee
										,erprefnumber: prod.erprefnumber
										,taskfromerp: prod.taskfromerp
										,calculatedtpp:prod.t_p_p
										,gap: 0
										,start: _time
										,finish: null
										,record_id: uuidv4()
										,energy: null
										,taskfinishtype: null
										,taskfinishdescription: null
									};
									_promises.push(t.createRecord(_tr,'client_production_details',_time,_tmp_data_emp));
								}
								if(_tmp_opnames.length>0&&v_employee!==0&&_jrday!==false&&_jrcode!==false&&t.processVariables.status_lost=='GÖREVDE KİMSE YOK'){
									_promises.push(t.get_sop_documents_on_join(v_client,_jrday,_jrcode,_tmp_opnames,v_employee,true));
								}
							}
							//göreve katılma olmuş ise kayıpları kapatır, kayba katılma olmuş ise kayıp kayıtları oluşturur
							if(t.processVariables.status_lost!=='-'){
								const _obj_where={
									type:{[Op.in]: ['l_c', 'l_c_t']}
									,client:v_client
									,start:{[Op.ne]:null}
									,finish:{[Op.eq]:null}
								};
								if(t.processVariables.status_work!=='-'&&t.processVariables.status_lost=='GÖREVDE KİMSE YOK'){
									//göreve katılma esnasında tesellüm evet olan istasyonda task_case kaydı kontrol edilecek
									if(t.objParam.task_delivery.value===true&&t.objConfig.dcs_server_IP.value==='10.10.0.10'){
										_promises.push(t.control_task_cases(_tr));
									}
									_promises.push(t.cjr_client_lost_details(_tr,_obj_where,_time));
								}else{
									_obj_where.type={[Op.in]: ['l_c']};
									_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{type: 'l_e',employee:v_employee},{},false));
									_obj_where.type={[Op.in]: ['l_c_t']};
									_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{type: 'l_e_t',employee:v_employee},{},false));
								}
							}
						}
						return Promise.all(_promises);
					}).then(function(){
						if(t.processVariables.status_work!=='-'&&t.processVariables.status_lost=='GÖREVDE KİMSE YOK'){
							if(t.objParam.task_materialpreparation.value===true&&t.processVariables.status_employee==1&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
								if(t.timer_missing_material!==false){
									clearTimeout(t.timer_missing_material);
									t.timer_missing_material=false;
								}
								setTimeout(() => {
									t.control_missing_materials('waitmaterial',3);
								}, 5000);
							}
							t.setStatus('-');
						}
						return Promise.all([t.controlDeviceStatus(_tr,_time,'join')]);
					});
				}
			});
		});
	}
	employeeLeave(_tr,_obj_where,_time,_change_jobrotation){
		let t=this;
		let v_employee=0;
		const v_client=t.objConfig.client_name.value;
		const _jrday=t.obj_jobrotation.day;
		const _jrcode=t.obj_jobrotation.code;
		const v_workflow=t.objConfig.workflow.value;
		let _promises=[];
		return Promise.all([t.isEmployeeWork(_tr,_obj_where)]).then(function(_obj_res){
			let x=t.getLastElementOfArray(_obj_res);
			_promises=[];
			v_employee=x.code;
			if(t.faultcodes.indexOf(t.processVariables.status_lost)>-1&&(((t.processVariables.status_lost=='ARIZA MAKİNE ELEKTRİK'||t.processVariables.status_lost=='ARIZA MAKİNE MEKANİK')&&x.ismaintenance==true)||(t.processVariables.status_lost=='ARIZA KALIP-APARAT'&&x.ismould==true))){
				let _start=null;
				if(t.json_status.fault.operators&&t.json_status.fault.operators.length>0){
					let idx=t.getIndex(t.json_status.fault.operators,'employee',v_employee);
					if(idx>-1){
						_start=t.json_status.fault.operators[idx].start;
						t.json_status.fault.operators.splice(idx,1);
					}
				}
				let _obj_cld_update={
					type:'f_e_f'
					,client:v_client
					,day:_jrday
					,employee:v_employee
					,finish:_time
					,faulttype:t.processVariables.status_lost
					,tasklist:t.json_status.fault.fault_unique_id?t.json_status.fault.fault_unique_id:''
					,taskcode:t.json_status.fault.taskcode?t.json_status.fault.taskcode:''
				};
				if(_start!==null){
					_obj_cld_update.start=_start;
				}
				t.json_status.fault.pos='f_e_f';
				t.json_status.fault.time=_time;
				_promises.push(t.find_fault_master_record(_tr,'employee_fault_finish',_obj_cld_update));
				try {
					fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
				} catch (error) {
					Sentry.captureException(error);
				}
			}
			if((t.processVariables.status_work!=='-'
			&& (t.objParam.process_wfSetup.value===false||(t.objParam.process_wfSetup.value===true&&t.processVariables.status_workflowprocess==='-') )
			)||(v_workflow==='El İşçiliği')){
				let _obj_where_t_p_d={
					type:'e_p'+(t.processVariables.status_work=='OP-OUT-SYSTEM'?'_unconfirm':'')
					,employee:v_employee
					,start:{[Op.ne]:null}
					,finish:{[Op.eq]:null}
				};
				_promises.push(t.cjr_client_production_details(_tr,_obj_where_t_p_d,_time));
			}
			if(t.processVariables.status_lost!=='-'||v_workflow==='El İşçiliği'){
				let _arr_in=v_workflow!=='El İşçiliği'?['l_e', 'l_e_t']:['l_e', 'l_e_t'/*, 'l_c_t'*/];
				let _obj_where_t_l_d={
					type:{[Op.in]: _arr_in}
					,employee:v_employee
					,start:{[Op.ne]:null}
					,finish:{[Op.eq]:null}
				};
				_promises.push(t.cjr_client_lost_details(_tr,_obj_where_t_l_d,_time));
			}
			_promises.push(t.updateRecord(_tr,'employees',_time,{code:v_employee},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:_time}));
			return Promise.all(_promises).then(function () {
				if(v_workflow==='El İşçiliği'){
					return null;
				}
				_promises=[];
				if(t.processVariables.status_employee>0){
					t.processVariables.status_employee--;
				}
				for(let j=0;j<t.processVariables.operators.length;j++){
					let op=t.processVariables.operators[j];
					if(op.code==v_employee){
						t.processVariables.operators.splice(j,1);
					}
				}
				//istasyonda kimse kaldı mı kontrol ediliyor
				if(t.processVariables.status_employee>0){
					return null;
				}else{
					//@@todo: tatil durumu için ekleme yapılacak kimse kalmadığında tatil dönemi ise kayıp kodu olarak tatil olacak
					if(t.timer_missing_material!==false){
						clearTimeout(t.timer_missing_material);
						t.timer_missing_material=false;
					}
					// 20220311 kimse kalmadığında vardiya sonuna 20 dakikadan az var ise mail gidecek
					let ltjr=t.getLastTimeOfJobRotaion(_jrday,_jrcode);
					let _s=new timezoneJS.Date(t.tickTimer.obj_time.strDateTime);
					let _f=new timezoneJS.Date(ltjr);
					let _diff = Math.abs(_f.getTime() - _s.getTime());
					if(Math.ceil(_diff/(1000))<=20*60){
						let mes=new dcs_message({
							type:'client_no_employee'
							,ack:true
							,data:{
								client:v_client
								,code:x.code
								,name:x.name
								,time:t.tickTimer.obj_time.strDateTime
							}
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					}
					t.processVariables.lost_close_on_no_employee=false;
					return t.db.lost_types.findOne({where:{code:t.processVariables.status_lost}},{transaction: _tr}).then(function(res_lt){
						if(res_lt){
							t.processVariables.lost_close_on_no_employee=res_lt.closenoemployee;
						}else{
							if(t.processVariables.status_lost=='YETKİNLİK GÖZLEM'){
								t.processVariables.lost_close_on_no_employee=true;
							}
						}
						return null;
					}).then(function(){
						if(t.faultcodes.indexOf(t.processVariables.status_lost)>-1){
							_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c', 'l_c_t','l_e','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
						}else{
							if(t.processVariables.status_workflowprocess==='-'){
								if(t.processVariables.lost_close_on_no_employee){
									if(t.processVariables.status_work!=='-'){
										t.setStatus('GÖREVDE KİMSE YOK');
										t.cpd_time=false;
										t.processVariables.lastprodstarttime=null;
									}else{
										t.setStatus('İŞ YOK');
										t.processVariables.productions=[];
										t.cpd_time=false;
										t.processVariables.lastprodstarttime=null;
										//t.processVariables.lastprodtime=null;
										_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c', 'l_c_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
									}
								}else{
									if(t.processVariables.status_work!=='-'){
										//çalışma devam ederken görevde kimse kalmadı
										if(t.processVariables.status_lost=='-'){
											t.setStatus('GÖREVDE KİMSE YOK');
											t.cpd_time=false;
										}else if(t.processVariables.status_lost==''){
											//çalışma içinde sebebi belli olmayan duruş devam ederken görevde kimse kalmadı
											t.setStatus('GÖREVDE KİMSE YOK');
											t.cpd_time=false;
										}
										t.processVariables.lastprodstarttime=null;
									}
								}
							}else{
								if(t.processVariables.status_workflowprocess==='SETUP'){
									//kalıp takma esnasında kimse kalmadı durumu
									t.setStatus('USTA BEKLEME');
								}
								if(t.processVariables.status_workflowprocess==='QUALITY'){
									if(t.objParam.process_remoteQualityConfirmation.value!==true){
										//kalite onay esnasında kimse kalmadı durumu
										t.setStatus('KALİTE BEKLEME');
									}
								}
								if(t.processVariables.status_workflowprocess==='SETUP-WORK-FINISH'){
									//kalıp sökme esnasında kimse kalmadı
									t.setStatus('USTA BEKLEME');
								}
								_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c', 'l_c_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
							}
						}
						if(t.objParam.task_finishnoemployee.value===true&&!_change_jobrotation){
							let _obj_where_t_p_d={
								type:'c_p'+(t.processVariables.status_work=='OP-OUT-SYSTEM'?'_unconfirm':'')
								,start:{[Op.ne]:null}
								,finish:{[Op.eq]:null}
							};
							_promises.push(t.cjr_client_production_details(_tr,_obj_where_t_p_d,_time));
							_promises.push(t.create_production_records(_tr,_time));
						}
						return Promise.all(_promises);		
					}).then(function(){
						_promises=[];
						if(t.processVariables.status_work!=='-'&&t.processVariables.status_workflowprocess==='-'){
							if(t.processVariables.status_pre_lost!=='-'||(t.processVariables.status_lost=='MALZEME BEKLEME'||t.processVariables.status_lost=='PLANLI TAŞIMA BEKLEME')){// kayıp varken ayrılma-vardiya sonu
								_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c', 'l_c_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
							}
							for(let i=0;i<t.processVariables.productions.length;i++){
								let item=t.processVariables.productions[i];
								item.productdoneactivity=0;
								item.scrapactivity=0;
								let _obj_update_tasklist={productdoneactivity:0,scrapactivity:0};
								_promises.push(t.updateRecord(_tr,'task_lists',_time,{code:item.tasklist},_obj_update_tasklist));
							}
							return Promise.all(_promises);
						}else{
							return null;
						}
					});
				}
			});
		});
	}
	employeeAuthentication(_tr,_obj_where,_label){
		console.log('employeeAuthentication:',_obj_where,_label);
		let t=this;
		const _time=t.tickTimer.obj_time.strDateTime;
		return t.db.employees.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where }, {transaction: _tr}).then(function(result){
			if(result){
				if(result.finish==null){
					if( (t.objConfig.dcs_server_IP.value==='10.10.0.10'||t.objConfig.dcs_server_IP.value==='192.168.1.40') ){
						const raportarihi=t.convertFullLocalDateString(result.dataValues.lastseen);
						if(_time<raportarihi){
							throw new Error('Rapor Bitiş Tarihi: '+raportarihi+'.<br>Bu zamandan önce sisteme giriş yapamazsınız.');
						}
					}
					return result.dataValues;
				}else{
					throw new Error('Operatör işten ayrılmış.');
				}
			}else{
				//sistemde tanımlı bir operatör verisi değil
				throw new Error(_label+' yetkisine sahip bir sicil bulunamadı.');
			}
		});
	}
	setAllOutputs(){
		//cihazın tüm çıkışlarını aktif eder
		let t=this;
		if(t.objConfig.workflow.value==='El İşçiliği'||t.objConfig.workflow.value==='Kataforez'){
			return null;
		}
		if(t.iodevice_manager.outputPorts){
			for(let k=0;k<t.iodevice_manager.outputPorts.length;k++){
				let _cur_port=t.iodevice_manager.outputPorts[k];
				//if(parseInt(_cur_port.curval)===0){
					t.emit('setoutput', _cur_port.number, 1);
					console.log('setAllOutputs:',_cur_port.number, 1);
				//}
			}
		}
	}
	setDeviceOutputs(/*_tr*/){
		//cihazın durumuna göre aktif-pasif yapıalacak olan çıkış portlarını belirler
		let t=this;
		const v_workflow=t.objConfig.workflow.value;
		if(v_workflow==='El İşçiliği'||v_workflow==='Kataforez'){
			return null;
		}
		let _openAllPortsLostTypes=['ARIZA KALIP-APARAT','ARIZA MAKİNE ELEKTRİK','ARIZA MAKİNE MEKANİK','ARIZA ÜRETİM','VERİMOT HARİCİ ÇALIŞMA','PLANLI BAKIM','DENEMELER','DENEME','DENEME ÜRETİM','NUMUNE URETIM'];
		if(t.show_scrap_before_finish||t.show_delivery||v_workflow=='El İşçiliği'||typeof t.client_control_document.length==='number'){
			return null;
		}
		const v_client=t.objConfig.client_name.value;
		const c_unused_port_val=1;
		//robot ise, çalışma olmayan çıkış portları her zaman 1 olmalı
		let _io_ports=[];
		//kullanımda olmayan portlar için c_unused_port_val değeri atanıyor
		if(t.iodevice_manager&&t.iodevice_manager.outputPorts){
			for(let k=0;k<t.iodevice_manager.outputPorts.length;k++){
				let _cur_port=t.iodevice_manager.outputPorts[k];
				let _idx=t.getIndex(t.ports.outputs,'portnumber',_cur_port.number);
				if(_idx===-1){
					_io_ports.push({portnumber:_cur_port.number,value:c_unused_port_val});
				}
			}
		}
		let getLostTypeRecord=function(_lost){
			let _sql='select * from lost_types where finish is null and code=:code';
			return t.db.sequelize.query(_sql,{replacements:{code:_lost}, type: t.db.sequelize.QueryTypes.SELECT}).then(function(record){
				if(record.length>0){
					record=record[0];
					if(record.isopenallports===true){
						if(_openAllPortsLostTypes.indexOf(record.code)===-1){
							_openAllPortsLostTypes.push(record.code);
						}
					}
				}
			});
		};
		return Promise.all([getLostTypeRecord(t.processVariables.status_lost),t.getLostIOEvent(/*_tr,*/t.processVariables.status_lost)]).then(function(res_cur){
			let _res_cur_events=t.getLastElementOfArray(res_cur);
			//console.log('getLostIOEvent_return:');
			//console.log(_res_cur_events);
			if(_res_cur_events.length>0){
				//kaybın olay tanımı ile uyumlu portları kapatılacak
				if(t.iodevice_manager&&t.iodevice_manager.outputPorts){
					for(let k=0;k<t.iodevice_manager.outputPorts.length;k++){
						let _cur_port=t.iodevice_manager.outputPorts[k];
						let _idx=t.getIndex(_io_ports,'portnumber',_cur_port.number);
						if(_idx===-1){
							let idx_cp=t.getIndex(t.ports.outputs,'portnumber',_cur_port.number);
							if(idx_cp>-1){
								let _p=t.ports.outputs[idx_cp];
								if(_res_cur_events.indexOf(_p.ioevent)>-1){
									_io_ports.push({portnumber:_cur_port.number,value:1});
								}
							}
						}
					}
				}
			}else{
				//portları aç
				let _flag_for_port_open=true;
				if(t.processVariables.status_lost=='ARIZA MAKİNE ELEKTRİK'||t.processVariables.status_lost=='ARIZA MAKİNE MEKANİK'){
					let _flag_required_operator_exists=false;
					for(let i=0;i<t.processVariables.operators.length;i++){
						let _op_item=t.processVariables.operators[i];
						if(!_flag_required_operator_exists){
							if(_op_item.ismaintenance){
								_flag_required_operator_exists=true;
							}
						}
					}
					_flag_for_port_open=_flag_required_operator_exists;
				}
				if(t.processVariables.status_lost=='ARIZA KALIP-APARAT'){
					let _flag_required_operator_exists=false;
					for(let i=0;i<t.processVariables.operators.length;i++){
						let _op_item=t.processVariables.operators[i];
						if(!_flag_required_operator_exists){
							if(_op_item.ismould){
								_flag_required_operator_exists=true;
							}
						}
					}
					_flag_for_port_open=_flag_required_operator_exists;
				}
				if(t.processVariables.status_lost=='ARIZA ÜRETİM'){
					let _flag_required_operator_exists=false;
					for(let i=0;i<t.processVariables.operators.length;i++){
						let _op_item=t.processVariables.operators[i];
						if(!_flag_required_operator_exists){
							if(_op_item.isexpert){
								_flag_required_operator_exists=true;
							}
						}
					}
					_flag_for_port_open=_flag_required_operator_exists;
				}
				if(t.iodevice_manager&&t.iodevice_manager.outputPorts){
					for(let k=0;k<t.iodevice_manager.outputPorts.length;k++){
						let _cur_port=t.iodevice_manager.outputPorts[k];
						let _idx=t.getIndex(_io_ports,'portnumber',_cur_port.number);
						if(_idx===-1){
							if(_openAllPortsLostTypes.indexOf(t.processVariables.status_lost)>-1||t.processVariables.status_work==='OP-OUT-SYSTEM'){
								//cihazda tanımlı tüm portları serbest bırak
								_io_ports.push({portnumber:_cur_port.number,value:((t.processVariables.operators.length>0&&_flag_for_port_open)?0:1)});
							}else{
								//sadece tanımlı iş için serbest bırak
								let idx_cp=t.getIndex(t.ports.outputs,'portnumber',_cur_port.number);
								if(idx_cp>-1){
									let _p=t.ports.outputs[idx_cp];
									if(_p.ioevent==='output'){
										if((v_workflow==='Sabit Üretim Aparatlı'||v_workflow==='CNC')&&_p.code!==v_client){
											const _idx=t.getIndex(t.processVariables.productions,'mould',_p.code);
											if(_idx===-1){
												_io_ports.push({portnumber:_cur_port.number,value:1});
											}else{
												_io_ports.push({portnumber:_cur_port.number,value:0});
											}
										}else{
											_io_ports.push({portnumber:_cur_port.number,value:0});
										}
									}
								}
							}
						}
					}
				}
			}
			for(let l=0;l<_io_ports.length;l++){
				const _item=_io_ports[l];
				const _idx_io=t.getIndex(t.iodevice_manager.outputPorts,'number',_item.portnumber);
				//console.log('_idx_io:'+_idx_io);
				if(_idx_io>-1){
					const _p=t.iodevice_manager.outputPorts[_idx_io];
					if(t.self.json_config.tcp_io){
						let _idx_o_=t.getIndex(t.ports.outputs,'portnumber',_item.portnumber);
						if(_idx_o_>-1){
							if(t.ports.outputs.length==1||(t.ports.outputs.length>1&&t.ports.outputs[_idx_o_].code==v_client)){
								let _io_tcp_val=_item.value;
								console.log('output_trigger_tcp:',_io_tcp_val);
								setTimeout(() => {
									t.iodevice_manager_tcp.send({
										event: 'output_trigger',
										pid: t.iodevice_manager_tcp.pid,
										value: _io_tcp_val,
										number: 1
									});
								}, 10);
							}
						}
					}
					if(parseInt(_p.curval)!==parseInt(_item.value)){
						t.emit('setoutput', _item.portnumber, _item.value);
						console.log('setoutput_1:',_item.portnumber, _item.value);
					}else{
						//console.log('_p.curval=_item.value');
					}
				}else{
					t.emit('setoutput', _item.portnumber, _item.value);
					console.log('setoutput_2:',_item.portnumber, _item.value);
				}
			}
		});
	}
	getLostIOEvent(/*_tr,*/_losttype){
		let t=this;
		//console.log('getLostIOEvent:'+_losttype);
		if(_losttype=='-'||_losttype==''){
			if(t.processVariables.status_work!=='-'&&t.processVariables.status_work!=='OP-OUT-SYSTEM'&&t.processVariables.status_employee!==0&&t.processVariables.status_workflowprocess==='-'){
				if(t.objConfig.workflow.value==='Lazer Kesim'){
					return [];
				}else if(t.objConfig.workflow.value==='CNC'&&t.arr_inputready.length>0){
					return [];
				}else{
					let v_pd_gap=Math.ceil((t.tickTimer.obj_time.ms-new timezoneJS.Date(t.processVariables.lastprodstarttime).getTime())/1000);
					if(v_pd_gap<=(t.processVariables.inputready>0?t.processVariables.inputready:t.processVariables.status_tpp)){
						//console.log('getLostIOEvent_return-1');
						return [];
					}else{
						//console.log('getLostIOEvent_return-2');
						return ['output'];
					}
				}
			}else{
				//console.log('getLostIOEvent_return-3');
				return [];
			}
		}else{
			return t.db.lost_types.findOne({where:{code:_losttype}}/*,{transaction: _tr}*/).then(function(res_lt){
				let _arr_events=[];
				if(res_lt){
					if(t.objParam.process_remoteQualityConfirmation.value===true&&_losttype==='KALİTE ONAY'){
						return ['output'];
					}else{
						_arr_events=res_lt.ioevent==null||res_lt.ioevent==''?[]:res_lt.ioevent.split(',');
						//console.log('getLostIOEvent_return-4');
						return _arr_events;
					}
				}else{
					//console.log('getLostIOEvent_return-5');
					return ['output'];
				}
			});
		}
	}
	processInputSignal(_item){
		let t=this;
		let _event='input_received';
		let _time=t.tickTimer.obj_time.strDateTime;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		t.status=t.status==''?'processInputSignal':t.status;
		if(!_jrcode){
			_jrday=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(_time),false,_time);
			_jrcode=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(_time),true,_time);
		}
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		let v_arr_productions=false;
		let v_flag_finish=false;
		let v_arr_finished=[];
		//console.log('t.processVariables.productions.length:'+t.processVariables.productions.length);
		//console.log('t.processVariables.status_workflowprocess:'+t.processVariables.status_workflowprocess);
		if(t.processVariables.productions.length==0||t.processVariables.status_workflowprocess!=='-'||t.processVariables.status_work=='OP-OUT-SYSTEM'){
			return null;
		}
		//console.log(_item);
		//elle başlatılan kayıplarda gelen üretim sinyallerini dikkate alma-arıza ise
		if((t.processVariables.status_lost!='-'&&t.processVariables.status_lost!=''&&t.processVariables.status_work!='-'&&t.faultcodes.indexOf(t.processVariables.status_lost)>-1)){
			return null;
		}
		let _idx_port=t.getIndex(t.arr_inputs_for_input_signal_after_lock,'number',_item.portnumber);
		if((t.processVariables.status_lost==''||t.processVariables.status_lost=='GÖREVDE KİMSE YOK')&&t.processVariables.lastprodstarttime!==null&&t.processVariables.status_work!='-'){
			if(_idx_port>-1){
				if(t.arr_inputs_for_input_signal_after_lock[_idx_port].value===true){
					t.setAllOutputs();
					return null;
				}else{
					if(!t.show_delivery){//tesellüm ekranı açık değilse gelen ilk sinyalde port işaretle,açık ise gelen tüm sinyalleri al
						//kayıp başladıktan sonra gelen ilk sinyal tasklist tablosunu güncellesin
						t.arr_inputs_for_input_signal_after_lock[_idx_port].value=true;
						t.setAllOutputs();
					}else{
						t.arr_inputs_for_input_signal_after_lock[_idx_port].value=false;
					}
				}
			}
		}else{
			if(_idx_port>-1){
				t.arr_inputs_for_input_signal_after_lock[_idx_port].value=false;
			}
		}
		let getmoulddetail=function(_code){
			//kalıp detayındaki operasyonlar array olarak geri dönecek
			//bu dizi içindeki operasyonlar client_production_details içinden aranacak ve güncellemesi yapılacak
			if(v_workflow==='Üretim Aparatsız'||v_workflow==='Lazer Kesim'){
				return t.processVariables.productions;
			}
			if(v_workflow==='Değişken Üretim Aparatlı'){
				// bağlı kalıp için maske kontrol edilecek
				return t.processVariables.productions;
			}
			if(v_workflow==='Sabit Üretim Aparatlı'){
				// item.portnumber ile üretimin geldiği kalıp için maske kontrol edilecek
				// t.ports.inputs içinden hangi kalıp olduğu belirlenecek 
				let arr_tmp=[];
				for(let i=0;i<t.processVariables.productions.length;i++){
					let row=t.processVariables.productions[i];
					if(row.mould===_code){
						arr_tmp.push(row);
					}
				}
				return arr_tmp;
			}
		};

		let update_cpd=function(_tr,obj_where,_prod,_prod_current){
			let __promises=[];
			return t.db.client_production_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:obj_where}).then(function(res_cpd){
				for (let i = 0; i < res_cpd.length; i++) {
					let result=res_cpd[i];
					__promises.push(t.updateRecord(_tr,'client_production_details',_time,{record_id:result.record_id},{production:result.production+_prod,productioncurrent:result.productioncurrent+_prod_current}));
				}
				return null;
			}).then(function(){
				if(__promises.length>0){
					return Promise.all(__promises);
				}else{
					return null;
				}
			});
		};
		let _promises=[];
		t.db.sequelize.transaction(function (_tr) {
			// let tn='';
			let arrtn=[];
			let ctpp=0;
			for(let i=0;i<t.processVariables.productions.length;i++){
				if(arrtn.indexOf(t.processVariables.productions[i].opname)===-1){
					arrtn.push(t.processVariables.productions[i].opname);
				}
				// tn+=(tn==''?'':',')+t.processVariables.productions[i].opname;
				if(v_workflow==='Lazer Kesim'){
					ctpp+=t.processVariables.productions[i].t_p_p;
				}else{
					if(ctpp===0){
						ctpp=t.processVariables.productions[i].t_p_p;
					}
				}
			}
			// c_p, e_p kayıtları yazılacak
			_promises=[];
			let arr_ret=getmoulddetail(_item.code);
			v_arr_productions=arr_ret;
			if(arr_ret.length>0&&t.arr_inputready.length>0&&v_workflow!=='Lazer Kesim'){
				t.arr_inputready.shift();
				console.log('arr_inputready.shift_input:'+t.arr_inputready.length);
			}
			for(let i=0;i<arr_ret.length;i++){
				let row=arr_ret[i];
				//çıktı miktarını belirle
				let _prod=0;
				let _prod_current=0;
				if(typeof row.step==='undefined'){
					_prod=parseInt(row.leafmask);
					_prod_current=parseInt(row.leafmaskcurrent);
				}else{
					// adıma göre üretim sayısı belirleme
					let _step=2;
					row.step=parseInt(row.leafmask.length/_step)>parseInt(row.step)?parseInt(row.step):0;
					_prod=parseInt(row.leafmask.substr(row.step*_step,_step));
					_prod_current=parseInt(row.leafmaskcurrent.substr(row.step*_step,_step));
					row.step++;
				}
				if(_prod>0||_prod_current>0){
					if(t.arr_inputs_for_input_signal_after_lock[_idx_port].value===false){
						t.processVariables.lastprodstarttime=_time;
						t.processVariables.lastprodtime=_time;
						if(t.status=='processInputSignal'){
							_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{lastprodstarttime:_time,lastprodtime:_time}));
						}
					}
					row.productdonecount+=_prod_current;
					row.productdoneactivity+=_prod_current;
					row.productdonejobrotation+=_prod_current;
					row.remaining-=_prod_current;
					let last_task=null;
					if(t.last_state.tasks.length>0){
						for(let counter=0;counter<t.last_state.tasks.length;counter++){
							last_task=t.last_state.tasks[counter];
							if(last_task.erprefnumber===row.erprefnumber&&last_task.opname===row.opname){
								//last_task.productcount=row.productcount;
								last_task.productdonecount=row.productdonecount;
								last_task.production=_prod;
							}
						}
					}
					if(last_task!==null&&typeof last_task.production!=='undefined'){
						let mes=new dcs_message({
							type:'device_state_change'
							,ack:true
							,data:{client:t.last_state.client,time:t.last_state.time,state:'Üretiyor',losttype:t.last_state.losttype,tasks:(t.last_state.losttype==='İŞ YOK'?[]:[last_task])}
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					}
					// if(row.productdonecount%2===0&&v_client==='03.06.0002'&&t.objConfig.dcs_server_IP.value==='192.168.1.10'){
					// 	setTimeout(function() {
					// 		t.eventListener({event:'device-lost-start',description:row.opname+' frekansiyel kontrol',selectedRow:{code:'FREKANSİYEL KONTROL'},op_transfer:true,status:t.generateClientStatus()});
					// 	}, 1000);
					// 	setTimeout(function() {
					// 		t.eventListener({event:'device_init'});
					// 		setTimeout(function(){
					// 			t.winmessend('cn_js_workflow',{event:'show_message',result:'ok',title:'Lütfen Dikkat',text:row.productdonecount+' Adet baskıya ulaşıldı. '+row.opname+' Parça için Tork Ölçüm Sonucu Giriniz:',type:'info',showConfirmButton:true,event_cb:false});
					// 		}, 500);
					// 	}, 2000);
					// }
					if(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.self.isDevMachine){
						if(row.productdonecount%100===0&&v_client==='HP011'){
							setTimeout(function() {
								t.eventListener({event:'device-lost-start',description:row.opname+' frekansiyel kontrol',selectedRow:{code:'FREKANSİYEL KONTROL'},op_transfer:true,status:t.generateClientStatus()});
							}, 1000);
							setTimeout(function() {
								t.eventListener({event:'device_init'});
								setTimeout(function(){
									t.winmessend('cn_js_workflow',{event:'show_input',result:'ok',title:'Lütfen Dikkat',text:row.productdonecount+' Adet baskıya ulaşıldı. '+row.opname+' Parça için Tork Ölçüm Sonucu Girmelisiniz.',type:'info',showConfirmButton:true,event_cb:false});
								}, 500);
							}, 2000);
						}
						if(row.productdonecount%100===0&&v_client==='PK004'){
							setTimeout(function() {
								t.eventListener({event:'device-lost-start',description:row.opname+' frekansiyel kontrol',selectedRow:{code:'FREKANSİYEL KONTROL'},op_transfer:true,status:t.generateClientStatus()});
							}, 1000);
							setTimeout(function() {
								t.eventListener({event:'device_init'});
								setTimeout(function(){
									t.winmessend('cn_js_workflow',{event:'show_input',result:'ok',title:'Lütfen Dikkat',text:row.productdonecount+' Adet baskıya ulaşıldı. '+row.opname+' Kaynak Kopma Test Sonucu Girmelisiniz.',type:'info',showConfirmButton:true,event_cb:false});
								}, 500);
							}, 2000);
						}
						if((row.productdonecount%30===0/*||(row.productdonecount%5===0)*/)&&row.productdonecount!==row.production&&row.opname==='55509P-50'){
							setTimeout(function() {
								t.eventListener({event:'device-lost-start',description:row.opname+' frekansiyel kontrol',selectedRow:{code:'FREKANSİYEL KONTROL'},op_transfer:true,status:t.generateClientStatus()});
							}, 1000);
							setTimeout(function() {
								t.eventListener({event:'device_init'});
								setTimeout(function(){
									t.winmessend('cn_js_workflow',{event:'show_message',result:'ok',title:'Lütfen Dikkat',text:'Dişi çeliğe (kaplamalı olan) hava tutun, kuru bezle silin, talaş kalmadığından emin olun.',type:'info',showConfirmButton:true,event_cb:'device-lost-finish'});
								}, 500);
							}, 2000);
						}
						if((row.productdonecount%50===0/*||(row.productdonecount%5===0)*/)&&row.productdonecount!==row.production){
							if(row.opname==='55509P-60'){
								setTimeout(function() {
									t.eventListener({event:'device-lost-start',description:row.opname+' frekansiyel kontrol',selectedRow:{code:'FREKANSİYEL KONTROL'},op_transfer:true,status:t.generateClientStatus()});
								}, 1000);
								setTimeout(function() {
									t.eventListener({event:'device_init'});
									setTimeout(function(){
										t.winmessend('cn_js_workflow',{event:'show_message',result:'ok',title:'Lütfen Dikkat',text:'Dişi çeliğe (kaplamalı olan) hava tutun, kuru bezle silin, talaş kalmadığından emin olun.',type:'info',showConfirmButton:true,event_cb:'device-lost-finish'});
									}, 500);
								}, 2000);
							}
							if (row.opname === '55534P-80' || row.opname === '55534P-4936-80') {
								setTimeout(function() {
									t.eventListener({event:'device-lost-start',description:row.opname+' frekansiyel kontrol',selectedRow:{code:'FREKANSİYEL KONTROL'},op_transfer:true,status:t.generateClientStatus()});
								}, 1000);
								setTimeout(function() {
									t.eventListener({event:'device_init'});
									setTimeout(function(){
										t.winmessend('cn_js_workflow',{event:'show_message',result:'ok',title:'Lütfen Dikkat',text:'Hurda delikleri kontrol edip temizleyiniz.',type:'info',showConfirmButton:true,event_cb:'device-lost-finish'});
									}, 500);
								}, 2000);
							}
							
						}
						if(v_workflow!=='CNC'&&v_workflow!=='Lazer Kesim'
							&&typeof row.taskinfo!=='undefined'&&row.taskinfo!==null
							){
							// let tmpObjTasklistInfo={
							// 	app:"argox_cp_2140_febi"
							// 	,number:"170174" 
							// 	,week:"9269 32" 
							// 	,replno:"3Q0 407 152 F" 
							// 	,tofit:"VW" 
							// 	,opname:"55509-k" 
							// 	,barcode:"4054224705148"
							// };
							let tmpInfo=row.taskinfo;
							//console.log('tmpInfo:',tmpInfo);
							let tmpObjTasklistInfo=null;
							if(typeof tmpInfo.printer1!=='undefined'){
								tmpObjTasklistInfo=tmpInfo.printer1;
								tmpObjTasklistInfo.printer=1;
								tmpObjTasklistInfo.sayac=1;
							}else{
								tmpObjTasklistInfo=tmpInfo;
								tmpObjTasklistInfo.printer=1;
							}
							if(typeof tmpObjTasklistInfo.sayac==='undefined'){
								tmpObjTasklistInfo.sayac=1;
							}
							console.log('tmpObjTasklistInfo:',tmpObjTasklistInfo);
							//if (tmpInfo.app === 'argox_cp_2140_federal_1') {
							//	tmpObjTasklistInfo.sayac = 2;
							//}
							if(tmpObjTasklistInfo!==null&&row.productdonecount%parseInt(tmpObjTasklistInfo.sayac)===0
								&&fs.existsSync(path.join(__dirname,'..','labelPrinter',tmpObjTasklistInfo.app+'.exe'))){
								if(tmpObjTasklistInfo.app==='argox_cp_2140_febi'){
									console.log([
										"number="+tmpObjTasklistInfo.number
										,"week="+tmpObjTasklistInfo.week
										,"replno="+tmpObjTasklistInfo.replno
										,"tofit="+tmpObjTasklistInfo.tofit
										,"opname="+tmpObjTasklistInfo.opname
										,"barcode="+tmpObjTasklistInfo.barcode
										,"printer="+tmpObjTasklistInfo.printer
									]);
									const ls = cp.spawn(path.join(__dirname,'..','labelPrinter',tmpObjTasklistInfo.app+'.exe'), [
										"number="+tmpObjTasklistInfo.number
										,"week="+tmpObjTasklistInfo.week
										,"replno="+tmpObjTasklistInfo.replno
										,"tofit="+tmpObjTasklistInfo.tofit
										,"opname="+tmpObjTasklistInfo.opname
										,"barcode="+tmpObjTasklistInfo.barcode
										,"printer="+tmpObjTasklistInfo.printer
									], {
											cwd: path.resolve(path.join(__dirname,'..','labelPrinter')),
											detached: true,
											windowsHide: true
										}
									);
									ls.stdout.on('data', (data) => {
										console.log(`stdout: ${data}`);
									});
									ls.stderr.on('data', (data) => {
										console.log(`stderr: ${data}`);
									});
									ls.on('close', (code) => {
										console.log(`child process exited with code ${code}`);
									});
								}
								if(tmpObjTasklistInfo.app==='argox_cp_2140_erexim'||tmpObjTasklistInfo.app==='argox_cp_2140_federal_1'){
									const ls = cp.spawn(path.join(__dirname,'..','labelPrinter',tmpObjTasklistInfo.app+'.exe'), [
										"veri1="+tmpObjTasklistInfo.veri1
										,"veri2="+tmpObjTasklistInfo.veri2
										,"printer="+tmpObjTasklistInfo.printer
									], {
											cwd: path.resolve(path.join(__dirname,'..','labelPrinter')),
											detached: true,
											windowsHide: true
										}
									);
									ls.stdout.on('data', (data) => {
										console.log(`stdout: ${data}`);
									});
									ls.stderr.on('data', (data) => {
										console.log(`stderr: ${data}`);
									});
									ls.on('close', (code) => {
										console.log(`child process exited with code ${code}`);
									});
								}
								if(tmpObjTasklistInfo.app==='argox_cp_2140_federal_2'){
									const ls = cp.spawn(path.join(__dirname,'..','labelPrinter',tmpObjTasklistInfo.app+'.exe'), [
										"veri1="+tmpObjTasklistInfo.veri1
										,"veri2="+tmpObjTasklistInfo.veri2
										,"veri3="+tmpObjTasklistInfo.veri3
										,"veri4="+tmpObjTasklistInfo.veri4
										,"veri5="+tmpObjTasklistInfo.veri5
										,"printer="+tmpObjTasklistInfo.printer
									], {
											cwd: path.resolve(path.join(__dirname,'..','labelPrinter')),
											detached: true,
											windowsHide: true
										}
									);
									ls.stdout.on('data', (data) => {
										console.log(`stdout: ${data}`);
									});
									ls.stderr.on('data', (data) => {
										console.log(`stderr: ${data}`);
									});
									ls.on('close', (code) => {
										console.log(`child process exited with code ${code}`);
									});
								}
							}
							tmpObjTasklistInfo=null;
							if(typeof tmpInfo.printer2!=='undefined'){
								tmpObjTasklistInfo=tmpInfo.printer2;
								tmpObjTasklistInfo.printer=2;
							}else{
								tmpObjTasklistInfo=tmpInfo;
								tmpObjTasklistInfo.printer=2;
							}
							if(typeof tmpObjTasklistInfo.sayac==='undefined'){
								tmpObjTasklistInfo.sayac=1;
							}
							//if (tmpObjTasklistInfo.app === 'argox_cp_2140_federal_2') {
							//	tmpObjTasklistInfo.sayac = 2;
							//}
							if(tmpObjTasklistInfo!==null&&row.productdonecount%parseInt(tmpObjTasklistInfo.sayac)===0
								&&fs.existsSync(path.join(__dirname,'..','labelPrinter',tmpObjTasklistInfo.app+'.exe'))){
								if(tmpObjTasklistInfo.app==='argox_cp_2140_federal_2'){
									let v2data = tmpObjTasklistInfo.veri2;
									if (tmpObjTasklistInfo.veri2.length > 5) {
										const v2arr = tmpObjTasklistInfo.veri2.split(' ');
										if (v2arr[v2arr.length - 1].length === 5) {
											v2data = v2arr[v2arr.length - 1];
										}
									}
									const ls = cp.spawn(path.join(__dirname,'..','labelPrinter',tmpObjTasklistInfo.app+'.exe'), [
										"veri1="+tmpObjTasklistInfo.veri1
										,"veri2="+v2data
										,"veri3="+tmpObjTasklistInfo.veri3
										,"veri4="+tmpObjTasklistInfo.veri4
										,"veri5="+tmpObjTasklistInfo.veri5
										,"printer="+tmpObjTasklistInfo.printer
									], {
											cwd: path.resolve(path.join(__dirname,'..','labelPrinter')),
											detached: true,
											windowsHide: true
										}
									);
									ls.stdout.on('data', (data) => {
										console.log(`stdout: ${data}`);
									});
									ls.stderr.on('data', (data) => {
										console.log(`stderr: ${data}`);
									});
									ls.on('close', (code) => {
										console.log(`child process exited with code ${code}`);
									});
								}
							}
						}
					}
					if(v_workflow!=='Lazer Kesim'){
						for(let j=0;j<t.processVariables.productions.length;j++){
							let _item_prod_other_mould=t.processVariables.productions[j];
							//row.mouldgroup!==_item_prod_other_mould.mouldgroup bu koşul varken,aynı grup kodunda farklı iki kalıptan aynı referans çıkarken-R5-diğerini saymıyor
							//if(row.mould!==_item_prod_other_mould.mould&&row.mouldgroup!==_item_prod_other_mould.mouldgroup&&row.opname==_item_prod_other_mould.opname){
							if(row.mould!==_item_prod_other_mould.mould&&row.opname==_item_prod_other_mould.opname){
								_item_prod_other_mould.productdonecount+=_prod_current;
								_item_prod_other_mould.productdoneactivity+=_prod_current;
								_item_prod_other_mould.productdonejobrotation+=_prod_current;
								_item_prod_other_mould.remaining-=_prod_current;
								console.log('_item_prod_other_mould',_item_prod_other_mould);
							}else{
								// console.log('_item_prod_other_mould bulunamadı',row);
							}
						}
					}
					// c_p, e_p güncelle
					let _obj_where_cpd={
						type:{[Op.in]: ['c_p','e_p']}
						,client:v_client
						,tasklist:row.tasklist
						,[Op.or]:[{finish:{[Op.eq]:null}},{finish:t.processVariables.lastprodstarttime}]
					};
					if(v_workflow==='Lazer Kesim'){
						_obj_where_cpd.opname=row.opname;
					}
					_promises.push(update_cpd(_tr,_obj_where_cpd,_prod,_prod_current));
					if(v_workflow!=='Lazer Kesim'){
						// task_lists güncelle
						_promises.push(t.updateRecord(_tr,'task_lists',_time,{code:row.tasklist},{productdonecount:row.productdonecount,productdoneactivity:row.productdoneactivity,productdonejobrotation:row.productdonejobrotation,clients:row.clients}));
					}else{
						_promises.push(t.updateRecord(_tr,'task_list_laser_details',_time,{record_id:row.record_id},{productdonecount:row.productdonecount,productdoneactivity:row.productdoneactivity,productdonejobrotation:row.productdonejobrotation}));
					}
					if(t.objParam.task_finishQuantityReach.value===true){
						if(row.remaining<=0){
							if(t.processVariables.status_lost!='-'){
								//duruşa geçtikten sonra üretim gelmesi ve bu üretimin son parça olma ihtimali için eklendi
								let _obj_where_c_l_d={
									start:{[Op.ne]:null}
									,finish:{[Op.eq]:null}
								};
								_promises.push(t.cjr_client_lost_details(_tr,_obj_where_c_l_d,_time,{losttype:'DURUS SONRASI GELEN URETİM'}));
							}
							if(v_workflow==='Üretim Aparatsız'){
								v_flag_finish=true;
								//commit içine alındı
								//t.eventListener({event:'show-scrap-before-finish'});
							}
							if(v_workflow==='Değişken Üretim Aparatlı'){
								//@@todo:pres için birden fazla çıktı ve farklı üretim maskeli kalıplarda (0001,0100) her ikisi bittiğinde event tetiklenmeli
								v_flag_finish=true;
								//commit içine alındı
								//t.eventListener({event:'show-scrap-before-finish'});
							}
							if(v_workflow==='Sabit Üretim Aparatlı'){
								v_arr_finished.push(row);
								t.show_scrap_before_finish=true;
							}
							if(v_workflow==='Lazer Kesim'){
								v_arr_finished.push(row);
							}
						}
					}
					//taskcase güncelleme-tesellüm
					if(t.objParam.task_delivery.value===true&&row.delivery===true){
						_promises.push(t.update_taskcase_record_add(_tr,_time,row.erprefnumber,row.opname,_prod_current,row.record_id));
					}
					//taskcase güncelleme-malzeme hazırlık
					if(t.objParam.task_materialpreparation.value===true){
						_promises.push(t.update_taskcase_record_add_mh(_tr,_time,row.erprefnumber,row.opname,_prod_current,'INPUT'));
					}
					//taskcase güncelleme-malzeme kontrol şahince
					if(t.objParam.task_production_case_stock_control.value===true&&t.objParam.app_case_stok_before_work.value===true){
						_promises.push(t.update_taskcase_record_add_tcc(_tr,_time,row.erprefnumber,row.opname,row.opcode,_prod_current));
					}
					let mes=new dcs_message({
						type:'watch_input_signal'
						,ack:true
						,data:{
							client:v_client
							,item:row
							,processVariables:t.processVariables
						}
					});
					t.tmp_queue.push(mes);
					if(t.holo_status===true){
						mes=new dcs_message({
							type:'watch_input_signal_hololens'
							,ack:true
							,data:{
								client:v_client
								,item:row
								,processVariables:t.processVariables
								,time:_time
							}
						});
						t.tmp_queue.push(mes);
					}
					mes=undefined;
				}
				// mevcut için c_s yaz
				let _obj_c_p_d={
					type:'c_s'
					,client:v_client
					,day:_jrday
					,jobrotation:_jrcode
					,time:_time
					,tasklist:'port:'+_item.portnumber
					,production:1//sinyal sayısı
					,productioncurrent:_prod//üretilen miktar
					,opdescription:arrtn.join(',').substr(0,250)
					,calculatedtpp:ctpp
					,gap:0
					,start:_time
					,finish:null
					,record_id:uuidv4()
				};
				_promises.push(t.createRecord(_tr,'client_production_details',_time,_obj_c_p_d));
			}
			return Promise.all(_promises).then(function(){
				if(v_workflow==='Sabit Üretim Aparatlı'||v_workflow==='Lazer Kesim'){
					if(v_arr_finished.length>0){
						v_flag_finish=true;
						//commit içine alındı
						//t.eventListener({event:'show-scrap-before-finish',item:v_arr_finished});
					}
				}
				return null;
			});
		}).then(function(){
			_promises=[];
			if(!v_flag_finish){//paramtre false ise izleme ekranında duruşa geçiyor
				_promises.push(t.calcTempo('processInputSignal'));
			}
			return Promise.all(_promises).then(function(){
				_promises=[];
				while(t.tmp_queue.length>0){
					_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
				}
				t.status=t.status=='processInputSignal'?'':t.status;
				if(!v_flag_finish){
					
					let _p=t.processVariables.productions;
					if(v_workflow==='Sabit Üretim Aparatlı'){
						let _tmp=[];
						for(let i=0;i<_p.length;i++){
							if(t.getIndex(_tmp,'opname|erprefnumber',_p[i].opname+'|'+_p[i].erprefnumber)==-1){
								_tmp.push(_p[i]);
							}
						}
						_p=_tmp;
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',productions:_p});
					t.eventListener({event:_event,items:v_arr_productions,productions:_p});
				}else{
					if(v_workflow==='Üretim Aparatsız'&&t.objConfig.dcs_server_IP.value!=='10.0.0.101'){
						t.eventListener({event:'show-scrap-before-finish'});
					}
					if(v_workflow==='Değişken Üretim Aparatlı'){
						t.eventListener({event:'show-scrap-before-finish'});
					}
					if(v_workflow==='Sabit Üretim Aparatlı'){
						t.eventListener({event:'show-scrap-before-finish',item:v_arr_finished});
					}
					if(v_workflow==='Lazer Kesim'){
						//t.eventListener({event:'show-scrap-before-finish',item:v_arr_finished});
						t.eventListener({event:'device-work-finish',taskfinishtype:'GÖREV BİTTİ',recreate:0,taskfinishdescription:'İş emri miktarına ulaşıldı.',item:v_arr_finished});
					}
				}
				return Promise.all(_promises).then(function(){
					//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',productions:t.processVariables.productions});
					return null;
				});	
			});
		}).catch(function (err) {
			t.mjd('rollback-'+_event);
			console.log(err);
			t.status=t.status=='processInputSignal'?'':t.status;
			classBase.writeError(err,t.ip,'catch_workflow_processInputSignal');
			t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
			t.tmp_queue=[];
		});
	}
	calcTempo(/*pos*/){
		let t=this;
		let _time=t.tickTimer.obj_time.strDateTime;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		let v_client=t.objConfig.client_name.value;
		if(!_jrcode||!_jrday){
			return null;
		}else{
			let _cpd_time=_time;
			let _obj_where_cpd={
				type:'e_p'//{[Op.in]: ['c_p','e_p']}
				,client:v_client
				,day:_jrday
				,jobrotation:_jrcode
				,finish:{[Op.eq]:null}
			};
			return t.db.client_production_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where_cpd,order: [['id','DESC'],['start','DESC']],limit: 1}).then(function(res_cpd){
				for (let i = 0; i < res_cpd.length; i++) {
					let result=res_cpd[i];
					if(t.convertFullLocalDateString(result.time)<_cpd_time){
						_cpd_time=t.convertFullLocalDateString(result.time);
					}
				}
				let sql_lost='SELECT coalesce(sum(DATE_PART(\'day\', cld.finish::timestamp - cld.start::timestamp) * 24*3600 '+
				' 	+ DATE_PART(\'hour\', cld.finish::timestamp - cld.start::timestamp) * 3600 '+
				' 	+ DATE_PART(\'minute\', cld.finish::timestamp - cld.start::timestamp)*60 '+
				'	+ DATE_PART(\'second\', cld.finish::timestamp - cld.start::timestamp)),0) mlosttime '+
				' from client_lost_details cld '+
				' where type=\'l_c\' and losttype in (\'CUMA PAYDOSU\',\'ÇAY MOLASI\',\'YEMEK MOLASI\') and finish is not null and day=:day and jobrotation=:jobrotation and start>:start  ';
				return t.db.sequelize.query(sql_lost, { replacements: {day:_jrday,jobrotation:_jrcode,start:_cpd_time}, type: t.db.sequelize.QueryTypes.SELECT }).then(function(res_lost){
					let arr_lost=t.getLastElementOfArray(res_lost);
					let _mlosttime=parseInt(arr_lost.mlosttime);
					let sql='select round(sum(a.ptime))ptime,round(sum(a.pctime))pctime from ( '+
					' SELECT z.client,round(sum(z.proctime)/count(*))ptime,round(sum(z.proctimecurrent)/count(*))pctime '+
					' from ( '+
					'	SELECT cpd.client,cpd.day,cpd.jobrotation,cpd.start,cpd.mould,cpd.mouldgroup '+
					'		,coalesce(cpd.production*coalesce(cpd.calculatedtpp,COALESCE(tl.tpp,pt.tpp)),0) proctime '+
					'		,coalesce(cpd.productioncurrent*coalesce(cpd.calculatedtpp,COALESCE(tl.tpp,pt.tpp)),0) proctimecurrent '+
					'	from client_production_details cpd  '+
					'	left join task_lists tl on tl.code=cpd.tasklist '+
					'	left join product_trees pt on pt.name=cpd.opname and pt.finish is null and pt.materialtype=\'O\' '+
					//(( (t.ip&&(t.ip.indexOf('10.10')>-1) ))?' and pt.client=:client ':'') +
					' 	where cpd.type in (\'e_p\') and tl.finish is null and cpd.day=:day and jobrotation=:jobrotation and cpd.start=:start '+
					' )z '+
					' group by z.client,z.day,z.jobrotation,z.start,z.mould,z.mouldgroup)a '+
					' group by a.client';
					let _rep={day:_jrday,jobrotation:_jrcode,start:_cpd_time};
					//if(t.ip&&(t.ip.indexOf('10.10')>-1) ){
					//	_rep.client=v_client;
					//}
					return t.db.sequelize.query(sql, { replacements: _rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(res){
						let arr_ret=t.getLastElementOfArray(res);
						t.cpd_time=_cpd_time;
						t.cpd_ptime=arr_ret.ptime;
						t.cpd_pctime=arr_ret.pctime;
						t.cld_mlosttime=_mlosttime;
						t.processVariables.cpd_time=t.cpd_time;
						t.processVariables.cpd_ptime=t.cpd_ptime;
						t.processVariables.cpd_pctime=t.cpd_pctime;
						t.processVariables.cld_mlosttime=t.cld_mlosttime;
						let mes=new dcs_message({
							type:'watch_calc_tempo'
							,ack:true
							,data:{
								client:v_client
								,cpd_time:_cpd_time
								,cpd_ptime:arr_ret.ptime
								,cpd_pctime:arr_ret.pctime
								,cld_mlosttime:t.cld_mlosttime
								,lastprodtime:t.processVariables.lastprodtime
								,lastprodstarttime:t.processVariables.lastprodstarttime
							}
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					});
				});
			});
		}
	}
	taskTppAdd(_name){
		let t=this;
		//cihaz tipine göre tpp ve intervalmultiplier değerlerinin alınması
		//	Üretim Aparatsız ise ürün ağacından
		//	Değişken Üretim Aparatlı ise kalıp grup tablosundan
		//	Sabit Üretim Aparatlı ise kalıp grup tablosundan
		if(t.objConfig.workflow.value=='Üretim Aparatsız'){
			//ürün ağacındaki birim (süre*productionmultiplier*intervalmultiplier) / 100
			let _where = {name:_name,materialtype:'O',[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:t.tickTimer.obj_time.strDateTime}}]};
			//if(t.ip&&(t.ip.indexOf('10.10')>-1) ){
			//if(t.ip&&(t.ip.indexOf('10.10')>-1) ){
			//	_where = {name:_name,materialtype:'O',client:t.objConfig.client_name.value,[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:t.tickTimer.obj_time.strDateTime}}]};
			//}
			return t.db.product_trees.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
				,where: _where}/*, {transaction: _tr}*/).then(function(result_pt){

				if(result_pt){
					t.processVariables.status_tpp=parseInt(t.processVariables.status_tpp)+parseInt((result_pt.tpp*result_pt.productionmultiplier*result_pt.intervalmultiplier)/100);
				}else{
					throw new Error('Ürün ağacı tablosunda '+_name+' operasyon kaydı bulunamadı!');
				}
				return null;
			});
		}
		if(t.objConfig.workflow.value=='CNC'){
			t.processVariables.status_tpp=(t.ip==='192.168.1.40'?60:300);
			return null;
		}
		if(t.objConfig.workflow.value=='Değişken Üretim Aparatlı'||t.objConfig.workflow.value=='Sabit Üretim Aparatlı'){
			let _m=_name.split('|_|')[0];
			let _mg=_name.split('|_|')[1];
			return t.db.mould_groups.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
				,where:{code:_mg,mould:_m,[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:t.tickTimer.obj_time.strDateTime}}]}}).then(function(result_mg){
				if(result_mg){
					t.processVariables.status_tpp=parseInt(t.processVariables.status_tpp)+parseInt(result_mg.intervalmultiplier*result_mg.cycletime*result_mg.productionmultiplier);
				}else{
					throw new Error('Kalıp Grup Tanımlarında '+_name+' grup kaydı bulunamadı!');
				}
				return null;
			});
		}
	}
	taskTppRemove(_name){
		let t=this;
		if(t.objConfig.workflow.value=='Sabit Üretim Aparatlı'){
			let _m=_name.split('|_|')[0];
			let _mg=_name.split('|_|')[1];
			return t.db.mould_groups.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
				,where:{code:_mg,mould:_m,[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:t.tickTimer.obj_time.strDateTime}}]}}).then(function(result_mg){
				if(result_mg){
					t.processVariables.status_tpp=parseInt(t.processVariables.status_tpp)-parseInt(result_mg.intervalmultiplier*result_mg.cycletime*result_mg.productionmultiplier);
				}else{
					throw new Error('Kalıp Grup Tanımlarında '+_name+' grup kaydı bulunamadı!');
				}
				return null;
			});
		}
	}
	find_fault_master_record(_tr,_type,_obj){
		let t=this;
		//_obj.tasklist=t.json_status.fault.fault_unique_id?t.json_status.fault.fault_unique_id:'';
		let mes1=new dcs_message({
			type:_type
			,ack:true
			,data:_obj
		});
		t.tmp_queue.push(mes1);
		mes1=undefined;
		return null;
	}
	create_lost_record(_tr,_time,_obj){
		let t=this;
		return Promise.all([t.createRecord(_tr,'client_lost_details',_time,_obj)]);
	}
	create_lost_records(_tr, v_time, v_obj_lost, v_status_time, v_prod, v_pos){
		let t=this;
		let _promises=[];
		const _time=t.tickTimer.obj_time.strDateTime;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		let _cur_jr_day=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(v_time),false,v_time);
		let _cur_jr_code=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(v_time),true,v_time);
		let s_time=t.processVariables.statustime;
		let _s_jr_day=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(s_time),false,s_time);
		let _s_jr_code=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(s_time),true,s_time);

		let v_s_jr_day=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(v_status_time),false,v_status_time);
		let v_s_jr_code=t.getJobRotationDay(t.self.jobrotations,t.convertFullLocalDateString(v_status_time),true,v_status_time);
		let v_start=v_time;
		let v_finish=null;
		if(_jrday!=_cur_jr_day||_jrcode!=_cur_jr_code){
			v_finish=t.getLastTimeOfJobRotaion(_cur_jr_day,_cur_jr_code);
		}else if(_jrday!=_s_jr_day||_jrcode!=_s_jr_code){
			v_start=t.getFirstTimeOfJobRotaion(_s_jr_day,_s_jr_code);
			v_finish=t.getLastTimeOfJobRotaion(_s_jr_day,_s_jr_code);
			if(s_time>v_start){
				v_start=s_time;
			}
		}else if((_jrday==_s_jr_day||_jrcode==_s_jr_code)&&v_pos==='controljobrotations'){
			v_start=t.getFirstTimeOfJobRotaion(_cur_jr_day,_cur_jr_code);
		}else if(_jrday==_s_jr_day&&_jrcode==_s_jr_code&&t.lost_recursive){
			v_start=t.getFirstTimeOfJobRotaion(_cur_jr_day,_cur_jr_code);
		}else if(_jrday==_s_jr_day&&_jrcode==_s_jr_code&&!t.lost_recursive&&(_jrday!==v_s_jr_day||_jrcode!==v_s_jr_code)){
			v_start=t.getFirstTimeOfJobRotaion(_cur_jr_day,_cur_jr_code);
		}
		let _tmp_data_lc={
			type:'l_c'
			,client:v_client
			,day:_s_jr_day
			,jobrotation:_s_jr_code
			,losttype:v_obj_lost.losttype
			,descriptionlost: v_obj_lost.descriptionlost
			,mould:v_obj_lost.mould
			,faulttype:v_obj_lost.faulttype?v_obj_lost.faulttype:null
			,faultdescription:v_obj_lost.faultdescription?v_obj_lost.faultdescription:null
			,faultgroupcode:v_obj_lost.faultgroupcode?v_obj_lost.faultgroupcode:null
			,faultdevice:v_obj_lost.faultdevice?v_obj_lost.faultdevice:null
			,employee:v_obj_lost.faulttype&&v_obj_lost.employee?v_obj_lost.employee:null
			,tasklist:v_obj_lost.faulttype&&v_obj_lost.tasklist?v_obj_lost.tasklist:null
			,start:v_start
			,finish: v_finish
			,record_id:uuidv4()
			,sourcedescriptionlost:'1-'+v_pos+'-'+(t.str_missing_material.length>0?t.str_missing_material.substr(0,220):'')
		};
		_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lc));
		let _tmp_emp=[];
		for(let j=0;j<t.processVariables.operators.length;j++){
			let operator=t.processVariables.operators[j];
			if(_tmp_emp.indexOf(operator.code)===-1){
				_tmp_emp.push(operator.code);
				let _tmp_data_le={
					type: 'l_e'
					,client: v_client
					,day: _s_jr_day
					,jobrotation: _s_jr_code
					,losttype: v_obj_lost.losttype
					,descriptionlost: v_obj_lost.descriptionlost
					,faulttype:v_obj_lost.faulttype?v_obj_lost.faulttype:null
					,faultdescription:v_obj_lost.faultdescription?v_obj_lost.faultdescription:null
					,faultgroupcode:v_obj_lost.faultgroupcode?v_obj_lost.faultgroupcode:null
					,faultdevice:v_obj_lost.faultdevice?v_obj_lost.faultdevice:null
					,tasklist:v_obj_lost.faulttype&&v_obj_lost.tasklist?v_obj_lost.tasklist:null
					,employee: operator.code
					,start: (v_start<v_time?v_start:v_time)
					,finish: (v_workflow==='CNC'?v_finish:null)
					,record_id: uuidv4()
					,energy: null
					,sourcedescriptionlost:'2'
				};
				_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
			}
		}
		let create_production_record_not_exists=function(_tr,_time,_data){
			let _obj_where_cpd_rne={
				type:_data.type
				,client:_data.client
				,day:_data.day
				,jobrotation:_data.jobrotation
				,opname:_data.opname
				,start:{[Op.ne]:null}
				,finish:{[Op.eq]:null}
			};
			return t.db.client_production_details.findOne({where:_obj_where_cpd_rne},{transaction: _tr}).then(function(result){
				if(result){
					return null;//Promise.all([t.updateRecord(_tr,'client_production_details',_time,{record_id:result.record_id},{finish:_time})]);
				}else{
					return Promise.all([t.createRecord(_tr,'client_production_details',_time,_data)]);
				}
			});
		};
		let _tmp_prod=[];
		for(let i=0;i<v_prod.length;i++){
			let prod=v_prod[i];
			let _str=prod.mould+'|'+prod.mouldgroup+'|'+prod.opname;
			if(_tmp_prod.indexOf(_str)===-1){
				_tmp_prod.push(_str);
				let _tmp_data_lct={
					type: 'l_c_t'
					,client: v_client
					,day: _s_jr_day
					,jobrotation: _s_jr_code
					,losttype: v_obj_lost.losttype
					,descriptionlost: v_obj_lost.descriptionlost
					,faulttype:v_obj_lost.faulttype?v_obj_lost.faulttype:null
					,faultdescription:v_obj_lost.faultdescription?v_obj_lost.faultdescription:null
					,faultgroupcode:v_obj_lost.faultgroupcode?v_obj_lost.faultgroupcode:null
					,faultdevice:v_obj_lost.faultdevice?v_obj_lost.faultdevice:null
					,tasklist: prod.tasklist
					,mould: prod.mould
					,mouldgroup: prod.mouldgroup
					,opcode: prod.opcode
					,opnumber: prod.opnumber
					,opname: prod.opname
					,opdescription: prod.opdescription
					,erprefnumber: prod.erprefnumber
					,taskfromerp:prod.taskfromerp
					,start: v_start
					,finish: v_finish
					,record_id: uuidv4()
					,energy: null
					,sourcedescriptionlost:'3'
				};
				_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
				let obj_cpd_cp={
					type:'c_p'+(t.processVariables.status_work=='OP-OUT-SYSTEM'?'_unconfirm':'')
					,client:v_client
					,day:_s_jr_day
					,jobrotation:_s_jr_code
					,tasklist:prod.tasklist
					,time:_time
					,mould:prod.mould
					,mouldgroup:prod.mouldgroup
					,opcode:prod.opcode
					,opnumber:prod.opnumber
					,opname:prod.opname
					,opdescription:prod.opdescription
					,leafmask:(v_workflow=='Üretim Aparatsız'||prod.leafmask==null?'01':prod.leafmask)
					,leafmaskcurrent:(v_workflow=='Üretim Aparatsız'||prod.leafmaskcurrent==null?'01':prod.leafmaskcurrent)
					,production:0
					,productioncurrent:0
					,employee:null
					,erprefnumber:prod.erprefnumber
					,taskfromerp:prod.taskfromerp
					,calculatedtpp:prod.t_p_p
					,gap:v_finish!==null?Math.ceil((new timezoneJS.Date(v_finish).getTime()-new timezoneJS.Date(v_start).getTime())/1000):0
					,start:v_start
					,finish:v_finish
					,record_id:uuidv4()
				};
				_promises.push(create_production_record_not_exists(_tr,_time,obj_cpd_cp));
				_tmp_emp=[];
				//console.log('t.processVariables.operators.length='+t.processVariables.operators.length);
				//if(v_pos!=='controljobrotations'){
				for(let j=0;j<t.processVariables.operators.length;j++){
					let operator=t.processVariables.operators[j];
					if(_tmp_emp.indexOf(operator.code)===-1){
						_tmp_emp.push(operator.code);
						let _tmp_data_let={
							type: 'l_e_t'
							,client: v_client
							,day: _s_jr_day
							,jobrotation: _s_jr_code
							,losttype: v_obj_lost.losttype
							,descriptionlost: v_obj_lost.descriptionlost
							,faulttype:v_obj_lost.faulttype?v_obj_lost.faulttype:null
							,faultdescription:v_obj_lost.faultdescription?v_obj_lost.faultdescription:null
							,faultgroupcode:v_obj_lost.faultgroupcode?v_obj_lost.faultgroupcode:null
							,faultdevice:v_obj_lost.faultdevice?v_obj_lost.faultdevice:null
							,tasklist: prod.tasklist
							,mould: prod.mould
							,mouldgroup: prod.mouldgroup
							,opcode: prod.opcode
							,opnumber: prod.opnumber
							,opname: prod.opname
							,opdescription: prod.opdescription
							,employee: operator.code
							,erprefnumber: prod.erprefnumber
							,taskfromerp: prod.taskfromerp
							,start: (v_start<v_time?v_start:v_time)
							,finish: (v_workflow==='CNC'?v_finish:null)
							,record_id: uuidv4()
							,energy: null
							,sourcedescriptionlost:'4'
						};
						_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
						if(v_workflow==='CNC'){
							let obj_cpd_ep={
								type:'e_p'+(t.processVariables.status_work=='OP-OUT-SYSTEM'?'_unconfirm':'')
								,client:v_client
								,day:_s_jr_day
								,jobrotation:_s_jr_code
								,tasklist:prod.tasklist
								,time:_time
								,mould:prod.mould
								,mouldgroup:prod.mouldgroup
								,opcode:prod.opcode
								,opnumber:prod.opnumber
								,opname:prod.opname
								,opdescription:prod.opdescription
								,leafmask:(v_workflow=='Üretim Aparatsız'||prod.leafmask==null?'01':prod.leafmask)
								,leafmaskcurrent:(v_workflow=='Üretim Aparatsız'||prod.leafmaskcurrent==null?'01':prod.leafmaskcurrent)
								,production:0
								,productioncurrent:0
								,employee:operator.code
								,erprefnumber:prod.erprefnumber
								,taskfromerp:prod.taskfromerp
								,calculatedtpp:prod.t_p_p
								,gap:v_finish!==null?Math.ceil((new timezoneJS.Date(v_finish).getTime()-new timezoneJS.Date(v_start).getTime())/1000):0
								,start:v_start
								,finish:v_finish
								,record_id:uuidv4()
							};
							_promises.push(create_production_record_not_exists(_tr,_time,obj_cpd_ep));
						}
					}
				}
				//}
			}
		}
		return Promise.all(_promises).then(function(){
			_promises=[];
			if(_jrday!=_s_jr_day||_jrcode!=_s_jr_code){
				t.lost_recursive=true;
				t.processVariables.statustime=t.getNextJobRotationFirstTime(_s_jr_day,_s_jr_code);
				_promises.push(t.create_lost_records(_tr,v_time,v_obj_lost,v_status_time,v_prod,v_pos));
			}else{
				t.lost_recursive=false;
				//t.mjd('t.call_init_after_lost_recursive:'+t.call_init_after_lost_recursive);
				if(t.call_init_after_lost_recursive){
					t.call_init_after_lost_recursive=false;
					return Promise.all([t.eventListener({event:'device_init'})]);
				}
			}
			return Promise.all(_promises);
		});
	}
	create_production_records(_tr, v_time){
		let t=this;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		let _promises=[];
		let _tmp_prod=[];
		let _tmp_emp=[];
		for(let i=0;i<t.processVariables.productions.length;i++){
			let prod=t.processVariables.productions[i];
			let _str=prod.mould+'|'+prod.mouldgroup+'|'+prod.opname;
			if(_tmp_prod.indexOf(_str)===-1){
				_tmp_prod.push(_str);
				let _tmp_data={
					type: 'c_p'
					,client: v_client
					,day: _jrday
					,jobrotation: _jrcode
					,tasklist: prod.tasklist
					,time : v_time
					,mould: prod.mould
					,mouldgroup: prod.mouldgroup
					,opcode: prod.opcode
					,opnumber: prod.opnumber
					,opname: prod.opname
					,opdescription: prod.opdescription
					,leafmask: (v_workflow=='Üretim Aparatsız'||prod.leafmask==null?'01':prod.leafmask)
					,leafmaskcurrent: (v_workflow=='Üretim Aparatsız'||prod.leafmaskcurrent==null?'01':prod.leafmaskcurrent)
					,production: 0
					,productioncurrent: 0
					,employee: null
					,erprefnumber: prod.erprefnumber
					,taskfromerp: prod.taskfromerp
					,calculatedtpp: prod.t_p_p
					,gap: 0
					,start: v_time
					,finish: null
					,record_id: uuidv4()
					,energy: null
					,taskfinishtype: null
					,taskfinishdescription: null
				};
				_promises.push(t.createRecord(_tr,'client_production_details',v_time,_tmp_data));
				_tmp_emp=[];
				for(let j=0;j<t.processVariables.operators.length;j++){
					let operator=t.processVariables.operators[j];
					if(_tmp_emp.indexOf(operator.code)===-1){
						_tmp_emp.push(operator.code);
						let _tmp_data_emp={
							type: 'e_p'
							,client: v_client
							,day: _jrday
							,jobrotation: _jrcode
							,tasklist: prod.tasklist
							,time : v_time
							,mould: prod.mould
							,mouldgroup: prod.mouldgroup
							,opcode: prod.opcode
							,opnumber: prod.opnumber
							,opname: prod.opname
							,opdescription: prod.opdescription
							,leafmask: (v_workflow=='Üretim Aparatsız'||prod.leafmask==null?'01':prod.leafmask)
							,leafmaskcurrent: (v_workflow=='Üretim Aparatsız'||prod.leafmaskcurrent==null?'01':prod.leafmaskcurrent)
							,production: 0
							,productioncurrent: 0
							,employee: operator.code
							,erprefnumber: prod.erprefnumber
							,taskfromerp: prod.taskfromerp
							,calculatedtpp: prod.t_p_p
							,gap: 0
							,start: v_time
							,finish: null
							,record_id: uuidv4()
							,energy: null
							,taskfinishtype: null
							,taskfinishdescription: null
						};
						_promises.push(t.createRecord(_tr,'client_production_details',v_time,_tmp_data_emp));
					}
				}
			}
		}
		return Promise.all(_promises);
	}
	create_production_finish_records(_tr, v_time,v_taskfinishtype,v_taskfinishdescription){
		let t=this;
		const _time=t.tickTimer.obj_time.strDateTime;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		let _promises=[];
		let _tmp_prod=[];
		for(let i=0;i<t.processVariables.productions.length;i++){
			let prod=t.processVariables.productions[i];
			let _str=prod.mould+'|'+prod.mouldgroup+'|'+prod.opname;
			if(_tmp_prod.indexOf(_str)===-1){
				_tmp_prod.push(_str);
				let _tmp_data={
					type: 'c_p'
					,client: v_client
					,day: _jrday
					,jobrotation: _jrcode
					,tasklist: prod.tasklist
					,time : v_time
					,mould: prod.mould
					,mouldgroup: prod.mouldgroup
					,opcode: prod.opcode
					,opnumber: prod.opnumber
					,opname: prod.opname
					,opdescription: prod.opdescription
					,leafmask: (v_workflow=='Üretim Aparatsız'||prod.leafmask==null?'01':prod.leafmask)
					,leafmaskcurrent: (v_workflow=='Üretim Aparatsız'||prod.leafmaskcurrent==null?'01':prod.leafmaskcurrent)
					,production: 0
					,productioncurrent: 0
					,employee: null
					,erprefnumber: prod.erprefnumber
					,taskfromerp: prod.taskfromerp
					,calculatedtpp: prod.t_p_p
					,gap: 0
					,start: _time
					,finish: _time
					,record_id: uuidv4()
					,energy: null
					,taskfinishtype: v_taskfinishtype
					,taskfinishdescription: v_taskfinishdescription
				};
				_promises.push(t.createRecord(_tr,'client_production_details',_time,_tmp_data));
				
			}
		}
		return Promise.all(_promises);
	}
	get_production_finish_data(_tr, v_time){
		let t=this;
		if(t.processVariables.productions.length>0){
			return t.db.client_production_details.findOne({where:{type:'c_p',tasklist:t.processVariables.productions[0].tasklist},order: [['id','DESC'],['start','DESC']]},{transaction: _tr}).then(function(result){
				if(result){
					return Promise.all([t.create_production_finish_records(_tr,v_time,result.taskfinishtype,result.taskfinishdescription)]);
				}
				return null;
			});
		}
		return null;
	}
	update_taskcase_record_add(_tr,_time,_erprefnumber,_opname,_productcount,_record_id,_where_not){
		let t=this;
		let _promises=[];
		let _where_tc={
			movementdetail:{[Op.eq]:null}
			,casetype:'O'
			,erprefnumber:_erprefnumber
			,opname:_opname
			,start:{[Op.ne]:null}
			,finish:{[Op.eq]:null}
			,status:{[Op.notIn]:['CLOSE','CLOSED','CANCELED','WAIT_FOR_DELIVERY','WAIT_FOR_INFO_SEND','DELETED','SYSTEMCLOSE','LABELDELETED']}
		};
		let miktarfazlasi=0;
		if(_where_not){
			Object.assign(_where_tc,_where_not);
		}
		return t.db.task_cases.findAll({where:_where_tc,order: [['casenumber','ASC'],['start','ASC']]}).then(function(_res){
			if(_res.length>0){
				let _f=false;
				for (let i = 0; i < _res.length; i++) {
					if(_f){
						break;
					}
					let item=_res[i];
					let _obj_update_tc={};
					if(_productcount===0){//task_cases|update tetiklenmişse
						_obj_update_tc.status='WORK';
						_f=true;
						_promises.push(t.updateRecord(_tr,'task_cases',_time,{record_id:item.record_id},_obj_update_tc));
					}
					if(_productcount>0){
						//kasaya eklenecek olan miktarın kasa kapasitesini aşıp aşmadığı kontrol ediliyor
						if((parseFloat(_productcount)+parseFloat(item.quantityremaining))<parseFloat(item.packcapacity)){
							//kasada daha boş yer var
							_obj_update_tc.quantityremaining=(parseFloat(item.quantityremaining)+_productcount);
							_obj_update_tc.status='WORK';
							_productcount=0;
							_f=true;
						}else if((parseFloat(_productcount)+parseFloat(item.quantityremaining))==parseFloat(item.packcapacity)){
							//kasa bu ekleme ile doldu
							_obj_update_tc.quantityremaining=parseFloat(item.packcapacity);
							_obj_update_tc.status='WAIT_FOR_DELIVERY';
							_obj_update_tc.finish=_time;
							_obj_update_tc.finishtype='Kasa doldu';
							_productcount=0;
							if(!t.show_scrap_before_finish&&!t.show_delivery){
								t.show_delivery=true;
								setTimeout(() => {
									t.processVariables.lastprodstarttime=null;
									t.winmessend('cn_js_workflow',{event:'open_delivery_screen',result:'ok',casenumber:item.casenumber,caselot:item.caselot,message:'<b>'+item.casenumber+'</b> Numaralı <b>'+item.caselot+'</b> sıralı etiktet tesellüm edilecek, lütfen asılmış etiketi kontrol ediniz.'});
								}, 200);
							}
							_f=true;
						}else{
							//kasa doldu ve başka kasaya ihtiyaç var
							miktarfazlasi=parseFloat(_productcount)-(parseFloat(item.packcapacity)-parseFloat(item.quantityremaining));
							_productcount=0;
							_obj_update_tc.quantityremaining=parseFloat(item.packcapacity);
							_obj_update_tc.status='WAIT_FOR_DELIVERY';
							_obj_update_tc.finish=_time;
							_obj_update_tc.finishtype='Kasa doldu';
							if(!t.show_scrap_before_finish&&!t.show_delivery){
								t.show_delivery=true;
								setTimeout(() => {
									t.processVariables.lastprodstarttime=null;
									t.winmessend('cn_js_workflow',{event:'open_delivery_screen',result:'ok',casenumber:item.casenumber,caselot:item.caselot,message:'<b>'+item.casenumber+'</b> Numaralı <b>'+item.caselot+'</b> sıralı etiktet tesellüm edilecek, lütfen asılmış etiketi kontrol ediniz.'});
								}, 200);
							}
							_f=true;
						}
						_promises.push(t.updateRecord(_tr,'task_cases',_time,{record_id:item.record_id},_obj_update_tc));
					}
				}
				if(_promises.length>0){
					return Promise.all(_promises).then(function(){
						if(miktarfazlasi>0){
							return t.update_taskcase_record_add(_tr,_time,_erprefnumber,_opname,miktarfazlasi,_record_id);
						}
						return null;
					});
				}else{
					return null;
				}
			}else{
				//otomatik tesellüm ile bunun olmaması gerekiyor
				//kasaya konulması gereken parça var ama kasa yok
				//üretim adedini kontrol et, gerekli ise yeni kasayı otomatik olarak aç
				delete _where_tc.status;
				return t.db.task_cases.findOne({where:_where_tc,order: [['id','ASC'],['start','ASC']]},{transaction: _tr}).then(function(result){
					if(result){
						let idx=t.getIndex(t.processVariables.productions,'record_id',_record_id);
						if(idx>-1){
							if((t.processVariables.productions[idx].remaining+_productcount)>=parseFloat(result.packcapacity)){
								_promises.push(t.updateRecord(_tr,'task_cases',_time,{record_id:result.record_id},{status:'WORK'}));
								return null;
							}
						}
					}else{
						return null;
					}
				}).then(function(){
					if(_promises.length>0){
						return Promise.all(_promises);
					}else{
						return null;
					}
				});
			}
		});
	}
	update_taskcase_record_remove(_tr,_time,_erprefnumber,_opname,_scrapcount){
		let t=this;
		let _promises=[];
		let _where_tc={
			movementdetail:{[Op.eq]:null}
			,casetype:'O'
			,erprefnumber:_erprefnumber
			,opname:_opname
			,start:{[Op.ne]:null}
			,status:{[Op.in]: ['WORK','WAIT_FOR_DELIVERY']}
		};
		return t.db.task_cases.findAll({where:_where_tc,order: [['id','DESC'],['start','DESC']]}).then(function(_res){
			if(_res.length>0){
				for (let i = 0; i < _res.length; i++) {
					let item=_res[i];
					if(_scrapcount>0){
						let _obj_update_tc={};
						//kasadan çıkarılacak olan miktarın kasa içi miktarı aşıp aşmadığı kontrol ediliyor
						if(parseFloat(_scrapcount)<parseFloat(item.quantityremaining)){
							//kasada içindekinden daha az miktarda ıskarta
							_obj_update_tc.quantityremaining=(parseFloat(item.quantityremaining)-_scrapcount);
							_obj_update_tc.status='WORK';
							_obj_update_tc.finish=null;
							_obj_update_tc.finishtype=null;
							_scrapcount=0;
						}else if(parseFloat(_scrapcount)==parseFloat(item.quantityremaining)){
							//kasa içindeki kadar ıskarta
							_obj_update_tc.quantityremaining=0;
							_obj_update_tc.status='WORK';
							_obj_update_tc.finish=null;
							_obj_update_tc.finishtype=null;
							_scrapcount=0;
						}else{
							//kasada içindekinden daha fazla miktarda ıskarta
							_scrapcount=parseFloat(_scrapcount)-parseFloat(item.quantityremaining);
							_obj_update_tc.quantityremaining=0;
							_obj_update_tc.status='WORK';
							_obj_update_tc.finish=null;
							_obj_update_tc.finishtype=null;
						}
						_promises.push(t.updateRecord(_tr,'task_cases',_time,{record_id:item.record_id},_obj_update_tc));
					}
				}
				return Promise.all(_promises).then(function(){
					if(_scrapcount>0){
						t.mjd('Girilen ıskarta miktarının düşülebileceği kadar tesellüm edilmemiş kasa bulunamadı');
						throw new Error('Girilen ıskarta miktarının düşülebileceği kadar tesellüm edilmemiş kasa bulunamadı');
					}
					return null;
				});
			}else{
				let _idx=t.getIndex(t.processVariables.productions,'opname',_opname);
				if(_idx>-1){
					if(t.processVariables.productions[_idx].delivery===true){
						//takip edilmiş kasaların kapasitesinden fazla ıskarta girildi.
						t.mjd('Girilen ıskarta miktarının düşülebileceği kadar tesellüm edilmemiş kasa bulunamadı');
						throw new Error('Girilen ıskarta miktarının düşülebileceği kadar tesellüm edilmemiş kasa bulunamadı');
					}
				}
				return null;
			}
		});
	}
	update_taskcase_record_add_mh(_tr,_time,_erprefnumber,_opname,_productcount,_type){
		let t=this;
		let _promises=[];
		
		let rec_update=function(_tr,row,pcount){
			let _obj_where={
				movementdetail:{[Op.ne]:null}
				,erprefnumber:row.erprefnumber
				,opname:row.opname
				,casetype:row.casetype
				,stockcode:row.stockcode
				,casename:row.casename
				,status:{[Op.in]:['WAIT','WORK']}
				,start:{[Op.ne]:null}
				,finish:{[Op.eq]:null}
			};
			return t.db.task_cases.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
				,where:_obj_where,order: [['id','ASC'],['movementdetail','ASC'],['start','ASC']] }, {transaction: _tr}).then(function(result_tc){
				if(result_tc){
					let _obj_update_tc={};
					if(result_tc.status=='WAIT'){
						_obj_update_tc.status='WORK';
					}
					if(result_tc.casetype=='I'){
						_obj_update_tc.quantityremaining=parseFloat(result_tc.quantityremaining)-(parseFloat(result_tc.quantityused)*pcount);
						if(_obj_update_tc.quantityremaining<=0){
							_obj_update_tc.status='WAIT_FOR_DELIVERY';
							_obj_update_tc.finish=_time;
							_obj_update_tc.finishtype='Kasa doldu';
						}
					}
					if(result_tc.casetype=='O'){
						_obj_update_tc.quantityremaining=parseFloat(result_tc.quantityremaining)+pcount;
						if(parseFloat(result_tc.packcapacity)==parseFloat(_obj_update_tc.quantityremaining)){
							_obj_update_tc.status='WAIT_FOR_DELIVERY';
							_obj_update_tc.finish=_time;
							_obj_update_tc.finishtype='Kasa doldu';
						}
					}
					return Promise.all([t.updateRecord(_tr,'task_cases',_time,{record_id:result_tc.record_id},_obj_update_tc)]);
				}else{
					t.mjd('Görev bulunamadı.ERP Ref:'+_obj_where.erprefnumber+' Opname:'+_obj_where.opname);
				}
			});
		};

		let rec_update_multi=function(_tr,row,pcount){
			let _prom=[];
			let _obj_where={
				erprefnumber:row.erprefnumber
				,opname:row.opname
				,casetype:row.casetype
				,stockcode:row.stockcode
				,casename:row.casename
			};
			let sql='SELECT * '+
			' from task_cases '+
			' where movementdetail is not null and status in (\'WAIT\',\'WORK\') and start is not null and finish is null '+
			' and erprefnumber=:erprefnumber and opname=:opname and casetype=:casetype and stockcode=:stockcode and casename=:casename '+
			' order by cast(movementdetail as integer),start ';
			return t.db.sequelize.query(sql, { replacements:_obj_where, type: t.db.sequelize.QueryTypes.SELECT}).then(function(_res){
				if(_res.length>0){
					let row_pcount=pcount*parseFloat(_res[0].quantityused);
					for (let i = 0; i < _res.length; i++) {
						let _obj_update_tc={};
						let item=_res[i];
						if(row_pcount>0){
							if(item.casetype=='I'){
								if(parseFloat(item.quantityremaining)>0){
									if(row_pcount<parseFloat(item.quantityremaining)){
										//kasada daha malzeme var
										_obj_update_tc.quantityremaining=(parseFloat(item.quantityremaining)-row_pcount);
										_obj_update_tc.status='WORK';
										row_pcount=0;
									}else if(row_pcount==parseFloat(item.quantityremaining)){
										//kasadaki malzeme bu üretim ile bitti
										_obj_update_tc.quantityremaining=0;
										_obj_update_tc.status='WAIT_FOR_DELIVERY';
										_obj_update_tc.finish=_time;
										_obj_update_tc.finishtype='Kasa doldu-1 type:'+_type+'-count:'+row_pcount;
										row_pcount=0;
									}else{
										//kasadaki malzemeden daha fazlasına ihtiyaç var
										row_pcount=row_pcount-parseFloat(item.quantityremaining);
										_obj_update_tc.quantityremaining=parseFloat(item.packcapacity);
										_obj_update_tc.status='WAIT_FOR_DELIVERY';
										_obj_update_tc.finish=_time;
										_obj_update_tc.finishtype='Kasa doldu-2 type:'+_type+'-count:'+row_pcount;
									}
								}else{
									//kasa içindeki eksi miktar diğer kasaya aktarılacak-kasa kalmayınca duracak kullanılmıyor
									row_pcount+=(parseFloat(item.quantityremaining)*-1);
									_obj_update_tc.quantityremaining=0;
									_obj_update_tc.status='WAIT_FOR_DELIVERY';
									_obj_update_tc.finish=_time;
									_obj_update_tc.finishtype='Kasa doldu-3 type:'+_type+'-count:'+row_pcount;
								}								
							}else{
								if(parseFloat(item.quantityremaining)<parseFloat(item.packcapacity)){
									if(row_pcount+parseFloat(item.quantityremaining)<parseFloat(item.packcapacity)){
										//kasada daha boş yer var
										_obj_update_tc.quantityremaining=parseFloat(item.quantityremaining)+row_pcount;
										_obj_update_tc.status='WORK';
										row_pcount=0;
									}else if(row_pcount+parseFloat(item.quantityremaining)==parseFloat(item.packcapacity)){
										//kasa bu ekleme ile doldu
										_obj_update_tc.quantityremaining=parseFloat(item.packcapacity);
										_obj_update_tc.status='WAIT_FOR_DELIVERY';
										_obj_update_tc.finish=_time;
										_obj_update_tc.finishtype='Kasa doldu-4'+_type;
										row_pcount=0;
									}else{
										//kasa doldu ve başka kasaya ihtiyaç var
										row_pcount=row_pcount-(parseFloat(item.packcapacity)-parseFloat(item.quantityremaining));
										_obj_update_tc.quantityremaining=parseFloat(item.packcapacity);
										_obj_update_tc.status='WAIT_FOR_DELIVERY';
										_obj_update_tc.finish=_time;
										_obj_update_tc.finishtype='Kasa doldu-5'+_type;
									}
								}else{
									//kasa içindeki fazla miktar diğer kasaya aktarılacak-kasa kalmayınca duracak kullanılmıyor
									row_pcount+=parseFloat(item.quantityremaining)-parseFloat(item.packcapacity);
									_obj_update_tc.quantityremaining=parseFloat(item.packcapacity);
									_obj_update_tc.status='WAIT_FOR_DELIVERY';
									_obj_update_tc.finish=_time;
									_obj_update_tc.finishtype='Kasa doldu-6'+_type;
								}
							}
							_prom.push(t.updateRecord(_tr,'task_cases',_time,{id:item.id,record_id:item.record_id},_obj_update_tc));
						}
					}
					return Promise.all(_prom); 
				}
				return null;
			});
		};

		let sql='SELECT erprefnumber,opname,casetype,stockcode,quantityused,casename,packcapacity,count(*)rc '+
		' from task_cases '+
		' where movementdetail is not null and status in (\'WAIT\',\'WORK\') and start is not null and finish is null '+
		' and erprefnumber=:erprefnumber and opname=:opname '+(_type=='SCRAP'?' and casetype=\'I\' ':'')+
		' group by erprefnumber,opname,casetype,opname,stockcode,quantityused,casename,packcapacity ';
		return t.db.sequelize.query(sql, { replacements:{erprefnumber:_erprefnumber,opname:_opname}, type: t.db.sequelize.QueryTypes.SELECT}).then((result_tc)=>{
			if(result_tc&&result_tc.length>0){
				for (let i = 0; i < result_tc.length; i++) {
					let row_tc=result_tc[i];
					if(parseInt(row_tc.rc)==1){
						_promises.push(rec_update(_tr,row_tc,_productcount));
					}else{
						_promises.push(rec_update_multi(_tr,row_tc,_productcount));
					}
				}
				return Promise.all(_promises); 
			}
			return null;
		});
	}
	update_taskcase_record_remove_mh(_tr,_time,_erprefnumber,_opname,_scrapcount){
		let t=this;
		let _promises=[];
		let _where_tc={
			movementdetail:{[Op.ne]:null}
			,casetype:'O'
			,erprefnumber:_erprefnumber
			,opname:_opname
			,start:{[Op.ne]:null}
			,status:{[Op.in]: ['WORK','WAITFORDELIVERY','WAIT_FOR_DELIVERY']}
		};
		return t.db.task_cases.findAll({where:_where_tc,order: [['id','DESC'],['start','DESC']]}).then(function(_res){
			if(_res.length>0){
				for (let i = 0; i < _res.length; i++) {
					let item=_res[i];
					if(_scrapcount>0){
						let _obj_update_tc={};
						//kasadan çıkarılacak olan miktarın kasa içi miktarı aşıp aşmadığı kontrol ediliyor
						if(parseFloat(_scrapcount)<parseFloat(item.quantityremaining)){
							//kasada içindekinden daha az miktarda ıskarta
							_obj_update_tc.quantityremaining=(parseFloat(item.quantityremaining)-parseFloat(_scrapcount));
							_obj_update_tc.status='WORK';
							_obj_update_tc.finish=null;
							_obj_update_tc.finishtype=null;
							_scrapcount=0;
						}else if(parseFloat(_scrapcount)==parseFloat(item.quantityremaining)){
							//kasa içindeki kadar ıskarta
							_obj_update_tc.quantityremaining=0;
							_obj_update_tc.status='WORK';
							_obj_update_tc.finish=null;
							_obj_update_tc.finishtype=null;
							_scrapcount=0;
						}else{
							//kasada içindekinden daha fazla miktarda ıskarta
							_scrapcount=parseFloat(_scrapcount)-parseFloat(item.quantityremaining);
							_obj_update_tc.quantityremaining=0;
							_obj_update_tc.status='WORK';
							_obj_update_tc.finish=null;
							_obj_update_tc.finishtype=null;
						}
						_promises.push(t.updateRecord(_tr,'task_cases',_time,{record_id:item.record_id},_obj_update_tc));
					}
				}
				return Promise.all(_promises).then(function(){
					if(_scrapcount>0){
						t.mjd('Girilen ıskarta miktarının düşülebileceği kadar tesellüm edilmemiş kasa bulunamadı-MH');
					}
					return null;
				});
			}
		});
	}
	update_taskcase_record_add_tcc(_tr,_time,_erprefnumber,_opname,_opcode,_productcount){
		let t=this;
		let v_workflow=t.objConfig.workflow.value;
		let _promises=[];
		let requirement=false;
		let isIExists=false;
		let isOExists=false;
		let sql=`SELECT id,stockcode,stockname,quantityused,quantityremaining,casetype
		from task_cases 
		where movementdetail is null and start is not null and finish is null 
			and case when casetype='I' then quantityremaining>0 else 1=1 end
			and status in ('TCC') and erprefnumber=:erprefnumber and opcode=:opcode
		order by casetype,casenumber
		`;//and opname=:opname  ,opname:_opname //2021-11-30
		return t.db.sequelize.query(sql, { replacements:{erprefnumber:_erprefnumber,opcode:_opcode}, type: t.db.sequelize.QueryTypes.SELECT}).then((result_tc)=>{
			if(result_tc&&result_tc.length>0){
				let stocknames=[];
				for (let i = 0; i < result_tc.length; i++) {
					let row_tc=result_tc[i];
					if(row_tc.casetype==='I'){
						isIExists=true;
					}
					if(row_tc.casetype==='O'){
						isOExists=true;
					}
					if(stocknames.indexOf(row_tc.stockname)===-1){
						stocknames.push(row_tc.stockname);
						let row_pcount=_productcount*parseFloat(row_tc.quantityused);
						if(requirement===false&&parseFloat(row_tc.quantityremaining)<=row_pcount){
							requirement=true;
						}
						const _obj_update_tc={
							quantityremaining:parseFloat(row_tc.quantityremaining)+((row_tc.casetype==='I'?-1:1)*row_pcount)
							,finish:((row_tc.casetype==='I'&&parseFloat(row_tc.quantityremaining)<=row_pcount)?_time:null)
						};
						_promises.push(t.updateRecord(_tr,'task_cases',_time,{id:row_tc.id},_obj_update_tc));
					}
				}
				return Promise.all(_promises).then(function(){
					_promises=[];
					if(!isIExists||!isOExists){
						requirement=true;
					}
					if(requirement===true){
						setTimeout(() => {
							t.hammaddeeksiklikleri=[];
							for(let i=0;i<t.processVariables.productions.length;i++){
								const row=t.processVariables.productions[i];
								if((row.productdonecount<row.productcount||(t.objConfig.dcs_server_IP.value==='10.10.0.10'||t.objConfig.dcs_server_IP.value==='192.168.1.40'))&&row.delivery===true){
									// let arr_opname=row.opname.split('-');
									// if(arr_opname.length>1){
									// 	arr_opname.pop();
									// }
									// let tmp_code=arr_opname.join('-');
									_promises.push(t.kontrol_hammadde(row.erprefnumber,_opcode));
								}
							}
							return Promise.all(_promises);
						}, 1000);
					}
				}); 
			}
			return null;
		});
	}
	update_taskcase_delivery(_tr,_obj_where){
		let t=this;
		const _time=t.tickTimer.obj_time.strDateTime;
		return t.db.task_cases.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
			,where:_obj_where }, {transaction: _tr}).then(function(result_tc){
			if(result_tc){
				let _obj_update_tc={
					status:'WAIT_FOR_INFO_SEND'
					,deliverytime:_time
					,record_id:result_tc.record_id
					,finish:_time
					,finishtype:parseFloat(result_tc.quantityremaining)!=parseFloat(result_tc.packcapacity)?'Eksik Tesellüm':'Kasa doldu'
				};
				return Promise.all([t.updateRecord(_tr,'task_cases',_time,{record_id:result_tc.record_id},_obj_update_tc)]);
			}else{
				t.mjd('Görev bulunamadı.ERP Ref:'+_obj_where.erprefnumber+' Opname:'+_obj_where.opname);
			}
		});
	}
	close_task_case(_tr,_obj_where){
		let t=this;
		const _time=t.tickTimer.obj_time.strDateTime;
		let _obj_update_tc={
			status:'CLOSE'
			,deliverytime:_time
			,finish:_time
			,finishtype:'İşi Yarım Bırakma'
		};
		_obj_where.finish={[Op.eq]:null};
		return t.db.task_cases.findAll({attributes: { exclude: ['createdAt','updatedAt'] }
			,where:_obj_where})
			.then(function(results) {
				let _prom=[];
				if(results.length>0){
					for(let i=0;i<results.length;i++){
						let item=results[i];
						_prom.push(t.updateRecord(_tr,'task_cases',_time,{record_id:item.record_id},_obj_update_tc));
					}
					return Promise.all(_prom);
				}else{
					return null;
				}
			});
	}
	control_open_task_multi(_tr,v_data,v_pos){
		let t=this;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		if(v_workflow!=='Kataforez'){
			if(t.processVariables.productions.length!==0){
				//çalışılan iş var ise, çalışılan işlerin bilgileri v_data içine aktarılıp, 
				//işlerin arka planda bilgileri kapatılacak-aşağıda-işlerin üretim-kayıp bilgilerinin kapatılması- etiketli kısımda yapılıyor(170 satır kadar)
				let _idx_vdata=-1;
				while(t.processVariables.productions.length>0){
					let item=t.processVariables.productions.shift();
					_idx_vdata=t.getIndex(v_data,'opname',item.opname);
					if(_idx_vdata>-1){
						v_data.splice(_idx_vdata,1);
					}
					v_data.push(item);
				}
			}
		}
		v_data=arraySort(v_data, 'opname');
		let _arr_tmp_data=[];
		let _tmp=[];
		for(let i=0;i<v_data.length;i++){
			if(_tmp.indexOf(v_data[i].opname)==-1){
				_tmp.push(v_data[i].opname);
				_arr_tmp_data.push(v_data[i]);
			}
		}
		_tmp=_tmp.sort();
		v_data=_arr_tmp_data;
		if(v_workflow==='Kataforez'){
			for(let i=0;i<v_data.length;i++){
				v_data[i].tasklist=v_data[i].code;
			}
			return {err:false,data:v_data};
		}
		let v_opname=_tmp.join('|');
		let v_opname_str=_tmp.join('\',\'');
		let sql=`
		select d.mould,d.mouldgroup,d.opname,d.leafmask,d.leafmaskcurrent 
			,cmg.cycletime,cmg.productionmultiplier,cmg.intervalmultiplier 
			,round((coalesce(cmg.productionmultiplier,100)*cmg.intervalmultiplier*cmg.cycletime)/100,2) "interval" 
			,round((coalesce(cmg.productionmultiplier,100)*cmg.cycletime)/100,2)tpp 
			,(select count(*) from client_mould_details where finish is null and clientmouldgroup=d.clientmouldgroup and client=d.client)groupcount 
		from ( 
			SELECT cmd.client,cmd.clientmouldgroup 
				,string_agg(md.opname,'|' ORDER BY cmd.id)opname 
				,string_agg(md.mould,'|' ORDER BY cmd.id)mould 
				,string_agg(md.mouldgroup,'|' ORDER BY cmd.id)mouldgroup 
				,string_agg(md.leafmask,'|' ORDER BY cmd.id)leafmask 
				,string_agg(md.leafmaskcurrent,'|' ORDER BY cmd.id)leafmaskcurrent 
				,string_agg(distinct md.opname,'|' order by md.opname)opname2 
			from client_mould_details cmd  
			join mould_details md on md.mouldgroup=cmd.mouldgroup 
			join client_details cd on cd.iotype='I' and cd.ioevent='input' and cd.code=md.mould and cd.client=cmd.client 
			where md.finish is null and cd.finish is null and cmd.finish is null and cmd.client=:client 
			GROUP BY cmd.client,cmd.clientmouldgroup)d 
		join client_mould_groups cmg on cmg.client=d.client and cmg.code=d.clientmouldgroup 
		where cmg.finish is null and d.opname2=:opname
		`;
		return t.db.sequelize.query(sql,{ replacements: {client: v_client,opname:v_opname}, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
			if(results&&results.length>0){
				let res=results[0];
				let _opnames=res.opname.split('|');
				let _lm=res.leafmask.split('|');
				let _lmc=res.leafmaskcurrent.split('|');
				let _m=res.mould.split('|');
				let _mg=res.mouldgroup.split('|');
				t.processVariables.status_tpp=parseInt(res.interval);
				for(let i=0;i<_opnames.length;i++){
					let idx=t.getIndex(v_data,'opname',_opnames[i]);//_opnames.indexOf(v_data[i]['opname']);
					if(idx>-1){
						if(typeof v_data[idx].leafmask=='undefined'){
							v_data[idx].leafmask=_lm[i];
							v_data[idx].leafmaskcurrent=_lmc[i];
							v_data[idx].mould=_m[i];
							v_data[idx].mouldgroup=_mg[i];
							v_data[idx].tasklist=v_data[idx].code;
							v_data[idx].t_p_p=Math.round(res.tpp/res.groupcount,2);
						}else{
							var newArray = JSON.parse(JSON.stringify(v_data[idx]));
							newArray.leafmask=_lm[i];
							newArray.leafmaskcurrent=_lmc[i];
							newArray.mould=_m[i];
							newArray.mouldgroup=_mg[i];
							newArray.tasklist=v_data[idx].code;
							newArray.t_p_p=Math.round(res.tpp/res.groupcount,2);
							v_data.push(newArray);
						}
					}
				}
				return {err:false,data:v_data};
			}else{
				//seçilen işler için tanımlı istasyon kalıp grup tanımı yok
				//eski usül seçili işlerin kalıp tanım bilgileri üzerinden birim süre hesaplanarak iş açılacak
				let sql;
				if(v_workflow==='Sabit Üretim Aparatlı'){
					sql='select b.*,mg.productionmultiplier,mg.intervalmultiplier,mg.cycletime '+
					'	,round((mg.productionmultiplier*mg.intervalmultiplier*mg.cycletime)/100,2) "interval" '+
					'	,round((mg.productionmultiplier*mg.cycletime)/100,2) tpp '+
					' 	,case when exists (select bantime from moulds where bantime is not null and finish is null and code=mg.mould limit 1) then true else false end bantime '+
					' from ( '+
					'	select md.mould,md.mouldgroup,max(a.opname)opname,max(a.leafmask)leafmask,max(a.leafmaskcurrent)leafmaskcurrent,count(md.*) sayi '+
					'	from ( '+
					'		SELECT md.mould,md.mouldgroup,count(*)sayi '+
					'			,string_agg(md.opname,\'|\' order by md.opname)opname '+
					'			,string_agg(md.leafmask,\'|\')leafmask,string_agg(md.leafmaskcurrent,\'|\')leafmaskcurrent '+
					'		from mould_details md '+
					'		join client_details cd on cd.code=md.mould and cd.iotype=\'I\' and cd.ioevent=\'input\' '+
					'		where md.finish is null and cd.finish is null and cd.client=\''+v_client+'\' and md.opname in (\''+v_opname_str+'\') '+
					'		GROUP BY md.mould,md.mouldgroup)a '+
					'	join mould_details md on md.mould=a.mould and md.mouldgroup=a.mouldgroup and md.finish is null '+
					'	GROUP BY md.mould,md.mouldgroup '+
					'	HAVING max(a.sayi)=count(md.*))b '+
					' join mould_groups mg on mg.mould=b.mould and mg.code=b.mouldgroup '+
					' order by b.sayi desc,mould asc,mouldgroup asc';
				}
				if(v_workflow==='Değişken Üretim Aparatlı'){
					sql='select b.*,mg.productionmultiplier,mg.intervalmultiplier,mg.cycletime '+
					'	,round((mg.productionmultiplier*mg.intervalmultiplier*mg.cycletime)/100,2) "interval" '+
					'	,round((mg.productionmultiplier*mg.cycletime)/100,2) tpp '+
					' 	,case when exists (select bantime from moulds where bantime is not null and finish is null and code=mg.mould limit 1) then true else false end bantime '+
					' from ( '+
					'	select md.mould,md.mouldgroup,max(a.opname)opname,max(a.leafmask)leafmask,max(a.leafmaskcurrent)leafmaskcurrent,count(md.*) sayi from ( '+
					'		SELECT md.mould,md.mouldgroup,count(*)sayi '+
					'			,string_agg(md.opname,\'|\' order by md.opname)opname '+
					'			,string_agg(md.leafmask,\'|\')leafmask,string_agg(md.leafmaskcurrent,\'|\')leafmaskcurrent '+
					'		from mould_details md '+
					'		join mould_groups mg on mg.code=md.mouldgroup and mg.mould=md.mould and mg.finish is null '+
					'		where md.finish is null and md.opname in (\''+v_opname_str+'\') '+
					'		GROUP BY md.mould,md.mouldgroup)a '+
					'	join mould_details md on md.mould=a.mould and md.mouldgroup=a.mouldgroup and md.finish is null '+
					'	GROUP BY md.mould,md.mouldgroup '+
					'	HAVING max(a.sayi)=count(md.*))b '+
					' join mould_groups mg on mg.mould=b.mould and mg.code=b.mouldgroup '+
					' order by b.sayi desc';
					if(t.objParam.process_selectfirstmould.value===true){
						sql+=' limit 1';
					}
				}
				return t.db.sequelize.query(sql,{ type: t.db.sequelize.QueryTypes.SELECT }).then(function(results_cmd) {
					if(results_cmd&&results_cmd.length>0){
						//console.log('control_open_task_multi-1:',results_cmd.length);
						let _f_bantime=false;
						for(let k=0;k<results_cmd.length;k++){
							let rowk=results_cmd[k];
							if(_f_bantime===false){
								if(rowk.bantime){
									_f_bantime=true;
								}
							}else{
								break;
							}
						}
						if(_f_bantime){
							throw new Error('Bu kalıp planlı bakımda olduğu için üretime kapatılmıştır.<br>'+v_opname_str);
						}
						t.processVariables.status_tpp=0;
						//2019-10-31
						//robotta (rk003) operasyon tek seçilirse olabilen tüm fikstürlede üretim yapılabilmesi
						//çoklu seçilirse her operasyon için tek fikstür seçmesi için eklendi
						let __arr_tmp=[];
						if((t.objConfig.dcs_server_IP.value==='10.0.0.101')&&v_data.length>1&&(v_client==='RK001'||v_client==='RK002'||v_client==='RK003'||v_client==='RK004')){
							for(let i=0;i<results_cmd.length;i++){
								let row=results_cmd[i];
								let _found_d=false;
								for(let j=0;j<results_cmd.length;j++){
									let row_d=results_cmd[j];
									if(row.mould!==row_d.mould&&row.opname===row_d.opname){
										_found_d=true;
										let _found=false;
										for(let k=0;k<results_cmd.length;k++){
											let row_k=results_cmd[k];
											if(row_d.mould===row_k.mould&&row_d.opname!==row_k.opname){
												_found=true;
											}
										}
										if(_found){
											__arr_tmp.push(row);
										}
									}
								}
								if(!_found_d){
									__arr_tmp.push(row);
								}
							}
							results_cmd=__arr_tmp;
						}
						for(let i=0;i<results_cmd.length;i++){
							let row=results_cmd[i];
							if(_tmp.length>0){
								t.processVariables.status_tpp+=parseInt(row.interval);
								let _lm=row.leafmask.split('|');
								let _lmc=row.leafmaskcurrent.split('|');
								let arr_opname=row.opname.split('|');
								for(let j=0;j<arr_opname.length;j++){
									let _op=arr_opname[j];
									let tmp_idx=_tmp.indexOf(_op);
									if(tmp_idx>-1){
										_tmp.splice(tmp_idx,1);
										let idx=t.getIndex(v_data,'opname',_op);
										if(idx>-1){
											v_data[idx].leafmask=_lm[j];
											v_data[idx].leafmaskcurrent=_lmc[j];
											v_data[idx].mould=row.mould;
											v_data[idx].mouldgroup=row.mouldgroup;
											v_data[idx].tasklist=v_data[idx].code;
											v_data[idx].t_p_p=row.tpp;
											for(let k=i;k<results_cmd.length;k++){
												let row_k=results_cmd[k];
												if(row.mould!==row_k.mould&&row.sayi==row_k.sayi&&row_k.opname.indexOf(_op)>-1){
													t.processVariables.status_tpp+=parseInt(row_k.interval);
													let _lm_k=row_k.leafmask.split('|');
													let _lmc_k=row_k.leafmaskcurrent.split('|');
													let arr_opname_k=row_k.opname.split('|');
													let idx_k=arr_opname_k.indexOf(_op);
													let __item_v_data=Object.assign({},v_data[idx]);
													__item_v_data.leafmask=_lm_k[idx_k];
													__item_v_data.leafmaskcurrent=_lmc_k[idx_k];
													__item_v_data.mould=row_k.mould;
													__item_v_data.mouldgroup=row_k.mouldgroup;
													__item_v_data.tasklist=v_data[idx].code;
													__item_v_data.t_p_p=row.tpp;
													v_data.push(__item_v_data);
												}
											}
										}
									}
								}
							}
						}
						//console.log('control_open_task_multi-88:',v_data.length);
						let _i=v_data.length;
						while(_i-->0){
							if(typeof v_data[_i].tasklist==='undefined'){
								v_data.splice(_i,1);
							}
						}
						if(v_workflow==='Değişken Üretim Aparatlı'){
							let _tmp_mould=[];
							for(let i=0;i<v_data.length;i++){
								let __str=v_data[i].mould+'|'+v_data[i].mouldgroup;
								if(_tmp_mould.indexOf(__str)==-1){
									_tmp_mould.push(__str);
								}
							}
							if(_tmp_mould.length>1&&t.objParam.process_selectfirstmould.value===false){
								throw new Error('Seçili işler için uyumlu üretim aparatı bulunamadı-1.<br>'+v_opname_str);
							}
						}
						//console.log('control_open_task_multi-99:',v_data.length);
						return {err:false,data:v_data};
					}else{
						if(v_workflow==='Sabit Üretim Aparatlı'){
							if(v_pos!=='device-work-finish-multi'){
								throw new Error('İstasyon cihaz detay bilgilerinde seçili işler için uyumlu üretim aparatı bulunamadı.-2<br>'+v_opname_str);
							}else{
								return {err:true,msg:'İstasyon cihaz detay bilgilerinde seçili işler için uyumlu üretim aparatı bulunamadı.-3<br>'+v_opname_str};
							}
						}
						if(v_workflow==='Değişken Üretim Aparatlı'){
							throw new Error('Seçili işler için uyumlu üretim aparatı bulunamadı-4.<br>'+v_opname_str);
						}
					}
				});
			}
		});
	}
	open_task_multi(_tr,v_data,v_employee,v_pos,b_outOfSystem){
		let t=this;
		let _promises=[];
		let _v_pos=v_pos;
		const _time=t.tickTimer.obj_time.strDateTime;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		let tmp_status_lost='-';
		let objwhere={code:v_employee};
		if(v_data.length===0){
			throw new Error('Listeden iş seçmelisiniz.');
		}
		if(_v_pos!=='device-work-finish-multi'&&!b_outOfSystem){//@@todo referansın biri bitince kalanlar için kontrol yapılmıyor.
			_promises.push(t.control_open_task_multi(_tr,v_data,_v_pos));
		}
		return Promise.all(_promises).then(function(res_x_arr){
			if(res_x_arr.length>0){
				let x=t.getLastElementOfArray(res_x_arr);
				v_data=x.data;
				console.log('open_task_multi-1:',v_data);
			}
			_promises=[];
			if(_v_pos!=='device-work-finish-multi'&&!b_outOfSystem){
				let _tmp_tc=[];
				for(let i=0;i<v_data.length;i++){
					_promises.push(t.updateRecord(_tr,'task_lists',_time,{code: v_data[i].tasklist,start:{[Op.eq]:null}},{start:_time,erprefnumber:v_data[i].erprefnumber}));
					//_promises.push(t.updateRecord(_tr,'task_lists',_time,{code: v_data[i].tasklist,client:{[Op.ne]:v_client}},{client:v_client}));
					if(t.objParam.task_delivery.value===true&&v_data[i].delivery&&v_data[i].taskcaseexists===0){
						let _change=false;
						if(v_data[i].preptime==null){
							throw new Error(v_data[i].opname+' operasyonunun ürün ağacında kasa ile ilgili bilgileri kontrol edilmeli.');
						}
						for(let j=0;j<v_data[i].detail.length;j++){
							let row=v_data[i].detail[j];
							let _tmp_data_tc={
								client:v_client
								,movementdetail:null
								,erprefnumber:v_data[i].erprefnumber
								,casetype:'O'
								,opcode:v_data[i].opcode
								,opnumber:v_data[i].opnumber
								,opname:v_data[i].opname
								,quantityused:v_data[i].leafmaskcurrent==null?1:parseFloat(v_data[i].leafmaskcurrent)
								,casename:v_data[i].casename==null?(row.casename?row.casename:''):v_data[i].casename
								,casenumber:row.casenumber
								,caselot:row.caselot
								,packcapacity:row.packcapacity
								,preptime:v_data[i].preptime
								,quantityremaining:0
								,quantitydeliver:0
								,conveyor:0
								,status:(_change?'WAIT':'WORK')
								,start:_time
								,record_id:uuidv4()
							};
							_change=true;
							if(t.getIndex(_tmp_tc,'casenumber',row.casenumber)==-1){
								_tmp_tc.push(_tmp_data_tc);
							}
						}
					}
				}
				let tc_create=function(_tr,_time,item){
					return t.db.task_cases.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{casenumber:item.casenumber,erprefnumber:item.erprefnumber
						,status:{[Op.notIn]:['CLOSE','CLOSED','CANCELED','WAIT_FOR_DELIVERY','WAIT_FOR_INFO_SEND','DELETED','SYSTEMCLOSE','LABELDELETED']}
					} }, {transaction: _tr}).then(function(result){
						if(result){
							return null;
						}else{
							return Promise.all([t.createRecord(_tr,'task_cases',_time,item)]);
						}
					});
				};
				for(let i=0;i<_tmp_tc.length;i++){
					_promises.push(tc_create(_tr,_time,_tmp_tc[i]));
				}
			}
			//işlerin kayıp bilgilerinin kapatılması
			if(t.processVariables.status_lost!=='-'){
				let _obj_where_c_l_d={
					start:{[Op.ne]:null}
					,finish:{[Op.eq]:null}
				};
				if(t.processVariables.status_work!=='-'&&t.processVariables.status_lost!=='URETIM SONU ISKARTA BEKLEME'){
					_obj_where_c_l_d.type={[Op.notIn]: ['l_c', 'l_e']};
				}
				_promises.push(t.cjr_client_lost_details(_tr,_obj_where_c_l_d,_time));
			}
			return Promise.all(_promises).then(function(){
				_promises=[];
				if(_v_pos!=='device-work-finish-multi'){
					let create_lost_record_not_exists=function(_tr,_time,_type,_losttype,_employee){
						let __promises=[];
						let _obj_where_clt_rne={
							type:_type
							,client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,losttype:_losttype
							,start:{[Op.ne]:null}
							,finish:{[Op.eq]:null}
						};
						if(_type=='l_e'){
							_obj_where_clt_rne.employee=_employee;
						}
						return t.db.client_lost_details.findOne({where:_obj_where_clt_rne},{transaction: _tr}).then(function(result){
							if(result){
								return null;
							}else{
								let _tmp_data_lc={
									type:_type
									,client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,losttype:_losttype
									,start:_time
									,record_id:uuidv4()
								};
								if(_type=='l_e'){
									_tmp_data_lc.employee=_employee;
								}
								__promises.push(t.create_lost_record(_tr,_time,_tmp_data_lc));
								return null;
							}
						}).then(function(){
							if(__promises.length>0){
								return Promise.all(__promises);
							}else{
								return null;
							}
						});
					};
					if((t.objParam.process_wsSetup.value===false&&t.objParam.process_wsQuality.value===false)||b_outOfSystem){
						// süreç yok
						if(v_workflow!=='Kataforez'){
							let _where_cjr_cpd={type:{[Op.in]: ['c_p', 'e_p']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}};
							_promises.push(t.cjr_client_production_details(_tr,_where_cjr_cpd,_time));
						}
						if(b_outOfSystem||t.processVariables.status_work=='-'){
							tmp_status_lost='GÖREVDE KİMSE YOK';
							t.processVariables.status_employee=0;
							t.processVariables.operators=[];
							let _tmp_data_lc={
								type:'l_c'
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,losttype:tmp_status_lost
								,start:_time
								,record_id:uuidv4()
							};
							_promises.push(t.create_lost_record(_tr,_time,_tmp_data_lc));
							_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:_time}));
						}
						
					}else{
						if(v_workflow!=='Kataforez'){
							let _where_cjr_cpd={type:{[Op.in]: ['c_p', 'e_p']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}};
							_promises.push(t.cjr_client_production_details(_tr,_where_cjr_cpd,_time));	
						}
						if(t.processVariables.status_work==='-'&&(t.processVariables.status_employee>0||t.processVariables.operators.length>0)){
							t.processVariables.status_employee=0;
							t.processVariables.operators=[];
							_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:_time}));
						}
						if(t.objParam.process_wsSetup.value===true){
							if(t.processVariables.status_lost!=='SETUP-AYAR'){
								t.processVariables.status_employee=1;
								t.processVariables.status_workflowprocess='SETUP';
								tmp_status_lost='SETUP-AYAR';
								if(v_workflow==='Değişken Üretim Aparatlı'){
									let _tmp_data_lc={
										type:'l_c'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,losttype:tmp_status_lost
										,start:_time
										,record_id:uuidv4()
									};
									_promises.push(t.create_lost_record(_tr,_time,_tmp_data_lc));
									let _tmp_data_le={
										type:'l_e'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,losttype:tmp_status_lost
										,employee:v_employee
										,start:_time
										,record_id:uuidv4()
									};
									_promises.push(t.create_lost_record(_tr,_time,_tmp_data_le));
								}
								if(v_workflow==='Sabit Üretim Aparatlı'){
									_promises.push(create_lost_record_not_exists(_tr,_time,'l_c',tmp_status_lost));
									_promises.push(create_lost_record_not_exists(_tr,_time,'l_e',tmp_status_lost,v_employee));
								}
								let _data_emp={
									client: v_client
									,day:_jrday
									,jobrotation:_jrcode
									,strlastseen:t.processVariables._time
								};
								_promises.push(t.updateRecord(_tr,'employees',_time,objwhere,_data_emp));
							}else{
								tmp_status_lost='SETUP-AYAR';
							}
						}else{
							if(t.objParam.process_wsQuality.value===true){
								let _control_lost_type=(t.objParam.process_remoteQualityConfirmation.value===true?'KALİTE ONAY':'KALİTE BEKLEME');
								if(t.processVariables.status_lost!==_control_lost_type){
									t.processVariables.status_employee=0;
									t.processVariables.operators=[];
									t.processVariables.status_workflowprocess='QUALITY';
									tmp_status_lost=_control_lost_type;
									if(v_workflow==='Değişken Üretim Aparatlı'){
										let _tmp_data_lc={
											type:'l_c'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,start:_time
											,record_id:uuidv4()
										};
										_promises.push(t.create_lost_record(_tr,_time,_tmp_data_lc));
									}
									if(v_workflow==='Sabit Üretim Aparatlı'){
										_promises.push(create_lost_record_not_exists(_tr,_time,'l_c',tmp_status_lost));
									}
								}else{
									tmp_status_lost=_control_lost_type;
								}
							}
						}
					}
				}
				for(let i=0;i<v_data.length;i++){
					let _item_prod=v_data[i];
					//console.log('open_task_multi:',_item_prod);
					let obj_cpd_cp={
						type:'c_p'+(b_outOfSystem?'_unconfirm':'')
						,client:v_client
						,day:_jrday
						,jobrotation:_jrcode
						,tasklist:(b_outOfSystem?_item_prod.code:_item_prod.tasklist)
						,time:_time
						,mould:(!b_outOfSystem?_item_prod.mould:null)
						,mouldgroup:(!b_outOfSystem?_item_prod.mouldgroup:null)
						,opcode:_item_prod.opcode
						,opnumber:_item_prod.opnumber
						,opname:_item_prod.opname
						,opdescription:_item_prod.opdescription
						,leafmask:(!b_outOfSystem?(v_workflow=='Üretim Aparatsız'||_item_prod.leafmask==null?'01':_item_prod.leafmask):'01')
						,leafmaskcurrent:(!b_outOfSystem?(v_workflow=='Üretim Aparatsız'||_item_prod.leafmaskcurrent==null?'01':_item_prod.leafmaskcurrent):'01')
						,production:0
						,productioncurrent:0
						,employee:null
						,erprefnumber:_item_prod.erprefnumber
						,taskfromerp:_item_prod.taskfromerp
						,calculatedtpp:_item_prod.t_p_p
						,gap:0
						,start:_time
						,finish:null
						,record_id:uuidv4()
					};
					_promises.push(t.createRecord(_tr,'client_production_details',_time,obj_cpd_cp));
					if(b_outOfSystem){
						if(t.processVariables.status_work=='-'){
							//iş yok konumundan sistem dışı iş başlatılmış ise
							let _tmp_data_lct={
								type:'l_c_t'
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,losttype:tmp_status_lost
								,tasklist:(b_outOfSystem?_item_prod.code:_item_prod.tasklist)
								,mould:(!b_outOfSystem?_item_prod.mould:null)
								,mouldgroup:(!b_outOfSystem?_item_prod.mouldgroup:null)
								,opcode:_item_prod.opcode
								,opnumber:_item_prod.opnumber
								,opname:_item_prod.opname
								,opdescription:_item_prod.opdescription
								,erprefnumber:_item_prod.erprefnumber
								,taskfromerp:_item_prod.taskfromerp
								,start:_time
								,record_id:uuidv4()
							};
							_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
						}else{//çalışan işe iş ekleme
							for(let j=0;j<t.processVariables.operators.length;j++){
								let _item_emp=t.processVariables.operators[j];
								if(t.processVariables.status_lost==='-'){
									let obj_cpd_ep={
										type:'e_p'+(b_outOfSystem?'_unconfirm':'')
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,tasklist:(b_outOfSystem?_item_prod.code:_item_prod.tasklist)
										,time:_time
										,mould:(!b_outOfSystem?_item_prod.mould:null)
										,mouldgroup:(!b_outOfSystem?_item_prod.mouldgroup:null)
										,opcode:_item_prod.opcode
										,opnumber:_item_prod.opnumber
										,opname:_item_prod.opname
										,opdescription:_item_prod.opdescription
										,leafmask:(!b_outOfSystem?(v_workflow=='Üretim Aparatsız'||_item_prod.leafmask==null?'01':_item_prod.leafmaskcurrent):'01')
										,leafmaskcurrent:(!b_outOfSystem?(v_workflow=='Üretim Aparatsız'||_item_prod.leafmaskcurrent==null?'01':_item_prod.leafmaskcurrent):'01')
										,production:0
										,productioncurrent:0
										,employee:_item_emp.code
										,erprefnumber:_item_prod.erprefnumber
										,taskfromerp:_item_prod.taskfromerp
										,calculatedtpp:_item_prod.t_p_p
										,gap:0
										,start:_time
										,finish:null
										,record_id:uuidv4()
									};
									_promises.push(t.createRecord(_tr,'client_production_details',_time,obj_cpd_ep));
								}
							}
						}
					}else{
						if(_v_pos!=='device-work-finish-multi'){
							if(t.processVariables.status_work=='-'||t.processVariables.status_lost!=='-'||t.objParam.process_wsSetup.value===true||t.objParam.process_wsQuality.value===true){//yeni işe başlama
								let _tmp_data_lct={
									type:'l_c_t'
									,client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,losttype:tmp_status_lost
									,tasklist:_item_prod.tasklist
									,mould:_item_prod.mould
									,mouldgroup:_item_prod.mouldgroup
									,opcode:_item_prod.opcode
									,opnumber:_item_prod.opnumber
									,opname:_item_prod.opname
									,opdescription:_item_prod.opdescription
									,erprefnumber:_item_prod.erprefnumber
									,taskfromerp:_item_prod.taskfromerp
									,start:_time
									,record_id:uuidv4()
								};
								_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
								if(t.objParam.process_wsSetup.value===true){
									let _tmp_data_let={
										type:'l_e_t'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,losttype:tmp_status_lost
										,tasklist:_item_prod.tasklist
										,mould:_item_prod.mould
										,mouldgroup:_item_prod.mouldgroup
										,opcode:_item_prod.opcode
										,opnumber:_item_prod.opnumber
										,opname:_item_prod.opname
										,opdescription:_item_prod.opdescription
										,employee:v_employee
										,erprefnumber:_item_prod.erprefnumber
										,taskfromerp:_item_prod.taskfromerp
										,start:_time
										,record_id:uuidv4()
									};
									_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
								}
							}else{//çalışan işe iş ekleme
								for(let j=0;j<t.processVariables.operators.length;j++){
									let _item_emp=t.processVariables.operators[j];
									if(t.processVariables.status_lost==='-'){
										let obj_cpd_ep={
											type:'e_p'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,tasklist:_item_prod.tasklist
											,time:_time
											,mould:_item_prod.mould
											,mouldgroup:_item_prod.mouldgroup
											,opcode:_item_prod.opcode
											,opnumber:_item_prod.opnumber
											,opname:_item_prod.opname
											,opdescription:_item_prod.opdescription
											,leafmask:(v_workflow=='Üretim Aparatsız'||_item_prod.leafmask==null?'01':_item_prod.leafmaskcurrent)
											,leafmaskcurrent:(v_workflow=='Üretim Aparatsız'||_item_prod.leafmaskcurrent==null?'01':_item_prod.leafmaskcurrent)
											,production:0
											,productioncurrent:0
											,employee:_item_emp.code
											,erprefnumber:_item_prod.erprefnumber
											,taskfromerp:_item_prod.taskfromerp
											,calculatedtpp:_item_prod.t_p_p
											,gap:0
											,start:_time
											,finish:null
											,record_id:uuidv4()
										};
										_promises.push(t.createRecord(_tr,'client_production_details',_time,obj_cpd_ep));
									}
								}
							}
						}else{//çalışan işten iş çıkarma ya da çoklu iş içinden bir kısmın bitmesi
							for(let j=0;j<t.processVariables.operators.length;j++){
								let _item_emp=t.processVariables.operators[j];
								if(t.processVariables.status_lost==='-'||t.processVariables.status_lost==='URETIM SONU ISKARTA BEKLEME'){
									let obj_cpd_ep={
										type:'e_p'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,tasklist:_item_prod.tasklist
										,time:_time
										,mould:_item_prod.mould
										,mouldgroup:_item_prod.mouldgroup
										,opcode:_item_prod.opcode
										,opnumber:_item_prod.opnumber
										,opname:_item_prod.opname
										,opdescription:_item_prod.opdescription
										,leafmask:(v_workflow=='Üretim Aparatsız'||_item_prod.leafmask==null?'01':_item_prod.leafmaskcurrent)
										,leafmaskcurrent:(v_workflow=='Üretim Aparatsız'||_item_prod.leafmaskcurrent==null?'01':_item_prod.leafmaskcurrent)
										,production:0
										,productioncurrent:0
										,employee:_item_emp.code
										,erprefnumber:_item_prod.erprefnumber
										,taskfromerp:_item_prod.taskfromerp
										,calculatedtpp:_item_prod.t_p_p
										,gap:0
										,start:_time
										,finish:null
										,record_id:uuidv4()
									};
									_promises.push(t.createRecord(_tr,'client_production_details',_time,obj_cpd_ep));
								}
							}
						}
					}
				}
				if(t.processVariables.status_work=='-'){//yeni işe başlama
					t.processVariables.status_work=b_outOfSystem?'OP-OUT-SYSTEM':'OP';
				}
				t.setStatus(tmp_status_lost);
				let _obj_upd_client_task_lists_select={status:t.generateClientStatus(),statustime:_time};
				if((t.objParam.process_wsSetup.value===false&&t.objParam.process_wsQuality.value===false)||(_v_pos==='device-work-finish-multi'||_v_pos==='task_lists|select|multi')||b_outOfSystem){
					t.processVariables.lastprodstarttime=_time;
					t.processVariables.lastprodtime=_time;
					_obj_upd_client_task_lists_select.lastprodstarttime=_time;
					_obj_upd_client_task_lists_select.lastprodtime=_time;
				}
				_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},_obj_upd_client_task_lists_select));
				return Promise.all(_promises);
			});
		});
	}
	task_cases_inserted(/*obj*/){
		let t=this;
		if(t.objParam.task_materialpreparation.value===true&&(t.processVariables.status_lost=='MALZEME BEKLEME'||t.processVariables.status_lost=='PLANLI TAŞIMA BEKLEME'||t.processVariables.status_lost=='ALT PARÇA YOK')){
			if(t.timer_missing_material!==false){
				clearTimeout(t.timer_missing_material);
				t.timer_missing_material=false;
			}
			t.timer_missing_material=setTimeout(() => {
				t.control_missing_materials('delivered',4);
			}, 2000);
		}
	}
	update_taskinfo(obj){
		let t=this;
		//console.log('update_taskinfo',obj);
		let sql="update task_lists set taskinfo=:taskinfo where erprefnumber=:erprefnumber and opname=:opname and record_id=:record_id";
		return t.db.sequelize.query(sql, { replacements:{taskinfo:JSON.stringify(obj.taskinfo),erprefnumber:obj.erprefnumber,opname:obj.opname,record_id:obj.record_id}, type: t.db.sequelize.QueryTypes.UPDATE})
		.catch(function(err){
			Sentry.captureException(err);
			return null;
		});
	}
	control_missing_material(_erprefnumber,_opname,_pcount){
		let t=this;
		let mt=" \'H\',\'K\' ";
		let lbl=" and aa.islabelexists=1 and aa.islabeldeleted=0 "
		if(t.objConfig.dcs_server_IP.value==='10.10.0.10'||t.objConfig.dcs_server_IP.value==='192.168.1.40'){
			mt="\'H\'";
			lbl="";
		}
		let sql='SELECT * from ( '+
		'	SELECT pt.id,pt.materialtype,pt.name,pt.stockcode,pt.stockname,pt.quantity,pt.pack,pt.packcapacity '+
		'		,case when pt.materialtype=\'K\' then :pcount else cast(pt.quantity*:pcount as integer) end quantitynecessary '+
		'		,COALESCE((select cast(sum(packcapacity)as integer)packcapacity from task_cases tc where tc.movementdetail is not null and tc.erprefnumber=:erprefnumber and tc.opname=:opname and tc.opname=pt.name and tc.stockcode=(case when pt.materialtype=\'K\' then pt.stockname else pt.stockcode end) and tc.casename=pt.pack),0)quantitydelivered '+
		'		,case when exists(select * from task_cases tc2 where tc2.movementdetail is not null and tc2.status in (\'WAIT\',\'WORK\') and tc2.erprefnumber=:erprefnumber and tc2.opname=:opname and tc2.opname=pt.name and tc2.stockcode=(case when pt.materialtype=\'K\' then pt.stockname else pt.stockcode end) and tc2.casename=pt.pack ) then 1 else 0 end iscaseexists '+
		'		,case when exists(select * from case_labels cl where cl.erprefnumber=:erprefnumber) then 1 else 0 end islabelexists '+
		'		,case when exists(select * from case_labels cl where cl.status in (\'DELETED\',\'LABELDELETED\',\'SYSTEMCLOSE\') and cl.erprefnumber=:erprefnumber) then 1 else 0 end islabeldeleted '+
		'	from product_trees pt  '+
		'	where pt.finish is null and pt.name in (:opname) and pt.materialtype in ('+mt+') and COALESCE(pt.feeder,\'K\')<>\'K\' and COALESCE(pt.carrierfeeder,\'\')<>\'\' )aa '+
		' where aa.iscaseexists=0 and aa.quantitynecessary<>aa.quantitydelivered '+lbl+
		' order by aa.id';
		return t.db.sequelize.query(sql, {replacements:{pcount:_pcount,erprefnumber:_erprefnumber,opname:_opname},type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
			if(results.length>0){
				let item=results[0];
				let str='ERP:'+_erprefnumber+' OP:'+_opname+' İŞ EMRİ:'+_pcount+' TİPİ:'+item.materialtype+' ADI:'+item.name+' S.KODU:'+item.stockcode+' S.ADI:'+item.stockname+' GEREKEN:'+item.quantitynecessary+' VERİLEN:'+item.quantitydelivered;
				if(str!==t.str_missing_material){
					item.erprefnumber=_erprefnumber;
					t.obj_missing_material=item;
					t.str_missing_material=str;
					t.mjd(t.str_missing_material);
				}
			}
			return {record:results.length>0};
		}).catch(function (err) {
			t.mjd('catch-control_missing_material');
			Sentry.captureException(err);
			Sentry.addBreadcrumb({
				category: 'control_missing_material',
				message: 'control_missing_material ',
				level: 'info',
				data:JSON.parse(JSON.stringify({pcount:_pcount,erprefnumber:_erprefnumber,opname:_opname}))
			});
			return null;
		});
	}
	control_missing_materials(changetype,pos){
		let t=this;
		if(t.objParam.task_materialpreparation.value===true){
			t.mjd('control_missing_materials:'+changetype+' pos:'+pos+' case_movement_target_time:'+t.case_movement_target_time);
			if(!t.device_inited||t.processVariables.status_work=='OP-OUT-SYSTEM'){
				if(t.timer_missing_material!==false){
					clearTimeout(t.timer_missing_material);
					t.timer_missing_material=false;
				}
				t.timer_missing_material=setTimeout(() => {
					t.control_missing_materials(changetype,pos);
				}, 2000);
				return;
			}
			let _promises=[];
			const _time=t.tickTimer.obj_time.strDateTime;
			let _jrday=t.obj_jobrotation.day;
			let _jrcode=t.obj_jobrotation.code;
			let v_client=t.objConfig.client_name.value;
			//if(t.objConfig.dcs_server_IP.value==='192.168.1.40'){
			//	t.self.waitmaterial={
			//		erprefnumber:'12321',
			//		stockcode:'13.14005'
			//	};
			//}
			if(t.self.waitmaterial===false){
				if(t.processVariables.status_lost==='ALT PARÇA YOK'){
					// 2021-04-01 hatalı şekilde kayıp kapatıldıktan sonra görevde kimse yok durumuna geçiş vardı kapatıldı
					// t.eventListener({event:'device-lost-start',description:'',selectedRow:{code:'GÖREVDE KİMSE YOK'},op_transfer:false,status:t.generateClientStatus()});
					// setTimeout(function() {
					//	 t.eventListener({event:'device_init'});
					// }, 500);
					t.eventListener({event:"device-lost-finish"});
					//kimse yokken taşıma yapılmasın denirse alt kısım kapatılacak
					if(t.timer_missing_material!==false){
						clearTimeout(t.timer_missing_material);
						t.timer_missing_material=false;
					}
					t.timer_missing_material=setTimeout(() => {
						t.control_missing_materials(changetype,pos);
					}, 5000);
				}
			}
			for(let i=0;i<t.processVariables.productions.length;i++){
				let row=t.processVariables.productions[i];
				_promises.push(t.control_missing_material(row.erprefnumber,row.opname,row.productcount));
			}
			return Promise.all(_promises).then(function(results){
				let _f=false;
				for(let i=0;i<results.length;i++){
					let row=results[i];
					if(!_f&&row){
						if(row.record==true){
							_f=true;
							t.winmessend('cn_js_workflow',{event:'missing_material',result:'ok',missing_material:t.obj_missing_material});
							break;
						}
					}else{
						break;
					}
				}
				if(_f){//eksik malzeme var
					if(t.processVariables.status_workflowprocess!=='-'){
						return _f;
					}
					if(changetype=='delivered'){
						if(t.timer_missing_material!==false){
							clearTimeout(t.timer_missing_material);
							t.timer_missing_material=false;
						}
						if(t.processVariables.status_lost==='PLANLI TAŞIMA BEKLEME'&&(t.case_movement_target_time===false||(t.case_movement_target_time!==false&&t.case_movement_target_time<_time))){
							t.db.sequelize.transaction(function (_tr) {
								_promises=[];
								if(t.processVariables.status_lost!=='-'){
									let _obj_where_cld={
										type:{[Op.in]: ['l_c', 'l_c_t', 'l_e', 'l_e_t']}
										,client:v_client
										,finish:{[Op.eq]:null}
									};
									_promises.push(t.cjr_client_lost_details(_tr,_obj_where_cld,_time,{descriptionlost:''}));
								}
								if(t.timer_missing_material!==false){
									clearTimeout(t.timer_missing_material);
									t.timer_missing_material=false;
								}
								let __clt='MALZEME BEKLEME';
								let _obj_cld_update={
									losttype: __clt
									,descriptionlost:__clt
								};
								t.setStatus(__clt);
								t.processVariables.lastprodstarttime=null;
								_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null}));
								t.lost_recursive=false;
								_promises.push(t.create_lost_records(_tr,_time,_obj_cld_update,_time,t.processVariables.productions,changetype+'-'+pos+'-'+t.case_movement_target_time));
								return Promise.all(_promises);
							}).then(function () {
								_promises=[];
								while(t.tmp_queue.length>0){
									_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
								}
								if(_f){
									if(!t.show_delivery){
										t.eventListener({event:'device_init'});
									}
									if(t.timer_missing_material!==false){
										clearTimeout(t.timer_missing_material);
										t.timer_missing_material=false;
									}
									t.timer_missing_material=setTimeout(() => {
										t.control_missing_materials(changetype,pos);
									}, 10000);
								}
								return Promise.all(_promises).then(function(){
									//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
								});
							}).catch(function (err) {
								t.mjd('rollback-waitmaterial');
								console.log(err);
								return null;
							});
						}else{
							t.timer_missing_material=setTimeout(() => {
								t.control_missing_materials(changetype,pos);
							}, 10000);
						}
						return null;
					}
					if(changetype=='waitmaterial'){
						if(t.self.waitmaterial!==false){
							if(t.processVariables.status_lost!=='ALT PARÇA YOK'){
								let desc=t.self.waitmaterial.erprefnumber+' için '+t.self.waitmaterial.stockcode+' malzemesi erp stoğunda bulunamadı.';
								t.eventListener({event:'device-lost-start',description:desc,selectedRow:{code:'ALT PARÇA YOK'},op_transfer:false,status:t.generateClientStatus()});
								setTimeout(function() {
									t.eventListener({event:'device_init'});
								}, 500);
							}
							if(t.timer_missing_material!==false){
								clearTimeout(t.timer_missing_material);
								t.timer_missing_material=false;
							}
							t.timer_missing_material=setTimeout(() => {
								t.control_missing_materials(changetype,pos);
							}, 5000);
							return;
						}
						if(t.processVariables.status_lost!=='MALZEME BEKLEME'&&t.processVariables.status_lost!=='PLANLI TAŞIMA BEKLEME'){
							t.db.sequelize.transaction(function (_tr) {
								_promises=[];
								if(t.processVariables.status_lost!=='-'){
									let _obj_where_cld={
										type:{[Op.in]: ['l_c', 'l_c_t', 'l_e', 'l_e_t']}
										,client:v_client
										,finish:{[Op.eq]:null}
									};
									_promises.push(t.cjr_client_lost_details(_tr,_obj_where_cld,_time,{descriptionlost:''}));
								}
								if(t.timer_missing_material!==false){
									clearTimeout(t.timer_missing_material);
									t.timer_missing_material=false;
								}
								let __clt=( (t.case_movement_target_time===false||(t.case_movement_target_time!==false&&t.case_movement_target_time>_time))?'PLANLI TAŞIMA BEKLEME':'MALZEME BEKLEME' );
								let _obj_cld_update={
									losttype: __clt
									,descriptionlost:__clt
								};
								t.setStatus(__clt);
								t.processVariables.lastprodstarttime=null;
								_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null}));
								t.lost_recursive=false;
								_promises.push(t.create_lost_records(_tr,_time,_obj_cld_update,_time,t.processVariables.productions,changetype+'-'+pos+'-'+t.case_movement_target_time));
								return Promise.all(_promises);
							}).then(function () {
								_promises=[];
								while(t.tmp_queue.length>0){
									_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
								}
								if(_f){
									if(!t.show_delivery){
										t.eventListener({event:'device_init'});
									}
									if(t.timer_missing_material!==false){
										clearTimeout(t.timer_missing_material);
										t.timer_missing_material=false;
									}
									t.timer_missing_material=setTimeout(() => {
										t.control_missing_materials('delivered',5);
									}, (t.processVariables.status_lost==='PLANLI TAŞIMA BEKLEME'?60000:10000));
								}
								return Promise.all(_promises).then(function(){
									//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
								});
							}).catch(function (err) {
								t.mjd('rollback-waitmaterial');
								console.log(err);
								return null;
							});
						}else{
							if(!t.show_delivery){
								t.eventListener({event:'device_init'});
							}
							if(t.timer_missing_material!==false){
								clearTimeout(t.timer_missing_material);
								t.timer_missing_material=false;
							}
							t.timer_missing_material=setTimeout(() => {
								t.control_missing_materials('delivered',6);
							}, 10000);
						}
					}
					return _f;
				}else{//eksik malzeme yok
					t.str_missing_material='';
					t.obj_missing_material=false;
					t.case_movement_target_time=false;
					if(changetype=='delivered'||changetype=='waitmaterial'){
						if(t.processVariables.status_work!=='-'){
							t.db.sequelize.transaction(function (_tr) {
								if(t.processVariables.status_lost==='MALZEME BEKLEME'||t.processVariables.status_lost==='PLANLI TAŞIMA BEKLEME'){
									if(t.processVariables.operators.length>0){
										_promises=[];
										//üretime geçiş
										let _obj_where_cld={
											type:{[Op.in]: ['l_c', 'l_c_t', 'l_e', 'l_e_t']}
											,client:v_client
											,finish:{[Op.eq]:null}
										};
										_promises.push(t.cjr_client_lost_details(_tr,_obj_where_cld,_time,{descriptionlost:''}));
										t.processVariables.lastprodstarttime=_time;
										t.processVariables.lastprodtime=_time;
										t.setStatus('-');
										_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
										return Promise.all(_promises).then(function(){
											_promises=[];
											while(t.tmp_queue.length>0){
												_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
											}
											return Promise.all(_promises).then(function(){
												if(!t.show_delivery){
													setTimeout(function() {
														t.eventListener({event:'device_init'});
													}, 2000);
												}
											});
										});
									}else{
										//görevde kimse yok kaybına geçiş
										_promises=[];
										let _obj_where={
											type:{[Op.in]: ['l_c', 'l_c_t','l_e', 'l_e_t']}
											,client:v_client
											,start:{[Op.ne]:null}
											,finish:{[Op.eq]:null}
										};
										t.cpd_time=false;
										t.processVariables.status_employee=0;
										t.processVariables.operators=[];
										t.processVariables.status_workflowprocess='-';
										t.setStatus('GÖREVDE KİMSE YOK');
										t.processVariables.lastprodstarttime=null;
										t.processVariables.lastprodtime=_time;
										_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_e','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:''}));
										_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
										_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
										_obj_where.type={[Op.in]: ['l_c', 'l_c_t']};
										_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: 'GÖREVDE KİMSE YOK',descriptionlost:'',sourcedescriptionlost:'control_missing_materials'},{finish:_time},true));
										return Promise.all(_promises).then(function(){
											if(!t.show_delivery){
												setTimeout(function() {
													t.eventListener({event:'device_init'});
												}, 2000);
											}
										});
									}
								}
								return null;
							}).then(function () {
								_promises=[];
								while(t.tmp_queue.length>0){
									_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
								}
								return Promise.all(_promises).then(function(){
									//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
									t.winmessend('cn_js_workflow',{event:'missing_material',result:'ok',missing_material:t.obj_missing_material});
								});
							}).catch(function (err) {
								t.mjd('rollback-delivered');
								console.log(err);
								return null;
							});
						}
					}
					return _f;
				}
			});
		}else{
			t.case_movement_target_time=false;
		}
		return null;
	}
	control_device_delivery(){
		let t=this;
		//console.log('control_device_delivery');
		let v_client=t.objConfig.client_name.value;
		return t.db.sequelize.query('SELECT record_id '+
			' from task_cases '+
			' where movementdetail is null and casetype=\'O\' and start is not null '+
			' and cast(packcapacity as int)=cast(quantityremaining as int) and cast(quantityremaining as int)>0 '+
			' and status in (\'WORK\',\'WAIT_FOR_DELIVERY\') and client=:client and start>\'2018-10-20\' '+ 
			' order by erprefnumber,start,id limit 1 '
		, {  replacements:{client:v_client},type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
			if(results&&results.length>0){
				t.show_delivery=true;
				setTimeout(() => {
					t.processVariables.lastprodstarttime=null;
					t.winmessend('cn_js_workflow',{event:'open_delivery_screen',result:'ok',casenumber:false,message:false});
				}, 200);
			}else{
				t.bool_control_device_delivery=false;
			}
			return null;
		});
	}
	control_task_cases(_tr){
		//göreve katılma esnasında tesellüm evet olan istasyonda task_case kaydı kontrol edilecek
		let t=this;
		let v_client=t.objConfig.client_name.value;
		let _time=t.tickTimer.obj_time.strDateTime;
		return t.db.sequelize.query('SELECT record_id,erprefnumber,opcode,opname,casename,casenumber,cast(packcapacity as int)packcapacity,cast(quantityremaining as int)quantityremaining,caselot '+
				' from task_cases '+
				' where movementdetail is null and casetype=\'O\' and start is not null and status in (\'WORK\',\'WAIT_FOR_DELIVERY\') and client=:client '+ 
				' order by erprefnumber,start,id limit 10 '
		, {  replacements:{client:v_client},type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
			if(results&&results.length===0){
				if(t.processVariables.productions.length>0){
					let _erprefs=[];
					for(let i=0;i<t.processVariables.productions.length;i++){
						let item=t.processVariables.productions[i];
						if(_erprefs.indexOf(item.erprefnumber)===-1&&item.delivery===true){
							_erprefs.push(item.erprefnumber);
						}
					}
					if(_erprefs.length>0){
						return t.db.sequelize.query('select * '+
						' from task_cases '+
						' where movementdetail is null and casetype=\'O\' and start is not null and status in (\'WAIT\') and client=:client and erprefnumber in (:erprefnumber) '+
						' order by start,casenumber ', {  replacements:{client:v_client,erprefnumber:_erprefs},type: t.db.sequelize.QueryTypes.SELECT})
							.then(function(results_tc) {
								let _promises=[];
								let erpref='';
								let update_tc=function(_tr,v_idx){
									let _prom=[];
									_prom.push(t.updateRecord(_tr,'task_cases',_time,{record_id:v_idx},{status:'WORK'}));
									return Promise.all(_prom);
								};
								if(results_tc&&results_tc.length>0){
									for(let i=0;i<results_tc.length;i++){
										let item=results_tc[i];
										if(erpref!==item.erprefnumber){
											erpref=item.erprefnumber;
											_promises.push(update_tc(_tr,item.record_id));
										}
									}
									return Promise.all(_promises);
								}else{
									throw new Error('Tesellüm için kasa bulunamadı!');
								}
							});
					}else{
						return null;
					}
				}
				return null;
			}
			return null;
		});
	}
	control_operationAuthorization_supervision(){
		let t=this;
		const _time=t.tickTimer.obj_time.strDateTime;
		if(t.json_status.operationAuthorization.nextControl!==null){
			if(t.json_status.operationAuthorization.nextControl<=_time){
				if(!t.show_delivery&&(t.processVariables.status_lost==='-'||t.processVariables.status_lost==='YETKİNLİK GÖZLEM')){
					let _tmp_opnames=[];
					//tetik için durum uygunluğu tespit ediliyor
					if(t.objParam.task_finishQuantityReach.value===true&&t.show_scrap_before_finish){
						//iş bitmiş iş sonu ıskarta bilgisi isteniyor ise sorma
						t.json_status.operationAuthorization.nextControl=null;
						t.json_status.operationAuthorization.data=[];
					}else{
						if(t.processVariables.status_lost==='-'){
							//kontrol edilmesi gereken sicil,operasyon devam ediyor mu?
							for(let i=0;i<t.json_status.operationAuthorization.data.length;i++){
								let row=t.json_status.operationAuthorization.data[i];
								if(row.level==1){
									if(t.getIndex(t.processVariables.operators,'code',row.employee)!==-1){
										//operatör çalışanlararasında var, operasyon var mı
										if(t.getIndex(t.processVariables.productions,'opname',row.opname)!==-1){
											//operasyon hala çalışıyor
											_tmp_opnames.push(row.opname);
										}
									}
								}
							}
							if(_tmp_opnames.length>0){
								t.eventListener({event:'operationAuthorization_supervision',opnames:_tmp_opnames.join(',')});
							}
						}
					}
					setTimeout(() => {
						t.control_operationAuthorization_supervision();
					}, 60000);
				}else{
					let _d=new timezoneJS.Date(t.json_status.operationAuthorization.nextControl);
					_d.setMinutes(_d.getMinutes()+5);
					t.json_status.operationAuthorization.nextControl=_d.toString();
					t.mjd('operationAuthorization_nextControl:'+t.json_status.operationAuthorization.nextControl);
					setTimeout(() => {
						t.control_operationAuthorization_supervision();
					}, 1000);
				}
			}else{
				setTimeout(() => {
					t.control_operationAuthorization_supervision();
				}, 60000);
			}
		}else{
			setTimeout(() => {
				t.control_operationAuthorization_supervision();
			}, 60000);
		}
	}
	initApp(){
		let t=this;
		if(t.objConfig.workflow.value!='El İşçiliği'){
			t.eventListener({event:'device_init'});
		}else{
			t.eventListener({event:'device_init'});
		}
	}
	remote_quality_confirm(obj){
		let t=this;
		if(t.processVariables.status_lost==='KALİTE ONAY'&&t.objParam.process_remoteQualityConfirmation.value===true){
			const _time=t.tickTimer.obj_time.strDateTime;
			let _jrday=t.obj_jobrotation.day;
			let _jrcode=t.obj_jobrotation.code;
			let v_client=t.objConfig.client_name.value;
			let _promises=[];
			let _cur_losttype='-';
			let _obj_where={
				type:{[Op.in]: ['l_c', 'l_c_t','l_e', 'l_e_t']}
				,client:v_client
				,start:{[Op.ne]:null}
				,finish:{[Op.eq]:null}
			};
			t.db.sequelize.transaction(function (_tr) {
				let production_transition=function(_clt,_adl,_cmp){
					if(t.objParam.task_materialpreparation.value===true&&_cmp===true&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
						//eksik malzeme kontrolü
						return Promise.all([t.control_missing_materials()]).then(function(result) {
							if(result[0]==true){
								if(t.timer_missing_material!==false){
									clearTimeout(t.timer_missing_material);
									t.timer_missing_material=false;
								}
								t.timer_missing_material=setTimeout(() => {
									t.control_missing_materials('waitmaterial',7);
								}, 10000);
								return Promise.all([production_transition('MALZEME BEKLEME',_adl,false)]);
							}else{
								return Promise.all([production_transition(_clt,_adl,false)]);
							}
						});
					}else{
						let _prom_p_t=[];
						//üretime geçiş, görevde kimse yok şeklinde
						t.cpd_time=false;
						t.processVariables.status_employee=0;
						t.processVariables.operators=[];
						t.processVariables.status_workflowprocess='-';
						if(_clt==='MALZEME BEKLEME'){
							_clt=( (t.case_movement_target_time===false||(t.case_movement_target_time!==false&&t.case_movement_target_time>_time))?'PLANLI TAŞIMA BEKLEME':'MALZEME BEKLEME' );
						}
						t.setStatus(_clt);
						t.processVariables.lastprodstarttime=null;
						t.processVariables.lastprodtime=_time;
						_prom_p_t.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_e','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:_adl}));
						_prom_p_t.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
						_prom_p_t.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
						_obj_where.type={[Op.in]: ['l_c', 'l_c_t']};
						_prom_p_t.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: _clt,descriptionlost:'',sourcedescriptionlost:'production_transition-0'},{finish:_time},true));
						return Promise.all(_prom_p_t);
					}
				};
				if(t.objParam.process_wsQuality.value===true){
					if(t.processVariables.status_workflowprocess==='QUALITY'){
						//üretime geçiş, görevde kimse yok şeklinde
						_cur_losttype='GÖREVDE KİMSE YOK';
						_promises.push(production_transition(_cur_losttype,obj.description,true));
						let _tmp_data_le={
							type:'l_e'
							,client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,losttype:'KALİTE ONAY'
							,employee:obj.employee
							,descriptionlost:obj.description
							,start:t.processVariables.statustime
							,finish:_time
							,record_id:uuidv4()
						};
						_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
						for(let i=0;i<t.processVariables.productions.length;i++){
							let row=t.processVariables.productions[i];
							let _tmp_data_let={
								type:'l_e_t'
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,losttype:'KALİTE ONAY'
								,tasklist:row.tasklist
								,opcode:row.opcode
								,opnumber:row.opnumber
								,opname:row.opname
								,opdescription:''//row.opdescription
								,employee:obj.employee
								,descriptionlost:obj.description
								,erprefnumber:row.erprefnumber
								,taskfromerp:true
								,start:t.processVariables.statustime
								,finish:_time
								,record_id:uuidv4()
							};
							_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
						}
					}
				}
				return Promise.all(_promises).then(function(){
					_promises=[];
					let _obj_where_cld={
						type:{[Op.in]: ['l_c','l_c_t', 'l_e', 'l_e_t']}
						,client:v_client
						,finish:{[Op.eq]:null}
					};
					_promises.push(t.cjr_client_lost_details(_tr,_obj_where_cld,_time,{descriptionlost:obj.description}));
					return Promise.all(_promises);
				});
			}).then(function () {
				_promises=[];
				while(t.tmp_queue.length>0){
					_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
				}
				t.winmessend('cn_js_workflow',{event:'remote_quality_confirm',result:'ok',message:''});
				return Promise.all(_promises).then(function(){
					//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
				});
			}).catch(function (err) {
				t.mjd('rollback-remote_quality_confirm');
				console.log(err);
				classBase.writeError(err,t.ip,'catch_workflow_device_lost_finish');
				t.tmp_queue=[];
				return Promise.all([t.getClientStatus()]).then(function(){
					t.winmessend('cn_js_workflow',{event:'remote_quality_confirm',result:'err',message:err.toString().replace('Error: ','')});
				});
			});
		}
	}
	remote_quality_rejection(obj){
		let t=this;
		if(t.processVariables.status_lost==='KALİTE ONAY'&&t.objParam.process_remoteQualityConfirmation.value===true){
			let _message=obj.description;
			const _time=t.tickTimer.obj_time.strDateTime;
			let _jrday=t.obj_jobrotation.day;
			let _jrcode=t.obj_jobrotation.code;
			let v_client=t.objConfig.client_name.value;
			let _cur_losttype='İŞ YOK';
			let _taskfinishdescription=obj.description;
			let _taskfinishtype='KALİTE RED';
			let _promises=[];
			t.db.sequelize.transaction(function (_tr) {
				if(t.objParam.process_wsSetup.value===true){
					let _obj_where_c_p_d={
						client:v_client
						,start:{[Op.ne]:null}
						,finish:{[Op.eq]:null}
						,type:{[Op.in]: ['c_p', 'e_p','c_p_unconfirm','e_p_unconfirm']}
					};
					_promises.push(t.cjr_client_production_details(_tr,_obj_where_c_p_d,_time,{taskfinishtype:_taskfinishtype,taskfinishdescription:_taskfinishdescription}));
					
					_promises.push(t.create_production_records(_tr,_time));
					_cur_losttype='USTA BEKLEME';
					t.processVariables.status_workflowprocess='SETUP';
					t.processVariables.status_employee=0;
					t.processVariables.operators=[];
					t.setStatus(_cur_losttype);
					t.processVariables.lastprodstarttime=null;
					//t.processVariables.lastprodtime=null;
					_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c','l_e','l_c_t','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:obj.description}));
					_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
					_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:_time}));
					let _tmp_data_le={
						type:'l_e'
						,client:v_client
						,day:_jrday
						,jobrotation:_jrcode
						,losttype:'KALİTE ONAY'
						,employee:obj.employee
						,descriptionlost:obj.description
						,start:t.processVariables.statustime
						,finish:_time
						,record_id:uuidv4()
					};
					_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
					let _tmp_data_lc={
						type:'l_c'
						,client:v_client
						,day:_jrday
						,jobrotation:_jrcode
						,losttype:_cur_losttype
						,start:_time
						,record_id:uuidv4()
					};
					_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lc));
					for(let i=0;i<t.processVariables.productions.length;i++){
						let row=t.processVariables.productions[i];
						let _tmp_data_lct={
							type:'l_c_t'
							,client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,losttype:_cur_losttype
							,tasklist:row.tasklist
							,opcode:row.opcode
							,opnumber:row.opnumber
							,opname:row.opname
							,opdescription:row.opdescription
							,erprefnumber:row.erprefnumber
							,taskfromerp:true
							,start:_time
							,record_id:uuidv4()
						};
						_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
						let _tmp_data_let={
							type:'l_e_t'
							,client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,losttype:'KALİTE ONAY'
							,tasklist:row.tasklist
							,opcode:row.opcode
							,opnumber:row.opnumber
							,opname:row.opname
							,opdescription:''//row.opdescription
							,employee:obj.employee
							,descriptionlost:obj.description
							,erprefnumber:row.erprefnumber
							,taskfromerp:true
							,start:t.processVariables.statustime
							,finish:_time
							,record_id:uuidv4()
						};
						_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
					}
					return Promise.all(_promises);				
				}else{
					let _obj_where_c_p_d={
						client:v_client
						,start:{[Op.ne]:null}
						,finish:{[Op.eq]:null}
						,type:{[Op.in]: ['c_p', 'e_p','c_p_unconfirm','e_p_unconfirm']}
					};
					_promises.push(t.cjr_client_production_details(_tr,_obj_where_c_p_d,_time,{taskfinishtype:_taskfinishtype,taskfinishdescription:_taskfinishdescription}));
					return Promise.all(_promises).then(function(){
						_promises=[];
						let _tmp_data_le={
							type:'l_e'
							,client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,losttype:'KALİTE ONAY'
							,employee:obj.employee
							,descriptionlost:obj.description
							,start:t.processVariables.statustime
							,finish:_time
							,record_id:uuidv4()
						};
						_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
						for(let i=0;i<t.processVariables.productions.length;i++){
							let row=t.processVariables.productions[i];
							let _tmp_data_let={
								type:'l_e_t'
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,losttype:'KALİTE ONAY'
								,tasklist:row.tasklist
								,opcode:row.opcode
								,opnumber:row.opnumber
								,opname:row.opname
								,opdescription:''//row.opdescription
								,employee:obj.employee
								,descriptionlost:obj.description
								,erprefnumber:row.erprefnumber
								,taskfromerp:true
								,start:t.processVariables.statustime
								,finish:_time
								,record_id:uuidv4()
							};
							_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
						}
						t.cpd_time=false;
						t.processVariables.lastprodstarttime=null;
						//t.processVariables.lastprodtime=null;
						t.processVariables.status_work='-';
						t.processVariables.status_employee=0;
						t.processVariables.status_tpp=0;
						t.processVariables.status_workflowprocess='-';
						t.setStatus(_cur_losttype);
						//görevde kimse yok durumunda iş bitirilebilmesi için eklendi
						_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c','l_e','l_c_t','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:obj.description}));
						_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));		
						t.processVariables.operators=[];
						t.processVariables.productions=[];
						let _tmp_data={
							type:'l_c'
							,client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,losttype:_cur_losttype
							,descriptionlost:_cur_losttype
							,start:_time
							,record_id:uuidv4()
						};
						_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
						_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
						return Promise.all(_promises);
					});
				}
			}).then(function () {
				_promises=[];
				while(t.tmp_queue.length>0){
					_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
				}
				t.winmessend('cn_js_workflow',{event:'remote_quality_rejection',result:'ok',message:_message});
				return Promise.all(_promises).then(function(){
					//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
				});
			}).catch(function (err) {
				t.mjd('rollback-remote_quality_rejection');
				console.log(err);
				classBase.writeError(err,t.ip,'catch_workflow_device_work_finish_multi');
				t.tmp_queue=[];
				return Promise.all([t.getDeviceInitValues('catch-device_work_finish_multi')]).then(function(){
					t.winmessend('cn_js_workflow',{event:'remote_quality_rejection',result:'err',message:err.toString().replace('Error: ','')});
				});
			});
		}
	}
	mh_case_movement_target_time(obj){
		let t=this;
		let val=(obj.targettime!==null&&obj.targettime!=='')?obj.targettime:false;
		console.log('mh_case_movement_target_time:',obj.targettime,' gorevsayi:',obj.gorevsayi);
		if(t.case_movement_target_time!==val){
			t.case_movement_target_time=val;
			if(obj.gorevsayi>0){
				if(t.timer_missing_material!==false){
					clearTimeout(t.timer_missing_material);
					t.timer_missing_material=false;
				}
				setTimeout(() => {
					t.control_missing_materials('waitmaterial',17);
				}, 5000);
			}
		}
		t.winmessend('cn_js_workflow',{event:'mh_case_movement_info',result:'ok',message:'',gorevsayi:obj.gorevsayi});
	}
	get_sop_documents_on_join(v_client,_jrday,_jrcode,v_opnames,v_employee,v_control){
		let t=this;
		if(t.objParam.app_showSOP.value===true&&t.objParam.app_showSOPOnJoin.value===true){
			let objRep={
				opnames:v_opnames
			};
			if(v_control){
				objRep.typeCPD="e_p";
				objRep.client=v_client;
				objRep.day=_jrday;
				objRep.jobrotation=_jrcode;
				objRep.employee=v_employee;
			}
			let v_select=(v_control===true?"case when exists(SELECT * from client_production_details where type=:typeCPD and client=:client and day=:day and jobrotation=:jobrotation and employee=:employee and opname in (:opnames) limit 1) then true else false end":"false");
			let sql=`
			select * 
			from (
				select d.* 
					,`+v_select+` is_record_exists
				from documents d 
				where d.type='SOP' and d.opname in (:opnames) 
			)d 
			where d.is_record_exists=false
			order by d.opname,d.path
			`;
			return t.db.sequelize.query(sql, {replacements:objRep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
				if(results.length>0){
					const _time=t.tickTimer.obj_time.strDateTime;
					let v_client=t.objConfig.client_name.value;
					let _event='set_sop_documents';
					t.sop_documents=results;
					let objRet={
						client:v_client
						,day:_jrday
						,jobrotation:_jrcode
						,employee:v_employee
						,data:results
					}
					setTimeout(() => {
						//cihazı kilitle, kayıp kayıtlarını aç, veriyi cihaza gönder, ekranda sop dokümanları görünsün
						if(t.processVariables.status_lost==='SOP DOKÜMAN GÖRÜNTÜLEME'){
							t.setAllOutputs();
							if(t.objParam.task_materialpreparation.value===true&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
								if(t.timer_missing_material!==false){
									clearTimeout(t.timer_missing_material);
									t.timer_missing_material=false;
								}
							}
							t.processVariables.lastprodstarttime=null;
							t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',data:objRet});
						}
						if(t.processVariables.status_lost==='-'){
							let _cur_losttype='SOP DOKÜMAN GÖRÜNTÜLEME';
							let _promises=[];
							t.setAllOutputs();
							t.setStatus(_cur_losttype);
							if(t.objParam.task_materialpreparation.value===true&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
								if(t.timer_missing_material!==false){
									clearTimeout(t.timer_missing_material);
									t.timer_missing_material=false;
								}
							}
							t.db.sequelize.transaction(function (_tr) {
								t.processVariables.lastprodstarttime=null;
								_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null}));
								let _obj_cld_update={
									losttype: _cur_losttype
									,descriptionlost:_cur_losttype
									,employee:v_employee
								};
								t.lost_recursive=false;
								_promises.push(t.create_lost_records(_tr,_time,_obj_cld_update,_time,t.processVariables.productions,_event));
								return Promise.all(_promises);
							}).then(function () {
								_promises=[];
								t.status='';
								while(t.tmp_queue.length>0){
									_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
								}
								t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',data:objRet});
								return Promise.all(_promises);
							}).catch(function (err) {
								t.mjd('rollback-'+_event);
								console.log(err);
								classBase.writeError(err,t.ip,'catch_workflow_'+_event);
								t.tmp_queue=[];
								return Promise.all([t.getClientStatus()]).then(function(){
									t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
								});
							});
						}
					}, 3000);
				}
			});
		}
		return;
	}
	get_all_documents(v_opnames){
		let t=this;
		let objRep={
			opnames:v_opnames
		};
		let sql=`
			select d.* 
			from documents d 
			where ( (d.type='ISG' ) or ( d.type in ('SOP') and d.opname in (:opnames) ) ) 
			order by case when d.type='ISG' then 1 else 2 end, d.opname, d.path
		`;
		return t.db.sequelize.query(sql, {replacements:objRep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
			t.all_documents=results;
		});
	}
	set_penetrations_response(params){
		let t=this;
		let _event='set_penetrations_response';
		setTimeout(() => {
			t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',title:params.title,text:params.text});
		}, 3000);
	}
	get_activity_control_questions(){
		let t=this;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		let v_client=t.objConfig.client_name.value;
		if((t.processVariables.status_workflowprocess==='SETUP'||t.processVariables.status_workflowprocess==='QUALITY')
			&&t.activity_control_questions===false
			&&t.processVariables.operators.length>0
			&&t.processVariables.productions.length>0
			&&_jrday!==false
			&&_jrcode!==false
		){
			if(t.self&&t.self.socket_io_client){
				let v_employee=t.processVariables.operators[0].code;
				let v_data=arraySort(t.processVariables.productions, 'opname');
				let v_opnames=[];
				for(let i=0;i<v_data.length;i++){
					let tmp_opname=v_data[i].opname;
					if(v_opnames.indexOf(tmp_opname)==-1){
						v_opnames.push(tmp_opname);
					}
				}
				t.self.socket_io_client.send({event:'cn_sn',data:{event:'activity_control_questions|get',client:v_client,day:_jrday,jobrotation:_jrcode,opnames:v_opnames,employee:v_employee}});
			}
		}
		return;
	}
	set_activity_control_questions(data){
		let t=this;
		t.activity_control_questions=data;
	}
	get_client_control_document(v_client,v_opnames){
		let t=this;
		let objRep={
			client:v_client
			,opnames:v_opnames
		};
		let sql=`
			select d.* 
			from documents d 
			where d.type in ('TEZGAH AYAR FÖYÜ') and d.opcode=:client and d.opname in (:opnames)
			order by d.opname,d.path
		`;
		return t.db.sequelize.query(sql, {replacements:objRep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
			if(results.length > 0){
				t.client_control_document=results;
			}
		});
	}
	imported_api_call(params){
		let t=this;
		//console.log('imported_api_call',params);
		switch(params.data.event){
			case 'holo_start':{
				t.holo_status=true;
				t.self.rabbitmq_send.send({
					event: params.data.event,
					pid: t.self.rabbitmq_send.pid
				});
				t.winmessend('cn_js_workflow',{event:'set_holo_status',result:'ok',message:'Hololens entegrasyonu başlatıldı.',holo_status:t.holo_status});
				break
			}
			case 'holo_finish':{
				let _promises=[];
				t.self.rabbitmq_send.send({
					event: params.data.event,
					pid: t.self.rabbitmq_send.pid
				});
				t.db.sequelize.transaction(function (_tr) {
					t.holo_status=false;
					t.objParam.app_mixedRealityDataSend.value=t.holo_status;
					_promises.push(t.updateRecord(_tr,'client_params',t.tickTimer.obj_time.strDateTime,{name:'app_mixedRealityDataSend'},{client:t.objConfig.client_name.value,name:'app_mixedRealityDataSend',value:t.holo_status}));
					return Promise.all(_promises);
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					return Promise.all(_promises).then(function(){
						t.winmessend('cn_js_workflow',{event:'set_holo_status',result:'ok',message:'Hololens entegrasyonu sonlandırıldı.',holo_status:t.holo_status});
						return null;
					});
				}).catch(function (err) {
					console.log('rollback');
					console.log(err);
					classBase.writeError(err,t.ip,'catch-client_params|save');
					t.winmessend('cn_js_dcsclient',{event:'set_holo_status',result:'err',message:err.toString().replace('Error: ',''),app_reset:false});
					t.tmp_queue=[];
					return null;
				});
				break
			}
			case 'task_production_case':{
				t.winmessend('cn_js_workflow',{event:'save_productioncasecontrol_requirements',result:'ok',message:'',data:params.data});
				break;
			}
		}
	}
	eventListener(arg){
		let t=this;
		let _event=arg.event;
		t.mjd(_event);
		const _time=t.tickTimer.obj_time.strDateTime;
		let _timems=t.tickTimer.obj_time.ms;
		let _jrday=t.obj_jobrotation.day;
		let _jrcode=t.obj_jobrotation.code;
		let v_client=t.objConfig.client_name.value;
		let v_workflow=t.objConfig.workflow.value;
		t.processVariables._time=_time;
		t.processVariables._timems=_timems;
		t.processVariables._jrday=_jrday;
		t.processVariables._jrcode=_jrcode;
		t.processVariables.v_client=v_client;
		if(t.processVariables.status_lost==='İŞ YOK'){
			t.activity_control_questions=false;
		}
		try {
			switch(_event) {
			case 'button_click':{
				if(arg.btn==='btn-work-finish'){
					t.processVariables.status_tpp=180;
				}
				console.log(arg);
				break;
			}
			case 'manual-StartWork-employee':{
				//objParam içindeki değere göre employee tablosundaki code veya secret alanından sorgulanacak
				//parametre değeri 
				//	barkod ise {secret:data}
				//	rfid ise {secret:data}-data uzunluğu=8 ise ilk3*65536+kalan8
				/*
					'08862904'.substr(0,3)*65536+'08862904'.substr(3,5)
				*/
				let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
				let objwhere={[t.processVariables.emp_identify]:v_data};
				if(t.objParam.app_empSecret.value===true){
					objwhere.secret=arg.secret;
				}
				t.db.sequelize.transaction().then(function (_tr) {
					let _promises=[];
					return Promise.all([t.isEmployeeFreeManual(_tr,objwhere,true)]).then(function(_obj_res){
						_obj_res=_obj_res[0];
						return _obj_res.code;
					}).then(function (_obj_res) {
						_promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',employee:_obj_res});
						return Promise.all(_promises).then(function(){
							return _tr.commit();
						});	
					}).catch(function (err) {
						t.mjd('rollback-'+_event);
						console.log(err);
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						t.tmp_queue=[];
						return _tr.rollback();
					});
				});
				break;
			}
			case 'product_trees|get':{
				let _w='';//+(( (t.ip&&(t.ip.indexOf('10.10')>-1) ))?' and client=:client ':'');
				let rep={materialtype:'O'};
				//if(t.ip&&(t.ip.indexOf('10.10')>-1) ){
				//	rep.client = v_client;
				//}
				if(arg.name){
					_w+=' and name like :name ';
					rep.name='%'+arg.name+'%';
				}
				let _promises=[];
				if(v_workflow==='Sabit Üretim Aparatlı'){
					let getDeviceOperations=function(_client){
						let _sql='SELECT md.opname '+
						' from client_details cd '+
						' join mould_details md on md.mould=cd.code '+
						' where cd.finish is null and md.finish is null and cd.client=:client and cd.iotype=:iotype and cd.ioevent=\'input\' '+
						' GROUP BY md.opname '+
						' ORDER BY md.opname';
						return t.db.sequelize.query(_sql,{replacements:{client:_client,iotype:'I'}, type: t.db.sequelize.QueryTypes.SELECT}).then(function(results){
							let _ret=[];
							for(let i=0;i<results.length;i++){
								_ret.push(results[i].opname);
							}
							return _ret;
						});
					};
					_promises.push(getDeviceOperations(v_client));
				}
				return Promise.all(_promises).then(function(p_res){
					if(v_workflow==='Sabit Üretim Aparatlı'){
						if(p_res[0].length>0){
							_w+=' and name in (:p_res) ';
							rep.p_res=p_res[0];
						}
					}
					let sql='SELECT name,description,tpp,code,coalesce(number,0)number,COALESCE(notificationtime,2)notificationtime from product_trees where materialtype=:materialtype and finish is null '+_w+' order by name limit 15';
					return t.db.sequelize.query(sql,{ replacements: rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
						t.winmessend('cn_js_workflow',{event:_event,product_trees:results});
						return null;
					});
				});
			}
			case 'product_trees|get|mp':{
				let rep={name:arg.opname};
				if(!arg.opname){
					t.winmessend('cn_js_workflow',{event:_event,product_trees:[]});
					return;
				}
				let sql='SELECT * from product_trees where finish is null and materialtype in (\'H\',\'K\') and name=:name order by name limit 15';
				return t.db.sequelize.query(sql,{ replacements: rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
					t.winmessend('cn_js_workflow',{event:_event,product_trees:results});
					return null;
				});
			}
			case 'client_details|get':{
				let sql='SELECT code,max(description)description from client_details where finish is null and COALESCE(ioevent,\'\') in (\'\',\'input\',\'output\') GROUP BY code order by code';
				t.db.sequelize.query(sql,{ type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
					t.winmessend('cn_js_workflow',{event:_event,client_details:results});
				});
				break;
			}
			case 'task_lists|add':{
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					let tmp_caselabel_control=function(_tr,_time,_data){
						return t.db.case_labels.findOne({where:{erprefnumber:_data.erprefnumber,status:{[Op.in]: ['OPEN', 'DELETED','LABELDELETED']},canceltime:{[Op.eq]:null}}},{transaction: _tr}).then(function(result){
							if(result){
								return;
							}else{
								throw new Error('Girilen iş emrinin etiketi yazdırılmamış. '+_data.erprefnumber);
							}
						});
					};
					let tmp_fn_create=function(_tr,_time,_data){
						_data.code=(_data.opname).substring(0, 20)+uuidv4();
						_data.record_id=uuidv4();
						_data.productdoneactivity=0;
						_data.productdonejobrotation=0;
						_data.scrapactivity=0;
						_data.scrapjobrotation=0;
						_data.scrappart=0;
						_data.opnumber=(_data.opnumber!==null?_data.opnumber:0);
						return Promise.all([t.createRecord(_tr,'task_lists',_time,_data)]);
					};
					if( (t.ip&&(t.ip.indexOf('172.16')>-1)) &&t.objParam.task_materialpreparation.value===true){
						_promises.push(tmp_caselabel_control(_tr,_time,arg.data));
					}
					if(arg.data.tpp>0){//listeden seçilmiş iş
						_promises.push(tmp_fn_create(_tr,_time,arg.data));
					}else{//elle yazılan iş, bilgileri bulunmalı
						let _where = {name:arg.data.opname,materialtype:'O',finish:{[Op.eq]:null}};
						//if(t.ip&&(t.ip.indexOf('10.10')>-1) ){
						//	_where = {name:arg.data.opname,materialtype:'O',client:v_client,finish:{[Op.eq]:null}};
						//}
						return t.db.product_trees.findOne({where:_where},{transaction: _tr}).then(function(result){
							if(result){
								arg.data.opcode=result.code;
								arg.data.opnumber=(result.number!==null?result.number:0);
								arg.data.opdescription=result.description;
								arg.data.tpp=result.tpp;
								if(t.ip&&(t.ip.indexOf('10.10')>-1)&&result.notificationtime&&result.notificationtime>0){
									arg.data.tpp=result.notificationtime;
								}
							}else{
								arg.data.opcode=arg.data.opname;
								arg.data.opnumber=0;
								arg.data.opdescription='Operasyon kaydı ürün ağacında bulunamadı.';
								arg.data.tpp=100;
							}
							_promises.push(tmp_fn_create(_tr,_time,arg.data));
						});
					}
					return Promise.all(_promises);
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.eventListener({event:'task_lists|get'});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess});
					});	
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_lists|add');
					t.tmp_queue=[];
					return Promise.all([t.getDeviceInitValues('catch-task_lists|add')]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'task_lists|get':{
				let _w=(t.objParam.task_listingOtherClients.value===false?(v_workflow==='Lazer Kesim'?' and (client1 ilike :client or client2 ilike :client or client3 ilike :client or client4 ilike :client or client5 ilike :client) ':' and (tl.client ilike :client '+(v_workflow==='El İşçiliği'?' or tl.client is null ':'')+') '):'');
				let rep={client: '%'+v_client+'%'};
				if (t.ip&&(t.ip.indexOf('10.10')>-1) ){
					rep.client2=v_client;
				}
				if(arg.opname){
					_w+=' and tl.opname=:opname ';
					rep.opname=arg.opname;
				}
				if(arg.name){
					//_w+=' and (tl.opname ilike :code '+(v_workflow!=='El İşçiliği'?' or tl.erprefnumber ilike :code ':'')+')';
					if(v_workflow!=='Lazer Kesim'){
						if(v_workflow==='El İşçiliği'){
							if(t.objParam.task_reworkType.value==='Yeniden İşlem'){
								_w+=' and (tl.opname ilike :code)';
							}else{
								_w+=' and (tl.opname ilike :code or tl.erprefnumber ilike :code)';
							}
						}else{
							_w+=' and (tl.opname ilike :code or tl.erprefnumber ilike :code)';
						}
						rep.code='%'+arg.name+'%';
					}else{
						_w+=' and (description ilike :code)';
						rep.code='%'+arg.name+'%';
					}
				}
				if(t.objParam.task_selectGroupPlan.value===true){
					_w+=' and coalesce(tl.idx,\'\')<>\'\' ';
				}
				let sql='SELECT tl.id,coalesce(tl.idx,\'\') idx,tl.code,tl.erprefnumber,tl.opcode,tl.opnumber,tl.opname,tl.opdescription ' +
				' 	,tl.productcount,tl.productdonecount,to_char(tl.plannedstart, \'DD/MM/YYYY\')plannedstart ' +
				' 	,tl.client,tl.taskfromerp,tl.productcount-tl.productdonecount remaining,tl.tpp,COALESCE(tl.mouldaddress,\'\') mouldaddress ' +
				' 	,case when tl.type=\'time\' then \'Zaman\' when tl.type=\'quantity\' then \'Miktar\' else \'\' end "type" ' +
				' 	,case when tl.start is null then \'\' else \'drow-started\' end rowclass '+
				' 	,tl.start,tl.record_id,pt_d.delivery,pt_p.pack as casename,cast(coalesce(nullif(pt_p.packcapacity,0),100) as integer)packcapacity ' +
				' 	,cast(floor((tl.productcount-tl.productdonecount)/coalesce(nullif(pt_p.packcapacity,0),100)) as integer)remainingpack ' +
				'	,pt_p.preptime,case when tc.id is null then 0 else 1 end taskcaseexists ' +
				'	,round((COALESCE(tl.tpp,pt_d.tpp)*COALESCE(pt_d.productionmultiplier,100)*pt_d.intervalmultiplier)/100,2) "interval" ' +
				'	,round(COALESCE(tl.tpp,pt_d.tpp)*COALESCE(pt_d.productionmultiplier,100)/100,2)t_p_p ' +
				' ,COALESCE(pt_d.notificationtime,2)notificationtime '+
				' FROM task_lists tl ' +//and cl.status=\'OPEN\' and cl.canceltime is null 
				' left join product_trees pt_d on pt_d.materialtype=\'O\' and pt_d.finish is null and pt_d.name=tl.opname ' +(( (t.ip&&(t.ip.indexOf('10.10')>-1) ))?' and pt_d.client=:client2 ':'')+
				' left join product_trees pt_p on pt_p.materialtype=\'K\' and pt_p.finish is null and pt_p.name=tl.opname ' +
				' left join (SELECT MIN(id)id,erprefnumber,opname from task_cases where finish is null and deliverytime is null and movementdetail is null GROUP BY erprefnumber,opname)tc on tc.erprefnumber=tl.erprefnumber and tc.opname=tl.opname '+
				' WHERE tl.finish is null  ' +//and pt_p.isdefault=true
				'		and (tl.type=\'time\' or (tl.type=\'quantity\' '+(( (t.ip&&(t.ip.indexOf('10.10')>-1) ))?' ':' and tl.productdonecount<tl.productcount ')+' ) ) ' + _w + ' ' +
				' order by case when tl.client ilike :client then 1 else 2 end,tl.plannedstart,tl."createdAt" ';
				//if(t.ip&&(t.ip.indexOf('10.10')>-1)&&t.objConfig.dcs_server_PORT.value!=='9000' ){
				//	sql='SELECT tl.id,coalesce(tl.idx,\'\') idx,tl.code,tl.erprefnumber,tl.opcode,tl.opnumber,tl.opname ,tl.opdescription ' +
				//	' 	,tl.productcount,tl.productdonecount,to_char(tl.plannedstart, \'DD/MM/YYYY\')plannedstart ' +
				//	' 	,tl.client,tl.taskfromerp,tl.productcount-tl.productdonecount remaining,tl.tpp,COALESCE(tl.mouldaddress,\'\') mouldaddress ' +
				//	' 	,case when tl.type=\'time\' then \'Zaman\' when tl.type=\'quantity\' then \'Miktar\' else \'\' end "type" ' +
				//	'		,case when tl.start is null then \'\' else \'drow-started\' end rowclass ' +
				//	' 	,tl.start,tl.record_id,pt_d.delivery,pt_p.pack as casename,cast(coalesce(nullif(pt_p.packcapacity,0),100) as integer)packcapacity ' +
				//	' 	,cast(floor((tl.productcount-tl.productdonecount)/coalesce(nullif(pt_p.packcapacity,0),100)) as integer)remainingpack ' +
				//	'	,pt_p.preptime,case when tc.id is null then 0 else 1 end taskcaseexists ' +
				//	'	,round((COALESCE(tl.tpp,pt_d.tpp)*COALESCE(pt_d.productionmultiplier,100)*pt_d.intervalmultiplier)/100,2) "interval" ' +
				//	'	,round(COALESCE(tl.tpp,pt_d.tpp)*COALESCE(pt_d.productionmultiplier,100)/100,2)t_p_p ' +
				//	' ,COALESCE(pt_d.notificationtime,2)notificationtime '+
				//	' FROM task_lists tl ' +
				//	' join product_trees pt_d on pt_d.materialtype=\'O\' and pt_d.finish is null and pt_d.name=tl.opname and pt_d.client=:client2 ' +
				//	' left join product_trees pt_p on pt_p.materialtype=\'K\' and pt_p.finish is null and pt_p.name=tl.opname ' +
				//	' left join (SELECT MIN(id)id,erprefnumber,opname from task_cases where finish is null and deliverytime is null and movementdetail is null GROUP BY erprefnumber,opname)tc on tc.erprefnumber=tl.erprefnumber and tc.opname=tl.opname '+
				//	' WHERE tl.finish is null ' +
				//	'		and (tl.type=\'time\' or (tl.type=\'quantity\' ) ) ' + _w + ' ' +
				//	' order by case when tl.client ilike :client then 1 else 2 end,tl.plannedstart,tl."createdAt" ';
				//}
				if(v_workflow==='El İşçiliği'){
					if(t.objParam.task_reworkType.value==='Yeniden İşlem'){
						sql='select tl.id,tl.code,tl.opcode,tl.opnumber,tl.opname,COALESCE(tl.opdescription,\'\')opdescription '+
						'	,tl.client,tl.tpp,tl.tpp t_p_p,tl.reworktype,tl.start,tl.record_id '+
						' 	,case when tl.start is null then \'\' else \'drow-started\' end rowclass ' +
						' FROM reworks tl ' +
						' WHERE tl.finish is null and tl.islisted=true ' + _w + ' ' +
						' order by case when tl.client=:client then 1 else 2 end,case when tl.listorder=0 then 9999 else tl.listorder end,tl.reworktype,tl.opname' +
						' limit 20 ';
					}else{//t.objParam.task_reworkType.value==='Üretim Planı'
						let v_select_docs='';
						let v_select_questions='';
						if(t.objParam.app_showSOPOnJoin.value===true){
							v_select_docs=`
								,(select string_agg(concat(d.type,'|',d.opcode,'|',d.opnumber,'|',d.opname,'|',d.currentversion,'|',d.path), '|~|') documents
								from (
									select d.* 
										,case when exists(SELECT * from client_production_details where type='e_p' and day=:day and jobrotation=:jobrotation and employee=:employee and opname=d.opname limit 1) then true else false end is_record_exists
									from documents d 
									where d.type='SOP' and d.opname=tl.opname 
								)d 
								where d.is_record_exists=false) documents
							`;
							rep.day=_jrday;
							rep.jobrotation=_jrcode;
							rep.employee=arg.employee;
							v_select_questions=`
								,(select string_agg(concat(cq.qtype,'|',cq.id,'|',cq.type,'|',cq.opname,'|',cq.question,'|',cq.answertype,'|',cq.valuerequire,'|',cq.valuemin,'|',cq.valuemax,'|',cq.documentnumber,'|',cq.path,'|',cq.cm_row_type,'|',cq.cm_row_layout), '|~|') controlquestions
								from (
									select 'control_questions' qtype,cq.* 
										,case answertype when 'Evet-Hayır' then 'radio' else 'text' end cm_row_type
										,case answertype when 'Rakam Değer' then 'numpadex' when 'Evet-Hayır' then 'radio' else 'tr_TR' end cm_row_layout
										,case when exists(SELECT * from client_production_details where type='e_p' and day=:day_cq and jobrotation=:jobrotation_cq and employee=:employee_cq and opname=cq.opname limit 1) then true else false end is_record_exists
									from control_questions cq
									where cq.type='SOP' and cq.opname=tl.opname 
								)cq
								where cq.is_record_exists=false) controlquestions
							`;
							rep.day_cq=_jrday;
							rep.jobrotation_cq=_jrcode;
							rep.employee_cq=arg.employee;
						}else{
							v_select_docs=', null documents ';
							v_select_questions=', null controlquestions ';
						}
						let _l=20;
						if(t.objConfig.dcs_server_IP.value==='10.0.0.101'){
							_l=9999;
						}
						sql=`SELECT tl.id,tl.code,tl.erprefnumber,tl.opcode,tl.opnumber,tl.opname,tl.opdescription 
						 	,tl.productcount,tl.productdonecount,to_char(tl.plannedstart, 'DD/MM/YYYY')plannedstart 
						 	,tl.client,tl.taskfromerp,tl.productcount-tl.productdonecount remaining,tl.tpp 
						 	,case when tl.type='time' then 'Zaman' when tl.type='quantity' then 'Miktar' else '' end "type" 
						 	,case when tl.start is null then '' else 'drow-started' end rowclass 
							,tl.mouldaddress,tl.mouldaddress bakimturu
						 	,tl.start,tl.record_id `+v_select_docs+` `+v_select_questions+`
						FROM task_lists tl 
						WHERE tl.finish is null and (tl.productdonecount<tl.productcount or tl.type='time') `+ _w + `  
						order by case when tl.client ilike :client then 1 else 2 end,tl.plannedstart,tl."createdAt"
						limit `+_l+` `;
					}
				}
				if(v_workflow==='Lazer Kesim'){
					sql='select id,code,description opdescription,production productcount,to_char(plannedstart, \'DD/MM/YYYY\') plannedstart,start'+
					' ,case when start is null then \'\' else \'drow-started\' end rowclass,record_id '+
					' from task_list_lasers '+
					' where finish is null ' + _w + ' ' +
					' order by plannedstart';
				}
				// console.log(rep);
				t.db.sequelize.query(sql,{ replacements: rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
					t.winmessend('cn_js_workflow',{event:_event,task_lists:results
						,sop_data:{
							client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,employee:(arg.employee?arg.employee:'')
							,data:[]
						}
					});
				});
				break;
			}
			case 'task_lists|select':{
				//employee tablosu kontrol edilecek, halen boşta ise update edilecek
				//task_lists tablosu kontrol edilecek, halen başlamamış ise update edilecek
				let v_employee=arg.employee;
				let v_tasklist='';
				let v_opnames=[];
				let _control_answers=arg.control_answers;
				if(typeof arg.selectedRows!=='undefined'){
					v_tasklist=[];
					for(let i=0;i<arg.selectedRows.length;i++){
						v_tasklist.push(arg.selectedRows[i].code);
					}
				}else{
					v_tasklist=arg.selectedRow.code;
				}
				let v_gfb=arg.gfbno?arg.gfbno:null;
				let v_description=arg.description?arg.description:'';
				let b_outOfSystem=arg.outOfSystem?arg.outOfSystem:false;
				let _promises=[];
				let msgControlTaskIsNewOrVeryOld='';
				let controlTaskIsNewOrVeryOld=async function(_opname){
					const _timeout=t.self.isDevMachine?10000:4000;
					try {
						const data = await t.getContent(t.self.serverHost, t.self.serverAppName, 'ClientProductionDetail/checkTaskNewOrVeryOld', { opname: _opname }, _timeout);
						let json = JSON.parse(data);
						let results = json.records;
						if (results.length > 0) {
							let _flagSonUretim=false;
							for (let i = 0; i < results.length; i++) {
								let row = results[i];
								if(row.sonuretim==true){
									_flagSonUretim=true;
								}
							}
							if(_flagSonUretim){
								msgControlTaskIsNewOrVeryOld = _opname +' operasyonu 3 aydan fazla zamandan beri işlem görmemiş. Operasyona başlamadan önce takım liderinden eğitim tekrar edilmelidir. \nTezgah: '+v_client+' \n Operatör: '+v_employee;
							}
						} else {
							msgControlTaskIsNewOrVeryOld = _opname +' operasyonu yeni devreye giriyor. Operasyona başlamadan önce takım lideri tarafından eğitim tekrar edilmelidir. \nTezgah: '+v_client+' \n Operatör: '+v_employee;
						}
						if(msgControlTaskIsNewOrVeryOld!==''){
							let mes_3 = new dcs_message({
								type: 'control_task_new_or_old',
								ack: true,
								data: {
									time: _time,
									msg:msgControlTaskIsNewOrVeryOld
								}
							});
							return t.db._sync_messages.create(mes_3.getdata()).then(() => {
								mes_3 = undefined;
							});
						}
						return null;
					} catch (err) {
						throw new Error(err.message);
					}
				}
				let objwhere={code:v_employee};
				t.db.sequelize.transaction(function (_tr) {
					if(_control_answers&&_control_answers.length>0){
						for(let c=0;c<_control_answers.length;c++){
							if(_control_answers[c].qtype==='control_questions_isg'){
								if(_control_answers[c].type==='GÖREVE KATILMA'){
									_control_answers[c].erprefnumber=_erp[e];
									for(let i=0;i<t.processVariables.productions.length;i++){
										let _p_item=t.processVariables.productions[i];
										if(_control_answers[c].erprefnumber==_p_item.erprefnumber){
											_control_answers[c].taskcode=_p_item.code;
											break;
										}
									}
								}else{
									for(let i=0;i<t.processVariables.productions.length;i++){
										let _p_item=t.processVariables.productions[i];
										if(_control_answers[c].opname==_p_item.opname){
											_control_answers[c].taskcode=_p_item.code;
											_control_answers[c].erprefnumber=_p_item.erprefnumber;
											break;
										}
									}
								}
							}
						}
						for(let c=0;c<_control_answers.length;c++){
							let row=_control_answers[c];
							_control_answers[c].client=v_client;
							_control_answers[c].day=_jrday;
							_control_answers[c].jobrotation=_jrcode;
							_control_answers[c].employee=arg.employee;
							_promises.push(t.createRecord(_tr,'control_answers',_time,{
								type:row.type
								,documentnumber:row.documentnumber
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,opname:row.opname
								,erprefnumber:row.erprefnumber
								,employee:arg.employee
								,time:_time
								,question:row.question
								,answertype:row.answertype
								,valuerequire:row.valuerequire
								,valuemin:row.valuemin
								,valuemax:row.valuemax
								,answer:row.answer
								,record_id:uuidv4()
							}));
						}
						let mes=new dcs_message({
							type:'control_answers'
							,ack:true
							,data:{
								client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,employee:arg.employee
								,time:_time
								,data:_control_answers
							}
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					}
					let _data_emp={client: v_client
						,day:_jrday
						,jobrotation:_jrcode
						,strlastseen:t.processVariables._time
					};
					if(v_workflow!=='Lazer Kesim'){
						if(arg.selectedRow&&arg.selectedRow.opname){
							v_opnames.push(arg.selectedRow.opname);
						}
						//20231122 oskim için kapatıldı
						// if(v_workflow=='El İşçiliği'){
						// 	_data_emp.tasklist=v_tasklist;
						// 	_promises.push(controlTaskIsNewOrVeryOld(arg.selectedRow.opname));
						// }
						if(!b_outOfSystem){
							_promises.push(t.taskTppAdd(arg.selectedRow.opname));
						}
						_promises.push(t.updateRecord(_tr,'employees',_time,objwhere,_data_emp));
						if(v_workflow!=='El İşçiliği'){
							_promises.push(t.updateRecord(_tr,'task_lists',_time,{code: v_tasklist,start:{[Op.eq]:null}},{start:_time,erprefnumber:arg.selectedRow.erprefnumber}));
						}
						if(!b_outOfSystem&&(t.objParam.process_wsSetup.value===false&&t.objParam.process_wsQuality.value===false)||v_workflow==='El İşçiliği'){
							let _obj_t_p_d={
								type:'e_p'
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,tasklist:v_tasklist
								,time:_time
								,opcode:arg.selectedRow.opcode
								,opnumber:arg.selectedRow.opnumber
								,opname:arg.selectedRow.opname
								//,opdescription:v_workflow==='El İşçiliği'?arg.selectedRow.reworktype:arg.selectedRow.opdescription
								,opdescription:arg.selectedRow.opdescription
								,leafmask:(v_workflow=='Üretim Aparatsız'||arg.selectedRow.leafmask==null?'01':arg.selectedRow.leafmaskcurrent)
								,leafmaskcurrent:(v_workflow=='Üretim Aparatsız'||arg.selectedRow.leafmaskcurrent==null?'01':arg.selectedRow.leafmaskcurrent)
								,employee:v_employee
								,erprefnumber:( (v_workflow==='El İşçiliği'&&t.objParam.task_reworkType.value==='Yeniden İşlem')?(_time.split('-')[0]+'-'+v_gfb):arg.selectedRow.erprefnumber )
								,gap:0
								,production:0
								,productioncurrent:0
								,taskfromerp:arg.selectedRow.taskfromerp
								,calculatedtpp:arg.selectedRow.t_p_p
								,start:_time
								,finish:null
								,record_id:uuidv4()
							};
							_promises.push(t.createRecord(_tr,'client_production_details',_time,_obj_t_p_d));
						}
						if(v_workflow!=='El İşçiliği'){
							let _obj_t_p_d={
								type:'c_p'+(b_outOfSystem?'_unconfirm':'')
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,tasklist:v_tasklist
								,time:_time
								,opcode:arg.selectedRow.opcode
								,opnumber:arg.selectedRow.opnumber
								,opname:arg.selectedRow.opname
								,opdescription:arg.selectedRow.opdescription
								,leafmask:(v_workflow=='Üretim Aparatsız'||arg.selectedRow.leafmask==null?'01':arg.selectedRow.leafmaskcurrent)
								,leafmaskcurrent:(v_workflow=='Üretim Aparatsız'||arg.selectedRow.leafmaskcurrent==null?'01':arg.selectedRow.leafmaskcurrent)
								,erprefnumber:arg.selectedRow.erprefnumber
								,gap:0
								,production:0
								,productioncurrent:0
								,taskfromerp:arg.selectedRow.taskfromerp
								,calculatedtpp:arg.selectedRow.t_p_p
								,start:_time
								,finish:null
								,record_id:uuidv4()
							};
							_promises.push(t.createRecord(_tr,'client_production_details',_time,_obj_t_p_d));
							//client_lost_details açık olan kayıtlar kapatılacak
							_promises.push(t.updateRecord(_tr,'client_lost_details',_time,{client:v_client,type:'l_c',finish:{[Op.eq]:null}},{finish:_time}));
							if(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40'){
								//operatör doğrudan süreç içine aktarılacak
							}else{
								for(let i=0;i<t.processVariables.operators.length;i++){
									let _emp=t.processVariables.operators[i];
									_promises.push(t.employeeLeave(_tr,{code:_emp.code},_time,false));
								}
							}
						}
						return Promise.all(_promises).then(function(){
							_promises=[];
							if(v_workflow!=='El İşçiliği'){
								let tmp_status_lost='-';
								//client tablosu status,statustime güncellenecek
								if(t.processVariables.status_work=='-'){
									if(!b_outOfSystem){
										t.processVariables.status_work=v_tasklist;
										t.processVariables.status_employee=1;
										if(t.objParam.process_wsSetup.value===true){
											t.processVariables.status_workflowprocess='SETUP';
											tmp_status_lost='SETUP-AYAR';
											let _tmp_data_lc={
												type:'l_c'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:tmp_status_lost
												,start:_time
												,record_id:uuidv4()
											};
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lc));
											let _tmp_data_lct={
												type:'l_c_t'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:tmp_status_lost
												,tasklist:v_tasklist
												,opcode:arg.selectedRow.opcode
												,opnumber:arg.selectedRow.opnumber
												,opname:arg.selectedRow.opname
												,opdescription:arg.selectedRow.opdescription
												,erprefnumber:arg.selectedRow.erprefnumber
												,taskfromerp:arg.selectedRow.taskfromerp
												,start:_time
												,record_id:uuidv4()
											};
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
											let _tmp_data_le={
												type:'l_e'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:tmp_status_lost
												,employee:v_employee
												,start:_time
												,record_id:uuidv4()
											};
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
											let _tmp_data_let={
												type:'l_e_t'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:tmp_status_lost
												,tasklist:v_tasklist
												,opcode:arg.selectedRow.opcode
												,opnumber:arg.selectedRow.opnumber
												,opname:arg.selectedRow.opname
												,opdescription:arg.selectedRow.opdescription
												,employee:v_employee
												,erprefnumber:arg.selectedRow.erprefnumber
												,taskfromerp:arg.selectedRow.taskfromerp
												,start:_time
												,record_id:uuidv4()
											};
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
										}else{
											if(t.objParam.process_wsQuality.value===true){
												t.processVariables.status_workflowprocess='QUALITY';
												tmp_status_lost=(t.objParam.process_remoteQualityConfirmation.value===true?'KALİTE ONAY':'KALİTE BEKLEME');
												if(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40'){
													tmp_status_lost='KALİTE ONAY';
													t.processVariables.lastprodstarttime=null;
													let _tmp_data_le={
														type:'l_e'
														,client:v_client
														,day:_jrday
														,jobrotation:_jrcode
														,losttype:tmp_status_lost
														,employee:v_employee
														,start:_time
														,record_id:uuidv4()
													};
													_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
													let _tmp_data_let={
														type:'l_e_t'
														,client:v_client
														,day:_jrday
														,jobrotation:_jrcode
														,losttype:tmp_status_lost
														,tasklist:v_tasklist
														,opcode:arg.selectedRow.opcode
														,opnumber:arg.selectedRow.opnumber
														,opname:arg.selectedRow.opname
														,opdescription:arg.selectedRow.opdescription
														,erprefnumber:arg.selectedRow.erprefnumber
														,employee:v_employee
														,taskfromerp:arg.selectedRow.taskfromerp
														,start:_time
														,record_id:uuidv4()
													};
													_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
												}else{
													t.processVariables.status_employee=0;
													t.processVariables.operators=[];
													//ustayı görevden ayır
													_data_emp={
														client: null
														,tasklist:null
														,day:null
														,jobrotation:null
														,strlastseen:t.processVariables._time
													};
													_promises.push(t.updateRecord(_tr,'employees',_time,objwhere,_data_emp));
												}
												
												let _tmp_data={
													type:'l_c'
													,client:v_client
													,day:_jrday
													,jobrotation:_jrcode
													,losttype:tmp_status_lost
													,start:_time
													,record_id:uuidv4()
												};
												_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
												let _tmp_data_lct={
													type:'l_c_t'
													,client:v_client
													,day:_jrday
													,jobrotation:_jrcode
													,losttype:tmp_status_lost
													,tasklist:v_tasklist
													,opcode:arg.selectedRow.opcode
													,opnumber:arg.selectedRow.opnumber
													,opname:arg.selectedRow.opname
													,opdescription:arg.selectedRow.opdescription
													,erprefnumber:arg.selectedRow.erprefnumber
													,taskfromerp:arg.selectedRow.taskfromerp
													,start:_time
													,record_id:uuidv4()
												};
												_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
											}
										}
										_promises.push(t.get_activity_control_questions());
									}else{
										tmp_status_lost='GÖREVDE KİMSE YOK';
										t.processVariables.status_employee=0;
										t.processVariables.status_work='OP-OUT-SYSTEM';
										t.processVariables.status_workflowprocess='-';
										let _tmp_data={
											type:'l_c'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,start:_time
											,record_id:uuidv4()
											,sourcedescriptionlost:'task_lists|select-1'
										};
										_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
										let _tmp_data_lct={
											type:'l_c_t'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,tasklist:v_tasklist
											,opcode:arg.selectedRow.opcode
											,opnumber:arg.selectedRow.opnumber
											,opname:arg.selectedRow.opname
											,opdescription:arg.selectedRow.opdescription
											,erprefnumber:arg.selectedRow.erprefnumber
											,taskfromerp:arg.selectedRow.taskfromerp
											,start:_time
											,record_id:uuidv4()
											,sourcedescriptionlost:'task_lists|select-2'
										};
										_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
									}
									t.setStatus(tmp_status_lost);
									let _obj_upd_client_task_lists_select={status:t.generateClientStatus(),statustime:_time};
									if((t.objParam.process_wsSetup.value===false&&t.objParam.process_wsQuality.value===false)||b_outOfSystem){
										t.processVariables.lastprodstarttime=_time;
										t.processVariables.lastprodtime=_time;
										_obj_upd_client_task_lists_select.lastprodstarttime=_time;
										_obj_upd_client_task_lists_select.lastprodtime=_time;
									}
									_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},_obj_upd_client_task_lists_select));
								}else{
									console.log('t.processVariables');
									console.log(t.processVariables);
								}
								return Promise.all(_promises);
							}else{
								let _table_=t.objParam.task_reworkType.value==='Yeniden İşlem'?'reworks':'task_lists';
								if(_table_==='task_lists'){
									return t.db[_table_].findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{code: v_tasklist} }, {transaction: _tr})
									.then(function(result_tl){
										if(result_tl){
											let mes=new dcs_message({
												type:'device_state_change'
												,ack:true
												,data:{client:v_client,time:_time,state:'Yeniden İşlem Başladı',losttype:'Yeniden İşlem Başladı',tasks:[{erprefnumber:result_tl.erprefnumber,opname:result_tl.opname,productcount:result_tl.productcount,productdonecount:0}]}
											});
											t.tmp_queue.push(mes);
											mes=undefined;
											if(result_tl.start===null){
												return Promise.all([t.updateRecord(_tr,'task_lists',_time,{record_id:result_tl.record_id},{start:_time})]);
											}
										}
									});
								}else{
									let mes=new dcs_message({
										type:'device_state_change'
										,ack:true
										,data:{client:v_client,time:_time,state:'Yeniden İşlem Başladı',losttype:'Yeniden İşlem Başladı',tasks:[{erprefnumber:arg.selectedRow.record_id,opname:arg.selectedRow.opname,productcount:0,productdonecount:0}]}
									});
									t.tmp_queue.push(mes);
									mes=undefined;
								}
								return null;
							}
						});
					}else{
						_promises.push(t.updateRecord(_tr,'employees',_time,objwhere,_data_emp));
						_promises.push(t.updateAllRecords(_tr,'task_list_lasers',_time,{code: {[Op.in]: v_tasklist},start:{[Op.eq]:null}},{start:_time}));
						return Promise.all(_promises).then(function(){
							let sql='SELECT tll.tasklistlaser,tll.opcode,tll.opnumber,tll.opname,tll.erprefnumber '+
							'	,tll.leafmask,tll.productcount,tll.productdonecount,tll.productdoneactivity '+
							'	,tll.productdonejobrotation,tll.scrappart,tll.scrapactivity,tll.scrapjobrotation '+
							'	,tll.record_id,cast(pt.tpp as integer) t_p_p '+
							'	,tll.leafmask leafmaskcurrent,tll.productcount-tll.productdonecount remaining '+
							'	,tll.tasklistlaser code,tll.tasklistlaser tasklist '+
							' from task_list_laser_details tll '+
							' left join product_trees pt on pt.name=tll.opname and pt.materialtype=\'O\' and pt.finish is null '+//(( (t.ip&&(t.ip.indexOf('10.10')>-1) ))?' and pt.client=:client ':'')+
							' where tll.tasklistlaser in (:code) '+
							' order by tll.opname ';
							let _rep={code: v_tasklist};
							//if(t.ip&&(t.ip.indexOf('10.10')>-1) ){
							//	_rep.client=v_client;
							//}
							return t.db.sequelize.query(sql,{ replacements: _rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
								if(results&&results.length>0){
									_promises=[];
									t.processVariables.productions=[];
									for(let i=0;i<results.length;i++){
										let row=results[i];
										v_opnames.push(row.opname);
										t.processVariables.productions.push(row);
										if(!b_outOfSystem&&(t.objParam.process_wsSetup.value===false&&t.objParam.process_wsQuality.value===false)){
											let _obj_t_p_d={
												type:'e_p'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,tasklist:row.code
												,time:_time
												,opcode:row.opcode
												,opnumber:row.opnumber
												,opname:row.opname
												//,opdescription:v_workflow==='El İşçiliği'?arg.selectedRow.reworktype:arg.selectedRow.opdescription
												,opdescription:''
												,leafmask:row.leafmask
												,leafmaskcurrent:row.leafmask
												,employee:v_employee
												,erprefnumber:row.erprefnumber
												,gap:0
												,production:0
												,productioncurrent:0
												,taskfromerp:true
												,calculatedtpp:row.t_p_p
												,start:_time
												,finish:null
												,record_id:uuidv4()
											};
											_promises.push(t.createRecord(_tr,'client_production_details',_time,_obj_t_p_d));
										}
										let _obj_t_p_d={
											type:'c_p'+(b_outOfSystem?'_unconfirm':'')
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,tasklist:row.code
											,time:_time
											,opcode:row.opcode
											,opnumber:row.opnumber
											,opname:row.opname
											,opdescription:''
											,leafmask:row.leafmask
											,leafmaskcurrent:row.leafmask
											,erprefnumber:row.erprefnumber
											,gap:0
											,production:0
											,productioncurrent:0
											,taskfromerp:true
											,calculatedtpp:row.t_p_p
											,start:_time
											,finish:null
											,record_id:uuidv4()
										};
										_promises.push(t.createRecord(_tr,'client_production_details',_time,_obj_t_p_d));
									}
									_promises.push(t.updateRecord(_tr,'client_lost_details',_time,{client:v_client,type:'l_c',finish:{[Op.eq]:null}},{finish:_time}));
									return Promise.all(_promises).then(function(){
										_promises=[];
										let tmp_status_lost='-';
										if(t.processVariables.status_work=='-'){
											if(!b_outOfSystem){
												t.processVariables.status_work=(typeof arg.selectedRows!=='undefined'?v_tasklist[0]:v_tasklist);
												t.processVariables.status_employee=1;
												if(t.objParam.process_wsSetup.value===true){
													t.processVariables.status_workflowprocess='SETUP';
													tmp_status_lost='SETUP-AYAR';
													let _tmp_data_lc={
														type:'l_c'
														,client:v_client
														,day:_jrday
														,jobrotation:_jrcode
														,losttype:tmp_status_lost
														,start:_time
														,record_id:uuidv4()
													};
													_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lc));
													let _tmp_data_le={
														type:'l_e'
														,client:v_client
														,day:_jrday
														,jobrotation:_jrcode
														,losttype:tmp_status_lost
														,employee:v_employee
														,start:_time
														,record_id:uuidv4()
													};
													_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
													for(let i=0;i<t.processVariables.productions.length;i++){
														let row=t.processVariables.productions[i];
														let _tmp_data_lct={
															type:'l_c_t'
															,client:v_client
															,day:_jrday
															,jobrotation:_jrcode
															,losttype:tmp_status_lost
															,tasklist:row.code
															,opcode:row.opcode
															,opnumber:row.opnumber
															,opname:row.opname
															,opdescription:''//row.opdescription
															,erprefnumber:row.erprefnumber
															,taskfromerp:true
															,start:_time
															,record_id:uuidv4()
														};
														_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
														let _tmp_data_let={
															type:'l_e_t'
															,client:v_client
															,day:_jrday
															,jobrotation:_jrcode
															,losttype:tmp_status_lost
															,tasklist:row.code
															,opcode:row.opcode
															,opnumber:row.opnumber
															,opname:row.opname
															,opdescription:''//row.opdescription
															,employee:v_employee
															,erprefnumber:row.erprefnumber
															,taskfromerp:true
															,start:_time
															,record_id:uuidv4()
														};
														_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
													}
												}else{
													if(t.objParam.process_wsQuality.value===true){
														t.processVariables.status_workflowprocess='QUALITY';
														tmp_status_lost=(t.objParam.process_remoteQualityConfirmation.value===true?'KALİTE ONAY':'KALİTE BEKLEME');
														if(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40'){
															tmp_status_lost='KALİTE ONAY';
															t.processVariables.lastprodstarttime=null;
															let _tmp_data_le={
																type:'l_e'
																,client:v_client
																,day:_jrday
																,jobrotation:_jrcode
																,losttype:tmp_status_lost
																,employee:v_employee
																,start:_time
																,record_id:uuidv4()
															};
															_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
														}else{
															t.processVariables.status_employee=0;
															t.processVariables.operators=[];
															//ustayı görevden ayır
															_data_emp={
																client: null
																,tasklist:null
																,day:null
																,jobrotation:null
																,strlastseen:t.processVariables._time
															};
															_promises.push(t.updateRecord(_tr,'employees',_time,objwhere,_data_emp));
														}
														let _tmp_data={
															type:'l_c'
															,client:v_client
															,day:_jrday
															,jobrotation:_jrcode
															,losttype:tmp_status_lost
															,start:_time
															,record_id:uuidv4()
														};
														_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
														for(let i=0;i<t.processVariables.productions.length;i++){
															let row=t.processVariables.productions[i];
															let _tmp_data_lct={
																type:'l_c_t'
																,client:v_client
																,day:_jrday
																,jobrotation:_jrcode
																,losttype:tmp_status_lost
																,tasklist:row.code
																,opcode:row.opcode
																,opnumber:row.opnumber
																,opname:row.opname
																,opdescription:''//row.opdescription
																,erprefnumber:row.erprefnumber
																,taskfromerp:true
																,start:_time
																,record_id:uuidv4()
															};
															_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
															if(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40'){
																let _tmp_data_let={
																	type:'l_e_t'
																	,client:v_client
																	,day:_jrday
																	,jobrotation:_jrcode
																	,losttype:tmp_status_lost
																	,tasklist:row.code
																	,opcode:row.opcode
																	,opnumber:row.opnumber
																	,opname:row.opname
																	,opdescription:row.opdescription
																	,erprefnumber:row.erprefnumber
																	,employee:v_employee
																	,taskfromerp:true
																	,start:_time
																	,record_id:uuidv4()
																};
																_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
															}
														}
													}
												}
											}else{
												tmp_status_lost='GÖREVDE KİMSE YOK';
												t.processVariables.status_employee=0;
												t.processVariables.status_work='OP-OUT-SYSTEM';
												t.processVariables.status_workflowprocess='-';
												let _tmp_data={
													type:'l_c'
													,client:v_client
													,day:_jrday
													,jobrotation:_jrcode
													,losttype:tmp_status_lost
													,start:_time
													,record_id:uuidv4()
													,sourcedescriptionlost:'task_lists|select-1'
												};
												_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
												for(let i=0;i<t.processVariables.productions.length;i++){
													let row=t.processVariables.productions[i];
													let _tmp_data_lct={
														type:'l_c_t'
														,client:v_client
														,day:_jrday
														,jobrotation:_jrcode
														,losttype:tmp_status_lost
														,tasklist:row.code
														,opcode:row.opcode
														,opnumber:row.opnumber
														,opname:row.opname
														,opdescription:''//row.opdescription
														,erprefnumber:row.erprefnumber
														,taskfromerp:true
														,start:_time
														,record_id:uuidv4()
														,sourcedescriptionlost:'task_lists|select-2'
													};
													_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
												}
											}
											t.setStatus(tmp_status_lost);
											let _obj_upd_client_task_lists_select={status:t.generateClientStatus(),statustime:_time};
											if((t.objParam.process_wsSetup.value===false&&t.objParam.process_wsQuality.value===false)||b_outOfSystem){
												t.processVariables.lastprodstarttime=_time;
												t.processVariables.lastprodtime=_time;
												_obj_upd_client_task_lists_select.lastprodstarttime=_time;
												_obj_upd_client_task_lists_select.lastprodtime=_time;
											}
											_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},_obj_upd_client_task_lists_select));
										}else{
											console.log('t.processVariables');
											console.log(t.processVariables);
										}
										return Promise.all(_promises);
									});
								}else{
									let _m='Seçili işe emri için detay bilgilere ulaşılamadı.';
									throw new Error(_m);
								}
							});
						});
					}
				}).then(function () {
					if(t.objConfig.dcs_server_IP.value==='10.0.0.101'){
						//t.self.socket_io_client.send({event:'cn_sn',data:{event:'penetrations|get',client:v_client,opnames:v_tasklist}});
						t.self.socket_io_client.send({event:'cn_sn',data:{event:'penetrations|get',client:v_client,opnames:v_opnames}});
					}
					if(b_outOfSystem){
						let item={
							client:v_client
							,day:_jrday
							,jobrotation:_jrcode
							,time:_time
							,description:v_description
							,employee:v_employee
							,opnames:v_tasklist
						};
						let mes=new dcs_message({
							type:'device_workstart_outofsystem'
							,ack:true
							,data:item
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					}
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess,msgControlTaskIsNewOrVeryOld:msgControlTaskIsNewOrVeryOld});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess});
					});	
				}).catch(function (err) {
					Sentry.captureException(err);
					Sentry.addBreadcrumb({
						category: 'task_lists|select',
						message: 'task_lists|select insert ',
						level: 'info',
						data:JSON.parse(JSON.stringify(v_tasklist))
					});
					t.mjd('rollback-'+_event);
					console.log(err);
					t.tmp_queue=[];
					return Promise.all([t.getDeviceInitValues('catch-task_lists|select')]).then(function(){
						//console.log('status_employee:'+t.processVariables.status_employee);
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'task_lists|select|multi':{
				if(!t.is_task_selected){
					t.is_task_selected=true;
					let v_description=arg.description?arg.description:'';
					let b_outOfSystem=arg.outOfSystem?arg.outOfSystem:false;
					let v_employee=arg.employee;
					let v_rows=arg.selectedRows;
					let _promises=[];
					t.db.sequelize.transaction(function (_tr) {
						return Promise.all([t.open_task_multi(_tr,v_rows,v_employee,_event,b_outOfSystem)]);
					}).then(function () {
						if(t.objConfig.dcs_server_IP.value==='10.0.0.101'){
							let _arr_work=[];
							for(let i=0;i<v_rows.length;i++){
								if(_arr_work.indexOf(v_rows[i].opname)==-1){
									_arr_work.push(v_rows[i].opname);
								}
							}
							t.self.socket_io_client.send({event:'cn_sn',data:{event:'penetrations|get',client:v_client,opnames:_arr_work}});
						}
						if(b_outOfSystem){
							let _arr_work=[];
							for(let i=0;i<v_rows.length;i++){
								if(_arr_work.indexOf(v_rows[i].opname)==-1){
									_arr_work.push(v_rows[i].opname);
								}
							}
							let item={
								client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,time:_time
								,description:v_description
								,employee:v_employee
								,opnames:_arr_work.join('|').substr(0,1000)
							};
							let mes=new dcs_message({
								type:'device_workstart_outofsystem'
								,ack:true
								,data:item
							});
							t.tmp_queue.push(mes);
							mes=undefined;
						}
						_promises=[];
						_promises.push(t.get_activity_control_questions());
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess});
						return Promise.all(_promises).then(function(){
							t.is_task_selected=false;
						});	
					}).catch(function (err) {
						Sentry.captureException(err);
						Sentry.addBreadcrumb({
							category: 'task_lists|select|multi',
							message: 'task_lists|select|multi insert ',
							level: 'info',
							data:JSON.parse(JSON.stringify(v_rows))
						});
						t.mjd('rollback-'+_event);
						console.log(err);
						t.tmp_queue=[];
						return Promise.all([t.getDeviceInitValues('catch-task_lists|select|multi')]).then(function(){
							t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
							t.is_task_selected=false;
						});
					});
				}
				break;
			}
			case 'moulds|get':{
				// v_workflow=='Değişken Üretim Aparatlı'?moulds:client_details.iotype='I'.then(moulds)
				let _promises=[];
				if(v_workflow==='Sabit Üretim Aparatlı'){
					let getdata_client_detail=function(){
						let _arr=[];
						if(typeof arg.exclude==='undefined'){
							arg.exclude=[];
						}
						for(let i=0;i<t.processVariables.productions.length;i++){
							let _prod=t.processVariables.productions[i];
							if(arg.exclude.indexOf(_prod.mould)===-1){
								arg.exclude.push(_prod.mould);
							}
						}
						for(let i=0;i<t.ports.inputs.length;i++){
							if(_arr.indexOf(t.ports.inputs[i].code)===-1&&arg.exclude.indexOf(t.ports.inputs[i].code)===-1){
								_arr.push(t.ports.inputs[i].code);
							}
						}
						return _arr;
					};
					_promises.push(getdata_client_detail());
				}
				Promise.all(_promises).then(function(res){
					let _w='';
					let _rep={};
					if(v_workflow==='Değişken Üretim Aparatlı'){
						if(arg.serialnumber&&arg.serialnumber!==''){
							_w+=' and serialnumber like :serialnumber ';
							_rep.serialnumber='%'+arg.serialnumber+'%';
						}
						if(arg.code&&arg.code!==''){
							_w+=' and code like :code ';
							_rep.code='%'+arg.code+'%';
						}
						if(arg.opcode&&arg.opcode!==''){
							_w+=' and code in (SELECT mould from mould_details where opcode like :opcode ) ';
							_rep.opcode='%'+arg.opcode+'%';
						}
					}
					if(v_workflow==='Sabit Üretim Aparatlı'){
						_w+=' and code IN (:code) ';// res[0];
						_rep.code=res[0];
					}
					t.db.sequelize.query('SELECT id,code,serialnumber,description,mouldtype,record_id,(SELECT string_agg(code,\'|_|\') from mould_groups where mould=moulds.code GROUP BY mould) groupcodes ' +
						' FROM moulds WHERE finish is null ' + _w + ' order by code limit 10',
					{ replacements: _rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
						t.winmessend('cn_js_workflow',{event:_event,moulds:results});
					});
				});
				break;
			}
			case 'mould_groups|get':{
				t.db.sequelize.query('SELECT id,code,mould,setup/60 setup,cast(cycletime as int)cycletime,productionmultiplier'+
					'	,intervalmultiplier,record_id,round((cycletime*productionmultiplier)/100,2)t_p_p ' +
					' FROM mould_groups WHERE finish is null and mould=:mould order by code',
				{ replacements: { mould: arg.mould }, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
					t.winmessend('cn_js_workflow',{event:_event,mould_groups:results});
				});
				break;
			}
			case 'mould_details|get':{
				t.db.sequelize.query('SELECT id,mould,mouldgroup,opcode,opnumber,opname,leafmask,leafmaskcurrent,record_id,\'\' erprefnumber,\'\' tasklist ' +
					' FROM mould_details WHERE finish is null and mould=:mould and mouldgroup=:mouldgroup order by opname',
				{ replacements: { mould: arg.mould,mouldgroup:arg.mouldgroup }, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
					t.winmessend('cn_js_workflow',{event:_event,mould_details:results});
				});
				break;
			}
			case 'mould_details|select':{
				let v_employee=arg.employee;
				let v_data=arg.data;
				let _promises=[];
				let tmp_status_lost='-';
				let objwhere={code:v_employee};
				let v_flag_addwork=false;
				t.db.sequelize.transaction(function (_tr) {
					_promises=[];
					let _arr_mg=[];
					for(let i=0;i<v_data.length;i++){
						let _mg=v_data[i].mould+'|_|'+v_data[i].mouldgroup;
						if(_arr_mg.indexOf(_mg)===-1){
							_arr_mg.push(_mg);
							_promises.push(t.taskTppAdd(_mg));
						}
						_promises.push(t.updateRecord(_tr,'task_lists',_time,{code: v_data[i].tasklist,start:{[Op.eq]:null}},{start:_time,erprefnumber:v_data[i].erprefnumber}));
						if(t.objParam.task_delivery.value===true&&v_data[i].delivery&&v_data[i].taskcaseexists===0){
							let _tmp_data_tc={
								client:v_client
								,movementdetail:null
								,erprefnumber:v_data[i].erprefnumber
								,casetype:'O'
								,opcode:v_data[i].opcode
								,opnumber:v_data[i].opnumber
								,opname:v_data[i].opname
								,quantityused:v_data[i].leafmaskcurrent==null?1:parseFloat(v_data[i].leafmaskcurrent)
								,casename:v_data[i].casename==null?'':v_data[i].casename
								,casenumber:v_data[i].casenumber
								,packcapacity:v_data[i].packcapacity
								,preptime:v_data[i].preptime
								,quantityremaining:0
								,quantitydeliver:0
								,conveyor:0
								,status:'WORK'
								,start:_time
								,record_id:uuidv4()
							};
							_promises.push(t.createRecord(_tr,'task_cases',_time,_tmp_data_tc));
						}
					}
					if(t.processVariables.productions.length===0){
						let _obj_where_c_l_d={
							start:{[Op.ne]:null}
							,finish:{[Op.eq]:null}
						};
						_promises.push(t.cjr_client_lost_details(_tr,_obj_where_c_l_d,_time));
					}
					return Promise.all(_promises).then(function(){
						_promises=[];
						if(t.processVariables.productions.length===0){
							if((t.objParam.process_wsSetup.value===false&&t.objParam.process_wsQuality.value===false)){
								// süreç yok, görevde kimse yok durumuna geçilecek
								tmp_status_lost='GÖREVDE KİMSE YOK';
								let create_lost_record_not_exists=function(_tr){
									let __promises=[];
									let _obj_where_clt_rne={
										type:'l_c'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,losttype:tmp_status_lost
										,start:{[Op.ne]:null}
										,finish:{[Op.eq]:null}
									};
									return t.db.client_lost_details.findOne({where:_obj_where_clt_rne},{transaction: _tr}).then(function(result){
										if(result){
											return null;
										}else{
											let _tmp_data_lc={
												type:'l_c'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:tmp_status_lost
												,start:_time
												,record_id:uuidv4()
											};
											__promises.push(t.create_lost_record(_tr,_time,_tmp_data_lc));
											return null;
										}
									}).then(function(){
										if(__promises.length>0){
											return Promise.all(__promises);
										}else{
											return null;
										}
									});
								};
								if(v_workflow==='Değişken Üretim Aparatlı'){
									let _tmp_data_lc={
										type:'l_c'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,losttype:tmp_status_lost
										,start:_time
										,record_id:uuidv4()
										,sourcedescriptionlost:'mould_details|select-1'
									};
									_promises.push(t.create_lost_record(_tr,_time,_tmp_data_lc));
								}
								if(v_workflow==='Sabit Üretim Aparatlı'){
									_promises.push(create_lost_record_not_exists(_tr));
								}
								for(let i=0;i<v_data.length;i++){
									let _tmp_data_lct={
										type:'l_c_t'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,losttype:tmp_status_lost
										,tasklist:v_data[i].tasklist
										,mould:v_data[i].mould
										,mouldgroup:v_data[i].mouldgroup
										,opcode:v_data[i].opcode
										,opnumber:v_data[i].opnumber
										,opname:v_data[i].opname
										,erprefnumber:v_data[i].erprefnumber
										,taskfromerp:v_data[i].taskfromerp
										,start:_time
										,record_id:uuidv4()
										,sourcedescriptionlost:'mould_details|select-2'
									};
									_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
								}
							}
							for(let i=0;i<t.processVariables.operators.length;i++){
								let _emp=t.processVariables.operators[i];
								_promises.push(t.employeeLeave(_tr,{code:_emp.code},_time,false));
							}
						}
						return Promise.all(_promises).then(function(){
							_promises=[];
							if(t.processVariables.status_work=='-'){
								t.processVariables.status_work='OP';
								if(t.objParam.process_wsSetup.value===true){
									t.processVariables.status_employee=1;
									t.processVariables.status_workflowprocess='SETUP';
									tmp_status_lost='SETUP-AYAR';
									let create_lost_record_lc=function(_tr){
										let _tmp_data_lc={
											type:'l_c'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,start:_time
											,record_id:uuidv4()
										};
										_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lc));
									};
									let create_lost_record_le=function(_tr){
										let _tmp_data_le={
											type:'l_e'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,employee:v_employee
											,start:_time
											,record_id:uuidv4()
										};
										_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
									};
									let create_lost_record_lc_not_exists=function(_tr){
										let __promises=[];
										let _obj_where_clt_rne={
											type:'l_c'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,start:{[Op.ne]:null}
											,finish:{[Op.eq]:null}
										};
										return t.db.client_lost_details.findOne({where:_obj_where_clt_rne},{transaction: _tr}).then(function(result){
											if(result){
												return null;
											}else{
												__promises.push(create_lost_record_lc(_tr));
												return null;
											}
										}).then(function(){
											if(__promises.length>0){
												return Promise.all(__promises);
											}else{
												return null;
											}
										});
									};
									let create_lost_record_le_not_exists=function(_tr){
										let __promises=[];
										let _obj_where_clt_rne={
											type:'l_e'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,employee:v_employee
											,start:{[Op.ne]:null}
											,finish:{[Op.eq]:null}
										};
										return t.db.client_lost_details.findOne({where:_obj_where_clt_rne},{transaction: _tr}).then(function(result){
											if(result){
												return null;
											}else{
												__promises.push(create_lost_record_le(_tr));
												return null;
											}
										}).then(function(){
											if(__promises.length>0){
												return Promise.all(__promises);
											}else{
												return null;
											}
										});
									};
									if(v_workflow==='Değişken Üretim Aparatlı'){
										_promises.push(create_lost_record_lc(_tr));
										_promises.push(create_lost_record_le(_tr));
									}
									if(v_workflow==='Sabit Üretim Aparatlı'){
										_promises.push(create_lost_record_lc_not_exists(_tr));
										_promises.push(create_lost_record_le_not_exists(_tr));
									}
									for(let i=0;i<v_data.length;i++){
										let _tmp_data_lct={
											type:'l_c_t'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,tasklist:v_data[i].tasklist
											,mould:v_data[i].mould
											,mouldgroup:v_data[i].mouldgroup
											,opcode:v_data[i].opcode
											,opnumber:v_data[i].opnumber
											,opname:v_data[i].opname
											,erprefnumber:v_data[i].erprefnumber
											,taskfromerp:v_data[i].taskfromerp
											,start:_time
											,record_id:uuidv4()
										};
										_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
										let _tmp_data_let={
											type:'l_e_t'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:tmp_status_lost
											,tasklist:v_data[i].tasklist
											,mould:v_data[i].mould
											,mouldgroup:v_data[i].mouldgroup
											,opcode:v_data[i].opcode
											,opnumber:v_data[i].opnumber
											,opname:v_data[i].opname
											,employee:v_employee
											,erprefnumber:v_data[i].erprefnumber
											,taskfromerp:v_data[i].taskfromerp
											,start:_time
											,record_id:uuidv4()
										};
										_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_let));
									}
									let _data_emp={
										client: v_client
										,day:_jrday
										,jobrotation:_jrcode
										,strlastseen:t.processVariables._time
									};
									_promises.push(t.updateRecord(_tr,'employees',_time,objwhere,_data_emp));									
								}else{
									t.processVariables.status_employee=0;
									t.processVariables.operators=[];
									if(t.objParam.process_wsQuality.value===true){
										t.processVariables.status_workflowprocess='QUALITY';
										tmp_status_lost=(t.objParam.process_remoteQualityConfirmation.value===true?'KALİTE ONAY':'KALİTE BEKLEME');
										let create_lost_record_lc=function(_tr){
											let _tmp_data_lc={
												type:'l_c'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:tmp_status_lost
												,start:_time
												,record_id:uuidv4()
											};
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lc));
										};
										let create_lost_record_lc_not_exists=function(_tr){
											let __promises=[];
											let _obj_where_clt_rne={
												type:'l_c'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:tmp_status_lost
												,start:{[Op.ne]:null}
												,finish:{[Op.eq]:null}
											};
											return t.db.client_lost_details.findOne({where:_obj_where_clt_rne},{transaction: _tr}).then(function(result){
												if(result){
													return null;
												}else{
													__promises.push(create_lost_record_lc(_tr));
													return null;
												}
											}).then(function(){
												if(__promises.length>0){
													return Promise.all(__promises);
												}else{
													return null;
												}
											});
										};
										if(v_workflow==='Değişken Üretim Aparatlı'){
											_promises.push(create_lost_record_lc(_tr));
										}
										if(v_workflow==='Sabit Üretim Aparatlı'){
											_promises.push(create_lost_record_lc_not_exists(_tr));
										}
										for(let i=0;i<v_data.length;i++){
											let _tmp_data_lct={
												type:'l_c_t'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:tmp_status_lost
												,tasklist:v_data[i].tasklist
												,mould:v_data[i].mould
												,mouldgroup:v_data[i].mouldgroup
												,opcode:v_data[i].opcode
												,opnumber:v_data[i].opnumber
												,opname:v_data[i].opname
												,erprefnumber:v_data[i].erprefnumber
												,taskfromerp:v_data[i].taskfromerp
												,start:_time
												,record_id:uuidv4()
											};
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
										}
									}
								}
								t.setStatus(tmp_status_lost);
								let _obj_upd_client_task_lists_select={status:t.generateClientStatus(),statustime:_time};
								if(t.objParam.process_wsSetup.value===false&&t.objParam.process_wsQuality.value===false){
									t.processVariables.lastprodstarttime=_time;
									t.processVariables.lastprodtime=_time;
									_obj_upd_client_task_lists_select.lastprodstarttime=_time;
									_obj_upd_client_task_lists_select.lastprodtime=_time;
								}
								_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},_obj_upd_client_task_lists_select));
							}else{
								// çalışan iş varken iş ekleme
								v_flag_addwork=true;
								//operatörler kadar cpd kayıt işlemi yapılacak
								for(let i=0;i<v_data.length;i++){
									let _item_prod=v_data[i];
									let obj_cpd_cp={
										type:'c_p'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,tasklist:_item_prod.tasklist
										,time:_time
										,mould:_item_prod.mould
										,mouldgroup:_item_prod.mouldgroup
										,opcode:_item_prod.opcode
										,opnumber:_item_prod.opnumber
										,opname:_item_prod.opname
										,opdescription:_item_prod.opdescription
										,leafmask:(_item_prod.leafmask==null?'01':_item_prod.leafmask)
										,leafmaskcurrent:(_item_prod.leafmaskcurrent==null?'01':_item_prod.leafmaskcurrent)
										,production:0
										,productioncurrent:0
										,employee:null
										,erprefnumber:_item_prod.erprefnumber
										,taskfromerp:_item_prod.taskfromerp
										,calculatedtpp:_item_prod.t_p_p
										,gap:0
										,start:_time
										,finish:null
										,record_id:uuidv4()
									};
									_promises.push(t.createRecord(_tr,'client_production_details',_time,obj_cpd_cp));
									for(let j=0;j<t.processVariables.operators.length;j++){
										let _item_emp=t.processVariables.operators[j];
										let obj_cpd_ep={
											type:'e_p'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,tasklist:_item_prod.tasklist
											,time:_time
											,mould:_item_prod.mould
											,mouldgroup:_item_prod.mouldgroup
											,opcode:_item_prod.opcode
											,opnumber:_item_prod.opnumber
											,opname:_item_prod.opname
											,opdescription:_item_prod.opdescription
											,leafmask:(_item_prod.leafmask==null?'01':_item_prod.leafmask)
											,leafmaskcurrent:(_item_prod.leafmaskcurrent==null?'01':_item_prod.leafmaskcurrent)
											,production:0
											,productioncurrent:0
											,employee:_item_emp.code
											,erprefnumber:_item_prod.erprefnumber
											,taskfromerp:_item_prod.taskfromerp
											,calculatedtpp:_item_prod.t_p_p
											,gap:0
											,start:_time
											,finish:null
											,record_id:uuidv4()
										};
										_promises.push(t.createRecord(_tr,'client_production_details',_time,obj_cpd_ep));
									}
								}
							}
							return Promise.all(_promises).then(function(){
								if(v_flag_addwork){
									return Promise.all([t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time})]);
								}
								return null;
							});
						});
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.tmp_queue=[];
					return Promise.all([t.getDeviceInitValues('catch-mould_details|select')]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'manual-StartLost-employee':{
				t.db.sequelize.transaction().then(function (_tr) {
					let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
					let objwhere={[t.processVariables.emp_identify]:v_data};
					if(t.objParam.app_empSecret.value===true){
						objwhere.secret=arg.secret;
					}
					let _promises=[];
					return Promise.all([t.isEmployeeWork(_tr,objwhere)]).then(function(_obj_res){
						_promises=[];
						_obj_res=_obj_res[0];
						let obj_where_cpd={
							type:'e_p'
							,day:_jrday
							,jobrotation:_jrcode
							,employee:_obj_res.code
							,finish:{[Op.eq]:null}
						};
						//let _table_=t.objParam.task_reworkType.value==='Yeniden İşlem'?'reworks':'task_lists';
						return t.db.client_production_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:obj_where_cpd }, {transaction: _tr})
						.then(function(result_cpd){
							if(result_cpd){
								return t.db.client_lost_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
									,where:{employee:_obj_res.code,finish:{[Op.eq]:null},tasklist:{[Op.ne]:null}} }, {transaction: _tr}).then(function(res_emp_lost){
									if(res_emp_lost){
										let _m='İstasyon: <b>'+res_emp_lost.client+'</b></br>Operasyon: <b>'+res_emp_lost.opname+'-'+res_emp_lost.opdescription+'</b></br>Kayıp Tipi: <b>'+res_emp_lost.losttype+'</b></br>Başlangıç: <b>'+t.convertFullLocalDateString(res_emp_lost.start.getTime())+'</b></br>Duruşu kapatmalısınız...';
										throw new Error(_m);
									}else{
										let _m='İstasyon: <b>'+_obj_res.client+'</b> Görev: <b>'+result_cpd.opname+'</b>';
										return {event:_event,result:'ok',message:_m,employee:_obj_res.code,tasklist:result_cpd.tasklist};
									}
								});
							}else{
								let _m='Görev bilgisi bulunamadı.';
								throw new Error(_m);
							}
						});
					}).then(function(_obj_res){
						if(_obj_res.result=='ok'){
							return t.db.lost_types.findAll({attributes: { exclude: ['createdAt','updatedAt'] }
								,where:{islisted:true,[Op.or]:[{finish:{[Op.eq]:null}},{finish:{[Op.gte]:_time}}]},order: [['listorder', 'ASC'],['code','ASC']]}).then(function(results) {
								let res=[];
								if(results.length>0){
									for (let i = 0; i < results.length; i++) {
										let result=results[i];
										res.push({id:result.id
											,code:result.code
											,losttype:result.code
										});
									}
								}
								_obj_res.lost_types=res;
								return _obj_res;
							});
						}else{
							throw new Error('Hata Oluştu.');
						}
					}).then(function (_obj_res) {
						_promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						t.winmessend('cn_js_workflow',_obj_res);
						return Promise.all(_promises).then(function(){
							return _tr.commit();
						});
					}).catch(function (err) {
						console.log('rollback');
						console.log(err);
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						t.tmp_queue=[];
						return _tr.rollback();
					});
				});
				break;
			}
			case 'manual-StartLost-lostselect':{
				//client_lost_details kayıt açılacak,process detail içinde aynı kayıp tipinde kayıt var ise süresi güncellenecek,yok ise açılacak
				let v_employee=arg.employee;
				let v_tasklist=arg.tasklist;
				let v_losttype=arg.selectedRow.losttype;
				let v_erprefnumber='';
				let v_opcode='';
				let v_opnumber='';
				let v_opname='';
				let v_opdescription='';
				let objwhere={code:v_employee};
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					return Promise.all([t.isEmployeeWork(_tr,objwhere)]).then(function(_obj_res){
						_promises=[];
						_obj_res=_obj_res[0];
						let obj_where_cpd={
							type:'e_p'
							,day:_jrday
							,jobrotation:_jrcode
							,employee:_obj_res.code
							,finish:{[Op.eq]:null}
						};
						//let _table_=t.objParam.task_reworkType.value==='Yeniden İşlem'?'reworks':'task_lists';
						return t.db.client_production_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:obj_where_cpd }, {transaction: _tr})
						.then(function(result_cpd){
							if(result_cpd){
								return t.db.client_lost_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
									,where:{employee:_obj_res.code,finish:{[Op.eq]:null},tasklist:{[Op.ne]:null}} }, {transaction: _tr}).then(function(res_emp_lost){
									if(res_emp_lost){
										let _m='<b>'+res_emp_lost.client+'</b> istasyonundaki <b>'+res_emp_lost.opname+'-'+res_emp_lost.opdescription+'</b> görevinde açılımış <b>'+res_emp_lost.losttype+'</b> duruşunu kapatmalısınız.';
										throw new Error(_m);
									}else{
										return null;
									}
								});
							}else{
								let _m='Görev bilgisi bulunamadı.';
								throw new Error(_m);
							}
						});
					}).then(function () {
						_promises=[];
						let _table_=t.objParam.task_reworkType.value==='Yeniden İşlem'?'reworks':'task_lists';
						return t.db[_table_].findOne({attributes: { exclude: ['createdAt','updatedAt'] }
							,where:{code: v_tasklist} }, {transaction: _tr}).then(function(result_tl){
							if(result_tl){
								v_erprefnumber=result_tl.erprefnumber;
								v_opcode=result_tl.opcode;
								v_opnumber=result_tl.opnumber;
								v_opname=result_tl.opname;
								v_opdescription=result_tl.opdescription;
								let _idx=t.getIndex(t.processVariables.productions,'tasklist|employee',result_tl.code+'|'+v_employee);
								if(_idx>-1){
									t.processVariables.productions[_idx].lastprodtime=_time;
								}
								let _obj_t_l_d_let={
									type: 'l_e_t'
									,day:_jrday
									,jobrotation:_jrcode
									,client:v_client
									,tasklist:v_tasklist
									,losttype: v_losttype
									,opcode: v_opcode
									,opnumber: v_opnumber
									,opname: v_opname
									,opdescription: v_opdescription
									,employee: v_employee
									,erprefnumber: v_erprefnumber
									,start: _time
									,record_id:uuidv4()
								};
								_promises.push(t.createRecord(_tr,'client_lost_details',_time,_obj_t_l_d_let));
								let _obj_t_l_d_le={
									type: 'l_e'
									,day:_jrday
									,jobrotation:_jrcode
									,client:v_client
									,losttype: v_losttype
									,employee: v_employee
									,start: _time
									,record_id:uuidv4()
								};
								_promises.push(t.createRecord(_tr,'client_lost_details',_time,_obj_t_l_d_le));
								return null;
							}else{
								throw new Error('Görev bulunamadı.');
							}
						}).then(function(){
							v_erprefnumber=arg.record_id;
							v_opname=arg.opname;
							if(_promises.length>0){
								return Promise.all(_promises);
							}else{
								return null;
							}
						});
					});
				}).then(function () {
					_promises=[];
					let mes=new dcs_message({
						type:'device_state_change'
						,ack:true
						,data:{client:v_client,time:_time,state:'Duruş Başladı',losttype:v_losttype,tasks:[{erprefnumber:v_erprefnumber,opname:v_opname,productcount:0,productdonecount:0}]}
					});
					t.tmp_queue.push(mes);
					mes=undefined;
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'Duruş başlatıldı'});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'Duruş başlatıldı'});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'manual-StopLost-employee':{
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
					let objwhere={[t.processVariables.emp_identify]:v_data};
					if(t.objParam.app_empSecret.value===true){
						objwhere.secret=arg.secret;
					}
					return Promise.all([t.isEmployeeWork(_tr,objwhere)]).then(function(_obj_res){
						_obj_res=_obj_res[0];
						return t.db.client_lost_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
							,where:{employee:_obj_res.code,finish:{[Op.eq]:null},tasklist:{[Op.ne]:null}} }, {transaction: _tr}).then(function(res_emp_lost){
							if(res_emp_lost){
								let _m='İstasyon: <b>'+res_emp_lost.client+'</b></br>Operasyon: <b>'+res_emp_lost.opname+'-'+res_emp_lost.opdescription+'</b></br>Kayıp Tipi: <b>'+res_emp_lost.losttype+'</b></br>Başlangıç: <b>'+t.convertFullLocalDateString(res_emp_lost.start.getTime())+'</b>';
								return {event:_event,result:'ok',message:_m,employee:_obj_res.code,losttype:res_emp_lost.losttype,tasklist:res_emp_lost.tasklist};
							}else{
								throw new Error('Açık duruş kaydı bulunamadı.');
							}
						});
					});
				}).then(function (_obj_res) {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',_obj_res);
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',_obj_res);
					});
				}).catch(function (err) {
					console.log('rollback');
					console.log(err);
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'manual-StopLost-employee-agree':{
				let _promises=[];
				let v_employee=arg.employee;
				let v_tasklist=arg.tasklist;
				let v_losttype=arg.losttype;
				let v_start='';
				let v_erprefnumber='';
				let v_opname='';
				t.db.sequelize.transaction(function (_tr) {
					let objwhere={code:v_employee};
					if(t.objParam.app_empSecret.value===true){
						objwhere.secret=arg.secret;
					}
					return Promise.all([t.isEmployeeWork(_tr,objwhere)]).then(function(/*_obj_res*/){
						_promises=[];
						return t.db.client_lost_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
							,where:{employee:v_employee,finish:{[Op.eq]:null},tasklist:v_tasklist} }, {transaction: _tr}).then(function(res_emp_lost){
							if(res_emp_lost){
								let _idx=t.getIndex(t.processVariables.productions,'tasklist|employee',res_emp_lost.tasklist+'|'+v_employee);
								if(_idx>-1){
									t.processVariables.productions[_idx].lastprodtime=null;
								}
								v_start=res_emp_lost.start;
								let _obj_where_t_l_d_let={
									type: 'l_e_t'
									,day:_jrday
									,jobrotation:_jrcode
									,client:v_client
									,tasklist: v_tasklist
									,losttype: v_losttype
									,employee: v_employee
									,start: v_start
									,finish: {[Op.eq]:null}
								};
								_promises.push(t.updateRecord(_tr,'client_lost_details',_time,_obj_where_t_l_d_let,{finish:_time}));
								let _obj_where_t_l_d_le={
									type: 'l_e'
									,day:_jrday
									,jobrotation:_jrcode
									,client:v_client
									,losttype: v_losttype
									,employee: v_employee
									,start: v_start
									,finish: {[Op.eq]:null}
								};
								_promises.push(t.updateRecord(_tr,'client_lost_details',_time,_obj_where_t_l_d_le,{finish:_time}));
								if(res_emp_lost.erprefnumber!==''&&res_emp_lost.erprefnumber!==null){
									v_erprefnumber=res_emp_lost.erprefnumber;
								}
								return null;
							}else{
								throw new Error('Açık duruş kaydı bulunamadı.');
							}
						}).then(function(){
							if(_promises.length>0){
								return Promise.all(_promises);
							}else{
								return null;
							}
						});
					});
				}).then(function () {
					_promises=[];
					let _table_=t.objParam.task_reworkType.value==='Yeniden İşlem'?'reworks':'task_lists';
					if(v_erprefnumber===''||_table_==='reworks'){
						v_erprefnumber=arg.record_id;
						v_opname=arg.opname;
					}
					let mes=new dcs_message({
						type:'device_state_change'
						,ack:true
						,data:{client:v_client,time:_time,state:'Duruş Bitti',losttype:v_losttype,tasks:[{erprefnumber:v_erprefnumber,opname:v_opname,productcount:0,productdonecount:0}]}
					});
					t.tmp_queue.push(mes);
					mes=undefined;
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'Duruş bitirildi'});
					return Promise.all(_promises);
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'manual-StopWork-employee':{
				t.db.sequelize.transaction().then(function (_tr) {
					let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
					let objwhere={[t.processVariables.emp_identify]:v_data};
					if(t.objParam.app_empSecret.value===true){
						objwhere.secret=arg.secret;
					}
					let _promises=[];
					return Promise.all([t.isEmployeeWork(_tr,objwhere)]).then(function(_obj_res){
						_obj_res=_obj_res[0];
						let obj_where_cpd={
							type:'e_p'
							,day:_jrday
							,jobrotation:_jrcode
							,employee:_obj_res.code
							,finish:{[Op.eq]:null}
						};
						//let _table_=t.objParam.task_reworkType.value==='Yeniden İşlem'?'reworks':'task_lists';
						return t.db.client_production_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:obj_where_cpd }, {transaction: _tr})
						.then(function(result_cpd){
							let _m='';
							if(result_cpd){
								return t.db.client_lost_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
									,where:{employee:_obj_res.code,finish:{[Op.eq]:null},tasklist:result_cpd.tasklist} }, {transaction: _tr}).then(function(res_emp_lost){
									if(res_emp_lost){
										_m='İstasyon: <b>'+res_emp_lost.client+'</b></br>Operasyon: <b>'+res_emp_lost.opname+'-'+res_emp_lost.opdescription+'</b></br>Kayıp Tipi: <b>'+res_emp_lost.losttype+'</b></br>Başlangıç: <b>'+t.convertFullLocalDateString(res_emp_lost.start.getTime())+'</b></br>Duruşu kapatmalısınız...';
										throw new Error(_m);
									}else{
										_m='İstasyon: <b>'+_obj_res.client+'</b> Görev: <b>'+result_cpd.opname+'</b>';
										return {event:_event,result:'ok',message:_m,employee:_obj_res.code,tasklist:result_cpd.tasklist};
									}
								});
							}else{
								if(_obj_res.client!==null){
									_m='Görev bilgisi bulunamadı.';
								}else{
									_m='İşleminiz yapılıyor.';
								}
								return {event:_event,result:'ok',message:_m,employee:_obj_res.code,tasklist:null};
							}
						});
					}).then(function (_obj_res) {
						_promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						t.winmessend('cn_js_workflow',_obj_res);
						return Promise.all(_promises).then(function(){
							return _tr.commit();
						});
					}).catch(function (err) {
						console.log('rollback');
						console.log(err);
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						t.tmp_queue=[];
						return _tr.rollback();
					});
				});
				break;
			}
			case 'manual-StopWork-productcount':{
				//client_production_details tablosunda sağlam adetler yazılacak, operatörün toplam çalışma zamanına bu süre ile birlikte ıskartalar için harcadığı süre eklenecek
				//console.log(arg);
				let v_productcount=arg.productcount!==false?parseInt(arg.productcount):0;
				let v_casecapacity=arg.casecapacity!==false?parseInt(arg.casecapacity):0;
				let v_description=arg.description;
				let v_employee=arg.employee;
				let v_tasklist=arg.tasklist;
				let v_task_reject_details=arg.task_reject_details;
				let v_erprefnumber=arg.erprefnumber;
				let v_opcode=arg.opcode;
				let v_opnumber=arg.opnumber;
				let v_opname=arg.opname;
				let v_opdescription='';
				let v_pd_start='';
				let v_pd_gap=0;
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					let objwhere={code:v_employee};
					_promises.push(t.isEmployeeWork(_tr,objwhere));
					let _data_emp={tasklist: null
						,client: null
						,day: null
						,jobrotation: null
						,strlastseen:t.processVariables._time
					};
					_promises.push(t.updateRecord(_tr,'employees',_time,objwhere,_data_emp));
					return Promise.all(_promises).then(function(_obj_res){
						_obj_res=_obj_res[0];
						if(_obj_res.tasklist!==null){
							v_tasklist=_obj_res.tasklist;
						}
						return t.db.client_lost_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
							,where:{employee:_obj_res.code,finish:{[Op.eq]:null},tasklist:v_tasklist} }, {transaction: _tr}).then(function(res_emp_lost){
							if(res_emp_lost){
								let _m='<b>'+res_emp_lost.client+'</b> istasyonundaki <b>'+res_emp_lost.opname+'-'+res_emp_lost.opdescription+'</b> görevinde açılımış <b>'+res_emp_lost.losttype+'</b> duruşunu kapatmalısınız.';
								throw new Error(_m);
							}else{
								return ;
							}
						});
					}).then(function(){
						let _obj_where_t_p_d_ep={
							type: 'e_p'
							//,day:_jrday
							,employee: v_employee
							//,tasklist:v_tasklist
							,finish: {[Op.eq]:null}
						};
						return t.db.client_production_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
							,where:_obj_where_t_p_d_ep }, {transaction: _tr}).then(function(result_tpd){
							if(result_tpd){
								v_pd_start=result_tpd.start;
								v_pd_gap=Math.ceil((_timems-v_pd_start.getTime())/1000);
								let obj_upd={
									finish:_time
									,gap:v_pd_gap
								};
								if(arg.productcount!==false){
									obj_upd.production=v_productcount;
									obj_upd.productioncurrent=v_productcount;
								}
								if(v_description!==''){
									obj_upd.opdescription=v_description;
								}
								return Promise.all([t.cjr_client_production_details(_tr,{id:result_tpd.id,record_id:result_tpd.record_id},_time,obj_upd)]);
							}else{
								t.mjd(_obj_where_t_p_d_ep);
								throw new Error('Görev bilgisi bulunamadı.');
							}
						});
					}).then(function () {
						let _table_=t.objParam.task_reworkType.value==='Yeniden İşlem'?'reworks':'task_lists';
						console.log({'_table_':_table_,'code':v_tasklist});
						console.log('arg',arg);
						if(_table_==='task_lists'){
							let mes=new dcs_message({
								type:'device_state_change'
								,ack:true
								,data:{client:v_client,time:_time,state:'Yeniden İşlem Bitti',losttype:'Yeniden İşlem Bitti',tasks:[{erprefnumber:v_erprefnumber,opname:v_opname,productcount:0,productdonecount:v_productcount}]}
							});
							t.tmp_queue.push(mes);
							mes=undefined;
							return t.db[_table_].findOne({attributes: { exclude: ['createdAt','updatedAt'] }
								,where:{code: v_tasklist} }, {transaction: _tr}).then(function(result_tl){
									console.log('result_tl',result_tl);
								if(result_tl){
									v_erprefnumber=result_tl.erprefnumber;
									v_opcode=result_tl.opcode;
									v_opnumber=result_tl.opnumber;
									v_opname=result_tl.opname;
									v_opdescription=result_tl.opdescription;
									let scrappart=0;
									if(arg.task_reject_details!==false){
										for(let i=0;i<v_task_reject_details.length;i++){
											scrappart+=parseInt(v_task_reject_details[i].quantityreject);
										}
									}
									//plansız işlerde productcount=0 gelecek, iş biterken Tamir Bitti mi? sorulacak
									console.log('if',result_tl.type==='time',parseInt(result_tl.productcount)===0,parseInt(arg.productdonecount)===1);
									if(result_tl.type==='time'&&parseInt(result_tl.productcount)===0&&parseInt(arg.productdonecount)===1){
										let mes=new dcs_message({
											type:'task_lists'
											,ack:true
											,data:{
												client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,erprefnumber:result_tl.erprefnumber
												,code:result_tl.code
												,opname:result_tl.opname
											}
										});
										t.tmp_queue.push(mes);
										mes=undefined;
										return Promise.all([t.updateRecord(_tr,'task_lists',_time,{record_id:result_tl.record_id},{productdonecount:1,finish:_time})]);
									}else{
										let control_passed=false;
										if(t.objParam.task_manual_time_control.value===true){
											let _tpp=result_tl.tpp!==null?parseInt(result_tl.tpp):0;
											let gap_production=v_productcount*_tpp*80/100;
											if(gap_production>v_pd_gap){
												let max_prod=Math.floor(v_pd_gap/(_tpp*80/100));
												let _m='Operasyon Birim Süresi:'+_tpp+' sn</br>Max Üretim:'+max_prod+'</br>Üretilen Adet:'+v_productcount+'</br>Girdiğiniz miktarı kotrol etmelisiniz.';
												throw new Error(_m);
											}
										}else{
											control_passed=true;
										}
										if(control_passed){
											return Promise.all([t.updateRecord(_tr,'task_lists',_time,{record_id:result_tl.record_id},{scrappart:parseInt(result_tl.scrappart)+scrappart,productdonecount:parseInt(result_tl.productdonecount)+v_productcount})]);
										}
									}
								}else{
									return null;
								}
							});
						}else{
							let mes=new dcs_message({
								type:'device_state_change'
								,ack:true
								,data:{client:v_client,time:_time,state:'Yeniden İşlem Bitti',losttype:'Yeniden İşlem Bitti',tasks:[{erprefnumber:arg.record_id,opname:v_opname,productcount:0,productdonecount:v_productcount}]}
							});
							t.tmp_queue.push(mes);
							mes=undefined;
							return null;
						}
					}).then(function(){

						if(arg.task_reject_details===false){
							return null;
						}else{
							let saverecord=function(_item,_type){
								let _obj_t_r_d={
									type: _type
									,day:_jrday
									,jobrotation:_jrcode
									,client:v_client
									,tasklist:v_tasklist
									,rejecttype: _item.rejecttype
									,materialunit: _item.materialunit
									,time: _time
									,opcode: v_opcode
									,opnumber: (v_opnumber!==''?v_opnumber:null)
									,opname: v_opname
									,opdescription: v_opdescription
									,quantityreject: _item.quantityreject
									,quantityretouch:(_item.retouch==1?_item.quantityreject:0)
									,quantityscrap: (_item.retouch==0?_item.quantityreject:0)
									,employee: (_type==='e_r'?v_employee:null)
									,erprefnumber: v_erprefnumber
									,description: (_item.description?_item.description:null)
									,record_id:uuidv4()
								};
								return Promise.all([t.createRecord(_tr,'task_reject_details',_time,_obj_t_r_d)]);
							};
							_promises=[];
							if(typeof v_task_reject_details.forEach=='function'){
								v_task_reject_details.forEach(function(item) {
									_promises.push(saverecord(item,'t_r'));
									_promises.push(saverecord(item,'e_r'));
								});
							}
							return Promise.all(_promises);
						}
					}).then(function(){
						if(arg.task_case_controls===false){
							return null;
						}else{
							let mes=new dcs_message({
								type:'task_case_controls'
								,ack:true
								,data:{
									client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,employee:v_employee
									,opcode:v_opcode
									,opnumber:v_opnumber
									,opname:v_opname
									,productcount:v_productcount
									,casecapacity:v_casecapacity
									,erprefnumber:v_erprefnumber
									,opdescription:v_opdescription
									,detail:arg.task_case_controls
								}
							});
							t.tmp_queue.push(mes);
							mes=undefined;
							return null;
						}
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş bitirildi'});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş bitirildi'});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'reject_types|get':{
				let obj_where={finish:{[Op.eq]:null},islisted:true};
				if(arg.name){
					//_w+=' and (code ilike :code)';
					obj_where.code={[Op.iLike]:'%'+arg.name+'%'};
				}
				return Promise.all([t.findAll('reject_types',{'id':'id','code':'code','rejecttype':'code','isproductionaffect':'isproductionaffect'},obj_where,[ ['listorder', 'ASC'],['code', 'ASC'] ],false)]).then(function(results){
					t.winmessend('cn_js_workflow',{event:_event,reject_types:t.getLastElementOfArray(results)});
					return null;
				});
			}
			case 'device_init_io':{
				if(v_workflow!=='El İşçiliği'){
					t.bool_init_io=true;
					if(t.wait_for_first_controljobrotation&&(!t.self.isDevMachine||(t.ip!=='172.16.1.143'&&t.ip!=='172.16.1.146'))){
						t.iodevice_manager.send({
							event: 'readall',
							pid: t.iodevice_manager.pid,
							pos: '1'
						});
					}else{
						t.eventListener({event:'device_init'});
					}
				}
				break;
			}
			case 'device_init':{
				if(v_workflow!=='El İşçiliği'){
					//let tmpObjTasklistInfo={
					//	app:"argox_cp_2140_febi"
					//	,number:"170174" 
					//	,week:"9269 32" 
					//	,replno:"3Q0 407 152 F" 
					//	,tofit:"VW" 
					//	,opname:"55509-k" 
					//	,barcode:"4054224705148"
					//};
					//if(fs.existsSync(path.join(__dirname,'..','labelPrinter',tmpObjTasklistInfo.app+'.exe'))){
					//	const ls = cp.spawn(path.join(__dirname,'..','labelPrinter',tmpObjTasklistInfo.app+'.exe'), [
					//		"number="+tmpObjTasklistInfo.number
					//		,"week="+tmpObjTasklistInfo.week
					//		,"replno="+tmpObjTasklistInfo.replno
					//		,"tofit="+tmpObjTasklistInfo.tofit
					//		,"opname="+tmpObjTasklistInfo.opname
					//		,"barcode="+tmpObjTasklistInfo.barcode
					//	], {
					//			cwd: path.resolve(path.join(__dirname,'..','labelPrinter')),
					//			detached: true,
					//			windowsHide: true
					//		}
					//	);
					//	ls.stdout.on('data', (data) => {
					//		console.log(`stdout: ${data}`);
					//	});
					//	ls.stderr.on('data', (data) => {
					//		console.log(`stderr: ${data}`);
					//	});
					//	ls.on('close', (code) => {
					//		console.log(`child process exited with code ${code}`);
					//	});
					//}
					if(t.lost_recursive){
						t.call_init_after_lost_recursive=true;
						t.mjd('t.call_init_after_lost_recursive=true');
						return;
					}else{
						t.call_init_after_lost_recursive=false;
					}
					t.scrap_timer_add=0;
					t.status=t.status=='fault'?'':t.status;
					t.show_scrap_before_finish=false;
					t.show_delivery=false;
					t.processVariables.emp_identify=t.objParam.app_empIdentify.value=='Klavye'?'code':'secret';
					let _message='';
					let _promises=[];
					setTimeout(() => {
						try {
							fs.accessSync(__dirname+'/../status.json');
							t.json_status = JSON.parse(fs.readFileSync(__dirname+'/../status.json', 'utf-8'));
						} catch (error) {
							classBase.writeError(error);
						}
					}, 100);
					t.db.sequelize.transaction(function (/*_tr*/) {
						_promises=[];
						_promises.push(t.getDeviceInitValues('device_init'));
						return Promise.all(_promises);
					}).then(function(){
						_promises=[];
						_promises.push(t.eventListener({event:'req_watch_info'}));
						_promises.push(t.calcTempo('device_init'));
						if(t.processVariables.status_lost==='İŞ YOK'){
							t.client_control_document=false;
						}
						if(t.objParam.task_delivery.value===true){
							if(!t.bool_control_device_delivery){
								t.bool_control_device_delivery=true;
								setTimeout(() => {
									t.control_device_delivery();
								}, 5000);
							}
						}
						if(t.objParam.task_OperationAuthorization.value===true){
							if(!t.bool_operationAuthorization_supervision){
								t.bool_operationAuthorization_supervision=true;
								setTimeout(() => {
									t.control_operationAuthorization_supervision();
								}, 10000);
							}
						}
						if(t.wait_for_first_controljobrotation){
							t.wait_for_first_controljobrotation=false;
						}		
						return Promise.all(_promises).then(function(){
							_promises=[];
							console.log('t.device_inited:',t.device_inited);
							if(t.device_inited===true){
								_promises.push(t.get_activity_control_questions());
								if(t.processVariables.status_lost==='SOP DOKÜMAN GÖRÜNTÜLEME'){
									let v_employee=0;
									if(t.processVariables.operators.length>0){
										v_employee=t.processVariables.operators[t.processVariables.operators.length-1].code;
									}
									_promises.push(t.get_sop_documents_on_join(v_client,_jrday,_jrcode,_tmp_opnames,v_employee,false));
								}
							}
							while(t.tmp_queue.length>0){
								_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
							}
							t.device_inited=true;
							t.inited_wf=2;
							if(t.bool_init_io==true){
								t.bool_init=true;
							}
							let _p=t.processVariables.productions;
							if(v_workflow==='Sabit Üretim Aparatlı'||v_client==='P202'||v_client==='P110'||v_client==='03.06.0013'||v_client==='03.06.0026'||v_client==='03.06.0005'){
								let _tmp=[];
								let _program='';
								for(let i=0;i<_p.length;i++){
									if(_program===''&&_p[i].program&&_p[i].program!==''){
										_program=_p[i].program;
									}
									if(t.getIndex(_tmp,'opname|erprefnumber',_p[i].opname+'|'+_p[i].erprefnumber)==-1){
										_tmp.push(_p[i]);
									}
								}
								if(_program===''){
									_program='0000';
								}
								console.log('_program:',_program);
								if(_program!==''){
									const _arr_program=_program.split('').reverse();
									for(let i=0;i<_arr_program.length;i++){
										const idx_p=t.getIndex(t.ports.outputs,'ioevent','p-output-'+(i+1));
										if(idx_p!==-1){
											if(t.iodevice_manager.outputPorts){
												const idx_iop=t.getIndex(t.iodevice_manager.outputPorts,'number',t.ports.outputs[idx_p].portnumber);
												if(idx_iop!==-1){
													let _cur_port=t.iodevice_manager.outputPorts[idx_iop];
													if(parseInt(_cur_port.curval)!==parseInt(_arr_program[i])){
														console.log('_program_setoutput:',_cur_port.number,parseInt(_arr_program[i]));
														t.emit('setoutput', _cur_port.number, parseInt(_arr_program[i]));
													}
												}
											}
										}
									}
									console.log('_program:',_arr_program);
								}
								_p=_tmp;
							}
							let missingMaterials=false;
							let requirements=false;
							if(t.processVariables.status_lost=='MALZEME BEKLEME'||t.processVariables.status_lost=='PLANLI TAŞIMA BEKLEME'){
								missingMaterials=t.obj_missing_material;
							}
							t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message,status:t.generateClientStatus()
								,lbl_status:t._lbl_status,lbl_cls:t._lbl_cls,operators:t.processVariables.operators,productions:_p
								,params:t.objParam,pins:t.securitypins,state_start:t._state_start,ports:t.ports
								,missing_material:missingMaterials,requirements:requirements
								,documents:t.all_documents
								,client_control_document:{
									client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,employee:(t.processVariables.operators.length>0?t.processVariables.operators[0].code:'')
									,data:t.client_control_document
								}
							});	
							return Promise.all(_promises);
						});
					}).then(function(){
						return t.db.sequelize.query('ALTER TABLE "public"."task_lists" ADD COLUMN IF NOT EXISTS "taskinfo" json', { });
					}).catch(function (err) {
						t.mjd('rollback-'+_event);
						console.log(err);
						classBase.writeError(err,t.ip,'catch_workflow_device_init');
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						t.tmp_queue=[];
					});
				}else{
					if(t.wait_for_first_controljobrotation){
						t.wait_for_first_controljobrotation=false;
					}
					if(_jrday===false||_jrcode===false){
						setTimeout(() => {
							t.eventListener(arg);
						}, 500);
						return;
					}
					t.bool_init_io=true;
					t.bool_init=true;
					t.processVariables.productions=[];
					const _obj_where_cpd={
						type:'e_p'
						,client:v_client
						,day:_jrday
						,jobrotation:_jrcode
					};
					let sql='SELECT cpd.*,e.name empname FROM client_production_details cpd left join employees e on e.code=cpd.employee where cpd.type=:type and cpd.client=:client and cpd.day=:day and cpd.jobrotation=:jobrotation and cpd.finish is null ORDER BY e.name asc';
					return t.db.sequelize.query(sql, { replacements:_obj_where_cpd,type: t.db.sequelize.QueryTypes.SELECT}).then(function(res_cpd){
						if(res_cpd&&res_cpd.length>0){
							for (let i = 0; i < res_cpd.length; i++) {
								let _cpd_item=res_cpd[i];
								_cpd_item.status='Çalışma';
								_cpd_item.lastprodtime=null;
								let _idx=t.getIndex(t.processVariables.productions,'tasklist|employee',_cpd_item.tasklist+'|'+_cpd_item.employee);
								if(_idx===-1){
									t.processVariables.productions.push(_cpd_item);
								}
							}
							return null;
						}
					}).then(function(){
						const _obj_where_cld={
							type:'l_e_t'
							,client:v_client
							,finish:{[Op.eq]:null}
						};
						return t.db.client_lost_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] }
							,where:_obj_where_cld}).then(function(res_cld){
							if(res_cld&&res_cld.length>0){
								for (let i = 0; i < res_cld.length; i++) {
									let _cld_item=res_cld[i];
									let _idx=t.getIndex(t.processVariables.productions,'tasklist|employee',_cld_item.tasklist+'|'+_cld_item.employee);
									if(_idx===-1){
										//
									}else{
										t.processVariables.productions[_idx].status=_cld_item.losttype;
										t.processVariables.productions[_idx].lastprodtime=t.convertFullLocalDateString(_cld_item.start);
									}
								}
							}
							return null;
						});
					}).then(function(){
						let _arr_tl=[];
						for(let i=0;i<t.processVariables.productions.length;i++){
							if(_arr_tl.indexOf(t.processVariables.productions[i].tasklist)===-1){
								_arr_tl.push(t.processVariables.productions[i].tasklist);
							}
						}
						let _table_=t.objParam.task_reworkType.value==='Yeniden İşlem'?'reworks':'task_lists';
						return t.db[_table_].findAll({attributes: { exclude: ['createdAt','updatedAt'] }
							,where:{code:{[Op.in]: _arr_tl}}}).then(function(results) {
							if(results.length>0){
								for (let j = 0; j < results.length; j++) {
									let result=results[j];
									for(let k=0;k<t.processVariables.productions.length;k++){
										if(t.processVariables.productions[k].tasklist===result.code){
											if(t.objParam.task_reworkType.value==='Yeniden İşlem'){
												t.processVariables.productions[k].reworktype=result.reworktype;
											}else{
												t.processVariables.productions[k].reworktype=result.type;
												t.processVariables.productions[k].productcount=result.productcount;
												t.processVariables.productions[k].remaining=result.productcount-result.productdonecount;
											}
										}
									}
								}
							}
							return null;
						});
					}).then(function(){
						return Promise.all([t.eventListener({event:'req_watch_info'})]).then(function(){
							let _promises=[];
							while(t.tmp_queue.length>0){
								_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
							}
							t.device_inited=true;
							t.inited_wf=2;
							if(t.wait_for_first_controljobrotation){
								t.wait_for_first_controljobrotation=false;
							}
							t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',params:t.objParam,pins:t.securitypins
								,productions:t.processVariables.productions
								,documents:t.all_documents
								,client_control_document:{
									client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,employee:''
									,data:t.client_control_document
								}
							});
							return Promise.all(_promises);
						});
					});
				}
				break;
			}
			case 'scrap_page_close':{
				t.scrap_timer_add=0;
				break;
			}
			case 'scrap_page_open':{
				t.scrap_timer_add=90;
				break;
			}
			case 'lost_types|get':{
				let _w='';
				let rep={finish:_time,islisted:true};
				if(arg.listtype){
					_w+=' and '+arg.listtype+'=true ';
				}
				if(arg.name){
					_w+=' and (code ilike :code)';
					rep.code='%'+arg.name+'%';
				}
				if(arg.work_lost){
					_w+=' and code not in (:code3)';
					rep.code3=t.faultcodes;
				}
				if(t.objConfig.dcs_server_IP.value==='10.0.0.101' && t.processVariables.status_work==='OP' ){
					_w+=' and code not in (:code_deneme)';
					rep.code_deneme='DENEMELER';
				}
				if((t.objConfig.dcs_server_IP.value==='172.16.1.144') && t.processVariables.status_work==='OP' ){
					_w+=' and code not in( '+
					'	case when not EXISTS (SELECT id from client_lost_details where losttype=\'SETUP-AYAR\' and client=:client_pres and finish>cast(:time_pres as timestamp) - interval \'5 minute\') '+
					'		then \'SETUP AYAR\' '+
					'		else \'\' '+
					'	end) ';
					rep.client_pres=v_client;
					rep.time_pres=_time;
				}
				if(t.processVariables.status_employee==0&&!arg.work_lost){
					_w+=' and closenoemployee=:closenoemployee';
					rep.closenoemployee=false;
				}
				let _date=new timezoneJS.Date();
				if(_date.getDay()!==5){
					arg.cuma=true;
				}else{
					if(!arg.cuma){
						if(!(t.tickTimer.obj_time.strHour=='13'||(t.tickTimer.obj_time.strHour=='14'&&t.tickTimer.obj_time.strMinute<='30'))){
							arg.cuma=true;
						}
					}
				}
				if(arg.cay||arg.yemek||arg.cuma||t.objParam.task_materialpreparation.value===true){
					let _arr=[];
					if(arg.cay){
						_arr.push('ÇAY MOLASI');
					}
					if(arg.yemek){
						_arr.push('YEMEK MOLASI');
					}
					if(arg.cuma){
						_arr.push('CUMA PAYDOSU');
					}
					if(t.objParam.task_materialpreparation.value===true){
						_arr.push('PLANLI TAŞIMA BEKLEME');
						_arr.push('MALZEME BEKLEME');
					}
					_w+=' and code not in (:code2) ';
					rep.code2=_arr;
				}
				let sql='select id,code,code as losttype,isexpertemployeerequired,coalesce(time,0) "time",isoperationnumberrequired '+
					' ,isclient,isemployee,authorizeloststart,authorizelostfinish,closenoemployee,ioevent,isdescriptionrequired '+
					' from lost_types '+
					' where (finish is null or finish>=:finish) and islisted=:islisted '+_w+
					' order by listorder,code ';
				return t.db.sequelize.query(sql,{ replacements: rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',lost_types:results});
					return null;
				});
			}
			case 'device-lost-start':{
				let _descriptionlost=arg.description==''?null:arg.description;
				let _pre_losttype=t.processVariables.status_lost;
				let _cur_losttype=arg.selectedRow.code;
				t.setStatus(_cur_losttype);
				if(t.objParam.task_materialpreparation.value===true&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
					if(t.timer_missing_material!==false){
						clearTimeout(t.timer_missing_material);
						t.timer_missing_material=false;
					}
					if((_cur_losttype=='MALZEME BEKLEME'||_cur_losttype=='PLANLI TAŞIMA BEKLEME'||t.processVariables.status_lost=='ALT PARÇA YOK')){
						t.timer_missing_material=setTimeout(() => {
							t.control_missing_materials('delivered',8);
						}, 15000);
					}
				}
				let _message='';
				let v_op_transfer=arg.op_transfer;
				let _promises=[];
				let _obj_cld_update={
					losttype: _cur_losttype
					,descriptionlost:_descriptionlost
					,faulttype:arg.faulttype?arg.faulttype:null
					,faultdescription:arg.faultdescription?arg.faultdescription:null
					,faultgroupcode:arg.faultgroupcode?arg.faultgroupcode:null
					,faultdevice:arg.faultdevice?arg.faultdevice:null
					,employee:arg.employee?arg.employee:null
					,tasklist:t.faultcodes.indexOf(_cur_losttype)>-1?uuidv4():null
					,mould:arg.mould?arg.mould:null
				};
				t.db.sequelize.transaction(function (_tr) {
					if(v_op_transfer){
						t.processVariables.lastprodstarttime=null;
						_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client,status:arg.status},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null}));
					}else{
						if(t.processVariables.status_work!=='-'&&parseInt(t.processVariables.status_employee)>0){
							let _where_cjr_cpd={type:'e_p',client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}};
							_promises.push(t.cjr_client_production_details(_tr,_where_cjr_cpd,_time));
						}
						t.processVariables.status_employee=0;
						t.processVariables.operators=[];
						t.processVariables.lastprodstarttime=null;
						_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client,status:arg.status},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null}));
						_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
					}
					if(_pre_losttype!='-'){
						_promises.push(t.cjr_client_lost_details(_tr,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
					}
					t.lost_recursive=false;
					_promises.push(t.create_lost_records(_tr,_time,_obj_cld_update,_time,t.processVariables.productions,_event));
					if(t.faultcodes.indexOf(_cur_losttype)>-1){
						let task_code='';
						for(let i=0;i<t.processVariables.productions.length;i++){
							let _p_item=t.processVariables.productions[i];
							if(_p_item.mould==arg.faultdevice){
								task_code=_p_item.code;
								break;
							}
						}
						t.json_status.fault={faulttype:_obj_cld_update.faulttype,faultdescription:_obj_cld_update.faultdescription,faultgroupcode:_obj_cld_update.faultgroupcode,faultdevice:_obj_cld_update.faultdevice,fault_unique_id:_obj_cld_update.tasklist,operators:[],taskcode:task_code};
						t.json_status.fault.pos='device-lost-start';
						t.json_status.fault.time=_time;
						//if(arg.mould){
						//	t.json_status.mould=arg.mould;
						//}
						try {
							fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
						} catch (error) {
							//
						}
						let _tmp_opnames=[];
						for(let i=0;i<t.processVariables.productions.length;i++){
							let prod=t.processVariables.productions[i];
							let tmp_opname=prod.opname;
							if(_tmp_opnames.indexOf(tmp_opname)==-1){
								_tmp_opnames.push(tmp_opname);
							}
						}
						let item={
							type:'f_c_s'
							,client:v_client
							,day:_jrday
							,start:_time
							,losttype: _obj_cld_update.losttype
							,descriptionlost:_obj_cld_update.descriptionlost
							,faulttype:_obj_cld_update.faulttype
							,faultdescription:_obj_cld_update.faultdescription
							,faultgroupcode:_obj_cld_update.faultgroupcode
							,faultdevice:_obj_cld_update.faultdevice
							,employee:_obj_cld_update.employee
							,tasklist:_obj_cld_update.tasklist
							,taskcode:task_code
							,opname:_tmp_opnames
						};
						let mes=new dcs_message({
							type:'device_fault_start'
							,ack:true
							,data:item
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					}
					if((v_client==='1600T'||v_client==='03.06.0002')&&arg.faultdevice==='03.03.0031'){
						const arrExportFaultStates = [
							'ARIZA KALIP-APARAT',
							'ARIZA MAKİNE ELEKTRİK',
							'ARIZA MAKİNE MEKANİK',
							'ÖNLEYİCİ BAKIM',
							'PLANLI BAKIM',
						];
						if (arrExportFaultStates.indexOf(_cur_losttype) !== -1) {
							let tmpProductions= [];
							for(let i=0;i<t.processVariables.productions.length;i++){
								let production=t.processVariables.productions[i];
								tmpProductions.push({ opname: production.opname, erprefnumber: production.erprefnumber });
							}
							let tmpEmployees = [];
							for(let i=0;i<t.processVariables.operators.length;i++){
								let employee=t.processVariables.operators[i];
								tmpEmployees.push({ code: employee.code, name: employee.name });
							}
							const sData = {
								client: v_client
								, day: _jrday
								, start: _time
								, jobrotation: _jrcode
								, employees: tmpEmployees
								, losttype: _cur_losttype
								, descriptionlost: _descriptionlost
								, faulttype: _cur_losttype
								, faultdescription: ''
								, faultgroupcode: _cur_losttype
								, faultdevice: (arg.faultdevice?arg.faultdevice:'')
								, productions: tmpProductions
							};
							let mes=new dcs_message({
								type:'export_data_machinaide'
								,ack:true
								,data:sData
							});
							t.tmp_queue.push(mes);
							mes=undefined;
						}
					}
					_promises.push(t.eventListener({event:'req_watch_info'}));
					if(_promises.length>0){
						return Promise.all(_promises);
					}else{
						return null;
					}
				}).then(function () {
					_promises=[];
					t.status='';
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					setTimeout(() => {
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					}, 500);
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_device_lost_start');
					t.tmp_queue=[];
					return Promise.all([t.getClientStatus()]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'device-lost-finish':{
				let _message='';
				let _productions=arg.productions?arg.productions:[];
				let _control_answers=arg.control_answers;
				let _employee=(arg.employee?arg.employee:(t.processVariables.operators.length>0?t.processVariables.operators[0].code:''));
				let _repairfinish=arg.repairfinish;
				let _cur_losttype='-';
				let _promises=[];
				let v_op_transfer=t.processVariables.status_work!='-';
				let _fault_losttype='';
				let _obj_where={
					type:{[Op.in]: ['l_c', 'l_c_t','l_e', 'l_e_t']}
					,client:v_client
					,start:{[Op.ne]:null}
					,finish:{[Op.eq]:null}
				};
				t.db.sequelize.transaction(function (_tr) {
					if(t.faultcodes.indexOf(t.processVariables.status_lost)>-1){
						_fault_losttype=t.processVariables.status_lost;
						for(let i=0;i<t.processVariables.operators.length;i++){
							let row=t.processVariables.operators[i];
							if(row.ismaintenance==true||row.ismould==true){
								let _start=null;
								if(t.json_status.fault.operators&&t.json_status.fault.operators.length>0){
									let idx=t.getIndex(t.json_status.fault.operators,'employee',row.code);
									if(idx>-1){
										_start=t.json_status.fault.operators[idx].start;
										t.json_status.fault.operators.splice(idx,1);
									}
								}
								let _obj_cld_update1={
									type:'f_e_f'
									,client:v_client
									,day:_jrday
									,finish:_time
									,employee:row.code
									,descriptionlost:arg.descriptionlost
									,faulttype:t.processVariables.status_lost
									,tasklist:t.json_status.fault.fault_unique_id?t.json_status.fault.fault_unique_id:''
									,taskcode:t.json_status.fault.taskcode?t.json_status.fault.taskcode:''
									,repairfinish:_repairfinish
								};
								if(_start!==null){
									_obj_cld_update1.start=_start;
								}
								t.json_status.fault.pos='f_e_f-device-lost-finish';
								t.json_status.fault.time=_time;
								_promises.push(t.find_fault_master_record(_tr,'employee_fault_finish',_obj_cld_update1));
							}
						}
						let _obj_cld_update={
							type:'f_c_f'
							,client:v_client
							,day:_jrday
							,finish:_time
							,descriptionlost:arg.descriptionlost
							,faulttype:t.processVariables.status_lost
							,tasklist:t.json_status.fault.fault_unique_id?t.json_status.fault.fault_unique_id:''
							,taskcode:t.json_status.fault.taskcode?t.json_status.fault.taskcode:''
							,repairfinish:_repairfinish
						};
						t.json_status.fault.pos='f_c_f-device-lost-finish';
						t.json_status.fault.time=_time;
						//if(t.json_status.mould){
						//	t.json_status.mould=null;
						//}
						_promises.push(t.find_fault_master_record(_tr,'device_fault_finish',_obj_cld_update));
						try {
							fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
						} catch (error) {
							Sentry.captureException(error);
						}
						if(t.processVariables.status_work!=='-'&&parseInt(t.processVariables.status_employee)>0){
							let _where_cjr_cpd={type:'e_p',client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}};
							_promises.push(t.cjr_client_production_details(_tr,_where_cjr_cpd,_time));
							v_op_transfer=false;
						}
						if(t.processVariables.status_workflowprocess!=='-'){
							v_op_transfer=true;
						}
					}
					if(_control_answers){
						if(_control_answers.length>0){
							for(let c=0;c<_control_answers.length;c++){
								for(let i=0;i<t.processVariables.productions.length;i++){
									let _p_item=t.processVariables.productions[i];
									if(_control_answers[c].opname==_p_item.opname){
										_control_answers[c].taskcode=_p_item.code;
										if(_control_answers[c].qtype==='activity_control_questions'){
											_control_answers[c].erprefnumber=_p_item.erprefnumber;
										}
										break;
									}
								}
							}
							let mes=new dcs_message({
								type:'control_answers'
								,ack:true
								,data:{
									client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,employee:_employee
									,time:_time
									,data:_control_answers
								}
							});
							t.tmp_queue.push(mes);
							mes=undefined;
						}
					}
					return Promise.all(_promises).then(function(){
						_promises=[];
						if(t.faultcodes.indexOf(t.processVariables.status_lost)>-1){
							t.json_status.fault={faulttype:false,faultdescription:false,faultgroupcode:false,faultdevice:false,fault_unique_id:false,operators:[]};
							t.json_status.fault.pos='device-lost-finish';
							t.json_status.fault.time=_time;
							try {
								fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
							} catch (error) {
								//
							}
						}
						let production_transition=function(_clt,_adl,_cmp){
							if(t.objParam.task_materialpreparation.value===true&&_cmp===true&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
								//eksik malzeme kontrolü
								return Promise.all([t.control_missing_materials()]).then(function(result) {
									if(result[0]==true){
										if(t.timer_missing_material!==false){
											clearTimeout(t.timer_missing_material);
											t.timer_missing_material=false;
										}
										t.timer_missing_material=setTimeout(() => {
											t.control_missing_materials('delivered',9);
										}, 10000);
										return Promise.all([production_transition('MALZEME BEKLEME',_adl,false)]);
									}else{
										return Promise.all([production_transition(_clt,_adl,false)]);
									}
								});
							}else{
								let _prom_p_t=[];
								//üretime geçiş, görevde kimse yok şeklinde
								t.cpd_time=false;
								t.processVariables.status_employee=0;
								t.processVariables.operators=[];
								t.processVariables.status_workflowprocess='-';
								if(_clt==='MALZEME BEKLEME'){
									_clt=( (t.case_movement_target_time===false||(t.case_movement_target_time!==false&&t.case_movement_target_time>_time))?'PLANLI TAŞIMA BEKLEME':'MALZEME BEKLEME' );
								}
								t.setStatus(_clt);
								if(t.objParam.task_materialpreparation.value===true){
									if(t.timer_missing_material!==false){
										clearTimeout(t.timer_missing_material);
										t.timer_missing_material=false;
									}
									t.timer_missing_material=setTimeout(() => {
										t.control_missing_materials('delivered',9);
									}, (_clt==='PLANLI TAŞIMA BEKLEME'?60000:10000));
								}
								t.processVariables.lastprodstarttime=null;
								t.processVariables.lastprodtime=_time;
								_prom_p_t.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_e','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:_adl}));
								_prom_p_t.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
								_prom_p_t.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
								_obj_where.type={[Op.in]: ['l_c', 'l_c_t']};
								_prom_p_t.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: _clt,descriptionlost:'',sourcedescriptionlost:'production_transition-1'},{finish:_time,descriptionlost:_adl},true));
								return Promise.all(_prom_p_t);
							}
						};
						let savetaskreject=function(_item,_type,_rejecttype){
							let _obj_t_r_d={
								type: _type
								,day:_jrday
								,jobrotation:_jrcode
								,client:v_client
								,tasklist:_item.tasklist
								,rejecttype: _rejecttype
								,materialunit: 'Adet'
								,time: _time
								,mould: _item.mould
								,mouldgroup: _item.mouldgroup
								,opcode: _item.opcode
								,opnumber: (_item.opnumber!==''?_item.opnumber:null)
								,opname: _item.opname
								,opdescription: _item.opdescription
								,quantityreject: _item.hurda
								,quantityretouch:0
								,quantityscrap: _item.hurda
								,employee: _item.employee
								,erprefnumber: _item.erprefnumber
								,description: (_item.description?_item.description:null)
								,record_id:uuidv4()
							};
							return Promise.all([t.createRecord(_tr,'task_reject_details',_time,_obj_t_r_d)]);
						};
						
						let update_tasklist_record=function(_tr,_tl_record,_rejecttype){
							return t.db.task_lists.findOne({where:{code:_tl_record.tasklist}},{transaction: _tr}).then(function(_res){
								if(_res){
									return Promise.all([t.updateRecord(_tr,'task_lists',_time,{code:_res.code},
										{	productdonecount:parseInt(_res.productdonecount)+parseInt(_tl_record.saglam)
											,productdoneactivity:parseInt(_res.productdoneactivity)+parseInt(_tl_record.saglam)
											,productdonejobrotation:parseInt(_res.productdonejobrotation)+parseInt(_tl_record.saglam)
											,scrappart:parseInt(_res.scrappart)+parseInt(_tl_record.hurda)
											,scrapactivity:parseInt(_res.scrapactivity)+parseInt(_tl_record.hurda)
											,scrapjobrotation:parseInt(_res.scrapjobrotation)+parseInt(_tl_record.hurda)
										})]);
								}
								return null;
							}).then(function(){
								if(_tl_record.hurda>0){
									let _tr_promises=[];
									_tr_promises.push(savetaskreject(_tl_record,'t_r',_rejecttype));
									if(t.processVariables.operators.length>0){
										let _emp_found=false;
										for(let _e=0;_e<t.processVariables.operators.length;_e++){
											_tl_record.employee=t.processVariables.operators[_e].code;
											if((_tl_record.employee===_employee)&&!_emp_found&&_employee!==''){
												_emp_found=true;
											}
											_tr_promises.push(savetaskreject(_tl_record,'e_r',_rejecttype));
										}
										if(!_emp_found&&_employee!==''){
											_tl_record.employee=_employee;
											_tr_promises.push(savetaskreject(_tl_record,'e_r',_rejecttype));
										}
									}else{
										if(_employee!==''){
											_tl_record.employee=_employee;
										}
										_tr_promises.push(savetaskreject(_tl_record,'e_r',_rejecttype));
									}
									return Promise.all(_tr_promises);
								}
								return null;
							});
						};	
						if(v_op_transfer){
							if(t.processVariables.status_workflowprocess==='-'){
								if(t.processVariables.operators.length>0){
									if(t.objParam.task_materialpreparation.value===true){
										if(t.timer_missing_material!==false){
											clearTimeout(t.timer_missing_material);
											t.timer_missing_material=false;
										}
										t.timer_missing_material=setTimeout(() => {
											t.control_missing_materials('waitmaterial',10);
										}, 10000);
									}
								}
								if(t.processVariables.status_lost==='KAYNAK ÜNİTESİ ARIZASI'){
									for(let i=0;i<t.processVariables.productions.length;i++){
										t.processVariables.productions[i].hurda=parseInt(t.processVariables.productions[i].leafmask);
										t.processVariables.productions[i].saglam=-1*parseInt(t.processVariables.productions[i].leafmask);
										_promises.push(update_tasklist_record(_tr,t.processVariables.productions[i],'PUNTA MUKAVEMTİ (PUNTA)'));
									}
								}
								t.setAllOutputs();
								_cur_losttype='-';
								t.setStatus(_cur_losttype);
								t.processVariables.lastprodstarttime=_time;
								_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:arg.descriptionlost}));
								_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:_time}));
							}else{
								let _f=false;
								if(t.objParam.process_wsQuality.value===true){
									if(t.processVariables.status_workflowprocess==='QUALITY'){
										if(t.processVariables.status_lost==='KALİTE ONAY'){
											_f=true;
											//üretime geçiş, görevde kimse yok şeklinde
											_cur_losttype='GÖREVDE KİMSE YOK';
											_promises.push(production_transition(_cur_losttype,arg.descriptionlost,true));
											if(_productions.length>0){
												for(let c=0;c<_productions.length;c++){
													_promises.push(update_tasklist_record(_tr,_productions[c],'Kalite Onay Iskartası'));
													//setup bitimi sırasındaki sağlam parçaların kasalara eklenmesi
													if(_productions[c].saglam>0){
														//tesellüm
														if(t.objParam.task_delivery.value===true){
															if(_productions[c].delivery===true){
																_promises.push(t.update_taskcase_record_add(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].saglam,_productions[c].record_id));
															}
														}
														//malzeme hazırlık
														if(t.objParam.task_materialpreparation.value===true){
															_promises.push(t.update_taskcase_record_add_mh(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].saglam,'INPUT'));
															_promises.push(t.update_taskcase_record_add_mh(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].hurda,'SCRAP'));
														}
													}
												}
											}
											if(_control_answers){
												if(_control_answers.length>0){
													for(let c=0;c<_control_answers.length;c++){
														let row=_control_answers[c];
														if(row.qtype==='control_questions'){
															_promises.push(t.createRecord(_tr,'control_answers',_time,{
																type:row.type
																,documentnumber:row.documentnumber
																,client:v_client
																,day:_jrday
																,jobrotation:_jrcode
																,opname:row.opname
																,erprefnumber:row.erprefnumber
																,employee:_employee
																,time:_time
																,question:row.question
																,answertype:row.answertype
																,valuerequire:row.valuerequire
																,valuemin:row.valuemin
																,valuemax:row.valuemax
																,answer:row.answer
																,record_id:uuidv4()
															}));
														}
													}
												}
											}
										}else{
											_cur_losttype=(t.objParam.process_remoteQualityConfirmation.value===true?'KALİTE ONAY':'KALİTE BEKLEME');
											t.processVariables.status_employee=0;
											t.processVariables.operators=[];
											t.processVariables.status_workflowprocess='QUALITY';
											t.setStatus(_cur_losttype);
											t.processVariables.lastprodstarttime=null;
											//t.processVariables.lastprodtime=null;
											_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_e','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:arg.descriptionlost}));
											_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
											_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
											_obj_where.type={[Op.in]: ['l_c', 'l_c_t']};
											_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: _cur_losttype,descriptionlost:'',faulttype:'',faultdescription:'',faultgroupcode:'',faultdevice:''},{finish:_time},true));
										}
									}
								}
								if(t.objParam.process_wsSetup.value===true){
									if(t.processVariables.status_workflowprocess==='SETUP'){
										if(t.processVariables.status_lost==='SETUP-AYAR'){
											_f=true;
											if(_productions.length>0){
												for(let c=0;c<_productions.length;c++){
													_promises.push(update_tasklist_record(_tr,_productions[c],'Ayar Iskartası'));
													//setup bitimi sırasındaki sağlam parçaların kasalara eklenmesi
													if(_productions[c].saglam>0){
														//tesellüm
														if(t.objParam.task_delivery.value===true){
															if(_productions[c].delivery===true){
																_promises.push(t.update_taskcase_record_add(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].saglam,_productions[c].record_id));
															}
														}
														//malzeme hazırlık
														if(t.objParam.task_materialpreparation.value===true){
															_promises.push(t.update_taskcase_record_add_mh(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].saglam,'INPUT'));
															_promises.push(t.update_taskcase_record_add_mh(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].hurda,'SCRAP'));
														}
													}
												}
											}
											if(_control_answers){
												if(_control_answers.length>0){
													for(let c=0;c<_control_answers.length;c++){
														let row=_control_answers[c];
														if(row.qtype==='control_questions'){
															_promises.push(t.createRecord(_tr,'control_answers',_time,{
																type:row.type
																,documentnumber:row.documentnumber
																,client:v_client
																,day:_jrday
																,jobrotation:_jrcode
																,opname:row.opname
																,erprefnumber:row.erprefnumber
																,employee:_employee
																,time:_time
																,question:row.question
																,answertype:row.answertype
																,valuerequire:row.valuerequire
																,valuemin:row.valuemin
																,valuemax:row.valuemax
																,answer:row.answer
																,record_id:uuidv4()
															}));
														}
													}
												}
											}
											if(t.objParam.process_wsQuality.value===true){
												if(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40'){
													_cur_losttype='KALİTE ONAY';
													t.processVariables.status_workflowprocess='QUALITY';
													t.setStatus(_cur_losttype);
													t.processVariables.lastprodstarttime=null;
													_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
													_obj_where.type={[Op.in]: ['l_c', 'l_c_t', 'l_e', 'l_e_t']};
													_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: _cur_losttype,descriptionlost:''},{finish:_time},true));
												}else{
													_cur_losttype=(t.objParam.process_remoteQualityConfirmation.value===true?'KALİTE ONAY':'KALİTE BEKLEME');
													t.processVariables.status_employee=0;
													t.processVariables.operators=[];
													t.processVariables.status_workflowprocess='QUALITY';
													t.setStatus(_cur_losttype);
													t.processVariables.lastprodstarttime=null;
													//t.processVariables.lastprodtime=null;
													_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_e','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:arg.descriptionlost}));
													_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
													_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
													_obj_where.type={[Op.in]: ['l_c', 'l_c_t']};
													_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: _cur_losttype,descriptionlost:''},{finish:_time},true));
												}
											}else{
												//üretime geçiş, görevde kimse yok şeklinde
												_cur_losttype='GÖREVDE KİMSE YOK';
												_promises.push(production_transition(_cur_losttype,arg.descriptionlost,true));
											}
										}else{
											_cur_losttype='USTA BEKLEME';
											t.processVariables.status_employee=0;
											t.processVariables.operators=[];
											t.setStatus(_cur_losttype);
											t.processVariables.lastprodstarttime=null;
											//t.processVariables.lastprodtime=null;
											_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_e','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:arg.descriptionlost}));
											_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
											_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
											_obj_where.type={[Op.in]: ['l_c', 'l_c_t']};
											_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: _cur_losttype,descriptionlost:'',faulttype:'',faultdescription:'',faultgroupcode:'',faultdevice:''},{finish:_time},true));
										}
									}
								}
								if(t.objParam.process_wfSetup.value===true){
									if(t.processVariables.status_workflowprocess==='SETUP-WORK-FINISH'){
										_f=true;
										if(t.processVariables.status_lost==='KALIP SÖKME'){
											_cur_losttype='İŞ YOK';
											t.cpd_time=false;
											t.processVariables.status_work='-';
											t.processVariables.status_employee=0;
											t.processVariables.operators=[];
											t.processVariables.productions=[];
											t.processVariables.status_workflowprocess='-';
											t.setStatus(_cur_losttype);
											t.processVariables.lastprodstarttime=null;
											//t.processVariables.lastprodtime=null;
											_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null,lastprodtime:null}));
											_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
											_promises.push(t.cjr_client_lost_details(_tr,_obj_where,_time,{descriptionlost:arg.descriptionlost}));
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,{
												type:'l_c'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:_cur_losttype
												,start:_time
												,record_id:uuidv4()
											}));
											let _where_cjr_cpd={type:'c_p',client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}};
											_promises.push(t.cjr_client_production_details(_tr,_where_cjr_cpd,_time));	
										}else{
											_cur_losttype='USTA BEKLEME';
											t.processVariables.status_employee=0;
											t.processVariables.operators=[];
											t.setStatus(_cur_losttype);
											t.processVariables.lastprodstarttime=null;
											//t.processVariables.lastprodtime=null;
											_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_e','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:arg.descriptionlost}));
											_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
											_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
											_obj_where.type={[Op.in]: ['l_c', 'l_c_t']};
											_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: _cur_losttype,descriptionlost:'',faulttype:'',faultdescription:'',faultgroupcode:'',faultdevice:''},{finish:_time},true));
										}
									}
								}
								if(t.processVariables.status_workflowprocess==='OP'){
									if(_control_answers){
										if(_control_answers.length>0){
											for(let c=0;c<_control_answers.length;c++){
												let row=_control_answers[c];
												if(row.qtype==='control_questions'){
													_promises.push(t.createRecord(_tr,'control_answers',_time,{
														type:row.type
														,documentnumber:row.documentnumber
														,client:v_client
														,day:_jrday
														,jobrotation:_jrcode
														,opname:row.opname
														,erprefnumber:row.erprefnumber
														,employee:_employee
														,time:_time
														,question:row.question
														,answertype:row.answertype
														,valuerequire:row.valuerequire
														,valuemin:row.valuemin
														,valuemax:row.valuemax
														,answer:row.answer
														,record_id:uuidv4()
													}));
												}
											}
										}
									}
								}
								if(!_f&&_fault_losttype!==''){
									if(_productions.length>0){
										for(let c=0;c<_productions.length;c++){
											_promises.push(update_tasklist_record(_tr,_productions[c],_fault_losttype+' Iskartası'));
											//setup bitimi sırasındaki sağlam parçaların kasalara eklenmesi
											if(_productions[c].saglam>0){
												//tesellüm
												if(t.objParam.task_delivery.value===true){
													if(_productions[c].delivery===true){
														_promises.push(t.update_taskcase_record_add(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].saglam,_productions[c].record_id));
													}
												}
												//malzeme hazırlık
												if(t.objParam.task_materialpreparation.value===true){
													_promises.push(t.update_taskcase_record_add_mh(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].saglam,'INPUT'));
													_promises.push(t.update_taskcase_record_add_mh(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].hurda,'SCRAP'));
												}
											}
										}
									}
								}
							}
						}else{
							if(t.processVariables.status_work!=='-'){
								_cur_losttype='GÖREVDE KİMSE YOK';
								if(_fault_losttype!==''){
									if(_productions.length>0){
										for(let c=0;c<_productions.length;c++){
											_promises.push(update_tasklist_record(_tr,_productions[c],_fault_losttype+' Iskartası'));
											//setup bitimi sırasındaki sağlam parçaların kasalara eklenmesi
											if(_productions[c].saglam>0){
												//tesellüm
												if(t.objParam.task_delivery.value===true){
													if(_productions[c].delivery===true){
														_promises.push(t.update_taskcase_record_add(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].saglam,_productions[c].record_id));
													}
												}
												//malzeme hazırlık
												if(t.objParam.task_materialpreparation.value===true){
													_promises.push(t.update_taskcase_record_add_mh(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].saglam,'INPUT'));
													_promises.push(t.update_taskcase_record_add_mh(_tr,_time,_productions[c].erprefnumber,_productions[c].opname,_productions[c].hurda,'SCRAP'));
												}
											}
										}
									}
								}
							}else{
								_cur_losttype='İŞ YOK';
								t.processVariables.productions=[];
							}
							t.cpd_time=false;
							t.processVariables.status_employee=0;
							t.processVariables.operators=[];
							if(t.processVariables.status_work!=='-'){
								if(t.objParam.task_finishnoemployee.value===true){
									let _obj_where_t_p_d={
										type:'c_p'+(t.processVariables.status_work=='OP-OUT-SYSTEM'?'_unconfirm':'')
										,start:{[Op.ne]:null}
										,finish:{[Op.eq]:null}
									};
									_promises.push(t.cjr_client_production_details(_tr,_obj_where_t_p_d,_time));
									_promises.push(t.create_production_records(_tr,_time));
								}
							}
							t.setStatus(_cur_losttype);
							t.processVariables.lastprodstarttime=null;
							//t.processVariables.lastprodtime=null;
							_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null,lastprodtime:null}));
							_obj_where.type={[Op.in]: ['l_c', 'l_c_t']};
							_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
							_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_e', 'l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{descriptionlost:arg.descriptionlost}));
							_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,_obj_where,{losttype: _cur_losttype,descriptionlost:'',faulttype:'',faultdescription:'',faultgroupcode:'',faultdevice:''},{finish:_time},true));
						}
						return Promise.all(_promises).then(function(){
							if(_productions.length>0){
								let _items_=[];
								for(let c=0;c<_productions.length;c++){
									const row=_productions[c];
									_items_.push({
										code:row.code
										,erprefnumber:row.erprefnumber
										,opcode:row.opcode
										,opnumber:row.opnumber
										,opname:row.opname
										,client:v_client
										,tasklist:row.tasklist
										,saglam:row.saglam
										,hurda:row.hurda
										,employee:row.employee
										,time:_time
									});
								}
								
								let mes=new dcs_message({
									type:'device_lost_finish'
									,ack:true
									,data:{records: _items_}
								});
								t.tmp_queue.push(mes);
								mes=undefined;
								//for(let c=0;c<_productions.length;c++){
								//	if(_productions[c].saglam>0){
								//		let mes=new dcs_message({
								//			type:'device_lost_finish'
								//			,ack:true
								//			,data:{
								//				client:v_client
								//				,day:_jrday
								//				,time:_time
								//				,employee:_employee
								//				,tasklist:uuidv4()
								//				,opname:_tmp_opnames
								//				,finish_data:_tmp_data_
								//			}
								//		});
								//		t.tmp_queue.push(mes);
								//		mes=undefined;
								//	}
								//	// if(_productions[c].hurda>0){
								//	// 	let _idx=t.getIndex(t.processVariables.productions,'code',_productions[c].code);
								//	// 	t.processVariables.productions[_idx].production+=parseInt(_productions[c].hurda);
								//	// 		t.processVariables.productions[_idx].productioncurrent+=parseInt(_productions[c].hurda);
								//	// }
								//}
							}
							for(let i=0;i<t.processVariables.productions.length;i++){
								delete t.processVariables.productions[i].hurda;
								delete t.processVariables.productions[i].saglam;
							}
							_promises=[];
							if(_cur_losttype=='-'
							||(t.objParam.process_wsSetup.value===true&&t.processVariables.status_pre_lost==='SETUP-AYAR')
							||(t.objParam.process_wsQuality.value===true&&t.processVariables.status_pre_lost==='KALİTE ONAY')){
							//çalışılan iş var
								let _obj_where_cld={
									type:{[Op.in]: ['l_c_t', 'l_e', 'l_e_t']}
									,client:v_client
									,finish:{[Op.eq]:null}
								};
								if((t.objParam.process_wsSetup.value===true&&t.processVariables.status_pre_lost==='SETUP-AYAR'&&t.processVariables.status_workflowprocess==='SETUP')
									||(t.objParam.process_wsQuality.value===true&&t.processVariables.status_pre_lost==='KALİTE ONAY'&&t.processVariables.status_workflowprocess==='QUALITY')){
									_obj_where_cld.type={[Op.in]: ['l_c_t']};
								}
								_promises.push(t.cjr_client_lost_details(_tr,_obj_where_cld,_time,{descriptionlost:arg.descriptionlost}));
								return Promise.all(_promises);
							}
							return null;
						});
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_device_lost_finish');
					t.tmp_queue=[];
					return Promise.all([t.getClientStatus()]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'device-employee-workstart':{
				let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
				let objwhere={[t.processVariables.emp_identify]:v_data,[arg.employeeFieldName]:true};
				if(t.objParam.app_empSecret.value===true){
					objwhere.secret=arg.secret;
				}
				let _employeeLabel=arg.employeeLabel;
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					return Promise.all([t.employeeAuthentication(_tr,objwhere,_employeeLabel)]).then(function (_emp) {
						_promises=[];
						if(typeof _emp=='object'&&_emp.length>0){
							_emp=t.getLastElementOfArray(_emp);
						}
						// oskim ustaya neredeydin diye sorması için eklendi
						if(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40'){
							_promises.push(t.isEmployeeFree(_tr,_emp,true,null));
						}
						return Promise.all(_promises).then(function(){
							return _emp;
						});
					});
				}).then(function (_emp) {
					_promises=[];
					let _emp_code=0;
					_emp_code=_emp.code;
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',employee:_emp_code});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',employee:_emp_code});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'task_finish_types|get':{
				let _promises=[];
				let _task_finish_types=[];
				let controlQuestions=[];
				t.db.sequelize.transaction(function (/*_tr*/) {
					_promises.push(t.findAll('task_finish_types',{'id':'id','code':'code','recreate':'recreate'},{},[ ['orderfield', 'ASC'] ],false));
					return Promise.all(_promises).then(function(results){
						_task_finish_types=t.getLastElementOfArray(results);
						return null;
					}).then(function(){
						let rep={type:['GÖREVİ BİTİRME','KALİTE ONAY İŞ SONU']};
						let sql_erprefnumber='';
						let sql_where='';
						if(t.processVariables.productions.length>0){
							sql_erprefnumber=' ,case ';
							sql_where =' and ( opname is null ';
							for(let i=0;i<t.processVariables.productions.length;i++){
								sql_where+=' or opname=:opname_'+i;
								rep['opname_'+i]=t.processVariables.productions[i].opname;
								sql_erprefnumber+=' when opname=\''+t.processVariables.productions[i].opname+'\' then \''+t.processVariables.productions[i].erprefnumber+'\' ';
							}
							sql_erprefnumber+=' end erprefnumber ';
							sql_where+= ' ) ';
						}
						let sql='SELECT \'control_questions\' qtype,id,type,opname,question,answertype,path '+
						'	,case answertype when \'Evet-Hayır\' then case when (valuerequire=\'1\' or valuerequire=\'Evet\') then \'Evet\' else \'Hayır\' end else valuerequire end valuerequire '+
						' ,valuemin,valuemax,documentnumber '+sql_erprefnumber+
						' ,case answertype when \'Evet-Hayır\' then \'radio\' else \'text\' end cm_row_type  '+
						' ,case answertype when \'Rakam Değer\' then \'numpadex\' when \'Evet-Hayır\' then \'radio\' else \'tr_TR\' end cm_row_layout '+
						' FROM control_questions where type in (:type) ' + sql_where+ ' order by opname,question';
						return t.db.sequelize.query(sql, {replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
							if(results.length>0){
								if(t.activity_control_questions===false){
									controlQuestions=results;
								}else{
									controlQuestions=results.concat(t.activity_control_questions);
								}
							}else{
								controlQuestions=t.activity_control_questions;
							}
						});
					})
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_finish_types,control_questions:controlQuestions});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_finish_types});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_finish_types|get');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'device-employee-workfinish':{
				let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
				let objwhere={[t.processVariables.emp_identify]:v_data,[arg.employeeFieldName]:true};
				if(t.objParam.app_empSecret.value===true){
					objwhere.secret=arg.secret;
				}
				let _employeeLabel=arg.employeeLabel;
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					return Promise.all([t.employeeAuthentication(_tr,objwhere,_employeeLabel)]);
				}).then(function (_emp) {
					let _emp_code=0;
					if(typeof _emp=='object'&&_emp.length>0){
						let x=t.getLastElementOfArray(_emp);
						_emp_code=x.code;
					}
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',employee:_emp_code});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',employee:_emp_code});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'device-work-finish':{
				let _message='';
				let _cur_losttype='İŞ YOK';
				let _control_answers=arg.control_answers;
				let _employee=(arg.employee!==false&&arg.employee!==''&&arg.employee!==undefined?arg.employee:(t.processVariables.operators.length>0?t.processVariables.operators[0].code:''));
				let _promises=[];
				let recreate=arg.recreate==1||arg.recreate=='1'?1:0;
				t.cpd_time=false;
				let _tmp_opnames=[];
				let _tmp_data_=[];
				for(let i=0;i<t.processVariables.productions.length;i++){
					let prod=t.processVariables.productions[i];
					const tmp_opname=prod.opname;
					const tmp_erprefnumber=prod.erprefnumber;
					if(_tmp_opnames.indexOf(tmp_opname)==-1){
						_tmp_opnames.push(tmp_opname);
					}
					_tmp_data_.push({erprefnumber:prod.erprefnumber,opname:prod.opname,productdonecount:prod.productdonecount,mould:prod.mould});
				}
				if(t.objParam.process_wfSetup.value===true){
					if(t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40'){
						if((_employee!==''&&typeof _employee=='string')||(t.processVariables.operators.length>0)){
							_cur_losttype='KALIP SÖKME';
						}else{
							_cur_losttype='USTA BEKLEME';
						}
					}else{
						if(t.objParam.task_finishRequireExpert.value===true&&arg.employee){
							_cur_losttype='KALIP SÖKME';
						}else{
							_cur_losttype='USTA BEKLEME';
						}
					}
					t.processVariables.status_workflowprocess='SETUP-WORK-FINISH';
				}else{
					t.processVariables.status_work='-';
					t.processVariables.status_workflowprocess='-';
				}
				if(t.timer_missing_material!==false){
					clearTimeout(t.timer_missing_material);
					t.timer_missing_material=false;
				}
				if((t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40')&&(t.processVariables.operators.length>0)){
					if(_cur_losttype==='İŞ YOK'){
						t.processVariables.status_employee=0;
					}
				}else{
					t.processVariables.status_employee=(t.objParam.task_finishRequireExpert.value===true&&arg.employee&&_cur_losttype!=='İŞ YOK'?1:0);
				}
				t.processVariables.status_tpp=0;
				t.setStatus(_cur_losttype);
				if(_cur_losttype=='KALIP SÖKME'){
					t.processVariables.status_pre_lost=_cur_losttype;
				}
				t.db.sequelize.transaction(function (_tr) {
					if(_control_answers){
						if(_control_answers.length>0){
							for(let c=0;c<_control_answers.length;c++){
								if(_control_answers[c].qtype==='control_questions_isg'){
									if(_control_answers[c].type==='GÖREVE KATILMA'){
										_control_answers[c].erprefnumber=_erp[e];
										for(let i=0;i<t.processVariables.productions.length;i++){
											let _p_item=t.processVariables.productions[i];
											if(_control_answers[c].erprefnumber==_p_item.erprefnumber){
												_control_answers[c].taskcode=_p_item.code;
												break;
											}
										}
									}else{
										for(let i=0;i<t.processVariables.productions.length;i++){
											let _p_item=t.processVariables.productions[i];
											if(_control_answers[c].opname==_p_item.opname){
												_control_answers[c].taskcode=_p_item.code;
												_control_answers[c].erprefnumber=_p_item.erprefnumber;
												break;
											}
										}
									}
								}else{
									for(let i=0;i<t.processVariables.productions.length;i++){
										let _p_item=t.processVariables.productions[i];
										if(_control_answers[c].opname==_p_item.opname){
											_control_answers[c].taskcode=_p_item.code;
											_control_answers[c].erprefnumber=_p_item.erprefnumber;
											break;
										}
									}
								}
							}
							for(let c=0;c<_control_answers.length;c++){
								let row=_control_answers[c];
								if(row.qtype==='control_questions'){
									_promises.push(t.createRecord(_tr,'control_answers',_time,{
										type:row.type
										,documentnumber:row.documentnumber
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,opname:row.opname
										,erprefnumber:row.erprefnumber
										,employee:(_employee?_employee:0)
										,time:_time
										,question:row.question
										,answertype:row.answertype
										,valuerequire:row.valuerequire
										,valuemin:row.valuemin
										,valuemax:row.valuemax
										,answer:row.answer
										,record_id:uuidv4()
									}));
								}
							}
							let mes=new dcs_message({
								type:'control_answers'
								,ack:true
								,data:{
									client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,employee:(_employee?_employee:0)
									,time:_time
									,data:_control_answers
								}
							});
							t.tmp_queue.push(mes);
							mes=undefined;
						}
					}
					t.processVariables.lastprodstarttime=null;
					//t.processVariables.lastprodtime=null;
					//görevde kimse yok durumunda iş bitirilebilmesi için eklendi
					//_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c', 'l_c_t','l_e', 'l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
					_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null,lastprodtime:null}));
					_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c', 'l_c_t','l_e', 'l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
					let _where_emp_update={client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}};
					if((t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40')&&t.processVariables.operators.length>0){
						let __tmp_op=[];
						for(let i=0;i<t.processVariables.operators.length;i++){
							__tmp_op.push(t.processVariables.operators[i].code);
						}
						let __obj_emp_update={client:v_client,day:_jrday,jobrotation:_jrcode,strlastseen:t.processVariables._time};
						if(_cur_losttype==='İŞ YOK'){
							__obj_emp_update={client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time};
						}
						_promises.push(t.updateRecord(_tr,'employees',_time,{code:{[Op.in]:__tmp_op}},__obj_emp_update));
					}else{
						if(t.objParam.task_finishRequireExpert.value===true&&arg.employee&&_cur_losttype!=='İŞ YOK'){
							_where_emp_update.code={[Op.ne]:arg.employee};
						}
						_promises.push(t.updateAllRecords(_tr,'employees',_time,_where_emp_update,{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
					}
					let _tmp_data={
						type:'l_c'
						,client:v_client
						,day:_jrday
						,jobrotation:_jrcode
						,losttype:_cur_losttype
						,descriptionlost:(_cur_losttype=='USTA BEKLEME'||_cur_losttype=='KALIP SÖKME')&&t.processVariables.status_workflowprocess=='SETUP-WORK-FINISH'?'KALIP SÖKME':''
						,start:_time
						,record_id:uuidv4()
					};
					_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
					return Promise.all(_promises).then(function(){
						_promises=[];
						if(v_workflow==='Lazer Kesim'&&arg.item){
							delete arg.item;
							recreate=0;
						}
						if(arg.item){//üretilerek bitmiş
							let _obj_where_c_p_d={
								client:v_client
								,start:{[Op.ne]:null}
								,finish:{[Op.eq]:null}
							};
							if(arg.item){
								let tmp=[];
								for(let i=0;i<arg.item.length;i++){
									tmp.push(arg.item[i].code);
								}
								_obj_where_c_p_d.tasklist={[Op.in]: tmp};
							}
							return t.db.client_production_details.findAll({attributes: { exclude: t.arr_cpd_exclude },where:_obj_where_c_p_d }).then(function(res_cpd){
								if(res_cpd){
									for(let i=0;i<res_cpd.length;i++){
										const _cpd=res_cpd[i];
										const _record_id=_cpd.record_id;
										if(_cpd.type=='c_p'){
											_promises.push(t.updateRecord(_tr,'task_lists',_time,{code:_cpd.tasklist},{productdoneactivity:0,scrapactivity:0,finish:(recreate==1?null:_time)}));
											if(t.objParam.process_wfSetup.value===true){
												let _tmp_data={};
												for(let _a in _cpd.dataValues){
													switch (_a) {
													case 'type':
														if(_cpd[_a]=='c_p'){
															_tmp_data[_a]='l_c_t';
														}
														if(_cpd[_a]=='e_p'){
															_tmp_data[_a]='l_e_t';
														}
														break;
													case 'day':
														_tmp_data[_a]=_jrday;
														break;
													case 'jobrotation':
														_tmp_data[_a]=_jrcode;
														break;
													case 'descriptionlost':
														_tmp_data[_a]=(_cur_losttype=='USTA BEKLEME'||_cur_losttype=='KALIP SÖKME')&&t.processVariables.status_workflowprocess=='SETUP-WORK-FINISH'?'KALIP SÖKME':'';
														break;
													case 'start':
														_tmp_data[_a]=_time;
														break;
													case 'finish':
														_tmp_data[_a]=null;
														break;
													case 'record_id':
														_tmp_data[_a]=uuidv4();
														break;
													default:
														_tmp_data[_a]=_cpd[_a];
														break;
													}
												}
												_tmp_data['losttype']=_cur_losttype;
												delete _tmp_data.calculatedtpp;
												delete _tmp_data.losttime;
												_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
												if((t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40')&&t.processVariables.operators.length>0){
													for(let i=0;i<t.processVariables.operators.length;i++){
														let __tmp_op=t.processVariables.operators[i];
														let _tmp_data_le={
															type:'l_e'
															,client:v_client
															,day:_jrday
															,jobrotation:_jrcode
															,losttype:_cur_losttype
															,descriptionlost:'KALIP SÖKME'
															,employee:__tmp_op.code
															,start:_time
															,record_id:uuidv4()
														};
														_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
														_tmp_data_le.type='l_e_t';
														_tmp_data_le.tasklist=_cpd.code;
														_tmp_data_le.mould=_cpd.mould;
														_tmp_data_le.mouldgroup=_cpd.mouldgroup;
														_tmp_data_le.opcode=_cpd.opcode;
														_tmp_data_le.opnumber=_cpd.opnumber;
														_tmp_data_le.opname=_cpd.opname;
														_tmp_data_le.opdescription=_cpd.opdescription;
														_tmp_data_le.erprefnumber=_cpd.erprefnumber;
														_tmp_data_le.employee=__tmp_op.code;
														_tmp_data_le.record_id=uuidv4();
														_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
													}
												}else{
													//her bir referans için işi bitiren usta ile l_e,l_e_t açılıyor
													if(t.objParam.task_finishRequireExpert.value===true&&arg.employee){
														_promises.push(t.updateRecord(_tr,'employees',t.processVariables._time,{code:arg.employee},{client:v_client,day:_jrday,jobrotation:_jrcode,strlastseen:t.processVariables._time}));
														let _tmp_data_le={
															type:'l_e'
															,client:v_client
															,day:_jrday
															,jobrotation:_jrcode
															,losttype:_cur_losttype
															,descriptionlost:'KALIP SÖKME'
															,employee:arg.employee
															,start:_time
															,record_id:uuidv4()
														};
														_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
														_tmp_data.type='l_e_t';
														_tmp_data.employee=arg.employee;
														_tmp_data.record_id=uuidv4();
														_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
													}
												}
											}
										}
										_promises.push(t.updateRecord(_tr,'client_production_details',_time,{record_id:_record_id},{taskfinishtype:arg.taskfinishtype,taskfinishdescription:arg.taskfinishdescription,finish:_time}));
									}
								}
								return null;
							}).then(function(){
								if(_promises.length>0){
									return Promise.all(_promises).then(function(){
										for(let i=0;i<t.processVariables.productions.length;i++){
											let item=t.processVariables.productions[i];
											if(arg.item){
												for(let i=0;i<arg.item.length;i++){
													if(item.code==arg.item[i].code){
														item.productdoneactivity=0;
														item.scrapactivity=0;
													}
												}
											}else{
												item.productdoneactivity=0;
												item.scrapactivity=0;
											}
										}
									});
								}else{
									return null;
								}
							}).then(function(){
								//bitmiş işin kasa tesellümleri
								_promises=[];
								if(t.objParam.task_delivery.value===true&&typeof arg.finish_from_delivery!=='undefined'&&arg.finish_from_delivery==true){
									for(let i=0;i<arg.item.length;i++){
										if(arg.item[i].delivery){
											let _where={
												movementdetail:{[Op.eq]:null}
												,erprefnumber:arg.item[i].erprefnumber
												,opname:arg.item[i].opname
												,casetype:'O'
												,status:{[Op.in]:['WORK','WAIT_FOR_DELIVERY']}
											};
											_promises.push(t.update_taskcase_delivery(_tr,_where));
										}
									}
									return Promise.all(_promises);
								}else{
									return null;
								}
							});
						}else{//iş bitir denilerek bitmiş
							let arr_laser_task=[];
							for(let i=0;i<t.processVariables.productions.length;i++){
								let item=t.processVariables.productions[i];
								item.productdoneactivity=0;
								item.scrapactivity=0;
								if(v_workflow==='Lazer Kesim'){
									if(arr_laser_task.indexOf(item.tasklistlaser)==-1){
										arr_laser_task.push(item.tasklistlaser);
										_promises.push(t.updateRecord(_tr,'task_list_lasers',_time,{code:item.tasklistlaser},{finish:(recreate==1?null:_time)}));
									}
									_promises.push(t.updateAllRecords(_tr,'task_list_laser_details',_time,{tasklistlaser:item.tasklistlaser},{productdoneactivity:0,scrapactivity:0}));
								}else{
									_promises.push(t.updateRecord(_tr,'task_lists',_time,{code:item.code},{productdoneactivity:0,scrapactivity:0,finish:(recreate==1?null:_time)}));
								}
								if(_cur_losttype!=='İŞ YOK'){
									let _tmp_data_lct={
										type:'l_c_t'
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,losttype:_cur_losttype
										,descriptionlost:'KALIP SÖKME'
										,tasklist:item.code
										,mould:item.mould
										,mouldgroup:item.mouldgroup
										,opcode:item.opcode
										,opnumber:item.opnumber
										,opname:item.opname
										,opdescription:item.opdescription
										,erprefnumber:item.erprefnumber
										,start:_time
										,record_id:uuidv4()
									};
									_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_lct));
								}
								if(t.objParam.process_wfSetup.value===true){
									if((t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40')&&t.processVariables.operators.length>0){
										for(let i=0;i<t.processVariables.operators.length;i++){
											let __tmp_op=t.processVariables.operators[i];
											let _tmp_data_le={
												type:'l_e'
												,client:v_client
												,day:_jrday
												,jobrotation:_jrcode
												,losttype:_cur_losttype
												,descriptionlost:'KALIP SÖKME'
												,employee:__tmp_op.code
												,start:_time
												,record_id:uuidv4()
											};
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
											_tmp_data_le.type='l_e_t';
											_tmp_data_le.tasklist=item.code;
											_tmp_data_le.mould=item.mould;
											_tmp_data_le.mouldgroup=item.mouldgroup;
											_tmp_data_le.opcode=item.opcode;
											_tmp_data_le.opnumber=item.opnumber;
											_tmp_data_le.opname=item.opname;
											_tmp_data_le.opdescription=item.opdescription;
											_tmp_data_le.erprefnumber=item.erprefnumber;
											_tmp_data_le.employee=__tmp_op.code;
											_tmp_data_le.record_id=uuidv4();
											_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
										}
									}else{
										if(_cur_losttype!=='İŞ YOK'){
											if(t.objParam.task_finishRequireExpert.value===true&&arg.employee){
												_promises.push(t.updateRecord(_tr,'employees',t.processVariables._time,{code:arg.employee},{client:v_client,day:_jrday,jobrotation:_jrcode,strlastseen:t.processVariables._time}));
												let _tmp_data_le={
													type:'l_e'
													,client:v_client
													,day:_jrday
													,jobrotation:_jrcode
													,losttype:_cur_losttype
													,descriptionlost:'KALIP SÖKME'
													,employee:arg.employee
													,start:_time
													,record_id:uuidv4()
												};
												_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
												_tmp_data_le.type='l_e_t';
												_tmp_data_le.tasklist=item.code;
												_tmp_data_le.mould=item.mould;
												_tmp_data_le.mouldgroup=item.mouldgroup;
												_tmp_data_le.opcode=item.opcode;
												_tmp_data_le.opnumber=item.opnumber;
												_tmp_data_le.opname=item.opname;
												_tmp_data_le.opdescription=item.opdescription;
												_tmp_data_le.erprefnumber=item.erprefnumber;
												_tmp_data_le.employee=arg.employee;
												_tmp_data_le.record_id=uuidv4();
												_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
											}
										}
									}
								}
								let _where_cjr_cpd={type:{[Op.in]:['e_p']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null},tasklist:item.code};
								if(_cur_losttype==='İŞ YOK'){
									_where_cjr_cpd.type={[Op.in]:['c_p','e_p']};
								}
								_promises.push(t.updateAllRecords(_tr,'client_production_details',_time,_where_cjr_cpd,{taskfinishtype:arg.taskfinishtype,taskfinishdescription:arg.taskfinishdescription,finish:_time}));
								//işi yarım bırakınca 
								//if(t.objParam.task_delivery.value===true&&t.objParam.task_closetaskcases.value===true){
								//	//if(item.delivery){
								//	let _where={
								//		movementdetail:{[Op.eq]:null}
								//		,erprefnumber:item.erprefnumber
								//		,opname:item.opname
								//		,casetype:'O'
								//		,status:{[Op.in]:['OPEN','WAIT','WORK']}
								//	};
								//	_promises.push(t.close_task_case(_tr,_where));
								//	//}
								//}
								if(t.objParam.task_materialpreparation.value===true&&t.objParam.task_closetaskcases.value===true){
									let _where={
										movementdetail:{[Op.ne]:null}
										,erprefnumber:item.erprefnumber
										,opname:item.opname
										,status:{[Op.in]:['OPEN','WAIT','WORK']}
									};
									_promises.push(t.close_task_case(_tr,_where));
								}
							}
						}
						if(_cur_losttype==='İŞ YOK'){
							t.processVariables.productions=[];
							t.processVariables.operators=[];
						}
						if(_promises.length>0){
							return Promise.all(_promises);
						}else{
							return null;
						}
					});
				}).then(function () {
					if(_tmp_data_.length>0){
						let mes=new dcs_message({
							type:'device_work_finish'
							,ack:true
							,data:{
								client:v_client
								,day:_jrday
								,start:_time
								,employee:_employee
								,tasklist:uuidv4()
								,opname:_tmp_opnames
								,finish_data:_tmp_data_
							}
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					}
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_device_work_finish');
					t.tmp_queue=[];
					return Promise.all([t.getDeviceInitValues('catch-device_work_finish')]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'device-work-finish-multi':{
				// ok+gelen verideki arg.data içindeki işleri sıra ile sonlandır
				// ok+her biri bitirilirken t.processvariables.productions içinden kaldır
				// biten işin kalıbında 2 iş varken biri bitiyor ve diğeri devam ediyor ise, t.processvariables.productions içindeki mouldgroup güncelle
				// veya işleri bitirdikten sonra kalanların kalıp grup-detay tablolarında uyumlu işi var mı kontrol et
				// ok+tpp her seferinde yeniden hesapla-taskTppRemove-transaction sonunda çağırılmalı
				// ok+sonunda iş kalmadı ise parametreyi kontrol et, durumunu ayarla
				let _message='';
				let _cur_losttype='İŞ YOK';
				let _control_answers=arg.control_answers;
				let _recreate=arg.recreate==1||arg.recreate=='1'?1:0;
				let _taskfinishdescription=arg.taskfinishdescription;
				let _taskfinishtype=arg.taskfinishtype;
				let _employee=((arg.employee!==false&&arg.employee!=='')?arg.employee:(t.processVariables.operators.length>0?t.processVariables.operators[0].code:''));
				let _promises=[];
				let _tmp_opnames=[];
				let _tmp_data_=[];
				t.db.sequelize.transaction(function (_tr) {
					if(_control_answers){
						if(_control_answers.length>0){
							for(let c=0;c<_control_answers.length;c++){
								if(_control_answers[c].qtype==='control_questions_isg'){
									if(_control_answers[c].type==='GÖREVE KATILMA'){
										_control_answers[c].erprefnumber=_erp[e];
										for(let i=0;i<t.processVariables.productions.length;i++){
											let _p_item=t.processVariables.productions[i];
											if(_control_answers[c].erprefnumber==_p_item.erprefnumber){
												_control_answers[c].taskcode=_p_item.code;
												break;
											}
										}
									}else{
										for(let i=0;i<t.processVariables.productions.length;i++){
											let _p_item=t.processVariables.productions[i];
											if(_control_answers[c].opname==_p_item.opname){
												_control_answers[c].taskcode=_p_item.code;
												_control_answers[c].erprefnumber=_p_item.erprefnumber;
												break;
											}
										}
									}
								}
							}
							for(let c=0;c<_control_answers.length;c++){
								let row=_control_answers[c];
								if(row.qtype==='control_questions'){
									_promises.push(t.createRecord(_tr,'control_answers',_time,{
										type:row.type
										,documentnumber:row.documentnumber
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,opname:row.opname
										,erprefnumber:row.erprefnumber
										,employee:(_employee?_employee:0)
										,time:_time
										,question:row.question
										,answertype:row.answertype
										,valuerequire:row.valuerequire
										,valuemin:row.valuemin
										,valuemax:row.valuemax
										,answer:row.answer
										,record_id:uuidv4()
									}));
								}
							}
							let mes=new dcs_message({
								type:'control_answers'
								,ack:true
								,data:{
									client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,employee:(_employee?_employee:0)
									,time:_time
									,data:_control_answers
								}
							});
							t.tmp_queue.push(mes);
							mes=undefined;
						}
					}
					let _obj_where_c_p_d={
						client:v_client
						,start:{[Op.ne]:null}
						,finish:{[Op.eq]:null}
						,type:{[Op.in]: ['c_p', 'e_p','c_p_unconfirm','e_p_unconfirm']}
					};
					if(typeof arg.data==='undefined'){
						//robot için iş listesi üzerinden iş seçimi güncellemesi ile değiştirildi alttaki satır eklendi
						arg.data=t.processVariables.productions;
					}
					if(v_workflow==='Sabit Üretim Aparatlı'){
						for(let i=arg.data.length-1;i>=0;i--){
							let row_a=arg.data[i];
							for(let k=0;k<t.processVariables.productions.length;k++){
								let row_p=t.processVariables.productions[k];
								if(row_a.opname===row_p.opname&&row_a.erprefnumber===row_p.erprefnumber&&row_a.mould!==row_p.mould){
									arg.data.push(row_p);
								}
							}
						}
					}
					while(arg.data.length>0){
						let item=arg.data.shift();
						if(t.processVariables.status_work==='OP'&&v_workflow!=='Kataforez'){
							_obj_where_c_p_d.mould=item.mould;
							_obj_where_c_p_d.mouldgroup=item.mouldgroup;
						}
						_obj_where_c_p_d.opname=item.opname;
						_obj_where_c_p_d.erprefnumber=item.erprefnumber;
						_obj_where_c_p_d.tasklist=item.tasklist;
						if(_tmp_opnames.indexOf(item.opname)==-1){
							_tmp_opnames.push(item.opname);
						}
						_promises.push(t.updateRecord(_tr,'task_lists',_time,{code:item.tasklist},{productdoneactivity:0,scrapactivity:0,finish:(_recreate==1?null:_time)}));
						////görevde kimse yok durumunda iş bitirilebilmesi için eklendi
						//_promises.push(t.cjr_client_lost_details(_tr,_obj_where_c_p_d,_time));
						_promises.push(t.cjr_client_production_details(_tr,_obj_where_c_p_d,_time,{taskfinishtype:_taskfinishtype,taskfinishdescription:_taskfinishdescription}));
						for(let j=0;j<t.processVariables.productions.length;j++){
							let _prod=t.processVariables.productions[j];
							if(_prod.tasklist==item.tasklist){
								if(_prod.delivery){
									_tmp_data_.push({erprefnumber:item.erprefnumber,opname:item.opname,productdonecount:_prod.productdonecount});
								}
								t.processVariables.productions.splice(j,1);
							}
						}
						if(t.objParam.task_delivery.value===true&&t.objParam.task_closetaskcases.value===true){
							//if(item.delivery){
							if(arg.taskfinishdescription==='İş emri miktarına gelmeden tesellüm edildi.'||arg.taskfinishdescription==='İş emri miktarına ulaşıldı.'){
								let _where={
									movementdetail:{[Op.eq]:null}
									,erprefnumber:item.erprefnumber
									,opname:item.opname
									,casetype:'O'
									,status:{[Op.in]:['WORK','WAIT_FOR_DELIVERY']}
								};
								_promises.push(t.update_taskcase_delivery(_tr,_where));
							//}else{
							//	let _where={
							//		movementdetail:{[Op.eq]:null}
							//		,erprefnumber:item.erprefnumber
							//		,opname:item.opname
							//		,casetype:'O'
							//		,status:{[Op.in]:['OPEN','WAIT','WORK']}
							//	};
							//	_promises.push(t.close_task_case(_tr,_where));
							}
							//}
						}
						if(t.objParam.task_materialpreparation.value===true&&t.objParam.task_closetaskcases.value===true){
							let _where={
								movementdetail:{[Op.ne]:null}
								,erprefnumber:item.erprefnumber
								,opname:item.opname
								,status:{[Op.in]:['OPEN','WAIT','WORK']}
							};
							_promises.push(t.close_task_case(_tr,_where));
						}
					}
					return Promise.all(_promises).then(function(){
						_promises=[];
						if(t.processVariables.productions.length==0){
							if(t.timer_missing_material!==false){
								clearTimeout(t.timer_missing_material);
								t.timer_missing_material=false;
							}
							t.cpd_time=false;
							t.processVariables.lastprodstarttime=null;
							//t.processVariables.lastprodtime=null;
							t.processVariables.status_work='-';
							t.processVariables.status_employee=0;
							t.processVariables.status_tpp=0;
							t.processVariables.status_workflowprocess='-';
							t.setStatus(_cur_losttype);
							//görevde kimse yok durumunda iş bitirilebilmesi için eklendi
							_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c','l_e','l_c_t','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
							_promises.push(t.cjr_client_production_details(_tr,{type:{[Op.in]:['c_p','e_p']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{taskfinishtype:arg.taskfinishtype,taskfinishdescription:arg.taskfinishdescription}));
							_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));		
							t.processVariables.operators=[];
							let _tmp_data={
								type:'l_c'
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,losttype:_cur_losttype
								,descriptionlost:_cur_losttype
								,start:_time
								,record_id:uuidv4()
							};
							_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
							_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
							return Promise.all(_promises);
						}else{
							// kalan işler için uygun kalıp grup tanımı olup olmadığı kontrol edilecek
							// bütün işleri sonlandır, status_tpp 0 yap 
							// grubu olan işler için yeni kayıt aç					
							let _tmp=[];
							while(t.processVariables.productions.length>0){
								let item=t.processVariables.productions.shift();
								_obj_where_c_p_d.mould=item.mould;
								_obj_where_c_p_d.mouldgroup=item.mouldgroup;
								_obj_where_c_p_d.opname=item.opname;
								_obj_where_c_p_d.erprefnumber=item.erprefnumber;
								_obj_where_c_p_d.tasklist=item.tasklist;
								delete item.mould;
								delete item.mouldgroup;
								delete item.leafmask;
								delete item.leafmaskcurrent;
								delete item.tasklist;
								_tmp.push(item);
								_promises.push(t.cjr_client_production_details(_tr,_obj_where_c_p_d,_time,{taskfinishtype:_taskfinishtype,taskfinishdescription:_taskfinishdescription}));
							}
							if(_tmp.length>0){
								t.processVariables.status_tpp=0;
								_promises.push(t.control_open_task_multi(_tr,_tmp,_event));
							}
							return Promise.all(_promises).then(function(res_arr){
								if(res_arr.length>0){
									_promises=[];
									let x=t.getLastElementOfArray(res_arr);
									if(x.err===true){//kalan işler için uygun aparat yok
										if(t.timer_missing_material!==false){
											clearTimeout(t.timer_missing_material);
											t.timer_missing_material=false;
										}
										_cur_losttype='İŞ YOK';
										t.cpd_time=false;
										t.processVariables.lastprodstarttime=null;
										//t.processVariables.lastprodtime=null;
										t.processVariables.status_work='-';
										t.processVariables.status_employee=0;
										t.processVariables.status_tpp=0;
										t.processVariables.status_workflowprocess='-';
										t.setStatus(_cur_losttype);
										_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c','l_e','l_c_t','l_e_t']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
										_promises.push(t.cjr_client_production_details(_tr,{type:{[Op.in]:['c_p','e_p']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time,{taskfinishtype:arg.taskfinishtype,taskfinishdescription:arg.taskfinishdescription}));
										_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));		
										t.processVariables.operators=[];
										t.processVariables.productions=[];
										let _tmp_data={
											type:'l_c'
											,client:v_client
											,day:_jrday
											,jobrotation:_jrcode
											,losttype:_cur_losttype
											,descriptionlost:_cur_losttype
											,start:_time
											,record_id:uuidv4()
										};
										_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
										_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
									}else{//kalan iş devam edebilir
										_promises.push(t.open_task_multi(_tr,x.data,null,_event,false));
									}
									return Promise.all(_promises);
								}
							});			
						}
					});
				}).then(function () {
					if(_tmp_data_.length>0){
						let mes=new dcs_message({
							type:'device_work_finish'
							,ack:true
							,data:{
								client:v_client
								,day:_jrday
								,start:_time
								,employee:_employee
								,tasklist:uuidv4()
								,opname:_tmp_opnames
								,finish_data:_tmp_data_
							}
						});
						t.tmp_queue.push(mes);
						mes=undefined;
					}
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_device_work_finish_multi');
					t.tmp_queue=[];
					return Promise.all([t.getDeviceInitValues('catch-device_work_finish_multi')]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'device-work-finish-outofsystem':{
				let _message='';
				let _cur_losttype='İŞ YOK';
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					let _obj_where_c_p_d={
						client:v_client
						,start:{[Op.ne]:null}
						,finish:{[Op.eq]:null}
					};
					let _obj_where_c_l_d={
						client:v_client
						,start:{[Op.ne]:null}
						,finish:{[Op.eq]:null}
					};
					if(arg.parentEvent=='leave'){
						t.processVariables.status_employee-=1;
						if(t.processVariables.status_employee>0){
							_obj_where_c_l_d.employee=arg.employee;
							_obj_where_c_l_d.type={[Op.in]:['l_e','l_e_t']};
						}
					}
					while(arg.data.length>0){
						let item=arg.data.shift();
						_obj_where_c_p_d.opname=item.opname;
						_obj_where_c_p_d.erprefnumber=item.erprefnumber;
						_obj_where_c_p_d.tasklist=item.tasklist;
						_promises.push(t.cjr_client_lost_details(_tr,_obj_where_c_l_d,_time));
						let production=0;
						let prod_jr=0;
						let prod_done=0;
						let b_finish=false;
						for(let j=0;j<t.processVariables.productions.length;j++){
							let _prod=t.processVariables.productions[j];
							if(_prod.tasklist==item.tasklist){
								production=parseInt(item.production);
								prod_jr=parseInt(t.processVariables.productions[j].productdonejobrotation)+parseInt(item.production);
								prod_done=parseInt(t.processVariables.productions[j].productdonecount)+parseInt(item.production);
								b_finish=parseInt(t.processVariables.productions[j].productcount)<=prod_done;
								if((arg.parentEvent=='leave'&&b_finish)||!arg.parentEvent){
									t.processVariables.productions.splice(j,1);
								}
							}
						}
						if(arg.parentEvent=='leave'){
							//kataforez için görevden ayrılma sırasında sadece operatöre üretim yazılacak
							if(v_workflow!=='Kataforez'){
								//_obj_where_c_p_d.type=v_workflow==='Kataforez'?'c_p':'c_p_unconfirm';
								_obj_where_c_p_d.type='c_p_unconfirm';
								_promises.push(t.updateAllRecords(_tr,'client_production_details',_time,_obj_where_c_p_d,{production:prod_jr,productioncurrent:prod_jr,finish:(b_finish?_time:null)}));
							}
							_obj_where_c_p_d.type=v_workflow==='Kataforez'?'e_p':'e_p_unconfirm';
							_obj_where_c_p_d.employee=arg.employee;
							_promises.push(t.updateAllRecords(_tr,'client_production_details',_time,_obj_where_c_p_d,{production:production,productioncurrent:production,finish:_time}));		
							_promises.push(t.updateRecord(_tr,'employees',_time,{code:arg.employee,client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
							let idx=t.getIndex(t.processVariables.operators,'code',arg.employee);
							if(idx>-1){
								t.processVariables.operators.splice(idx,1);
							}
						}else{
							_promises.push(t.cjr_client_production_details(_tr,_obj_where_c_p_d,_time,{production:production,productioncurrent:production}));
						}
						if(v_workflow!=='Kataforez'||arg.parentEvent!=='leave'){
							//sistem dışı çalışma sonrasında sistemde tesellüm aktif ise productdonecount arttırma
							let obj_update_tl={productdoneactivity:0,scrapactivity:0};
							if(t.objParam.task_delivery.value===false){
								obj_update_tl.productdonejobrotation=prod_jr;
								obj_update_tl.productdonecount=prod_done;
								obj_update_tl.finish=(b_finish?_time:null);
							}
							_promises.push(t.updateRecord(_tr,'task_lists',_time,{code:item.tasklist},obj_update_tl));
						}
					}
					return Promise.all(_promises).then(function(){
						_promises=[];
						if(t.processVariables.productions.length==0){
							t.cpd_time=false;
							t.processVariables.lastprodstarttime=null;
							//t.processVariables.lastprodtime=null;
							t.processVariables.status_work='-';
							t.processVariables.status_employee=0;
							t.processVariables.status_tpp=0;
							t.processVariables.status_workflowprocess='-';
							t.setStatus(_cur_losttype);
							//görevde kimse yok durumunda iş bitirilebilmesi için eklendi
							_promises.push(t.cjr_client_lost_details(_tr,{type:{[Op.in]: ['l_c','l_e']},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
							_promises.push(t.cjr_client_production_details(_tr,{type:{[Op.in]:(v_workflow==='Kataforez'?['c_p','e_p']:['c_p_unconfirm','e_p_unconfirm'])},client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
							_promises.push(t.updateAllRecords(_tr,'employees',_time,{client:v_client,day:_jrday,jobrotation:_jrcode,start:{[Op.ne]:null},finish:{[Op.eq]:null}},{client:null,tasklist:null,day:null,jobrotation:null,strlastseen:t.processVariables._time}));
							t.processVariables.operators=[];
							let _tmp_data={
								type:'l_c'
								,client:v_client
								,day:_jrday
								,jobrotation:_jrcode
								,losttype:_cur_losttype
								,descriptionlost:_cur_losttype
								,start:_time
								,record_id:uuidv4()
							};
							_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
							_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
						}else{
							if(arg.parentEvent=='leave'){
								_promises.push(t.cjr_client_production_details(_tr,{type:(v_workflow==='Kataforez'?'e_p':'e_p_unconfirm'),employee:arg.employee,client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null}},_time));
							}
							if(t.processVariables.status_employee>0){
								_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
							}else{
								_cur_losttype='GÖREVDE KİMSE YOK';
								t.setStatus(_cur_losttype);
								let _tmp_data={
									type:'l_c'
									,client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,losttype:_cur_losttype
									,descriptionlost:_cur_losttype
									,start:_time
									,record_id:uuidv4()
									,sourcedescriptionlost:'device-work-finish-outofsystem-1'
								};
								_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data));
								for(let i=0;i<t.processVariables.productions.length;i++){
									let prod=t.processVariables.productions[i];
									let _tmp_data_lct={
										type: 'l_c_t'
										,client: v_client
										,day: _jrday
										,jobrotation: _jrcode
										,losttype: 'GÖREVDE KİMSE YOK'
										,descriptionlost: null
										,tasklist: prod.tasklist
										,mould: prod.mould
										,mouldgroup: prod.mouldgroup
										,opcode: prod.opcode
										,opnumber: prod.opnumber
										,opname: prod.opname
										,opdescription: prod.opdescription
										,erprefnumber: prod.erprefnumber
										,taskfromerp:prod.taskfromerp
										,start: _time
										,finish: null
										,record_id: uuidv4()
										,energy: null
										,sourcedescriptionlost:'device-work-finish-outofsystem-2'
									};
									_promises.push(t.createRecord(null,'client_lost_details',_time,_tmp_data_lct));
								}
								_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime}));
							}
						}
						return Promise.all(_promises);
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_device_work_finish_outofsystem');
					t.tmp_queue=[];
					return Promise.all([t.getDeviceInitValues('catch-device_work_finish_outofsystem')]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'device-employee-authorize':{
				let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
				let objwhere={[t.processVariables.emp_identify]:v_data,[arg.employeeFieldName]:true};
				//if(t.objParam.app_empSecret.value===true||(typeof arg.current_event!=='undefined'&&arg.current_event==='qualitycontrol')){
				if(t.objParam.app_empSecret.value===true){
					objwhere.secret=arg.secret;
				}
				let _employeeLabel=arg.employeeLabel;
				let _promises=[];
				let controlQuestions=[];
				t.db.sequelize.transaction(function (_tr) {
					if(typeof arg.current_event!=='undefined'&&arg.current_event==='qualitycontrol'){
						let rep={type:['KALİTE ONAY']};
						let sql_erprefnumber='';
						let sql_where='';
						if(t.processVariables.productions.length>0){
							sql_erprefnumber=' ,case ';
							sql_where =' and ( opname is null ';
							for(let i=0;i<t.processVariables.productions.length;i++){
								sql_where+=' or opname=:opname_'+i;
								rep['opname_'+i]=t.processVariables.productions[i].opname;
								sql_erprefnumber+=' when opname=\''+t.processVariables.productions[i].opname+'\' then \''+t.processVariables.productions[i].erprefnumber+'\' ';
							}
							sql_erprefnumber+=' end erprefnumber ';
							sql_where+= ' ) ';
						}
						let sql='SELECT \'control_questions\' qtype,id,type,opname,question,answertype,path '+
						'	,case answertype when \'Evet-Hayır\' then case when (valuerequire=\'1\' or valuerequire=\'Evet\') then \'Evet\' else \'Hayır\' end else valuerequire end valuerequire '+
						' ,valuemin,valuemax,documentnumber '+sql_erprefnumber+
						' ,case answertype when \'Evet-Hayır\' then \'radio\' else \'text\' end cm_row_type  '+
						' ,case answertype when \'Rakam Değer\' then \'numpadex\' when \'Evet-Hayır\' then \'radio\' else \'tr_TR\' end cm_row_layout '+
						' FROM control_questions where type in (:type) ' + sql_where+ ' order by opname,question';
						return t.db.sequelize.query(sql, {replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
							if(results.length>0){
								controlQuestions=results;
								//if(t.activity_control_questions===false){
								//	controlQuestions=results;
								//}else{
								//	controlQuestions=results.concat(t.activity_control_questions);
								//}
							//}else{
							//	controlQuestions=t.activity_control_questions;
							}
						});
					}
					return null;
				}).then(function (_tr) {
					return Promise.all([t.employeeAuthentication(_tr,objwhere,_employeeLabel)]);
				}).then(function (_emp) {
					let _emp_code=0;
					if(typeof _emp=='object'&&_emp.length>0){
						let x=t.getLastElementOfArray(_emp);
						_emp_code=x.code;
					}
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',employee:_emp_code,control_questions:controlQuestions});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',employee:_emp_code});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'device-employee-join':{
				let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
				let objwhere={[t.processVariables.emp_identify]:v_data,[Op.or]: []};
				if(t.objParam.app_empSecret.value===true){
					objwhere.secret=arg.secret;
				}
				let arr_employeeFieldName=arg.employeeFieldName.split('|');
				for(let i=0;i<arr_employeeFieldName.length;i++){
					objwhere[Op.or].push({[arr_employeeFieldName[i]]:true});
				}
				let _join_employeeLabel=arg.employeeLabel;
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					return Promise.all([t.employeeJoin(_tr,objwhere,_time,_join_employeeLabel,(_join_employeeLabel==='Operatör'||_join_employeeLabel==='Kalite Personeli/Operatör'||t.objConfig.dcs_server_IP.value==='10.0.0.101'),arg.authemployee)]);					
				}).then(function (res) {
					let obj_ret={event:_event,result:'ok',message:''};
					let _res=t.getLastElementOfArray(res);
					if(typeof _res.event==='string'){
						obj_ret.event=_res.event;
						if(typeof _res.data==='object'){
							obj_ret.data=_res.data;
						}
					}
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',obj_ret);
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					});
				}).catch(function (err) {
					let _arr_join=['sicil bulunamadı','Operatör bulunamadı','Operatör işten ayrılmış','ayrılmalısınız','outofsystem','çalışmak için yetkinliğiniz','yetkinliğiniz'];
					let _f_join=false;
					for(let _i_join=0;_i_join<_arr_join.length;_i_join++){
						if(!_f_join&&err.toString().indexOf(_arr_join[_i_join])>-1){
							_f_join=true;
						}
					}
					if(!_f_join){
						t.mjd('rollback-'+_event);
						console.log(err);
						classBase.writeError(err,t.ip,'catch_workflow_device_employee_join');
					}
					t.tmp_queue=[];
					return Promise.all([t.getClientStatus()]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'device-employee-join|operation_authorization':{
				let _promises=[];
				let objwhere={code:arg.employee};
				if(t.objParam.app_empSecret.value===true){
					objwhere.secret=arg.secret;
				}
				t.db.sequelize.transaction(function (_tr) {
					return Promise.all([t.employeeJoin(_tr,objwhere,_time,'',false,arg.authemployee)]);	
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					});
				}).catch(function (err) {
					let _arr_join=['sicil bulunamadı','Operatör bulunamadı','Operatör işten ayrılmış','ayrılmalısınız','outofsystem','yetersiz Seviye'];
					let _f_join=false;
					for(let _i_join=0;_i_join<_arr_join.length;_i_join++){
						if(!_f_join&&err.toString().indexOf(_arr_join[_i_join])>-1){
							_f_join=true;
						}
					}
					if(!_f_join){
						t.mjd('rollback-'+_event);
						console.log(err);
						classBase.writeError(err,t.ip,'catch_workflow_device_employee_join');
					}
					t.tmp_queue=[];
					return Promise.all([t.getClientStatus()]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'device-employee-leave':{
				let v_data=t.objParam.app_empIdentify.value!='RF ID'?arg.data:arg.data.substr(0,3)*65536+arg.data.substr(3,5);
				let objwhere={[t.processVariables.emp_identify]:v_data};
				if(t.objParam.app_empSecret.value===true){
					objwhere.secret=arg.secret;
				}
				let _promises=[];
				let _f_timer=false;
				if(t.timer_missing_material!==false){
					_f_timer=true;
					clearTimeout(t.timer_missing_material);
					t.timer_missing_material=false;
				}
				if(t.processVariables.status_work=='OP-OUT-SYSTEM'||v_workflow=='Kataforez'){
					t.winmessend('cn_js_workflow',{event:_event,result:'showscreen:device_workfinish_outofsystem',employee:v_data,message:''});
					return;
				}else{
					t.db.sequelize.transaction(function (_tr) {
						return Promise.all([t.isEmployeeWork(_tr,objwhere)]).then(function(_obj_res){
							if(_obj_res[0].isexpert&&_obj_res[0].tasklist==null&&_obj_res[0].client==null){
								//okutulan kart usta ise ve kendisi burada yok ise herkesi çıkar
								for (let i = 0; i < t.processVariables.operators.length; i++) {
									const _emp=t.processVariables.operators[i];
									_promises.push(t.employeeLeave(_tr,{code:_emp.code},_time,false));
								}
							}else{
								_promises.push(t.employeeLeave(_tr,objwhere,_time,false));
							}
							return Promise.all(_promises).then(function(){
								return Promise.all([t.controlDeviceStatus(_tr,_time,'device-employee-leave')]);
							});
						});
					}).then(function () {
						_promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						if(t.processVariables.operators.length>0&&_f_timer&&t.objParam.task_materialpreparation.value===true){
							t.timer_missing_material=setTimeout(() => {
								t.control_missing_materials('waitmaterial',11);
							}, 10000);
						}
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
						return Promise.all(_promises).then(function(){
							//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
						});
					}).catch(function (err) {
						console.log('rollback');
						console.log(err);
						let _arr_leave=['çalışma bilgisi bulunamadı','Operatör bulunamadı'];
						let _f_leave=false;
						for(let _i_leave=0;_i_leave<_arr_leave.length;_i_leave++){
							if(!_f_leave&&err.toString().indexOf(_arr_leave[_i_leave])>-1){
								_f_leave=true;
							}
						}
						if(!_f_leave){
							classBase.writeError(err,t.ip,'catch_workflow_device_employee_leave');
						}
						t.tmp_queue=[];
						if(t.processVariables.operators.length>0&&_f_timer&&t.objParam.task_materialpreparation.value===true){
							t.timer_missing_material=setTimeout(() => {
								t.control_missing_materials('waitmaterial',12);
							}, 10000);
						}
						return Promise.all([t.getClientStatus()]).then(function(){
							t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						});
					});
				}
				break;
			}
			case 'task_reject_details|save':{
				let v_task_reject_details=arg.task_reject_details;
				let v_tasklist=arg.tasklist;
				let obj_reject_details=[];
				for(let i=0;i<v_task_reject_details.length;i++){
					let row=v_task_reject_details[i];
					let idx=t.getIndex(t.processVariables.productions,(row.opname?'opname':'tasklist'),(row.opname?row.opname:v_tasklist));
					if(idx>-1){
						let pitem=t.processVariables.productions[idx];
						row.erprefnumber=pitem.erprefnumber;
						row.opcode=pitem.opcode;
						row.opnumber=pitem.opnumber;
						row.opname=pitem.opname;
						row.opdescription=pitem.opdescription;
						let _idx=t.getIndex(obj_reject_details,'tasklist',pitem.tasklist);
						if(_idx===-1){
							obj_reject_details.push({tasklist:pitem.tasklist,scrap:parseInt(row.quantityreject),isproductionaffect:(row.isproductionaffect===false?false:true)});
						}else{
							obj_reject_details[_idx].scrap+=parseInt(row.quantityreject);
						}
					}else{
						//üretim bilgisi bulunamadı
					}
				}
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					if(obj_reject_details.length>0){
						if(v_workflow!=='Kataforez'&&arg.parentEvent!=='input_nok'){
							for(let i=0;i<obj_reject_details.length;i++){
								let _row=obj_reject_details[i];
								let _idx=t.getIndex(t.processVariables.productions,'tasklist',_row.tasklist);
								if(_idx>-1){
									if(parseInt(t.processVariables.productions[_idx].productdoneactivity)<_row.scrap&&_row.isproductionaffect===true){
										throw new Error('Aktivitede üretilen miktardan fazla ıskarta girilemez');
									}
								}
							}
						}
						let v_erprefnumber='';
						let v_opname='';
						let update_tasklist=function(v_tasklist,v_total_scrap,v_isproductionaffect){
							return t.db.task_lists.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
								,where:{code: v_tasklist} }, {transaction: _tr}).then(function(result_tl){
								if(result_tl){
									v_erprefnumber=result_tl.erprefnumber;
									v_opname=result_tl.opname;
									let _obj_tmp_upd={
										scrapactivity:parseInt(result_tl.scrapactivity)+v_total_scrap
										,scrapjobrotation:parseInt(result_tl.scrapjobrotation)+v_total_scrap
										,scrappart:parseInt(result_tl.scrappart)+v_total_scrap
										,record_id:result_tl.record_id
										,update_user_id:0
										,updatedAt:_time
									};
									if(v_isproductionaffect===true){
										_obj_tmp_upd.productdoneactivity=parseInt(result_tl.productdoneactivity)-v_total_scrap;
										_obj_tmp_upd.productdonejobrotation=parseInt(result_tl.productdonejobrotation)-v_total_scrap;
										_obj_tmp_upd.productdonecount=parseInt(result_tl.productdonecount)-v_total_scrap;
									}
									return Promise.all([t.updateRecord(_tr,'task_lists',_time,{record_id:result_tl.record_id},_obj_tmp_upd)])
									.then(function(){
										let _p=[];
										if(t.objParam.task_delivery.value===true){
											_p.push(t.update_taskcase_record_remove(_tr,_time,v_erprefnumber,v_opname,v_total_scrap));
										}
										//taskcase güncelleme-malzeme hazırlık
										if(t.objParam.task_materialpreparation.value===true){
											_p.push(t.update_taskcase_record_remove_mh(_tr,_time,v_erprefnumber,v_opname,v_total_scrap));
										}
										return Promise.all(_p);
									});
								}
							});
						};
						for(let i=0;i<obj_reject_details.length;i++){
							let item=obj_reject_details[i];
							let item_tl=item.tasklist;
							let item_sc=item.scrap;
							let item_isproductionaffect=(item.isproductionaffect&&arg.parentEvent!=='input_nok');
							_promises.push(update_tasklist(item_tl,item_sc,item_isproductionaffect));
						}
						if(arg.parentEvent&&arg.parentEvent==='input_nok'){
							t.processVariables.lastprodstarttime=_time;
							let obj_client_update={statustime:_time,status:t.generateClientStatus(),lastprodstarttime:t.processVariables.lastprodstarttime,lastprodtime:t.processVariables.lastprodtime};
							_promises.push(t.updateRecord(null,'clients',_time,{code:v_client},obj_client_update));
						}
					}
					return Promise.all(_promises).then(function(){
						let saverecord=function(_item,_type,_emp){
							let _obj_t_r_d={
								type: _type
								,day:_jrday
								,jobrotation:_jrcode
								,client:v_client
								,tasklist:v_tasklist
								,rejecttype: _item.rejecttype
								,materialunit: _item.materialunit
								,time: _time
								,opcode: _item.opcode
								,opnumber: (_item.opnumber!==''?_item.opnumber:null)
								,opname: _item.opname
								,opdescription: _item.opdescription
								,quantityreject: _item.quantityreject
								,quantityretouch:(_item.retouch==1?_item.quantityreject:0)
								,quantityscrap: (_item.retouch==0?_item.quantityreject:0)
								,employee: (_type==='e_r'?_emp:null)
								,erprefnumber: _item.erprefnumber
								,description: (_item.description?_item.description:null)
								,record_id:uuidv4()
							};
							return Promise.all([t.createRecord(_tr,'task_reject_details',_time,_obj_t_r_d)]);
						};
						_promises=[];
						if(typeof v_task_reject_details.forEach=='function'){
							v_task_reject_details.forEach(function(item) {
								_promises.push(saverecord(item,'t_r'));
								for(let i=0;i<t.processVariables.operators.length;i++){
									_promises.push(saverecord(item,'e_r',t.processVariables.operators[i].code));
								}
							});
						}
						return Promise.all(_promises);
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					if(arg.parentEvent&&arg.parentEvent==='show-scrap-before-finish'&&t.objParam.task_finishQuantityReach.value===true){
						t.show_scrap_before_finish=false;
						let _data=[];
						for(let i=0;i<obj_reject_details.length;i++){
							let _row=obj_reject_details[i];
							let _idx=t.getIndex(t.processVariables.productions,'tasklist',_row.tasklist);
							if(_idx>-1){
								let item=t.processVariables.productions[_idx];
								item.scrapactivity+=_row.scrap;
								item.scrapjobrotation+=_row.scrap;
								item.scrappart+=_row.scrap;
								item.productdoneactivity-=_row.scrap;
								item.productdonejobrotation-=_row.scrap;
								item.productdonecount-=_row.scrap;
								item.remaining=item.productcount-item.productdonecount;
							}
						}
						//üretimdeki referansların miktara ulaşıp ulaşmadığı kontrol edilecek
						for(let i=0;i<t.processVariables.productions.length;i++){
							let row=t.processVariables.productions[i];
							if(row.remaining<=0){
								_data.push(row);
							}
						}
						if(_data.length>0){
							if(v_workflow==='Üretim Aparatsız'){
								t.eventListener({event:'device-work-finish',taskfinishtype:'GÖREV BİTTİ',recreate:0,taskfinishdescription:'İş emri miktarına ulaşıldı.',item:_data});
							}
							if(v_workflow==='Değişken Üretim Aparatlı'){
								//@@todo:pres için birden fazla çıktı ve farklı üretim maskeli kalıplarda (0001,0100) her ikisi bittiğinde event tetiklenmeli
								t.eventListener({event:'device-work-finish',taskfinishtype:'GÖREV BİTTİ',recreate:0,taskfinishdescription:'İş emri miktarına ulaşıldı.',item:_data});
							}
							if(v_workflow==='Sabit Üretim Aparatlı'){
								t.eventListener({event:'device-work-finish-multi',taskfinishtype:'GÖREV BİTTİ',recreate:0,taskfinishdescription:'İş emri miktarına ulaşıldı.',data:_data});
							}
						}else{
							t.scrap_timer_add=0;
							t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
						}
					}else{
						t.scrap_timer_add=0;
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					}
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş bitirildi'});
					});
				}).catch(function (err) {
					let _arr_join=['tesellüm edilmemiş kasa bulunamadı'];
					let _f_join=false;
					for(let _i_join=0;_i_join<_arr_join.length;_i_join++){
						if(!_f_join&&err.toString().indexOf(_arr_join[_i_join])>-1){
							_f_join=true;
						}
					}
					if(!_f_join){
						t.mjd('rollback-'+_event);
						console.log(err);
						classBase.writeError(err,t.ip,'catch_workflow_employee_outofsystem|save');
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'work_lost':{
				let _promises=[];
				t.status=t.status==''?'work_lost':t.status;
				if(t.status=='work_lost'){
					let _lastproductiontime=t.processVariables.lastprodstarttime;
					t.db.sequelize.transaction(function (_tr) {
						return t.db.client_lost_details.findOne({where:{type:'l_c',client:v_client,day:_jrday,jobrotation:_jrcode,losttype:'',finish:{[Op.eq]:null}}},{transaction: _tr}).then(function(_res){
							if(_res){
								return null;
							}else{
								t.lost_recursive=false;
								_promises.push(t.create_lost_records(_tr,_lastproductiontime,{losttype:(t.show_scrap_before_finish?'URETIM SONU ISKARTA BEKLEME':''),descriptionlost:null},_lastproductiontime,t.processVariables.productions,_event));
								return Promise.all(_promises);
							}
						}).then(function(){
							if(t.show_scrap_before_finish){
								t.setStatus('URETIM SONU ISKARTA BEKLEME');
							}else{
								t.setStatus('');
							}
							return Promise.all([t.setDeviceOutputs(/*_tr*/)]).then(function(){
								return Promise.all([t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time})]);
							});
						});
					}).then(function(){
						_promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',status:t.generateClientStatus()});
						return Promise.all(_promises).then(function(){
							//t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message,status:t.generateClientStatus(),lbl_status:_lbl_status,lbl_cls:_lbl_cls,operators:t.processVariables.operators,productions:t.processVariables.productions,params:t.objParam,pins:t.securitypins,state_start:_state_start});
						});
					}).catch(function (err) {
						t.mjd('rollback-'+_event);
						console.log(err);
						t.status='';
						classBase.writeError(err,t.ip,'catch_workflow_work_lost');
						t.tmp_queue=[];
					});
				}else{
					t.mjd('work_lost-status:'+t.status);
				}
				break;
			}
			case 'work_lost|save':{
				let _promises=[];
				t.status=(t.status==''||t.status=='work_lost')?'work_lost|save':t.status;
				if(t.status=='work_lost|save'){
					let _finish_time=_time;
					let _losts=arg.data;
					let _arr_losttype=[];
					//kayıplar içinden molalar kontrol ediliyor, süresinden uzun mola var ise duruş olarak değiştiriliyor
					for(let i=0;i<_losts.length;i++){
						let item=_losts[i];
						if((item.losttype=='ÇAY MOLASI'||item.losttype=='YEMEK MOLASI'||item.losttype=='CUMA PAYDOSU')&&item.time>0){
							let _s=new timezoneJS.Date(item.start);
							let _f=item.last||item.finish==0?new timezoneJS.Date(_finish_time):new timezoneJS.Date(item.finish);
							let _diff = Math.abs(_f.getTime() - _s.getTime());
							if(Math.ceil(_diff/(1000))>item.time*60){
								let _obj=Object.assign({},item);
								if(item.last){
									_losts[i].last=false;
								}
								let _date=new timezoneJS.Date(_s.getTime()+(item.time*60000));
								_losts[i].finish=_date.toString('yyyy-MM-dd HH:mm:ss');
								_obj.start=_losts[i].finish;
								_obj.losttype=item.losttype=='ÇAY MOLASI'?'ÇAY DURUŞU':item.losttype=='YEMEK MOLASI'?'YEMEK DURUŞU':item.losttype=='CUMA PAYDOSU'?'CUMA DURUŞU':item.losttype;
								_losts.push(_obj);
							}
						}
					}
					_losts=arraySort(_losts, 'start');
					//_losts=[ { description: '', finish: '2017-05-29 08:00:00', last: false, losttype: 'ÇAY MOLASI', start: '2017-05-29 07:47:03' },
					//			{ description: '', finish: '2017-05-29 08:30:00', last: false, losttype: 'EĞİTİM', start: '2017-05-29 08:00:00' },
					//			{ description: '', finish: '2017-05-29 09:00:00', last: false, losttype: 'TOPLANTI', start: '2017-05-29 08:30:00' },
					//			{ description: '', finish: '2017-05-29 09:30:00', last: false, losttype: 'TEMİZLİK', start: '2017-05-29 09:00:00' },
					//			{ description: 'toplantı 2', finish: '2017-05-29 10:00:00', last: false, losttype: 'EĞİTİM', start: '2017-05-29 09:30:00' },
					//			{ description: '', finish: '2017-05-29 11:12:00', last: true, losttype: 'KALİTE ONAY', start: '2017-05-29 10:00:00' } ];
					t.db.sequelize.transaction(function (_tr) {
						return t.db.client_lost_details.findAll({where:{client:v_client,finish:{[Op.eq]:null}}}).then(function(_res_cld){
							if(_res_cld){
								for (let i = 0; i < _res_cld.length; i++) {
									let _cld=_res_cld[i];
									for(let j=0;j<_losts.length;j++){
										let _lost_item=_losts[j];
										let finish_time=_lost_item.last==true?_finish_time:_lost_item.finish;
										let idx=t.getIndex(_arr_losttype,'start',_lost_item.start);
										if(idx===-1){
											_arr_losttype.push({start:_lost_item.start,losttype:_lost_item.losttype});
										}
										if(j==0){
											_promises.push(t.updateRecord(_tr,'client_lost_details',_time,{record_id:_cld.record_id},{losttype:_lost_item.losttype,descriptionlost:_lost_item.description,finish:finish_time,faultdevice:_lost_item.faultdevice}));
										}else{
											_promises.push(t.copyAllRecords(_tr,'client_lost_details',_time,{record_id:_cld.record_id},{losttype:_lost_item.losttype,descriptionlost:_lost_item.description,finish:finish_time,start:_lost_item.start,faultdevice:_lost_item.faultdevice},{},false));
										}
									}
								}
								return Promise.all(_promises);
							}
							return null; 
						}).then(function(){
							let _cpd_time=_time;
							_promises=[];
							let arr_tmp=[];
							for(let i=0;i<t.processVariables.productions.length;i++){
								let row=t.processVariables.productions[i];
								if(arr_tmp.indexOf(row.tasklist)==-1){
									arr_tmp.push(row.tasklist);
								}
							}
							let _obj_where_cpd={
								type:'c_p'//{[Op.in]: ['c_p','e_p']}
								,client:v_client
								,tasklist:{[Op.in]:arr_tmp}
							};
							return t.db.client_production_details.findAll({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where_cpd,order: [['time','DESC']],limit: 1}).then(function(res_cpd){
								for (let i = 0; i < res_cpd.length; i++) {
									let result=res_cpd[i];
									if(t.convertFullLocalDateString(result.time)<_cpd_time){
										_cpd_time=t.convertFullLocalDateString(result.time);
									}
								}
								t.setStatus('-');
								let _obj_upd_client={status:t.generateClientStatus(),statustime:_time,lastprodstarttime:_finish_time};
								t.processVariables.lastprodstarttime=_finish_time;
								return Promise.all([t.updateRecord(_tr,'clients',_time,{code:v_client},_obj_upd_client)]); 
							});
						}).then(function(){
							if(t.objParam.task_materialpreparation.value===true){
								if(t.timer_missing_material!==false){
									clearTimeout(t.timer_missing_material);
									t.timer_missing_material=false;
								}
								t.timer_missing_material=setTimeout(() => {
									t.control_missing_materials('waitmaterial',13);
								}, 10000);
							}
							return null;
						});
					}).then(function(){
						_promises=[];
						let update_cld_losttype=function(_start,_losttype){
							let _prom=[];
							return t.db.client_lost_details.findAll({where:{client:v_client,start:_start,[Op.or]:[{losttype:{[Op.eq]:null}},{losttype:''}]}}).then(function(_res_cld){
								if(_res_cld){
									for (let i = 0; i < _res_cld.length; i++) {
										let _cld=_res_cld[i];
										_prom.push(t.updateRecord(null,'client_lost_details',_time,{record_id:_cld.record_id},{losttype:_losttype}));
									}
									return Promise.all(_prom); 
								}
								return null;
							});
						};
						for(let i=0;i<_arr_losttype.length;i++){
							_promises.push(update_cld_losttype(_arr_losttype[i].start,_arr_losttype[i].losttype));
						}
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
						t.status='';
						return Promise.all(_promises).then(function(){
							_promises=[];
							while(t.tmp_queue.length>0){
								_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
							}
							return Promise.all(_promises);
						});				
					}).catch(function (err) {
						t.mjd('rollback-'+_event);
						console.log(err);
						t.status='';
						classBase.writeError(err,t.ip,'catch_workflow_work_lost|save');
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						t.tmp_queue=[];
					});
				}else{
					t.status='';
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:'Lütfen tekrar deneyiniz.'});
				}
				break;
			}
			case 'operationAuthorization_supervision':{
				let _promises=[];
				let _lastproductiontime=t.processVariables.lastprodstarttime;
				t.processVariables.lastprodstarttime=null;//cihazda work_lost tetiklenmesin diye
				if(_lastproductiontime===null){
					_lastproductiontime=_time;
				}
				t.setStatus('YETKİNLİK GÖZLEM');
				t.db.sequelize.transaction(function (_tr) {
					t.lost_recursive=false;
					_promises.push(t.create_lost_records(_tr,_lastproductiontime,{losttype:t.processVariables.status_lost,descriptionlost:null},_lastproductiontime,t.processVariables.productions,_event));
					return Promise.all(_promises).then(function(){
						return Promise.all([t.setDeviceOutputs(/*_tr*/)]).then(function(){
							return Promise.all([t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time})]);
						});
					});
				}).then(function(){
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',status:t.generateClientStatus(),title:'Gözetimli Üretim Operasyon(lar):'+arg.opnames});
					return Promise.all(_promises).then(function(){
						//t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message,status:t.generateClientStatus(),lbl_status:_lbl_status,lbl_cls:_lbl_cls,operators:t.processVariables.operators,productions:t.processVariables.productions,params:t.objParam,pins:t.securitypins,state_start:_state_start});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.status='';
					classBase.writeError(err,t.ip,'catch_workflow_work_lost');
					t.tmp_queue=[];
				});
				break;
			}
			case 'operation_authorization|supervision':{
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					let saverecord=function(_tr,_data){
						return t.db._sync_messages.create(_data, {transaction: _tr}).then(()=>{
							return null;
						});
					}; 
					for(let i=0;i<t.json_status.operationAuthorization.data.length;i++){
						let row=t.json_status.operationAuthorization.data[i];
						if(row.level==1){
							let mes=new dcs_message({
								type:'operation_authorization_device_log'
								,ack:true
								,data:{
									time:_time
									,employee:row.employee
									,client:v_client
									,opname:row.opname
									,authemployee:arg.authemployee
									,type:'Üretim Gözetimi'
								}
							});
							_promises.push(saverecord(_tr,mes.getdata()));
						}
					}
					_promises.push(t.cjr_client_lost_details(null,{client:v_client,start:{[Op.ne]:null},finish:{[Op.eq]:null},losttype:'YETKİNLİK GÖZLEM'},_time));
					t.processVariables.lastprodstarttime=_time;
					t.setStatus('-');
					_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time}));
					let _d=new timezoneJS.Date(t.tickTimer.obj_time.strDateTime);
					_d.setMinutes(_d.getMinutes()+((t.ip==='192.168.1.40'||t.ip==='172.16.1.146')?5:(v_client.split('03.08').length>1?120:60)));
					t.json_status.operationAuthorization.nextControl=_d.toString();
					try {
						fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
					} catch (error) {
						Sentry.captureException(error);
					}
					return Promise.all(_promises);	
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_operation_authorization|supervision');
					t.tmp_queue=[];
					return Promise.all([t.getClientStatus()]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'employee_outofsystem|save':{
				let _promises=[];
				let _finish_time=_time;
				let _losts=arg.data;
				if(arg.employee==false){
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:'Operatör bilgisi alınamadı. Lütfen tekrar deneyiniz.'});
					return;
				}
				//kayıplar içinden molalar kontrol ediliyor, süresinden uzun mola var ise duruş olarak değiştiriliyor
				for(let i=0;i<_losts.length;i++){
					let item=_losts[i];
					if((item.losttype=='ÇAY MOLASI'||item.losttype=='YEMEK MOLASI'||item.losttype=='CUMA PAYDOSU')&&item.time>0){
						let _s=new timezoneJS.Date(item.start);
						let _f=item.last||item.finish==0?new timezoneJS.Date(_finish_time):new timezoneJS.Date(item.finish);
						let _diff = Math.abs(_f.getTime() - _s.getTime());
						if(Math.ceil(_diff/(60000))>item.time){
							let _obj=Object.assign({},item);
							if(item.last){
								_losts[i].last=false;
							}
							let _date=new timezoneJS.Date(_s.getTime()+(item.time*60000));
							_losts[i].finish=_date.toString('yyyy-MM-dd HH:mm:ss');
							_obj.start=_losts[i].finish;
							_obj.losttype=item.losttype=='ÇAY MOLASI'?'ÇAY DURUŞU':item.losttype=='YEMEK MOLASI'?'YEMEK DURUŞU':item.losttype=='CUMA PAYDOSU'?'CUMA DURUŞU':item.losttype;
							_losts.push(_obj);
						}
					}
				}
				_losts=arraySort(_losts, 'start');
				let _join_employeeLabel=arg.employeeLabel;
				t.db.sequelize.transaction(function (_tr) {
					for(let j=0;j<_losts.length;j++){
						let _lost_item=_losts[j];
						let finish_time=_lost_item.last==true?_time:_lost_item.finish;
						let _tmp_data_le={
							type: 'l_e'
							,client: v_client
							,day: _jrday
							,jobrotation: _jrcode
							,losttype: _lost_item.losttype
							,descriptionlost: _lost_item.description
							,employee: arg.employee
							,start: _lost_item.start
							,finish: finish_time
							,record_id: uuidv4()
							,energy: null
						};
						_promises.push(t.createRecord(_tr,'client_lost_details',_time,_tmp_data_le));
					}
					if(v_workflow!=='El İşçiliği'&&!((t.objConfig.dcs_server_IP.value==='10.0.0.101'||t.objConfig.dcs_server_IP.value==='192.168.1.40')&&t.processVariables.status_lost==='İŞ YOK')){
						_promises.push(t.employeeJoin(_tr,{code:arg.employee},_time,_join_employeeLabel,false,arg.authemployee));
					}else{
						_promises.push(t.updateRecord(_tr,'employees',t.processVariables._time,{code:arg.employee},{strlastseen:t.tickTimer.obj_time.strDateTime}));
					}
					return Promise.all(_promises);
				}).then(function(){
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					});				
				}).catch(function (err) {
					let _arr_join=['sicil bulunamadı','Operatör bulunamadı','Operatör işten ayrılmış','ayrılmalısınız','outofsystem','çalışmak için yetkinliğiniz','yetkinliğiniz'];
					let _f_join=false;
					for(let _i_join=0;_i_join<_arr_join.length;_i_join++){
						if(!_f_join&&err.toString().indexOf(_arr_join[_i_join])>-1){
							_f_join=true;
						}
					}
					if(!_f_join){
						t.mjd('rollback-'+_event);
						console.log(err);
						classBase.writeError(err,t.ip,'catch_workflow_employee_outofsystem|save');
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
				});
				break;
			}
			case 'screen_kanban|show':{
				if(!t.screen_kanban){
					let onError_ud = function(e) {
						console.log('Error_screen_kanban');
						console.log(e.stack);
						if(!this.killed){
							onDisconnect_ud();
						}
					};
					let onDisconnect_ud = function(/*e*/) {
						console.log('kill-onDisconnect_screen_kanban');
						if(this&&typeof this.kill==='function'){
							this.kill();
						}
						delete t.screen_kanban;
					};
					t.screen_kanban=cp.fork(path.join(__dirname, 'main_kanban.js'), [v_client]);
					t.screen_kanban.on('error',onError_ud);
					t.screen_kanban.on('disconnect',onDisconnect_ud);
				}
				break;
			}
			case 'kanban_operations|get':{
				let _promises=[];
				let _kanban_products=[];
				t.db.sequelize.transaction(function (/*_tr*/) {
					_promises.push(t.findAll('kanban_operations',false,{client:(arg.client?arg.client:v_client)},[ ['product', 'ASC'] ],false));
					return Promise.all(_promises).then(function(results){
						_kanban_products=t.getLastElementOfArray(results);
						return null;
					});
				}).then(function(){
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',kanban_operations:_kanban_products,serverKanban:arg.serverKanban});
					return Promise.all(_promises);
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_kanban_operations|get');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
				});
				break;
			}
			case 'req_watch_info':{
				let mes=new dcs_message({
					type:'watch_info'
					,ack:true
					,data:{
						client:v_client
						,message:''
						,status:t.generateClientStatus()
						,lbl_status:t._lbl_status
						,pins:t.securitypins
						,state_start:t._state_start
						,processVariables:t.processVariables
					}
				});
				return t.db._sync_messages.create(mes.getdata()).then(function(){
					mes=undefined;
				});
			}
			case 'show-scrap-before-finish':{
				//üretim bitimi öncesinde ıskarta giriş ekranının gösterilmesi
				//ıskarta penceresini göster
				//ıskarta kaydı sonrasında biten üretim dışındakiler ile devam et ya da dur
				//cihazın tüm çıkışlarını aktif et
				t.scrap_timer_add=90;
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					t.setStatus('URETIM SONU ISKARTA BEKLEME');
					t.processVariables.lastprodstarttime=null;
					_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),lastprodstarttime:t.processVariables.lastprodstarttime}));
					t.lost_recursive=false;
					_promises.push(t.create_lost_records(_tr,_time,{losttype:'URETIM SONU ISKARTA BEKLEME',descriptionlost:null},_time,t.processVariables.productions,_event));
					return Promise.all(_promises);
				}).then(function () {
					t.show_scrap_before_finish=true;
					t.setAllOutputs();
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_show_scrap_before_finish');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'control_delivery_package_info':{
				if(t.timer_delivery_control===false){
					try {
						if(t.self&&t.self.socket_io_client){
							if((arg.data!==null&&arg.data!=='null'&&arg.data!=='')||(t.objConfig.dcs_server_IP.value==='10.10.0.10')){
								t.self.socket_io_client.send({event:'cn_sn',data:{event:'control_delivery_package_info',barcode:arg.data,erprefnumber:arg.selectedRow.erprefnumber,client:v_client}});
								t.timer_delivery_control=setTimeout(() => {
									//timeout süresi içinde yanıt gelmedi
									t.timer_delivery_control=false;
									t.winmessend('cn_js_workflow',{
										event: 'control_delivery_package_info',
										result: 'err',
										message: 'Zaman aşımı oluştu'
									});
								}, 10000);
							}else{
								t.winmessend('cn_js_workflow',{
									event: 'control_delivery_package_info',
									result: 'err',
									message: 'Tekrar deneyiniz-1'
								});
							}
						}else{
							t.winmessend('cn_js_workflow',{
								event: 'control_delivery_package_info',
								result: 'err',
								message: 'Tekrar deneyiniz-2'
							});
						}
					} catch (error) {
						Sentry.captureException(error);
						t.winmessend('cn_js_workflow',{
							event: 'control_delivery_package_info',
							result: 'err',
							message: 'Tekrar deneyiniz-3'
						});
					}
				}
				break;
			}
			case 'res_control_delivery_package_info':{
				if(t.timer_delivery_control!=false){
					clearTimeout(t.timer_delivery_control);
					t.timer_delivery_control=false;
					arg.event='control_delivery_package_info';
					t.winmessend('cn_js_workflow',arg);
				}
				break;
			}
			case 'refresh_delivery_package_info':{
				if(t.timer_delivery_control===false){
					try {
						if(t.self&&t.self.socket_io_client){
							if((arg.data!==null&&arg.data!=='null'&&arg.data!=='')||(t.objConfig.dcs_server_IP.value==='10.10.0.10')){
								t.self.socket_io_client.send({event:'cn_sn',data:{event:'refresh_delivery_package_info',barcode:arg.data,erprefnumbers:arg.erprefnumbers,client:v_client}});
								t.timer_delivery_control=setTimeout(() => {
									//timeout süresi içinde yanıt gelmedi
									t.timer_delivery_control=false;
									t.winmessend('cn_js_workflow',{
										event: 'refresh_delivery_package_info',
										result: 'err',
										message: 'Zaman aşımı oluştu'
									});
								}, 10000);
							}else{
								t.winmessend('cn_js_workflow',{
									event: 'refresh_delivery_package_info',
									result: 'err',
									message: 'Tekrar deneyiniz-1'
								});
							}
						}else{
							t.winmessend('cn_js_workflow',{
								event: 'refresh_delivery_package_info',
								result: 'err',
								message: 'Tekrar deneyiniz-2'
							});
						}
					} catch (error) {
						Sentry.captureException(error);
						t.winmessend('cn_js_workflow',{
							event: 'refresh_delivery_package_info',
							result: 'err',
							message: 'Tekrar deneyiniz-3'
						});
					}
				}
				break;
			}
			case 'res_refresh_delivery_package_info':{
				if(t.timer_delivery_control!=false){
					clearTimeout(t.timer_delivery_control);
					t.timer_delivery_control=false;
					//gelen kasaları yaz
					let _promises=[];
					let _tmp_tc=[];
					t.db.sequelize.transaction(function (_tr) {
						if(arg.data){
							for(let i=0;i<arg.data.length;i++){
								let row=arg.data[i];
								let _idx=t.getIndex(t.processVariables.productions,'erprefnumber|delivery',row.erprefnumber+'|'+true);
								let _p=null;
								if(_idx>-1){
									_p=t.processVariables.productions[_idx];
									let _tmp_data_tc={
										client:v_client
										,movementdetail:null
										,erprefnumber:row.erprefnumber
										,casetype:'O'
										,opcode:(_p.opcode!==null?_p.opcode:null)
										,opnumber:(_p.opnumber!==null?_p.opnumber:null)
										,opname:(_p.opname!==null?_p.opname:null)
										,quantityused:(_p!==null? (_p.leafmaskcurrent==null?1:parseFloat(_p.leafmaskcurrent)) :null) 
										,casename:(_p!==null? (_p.casename==null?(row.casename?row.casename:''):_p.casename) :null)
										,casenumber:row.casenumber
										,caselot:row.caselot
										,packcapacity:row.packcapacity
										,preptime:(_p.preptime!==null?_p.preptime:null)
										,quantityremaining:0
										,quantitydeliver:0
										,conveyor:0
										,status:'WAIT'
										,start:_time
										,record_id:uuidv4()
									};
									if(t.getIndex(_tmp_tc,'casenumber',row.casenumber)==-1){
										_tmp_tc.push(_tmp_data_tc);
									}
								}
							}
							let tc_create=function(_tr,_time,item){
								return t.db.task_cases.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{casenumber:item.casenumber,erprefnumber:item.erprefnumber
									,status:{[Op.notIn]:['CLOSE','CLOSED','CANCELED','WAIT_FOR_DELIVERY','WAIT_FOR_INFO_SEND','DELETED','SYSTEMCLOSE','LABELDELETED']}
								} }, {transaction: _tr}).then(function(result){
									if(result){
										return null;
									}else{
										return Promise.all([t.createRecord(_tr,'task_cases',_time,item)]);
									}
								});
							};
							for(let i=0;i<_tmp_tc.length;i++){
								_promises.push(tc_create(_tr,_time,_tmp_tc[i]));
							}
							return Promise.all(_promises);
						}
						return;
					}).then(function () {
						_promises=[];
						if(_tmp_tc.length>0){
							let tc_kontrol=function(_tr,objwhere){
								return t.db.task_cases.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:objwhere}, {transaction: _tr}).then(function(result){
									if(result){
										return null;
									}else{
										objwhere.status='WAIT';
										return t.db.task_cases.findOne({where:objwhere,order: [['casenumber','ASC']]},{transaction: _tr}).then(function(result){
											if(result){
												return Promise.all([t.updateRecord(_tr,'task_cases',_time,{record_id:result.record_id},{status:'WORK'})]);
											}
											return null;
										});
									}
								});
							};
							for(let i=0;i<t.processVariables.productions.length;i++){
								if(t.processVariables.productions[i].delivery===true){
									let _where_tc={
										movementdetail:{[Op.eq]:null}
										,casetype:'O'
										,erprefnumber:t.processVariables.productions[i].erprefnumber
										,opname:t.processVariables.productions[i].opname
										,start:{[Op.ne]:null}
										,finish:{[Op.eq]:null}
										,status:'WORK'
									};
									_promises.push(tc_kontrol(null,_where_tc));
								}
							}
							//return Promise.all(_promises);
						}
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						arg.event='refresh_delivery_package_info';
						t.winmessend('cn_js_workflow',arg);
						return Promise.all(_promises).then(function(){
							//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess});
						});
					}).catch(function (err) {
						t.mjd('rollback-'+_event);
						console.log(err);
						t.tmp_queue=[];
						return Promise.all([t.getDeviceInitValues('catch-res_refresh_delivery_package_info')]).then(function(){
							t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
						});
					});
				}
				break;
			}
			case 'task_cases|add':{
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					let tmp_fn_create=function(_tr,_time,_erprefnumber,_data){
						let _tmp={
							movementdetail:1
							,client:v_client
							,erprefnumber:_erprefnumber
							,casetype:(_data.materialtype=='K'?'O':'I')
							,opcode:_data.code
							,opnumber:_data.number
							,opname:_data.name
							,stockcode:(_data.materialtype=='K'?_data.stockname:_data.stockcode)
							,stockname:(_data.materialtype=='K'?_data.stockname:_data.stockname)
							,quantityused:(_data.quantity!==null?_data.quantity:1)
							,casename:_data.pack
							,packcapacity:parseFloat(_data.packcapacity)
							,feeder:_data.feeder
							,preptime:_data.preptime
							,quantityremaining:(_data.materialtype=='K'?0:parseFloat(_data.packcapacity))
							,quantitydeliver:parseFloat(0)
							,conveyor:0
							,goodsplanner:_data.goodsplanner
							,record_id:uuidv4()
							,status:'WORK'
							,start:_time
							,finish:null
						};
						return Promise.all([t.createRecord(_tr,'task_cases',_time,_tmp)]);
					};
					if(arg.data.id>0){//listeden seçilmiş hammadde
						return Promise.all([tmp_fn_create(_tr,_time,arg.erprefnumber,arg.data)]);
					}
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					if(t.objParam.task_materialpreparation.value===true){
						if(t.timer_missing_material!==false){
							clearTimeout(t.timer_missing_material);
							t.timer_missing_material=false;
						}
						t.timer_missing_material=setTimeout(() => {
							t.control_missing_materials('waitmaterial',14);
						}, 10000);
					}
					_promises.push(t.eventListener({event:'task_cases|get|mp',erprefnumber:arg.erprefnumber,opname:arg.data.name}));
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess});
					});	
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					t.tmp_queue=[];
					return Promise.all([t.getDeviceInitValues('catch-task_cases|add')]).then(function(){
						t.eventListener({event:'task_cases|get|mp'});
					});
				});
				break;
			}
			case 'task_cases|get':{
				let _promises=[];
				let _w=' and start>\'2018-10-20\' ';
				let _task_cases=[];
				t.db.sequelize.transaction(function (_tr) {
					return t.db.sequelize.query('SELECT record_id,erprefnumber,opcode,opname,casename,casenumber,cast(packcapacity as int)packcapacity,cast(quantityremaining as int)quantityremaining,caselot '+
					' from task_cases '+
					' where movementdetail is null and casetype=\'O\' and start is not null and status in (\'WORK\',\'WAIT_FOR_DELIVERY\') and client=:client '+ _w +
					' order by erprefnumber,start,id limit 1000 '
					, {  replacements:{client:v_client},type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
						_task_cases=(results&&results.length>0?results:[]);
						//if(t.bool_control_device_delivery){
						let _f=false;
						for(let i=0;i<_task_cases.length;i++){
							let _c=_task_cases[i];
							if(!_f){
								if(_c.packcapacity==_c.quantityremaining){
									_f=true;
								}
							}
						}
						t.bool_control_device_delivery=_f;
						//}			
						return null;
					}).then(function(){
						if(_task_cases.length===0&&t.processVariables.productions.length>0){
							let _erprefs=[];
							for(let i=0;i<t.processVariables.productions.length;i++){
								let item=t.processVariables.productions[i];
								if(_erprefs.indexOf(item.erprefnumber)===-1&&item.delivery===true){
									_erprefs.push(item.erprefnumber);
								}
							}
							if(_erprefs.length>0){
								return t.db.sequelize.query('select * '+
								' from task_cases '+
								' where movementdetail is null and casetype=\'O\' and start is not null and status in (\'WAIT\') and client=:client and erprefnumber in (:erprefnumber) '+
								' order by start,casenumber ', {  replacements:{client:v_client,erprefnumber:_erprefs},type: t.db.sequelize.QueryTypes.SELECT})
									.then(function(results_tc) {
										let _promises=[];
										let erpref='';
										let update_tc=function(_tr,v_idx){
											let _prom=[];
											_prom.push(t.updateRecord(_tr,'task_cases',_time,{record_id:v_idx},{status:'WORK'}));
											return Promise.all(_prom);
										};
										for(let i=0;i<results_tc.length;i++){
											let item=results_tc[i];
											if(erpref!==item.erprefnumber){
												erpref=item.erprefnumber;
												_promises.push(update_tc(_tr,item.record_id));
												_task_cases.push(item);
											}
										}
										return Promise.all(_promises);
									});
							}else{
								return null;
							}
						}
						return null;
					});
				}).then(function () {
					t.show_delivery=true;
					if(t.bool_control_device_delivery/*&&t.ip.indexOf('172.16')>-1*/){
						t.setAllOutputs();
					}
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_cases:_task_cases});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_cases|get');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'task_cases|get|tcc':{
				let _promises=[];
				let _task_cases=[];
				if(!arg.erprefnumber){
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_cases:_task_cases});
					return;
				}
				t.db.sequelize.transaction(function (/*_tr*/) {
					return t.db.sequelize.query('SELECT record_id,erprefnumber,opcode,opname,casename,casenumber,cast(packcapacity as int)packcapacity '+
					'		,cast(quantityremaining as int)quantityremaining,caselot '+
					'		,case when casetype=\'I\' then \'Hammadde\' else \'Çıktı\' end kasatipi,coalesce(stockname,stockcode)stockname '+
					' 	,\'none\' packcapacity__type '+
					' 	,\'none\' quantityremaining__type '+
					' from task_cases '+
					' where movementdetail is null and finish is null and status in (\'TCC\') '+
					' and casetype=\'I\' and quantityremaining>0 and client=:client and erprefnumber=:erprefnumber and opcode=:opcode '+ //and opname=:opname 2021-11-30 kaldırıldı ,opname:arg.opname
					' order by erprefnumber,casetype,stockname,start,id limit 1000 '
					, {  replacements:{client:v_client,erprefnumber:arg.erprefnumber,opcode:arg.opcode},type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
						_task_cases=(results&&results.length>0?results:[]);
						return null;
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_cases:_task_cases});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_cases|get|mp');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'task_cases|delete|tcc':{
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					let _obj_update={
						finish:_time
						,finishtype:'Cihaz Üzerinden Müdahale'
					};
					_promises.push(t.updateRecord(_tr,'task_cases',_time,{record_id:arg.data.record_id},_obj_update));
					return Promise.all(_promises);
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					//if(t.objParam.task_materialpreparation.value===true){
					//	if(t.timer_missing_material!==false){
					//		clearTimeout(t.timer_missing_material);
					//		t.timer_missing_material=false;
					//	}
					//	t.timer_missing_material=setTimeout(() => {
					//		t.control_missing_materials('waitmaterial',15);
					//	}, 5000);
					//}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_cases|delete|tcc');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'task_cases|get|mp':{
				let _promises=[];
				let _task_cases=[];
				if(!arg.erprefnumber){
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_cases:_task_cases});
					return;
				}
				t.db.sequelize.transaction(function (/*_tr*/) {
					//2021-12-09 çıktı kasaları sadece görüntüleme amaçlı gelsin istendi
					//2020-10-26 serkan tan talebi ile tipi O olanların gösterimi kaldırıldı
					//' and ((casetype=\'I\' and quantityremaining>0) or (casetype=\'O\' and (cast(packcapacity as int)-cast(quantityremaining as int))>0)) and client=:client and opname=:opname and erprefnumber=:erprefnumber and start>\'2020-01-01\' '+
					//2020-02-20 serkan tan talebi ile çıktı kasalarının da sadece kasa içi miktarına müdahalesi istendi
					//' 	,case casetype when \'O\' then \'text\' else \'none\' end packcapacity__type '+
					//' 	,case casetype when \'I\' then \'text\' else \'none\' end quantityremaining__type '+
					return t.db.sequelize.query('SELECT record_id,erprefnumber,opcode,opname,casename,casenumber,cast(packcapacity as int)packcapacity '+
					'		,cast(quantityremaining as int)quantityremaining,caselot,cast(cast(start as time) as varchar(5)) saat '+
					'		,case when casetype=\'I\' then \'Hammadde\' else \'Çıktı\' end kasatipi,coalesce(stockname,stockcode)stockname '+
					' 	,\'none\' packcapacity__type '+
					' 	,case casetype when \'I\' then \'text\' else \'none\' end quantityremaining__type '+
					' from task_cases '+
					' where movementdetail is not null and finish is null and status in (\'WORK\',\'WAIT\') '+
					' and ((casetype=\'I\' and quantityremaining>0) or (casetype=\'O\' and quantityremaining<packcapacity)) and client=:client and opname=:opname and erprefnumber=:erprefnumber and start>\'2020-01-01\' '+
					' order by erprefnumber,casetype,stockname,start,id limit 1000 '
					, {  replacements:{client:v_client,opname:arg.opname,erprefnumber:arg.erprefnumber},type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
						_task_cases=(results&&results.length>0?results:[]);
						return null;
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_cases:_task_cases});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_cases|get|mp');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'task_cases|update|mp':{
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					for(let i=0;i<arg.data.length;i++){
						let row=arg.data[i];
						if(row.quantityremaining==null||row.quantityremaining==''){
							row.quantityremaining=0;
						}else{
							row.quantityremaining=parseInt(row.quantityremaining);
						}
						if(row.kasatipi=='Hammadde'){
							row.quantityremaining=(row.quantityremaining>row.packcapacity?row.packcapacity:row.quantityremaining);
						}else{
							row.quantityremaining=(row.quantityremaining>row.packcapacity?row.packcapacity:row.quantityremaining);
						}
						let _obj_update={
							packcapacity:(parseInt(row.packcapacity)>=0?parseInt(row.packcapacity):0)
							,quantityremaining:((row.quantityremaining==null||row.quantityremaining=='')?0:row.quantityremaining)
						};
						if(row.kasatipi=='Hammadde'){
							if(row.quantityremaining==0){
								_obj_update.status='WAIT_FOR_DELIVERY';
								_obj_update.finish=_time;
								_obj_update.finishtype='Cihaz Üzerinden Müdahale';
							}
						}else{
							if(row.quantityremaining==row.packcapacity){
								_obj_update.status='WAIT_FOR_DELIVERY';
								_obj_update.finish=_time;
								_obj_update.finishtype='Cihaz Üzerinden Müdahale';
							}
						}
						_promises.push(t.updateRecord(_tr,'task_cases',_time,{record_id:row.record_id},_obj_update));
					}
					return Promise.all(_promises);
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					if(t.objParam.task_materialpreparation.value===true){
						if(t.timer_missing_material!==false){
							clearTimeout(t.timer_missing_material);
							t.timer_missing_material=false;
						}
						t.timer_missing_material=setTimeout(() => {
							t.control_missing_materials('waitmaterial',15);
						}, 5000);
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_cases|update|mp');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'task_cases|updatecasenumber':{
				let _obj_update_tc={
					movementdetail:null
					,casenumber:arg.data.casenumber
					,status:'WORK'
				};
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					_promises.push(t.updateRecord(_tr,'task_cases',_time,{record_id:arg.data.record_id},_obj_update_tc));
					return Promise.all(_promises);
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_cases|updatecasenumber');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'task_cases|update':{
				let _obj_update_tc={
					movementdetail:null
					,status:(arg.retouch?'WAIT_FOR_INFO_SEND_RETOUCH':'WAIT_FOR_INFO_SEND')
					,deliverytime:_time
				};
				if(arg.data){
					if(parseFloat(arg.data.packcapacity)!==parseFloat(arg.data.quantityremaining)){
						_obj_update_tc.finish=_time;
						_obj_update_tc.finishtype='Eksik Tesellüm';
					}
				}
				//console.log(arg.data);
				let _casenumber=(arg.data?arg.data.casenumber:'');
				let _idx=-1;
				let _promises=[];
				let _item_finished=[];
				t.db.sequelize.transaction(function (_tr) {
					if(arg.data){
						_promises.push(t.updateRecord(_tr,'task_cases',_time,{casenumber:_casenumber,record_id:arg.data.record_id},_obj_update_tc));
						//console.log('casenumber:'+_casenumber);
						//console.log(_obj_update_tc);
						//2020-07-21 task_cases status alanı değişikliği
						let _update_tc_status=function(_tr,_obj_where_){
							return t.db.task_cases.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:_obj_where_,order: [['casenumber','ASC']] }, {transaction: _tr})
							.then(function(_result_tc_){
								if(_result_tc_){
									return Promise.all([t.updateRecord(_tr,'task_cases',_time,{record_id:_result_tc_.record_id},{status:'WORK'})]);
								}
								Sentry.addBreadcrumb({
									category: '_update_tc_status',
									message: '_update_tc_status-not find task case record',
									level: 'info',
									data:JSON.parse(JSON.stringify(_obj_where_))
								});
								return null;
							});
						};
						let ___obj_where___={
							movementdetail:{[Op.eq]:null}
							,casetype:'O'
							,erprefnumber:arg.data.erprefnumber
							,opname:arg.data.opname
							,start:{[Op.ne]:null}
							,finish:{[Op.eq]:null}
							,status:'WAIT'
							,casenumber:{[Op.gt]:_casenumber}
						};
						_promises.push(_update_tc_status(_tr,___obj_where___));
						return Promise.all(_promises).then(function(){
							_promises=[];
							// if(_idx>-1){
								t.processVariables.lastprodstarttime=_time;
								if(t.objParam.task_delivery_half_case_completion.value===true){
									let update_tl=function(_tr,v_idx,_fark){
										//üretimde olan operasyonun üretim adedinin tesellüm edilmiş kasa ile oranlanarak üste tamamlanması
										let item_prod=t.processVariables.productions[v_idx];
										if(_fark>0){
											if(item_prod.productdonecount+_fark==item_prod.productcount){
												_item_finished.push(item_prod);
											}
											return t.db.task_lists.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{code: item_prod.tasklist} }, {transaction: _tr}).then(function(result_tl){
												let _prom=[];
												//taskcase güncelleme-malzeme hazırlık
												if(t.objParam.task_materialpreparation.value===true){
													_prom.push(t.update_taskcase_record_add_mh(_tr,_time,item_prod.erprefnumber,item_prod.opname,_fark,'DELIVERY'));
												}
												if(result_tl){
													t.processVariables.productions[v_idx].productdonecount+=_fark;
													t.processVariables.productions[v_idx].productdoneactivity+=_fark;
													t.processVariables.productions[v_idx].productdonejobrotation+=_fark;
													t.processVariables.productions[v_idx].remaining-=_fark;
													_prom.push(t.updateRecord(_tr,'task_lists',_time,{code:result_tl.code},{productdoneactivity:(result_tl.productdoneactivity+_fark),productdonejobrotation:(result_tl.productdonejobrotation+_fark),productdonecount:(result_tl.productdonecount+_fark)}));
												}
												return Promise.all(_prom);
											});
										}else{
											return null;
										}
									};
									for(let i=0;i<t.processVariables.productions.length;i++){
										let _prod=t.processVariables.productions[i];
										if(_prod.erprefnumber===arg.data.erprefnumber){
											_promises.push(update_tl(_tr,i,parseFloat(arg.data.packcapacity)-parseFloat(arg.data.quantityremaining)));
										}
									}
									return Promise.all(_promises);
								}else{
									return null;
								}
							// }else{
							// 	//iş bitmiş
							// 	//şimdilik iş bilgisi güncelleme yapılmayacak
							// 	return null;
							// }
						});
					}
					return null;
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					if(_item_finished.length>0){
						if(v_workflow==='Üretim Aparatsız'){
							t.eventListener({event:'device-work-finish',taskfinishtype:'GÖREV BİTTİ ERKEN TESELLUM',recreate:0,taskfinishdescription:'İş emri miktarına gelmeden tesellüm edildi.',item:_item_finished,finish_from_delivery:true});
						}
						if(v_workflow==='Değişken Üretim Aparatlı'){
							//@@todo:pres için birden fazla çıktı ve farklı üretim maskeli kalıplarda (0001,0100) her ikisi bittiğinde event tetiklenmeli
							t.eventListener({event:'device-work-finish',taskfinishtype:'GÖREV BİTTİ ERKEN TESELLUM',recreate:0,taskfinishdescription:'İş emri miktarına gelmeden tesellüm edildi.',item:_item_finished,finish_from_delivery:true});
						}
						if(v_workflow==='Sabit Üretim Aparatlı'){
							t.eventListener({event:'device-work-finish-multi',taskfinishtype:'GÖREV BİTTİ ERKEN TESELLUM',recreate:0,taskfinishdescription:'İş emri miktarına gelmeden tesellüm edildi.',data:_item_finished,finish_from_delivery:true});
						}
					}else{
						//idx>-1 nerede atanıyor belli değil
						if(_idx>-1&&t.objParam.task_materialpreparation.value===true){
							t.eventListener({event:'input_received',items:[t.processVariables.productions[_idx]]});
						}else{
							t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
						}
					}
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_cases|update');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'task_cases|add_delivery':{
				// tesellüm ekranına kasa miktarını arttırma butonu ekle, arttırınca malzeme kasaları da etkilensin
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					if(arg.data){
						let _casenumber=(arg.data?arg.data.casenumber:'');
						let _obj_update_tc={
							quantityremaining:arg.data.quantityremaining+arg.addProduction
						};
						_promises.push(t.updateRecord(_tr,'task_cases',_time,{casenumber:_casenumber,record_id:arg.data.record_id},_obj_update_tc));
						return Promise.all(_promises).then(function(){
							_promises=[];
							// if(_idx>-1){
								t.processVariables.lastprodstarttime=_time;
								let update_tl=function(_tr,v_idx,_fark){
									//üretimde olan operasyonun üretim adedinin tesellüm edilmiş kasa ile oranlanarak üste tamamlanması
									let item_prod=t.processVariables.productions[v_idx];
									if(_fark>0){
										return t.db.task_lists.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{code: item_prod.tasklist} }, {transaction: _tr}).then(function(result_tl){
											let _prom=[];
											//taskcase güncelleme-malzeme hazırlık
											if(t.objParam.task_materialpreparation.value===true){
												_prom.push(t.update_taskcase_record_add_mh(_tr,_time,item_prod.erprefnumber,item_prod.opname,_fark,'DELIVERY'));
											}
											if(result_tl){
												t.processVariables.productions[v_idx].productdonecount+=_fark;
												t.processVariables.productions[v_idx].productdoneactivity+=_fark;
												t.processVariables.productions[v_idx].productdonejobrotation+=_fark;
												t.processVariables.productions[v_idx].remaining-=_fark;
												_prom.push(t.updateRecord(_tr,'task_lists',_time,{code:result_tl.code},{productdoneactivity:(result_tl.productdoneactivity+_fark),productdonejobrotation:(result_tl.productdonejobrotation+_fark),productdonecount:(result_tl.productdonecount+_fark)}));
											}
											return Promise.all(_prom);
										});
									}else{
										return null;
									}
								};
								for(let i=0;i<t.processVariables.productions.length;i++){
									let _prod=t.processVariables.productions[i];
									if(_prod.erprefnumber===arg.data.erprefnumber){
										_promises.push(update_tl(_tr,i,arg.addProduction));
									}
								}
								return Promise.all(_promises);
							// }else{
							// 	//iş bitmiş
							// 	//şimdilik iş bilgisi güncelleme yapılmayacak
							// 	return null;
							// }
						});
					}
					return null;
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					return Promise.all(_promises).then(function(){
						return t.eventListener({event:'task_cases|get'});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_cases|update');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'case_delivery_manual':{
				//console.log('case_delivery_manual:'+arg.casenumber);
				if(t.timer_manual_delivery_control===false){
					try {
						if(t.self&&t.self.socket_io_client){
							if(arg.casenumber!==null&&arg.casenumber!=='null'&&arg.casenumber!==''&&arg.erprefnumber!==null&&arg.erprefnumber!=='null'&&arg.erprefnumber!==''){
								t.self.socket_io_client.send({event:'cn_sn',data:{event:'case_delivery_manual',barcode:arg.casenumber,client:v_client,erprefnumber:arg.erprefnumber}});
								t.timer_manual_delivery_control=setTimeout(() => {
									//timeout süresi içinde yanıt gelmedi
									t.timer_manual_delivery_control=false;
									t.winmessend('cn_js_workflow',{
										event: 'case_delivery_manual',
										result: 'err',
										message: 'Zaman aşımı oluştu. Tekrar deneyiniz'
									});
								}, 10000);
							}else{
								t.winmessend('cn_js_workflow',{
									event: 'case_delivery_manual',
									result: 'err',
									message: 'Tekrar deneyiniz-4'
								});
							}
						}else{
							t.winmessend('cn_js_workflow',{
								event: 'case_delivery_manual',
								result: 'err',
								message: 'Tekrar deneyiniz-5'
							});
						}
					} catch (error) {
						Sentry.captureException(error);
						t.winmessend('cn_js_workflow',{
							event: 'case_delivery_manual',
							result: 'err',
							message: 'Tekrar deneyiniz-6'
						});
					}
				}
				break;
			}
			case 'res_case_delivery_manual':{
				if(t.timer_manual_delivery_control!=false){
					clearTimeout(t.timer_manual_delivery_control);
					t.timer_manual_delivery_control=false;
					arg.event='case_delivery_manual';
					t.winmessend('cn_js_workflow',arg);
				}
				break;
			}
			case 'calculated_oee':{
				t.winmessend('cn_js_workflow',arg);
				break;
			}
			case 'getdata_fault':{
				t.status=t.status==''?'fault':t.status;
				let _promises=[];
				let fgc='';
				switch (arg.faultgroupcode) {
				case 'ARIZA KALIP-APARAT':
					fgc='Kalıp-Fikstür-Aparat';
					break;
				case 'ARIZA MAKİNE ELEKTRİK':
					fgc='İstasyon Elektrik';
					break;
				case 'ARIZA MAKİNE MEKANİK':
					fgc='İstasyon Mekanik';
					break;
				case 'ARIZA ÜRETİM':
					fgc='Üretim';
					break;
				default:
					break;
				}
				let _where_ft={
					faultgroupcode:fgc
					,finish:{[Op.eq]:null}
					,[Op.or]:[{client: {[Op.eq]:null}}, {client: v_client}]
				};
				let _client_details=[];
				let _fault_types=[];
				_promises.push(t.findAll('fault_types',{'code':'code','description':'description'},_where_ft,[ ['code', 'ASC']],false));
				return Promise.all(_promises).then(function(results_ft){
					_fault_types=t.getLastElementOfArray(results_ft);
					if(v_workflow!=='Sabit Üretim Aparatlı'&&fgc==='Kalıp-Fikstür-Aparat'){
						for(let i=0;i<t.processVariables.productions.length;i++){
							let idx=t.getIndex(_client_details,'code',t.processVariables.productions[i].mould);
							if(idx===-1){
								_client_details.push({code:t.processVariables.productions[i].mould,clienttype:t.processVariables.productions[i].opname});
							}
						}
						return null;
					}else{
						_promises=[];
						let _where_cd={
							client: v_client
							,[Op.or]:[{ioevent:{[Op.notIn]: ['input_ready','input_mandatory']}},{ioevent:{[Op.eq]:null}}]
							,finish:{[Op.eq]:null}
						};
						if(fgc==='Kalıp-Fikstür-Aparat'){
							_where_cd.iotype='I';
							_where_cd.ioevent='input';
							_where_cd.code={[Op.ne]:v_client};
						}else{
							if(t.ip&&(t.ip.indexOf('172.16')>-1)&&v_client.indexOf('03.06')>-1||t.ip==='192.168.1.40' ){
								_where_cd.code={[Op.notLike]: '03.23%'};
							}
						}
						_promises.push(t.findAll('client_details',{'code':'code','description':'description','clienttype':'clienttype'},_where_cd,[ ['code', 'ASC']],false));
						return Promise.all(_promises).then(function(results_cd){
							let _tmp=t.getLastElementOfArray(results_cd);
							for(let i=0;i<_tmp.length;i++){
								let idx=t.getIndex(_client_details,'code',_tmp[i].code);
								if(idx===-1){
									_client_details.push(_tmp[i]);
								}
							}
							return null;
						});
					}
				}).then(function(){
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',fault_types:_fault_types,client_details:_client_details});
					return null;
				});
			}
			case 'query_losttype':{
				return t.db.client_lost_details.findOne({attributes: { exclude: ['createdAt','updatedAt'] },where:{client:arg.client,record_id:arg.record_id,finish:{[Op.ne]:null}} }/*, {transaction: _tr}*/)
					.then(function(res_cld){ 
						if(res_cld){
							t.self.socket_io_client.send({event:'cn_sn',data:{event:'res_query_losttype',losttype:res_cld.losttype,client:arg.client,record_id:arg.record_id}});
						}
						return null;
					});
			}
			case 'input_received':{
				//taskcase güncelleme-malzeme hazırlık
				if(t.objParam.task_materialpreparation.value===true&&t.processVariables.status_work!=='OP-OUT-SYSTEM'){
					let _promises=[];
					let _f=false;
					t.db.sequelize.transaction(function (_tr) {
						if(t.timer_missing_material!==false){
							clearTimeout(t.timer_missing_material);
							t.timer_missing_material=false;
						}
						for(let i=0;i<arg.items.length;i++){
							let row=arg.items[i];
							_promises.push(t.control_missing_material(row.erprefnumber,row.opname,row.productcount));
						}
						return Promise.all(_promises).then(function(results){
							_promises=[];
							for(let i=0;i<results.length;i++){
								let row=results[i];
								if(!_f){
									if(row.record==true){
										_f=true;
										break;
									}else{
										break;
									}
								}
							}
							if(_f&&t.processVariables.status_lost!=='PLANLI TAŞIMA BEKLEME'&&t.processVariables.status_lost!=='MALZEME BEKLEME'){
								if(t.processVariables.status_lost!=='-'){
									let _obj_where_cld={
										type:{[Op.in]: ['l_c', 'l_c_t', 'l_e', 'l_e_t']}
										,client:v_client
										,finish:{[Op.eq]:null}
									};
									_promises.push(t.cjr_client_lost_details(_tr,_obj_where_cld,_time,{descriptionlost:''}));
								}
								t.setAllOutputs();
								let __clt=( (t.case_movement_target_time===false||(t.case_movement_target_time!==false&&t.case_movement_target_time>_time))?'PLANLI TAŞIMA BEKLEME':'MALZEME BEKLEME' );
								let _obj_cld_update={
									losttype: __clt
									,descriptionlost:__clt
								};
								t.setStatus(__clt);
								t.processVariables.lastprodstarttime=null;
								_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:null}));
								t.lost_recursive=false;
								_promises.push(t.create_lost_records(_tr,_time,_obj_cld_update,_time,t.processVariables.productions,_event));
								return Promise.all(_promises).then(function(){
									t.eventListener({event:'device_init'});
								});
							}
							return null;
						});
					}).then(function () {
						_promises=[];
						while(t.tmp_queue.length>0){
							_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
						}
						if(_f){
							if(t.show_delivery){
								t.winmessend('cn_js_workflow',{event:'task_cases|update',result:'ok',message:''});
							}
							t.timer_missing_material=setTimeout(() => {
								t.control_missing_materials('delivered',16);
							}, 10000);
						}else{
							if(t.show_delivery){
								t.winmessend('cn_js_workflow',{event:'task_cases|update',result:'ok',message:''});
							}
						}
						return Promise.all(_promises).then(function(){
							//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',productions:arg.productions});
						});
					}).catch(function (err) {
						t.mjd('rollback-'+_event);
						console.log(err);
						return null;
					});
				}
				break;
			}
			case 'authorizeloststart':{
				return t.db.lost_types.findOne({where:{code:t.processVariables.status_lost,finish:{[Op.eq]:null}}}).then(function(_res){
					let authtypefinish=null;
					if(_res){
						authtypefinish=_res.authorizelostfinish;
					}
					let authforsetuporquality=false;
					if(t.processVariables.status_workflowprocess==='SETUP'&&(t.processVariables.status_lost==='SETUP-AYAR'||t.processVariables.status_lost==='USTA BEKLEME')){
						authforsetuporquality=true;
					}
					if(t.processVariables.status_workflowprocess==='SETUP-WORK-FINISH'&&(t.processVariables.status_lost==='KALIP SÖKME'||t.processVariables.status_lost==='USTA BEKLEME')){
						authforsetuporquality=true;
					}
					if(t.processVariables.status_workflowprocess==='QUALITY'&&(t.processVariables.status_lost==='KALİTE ONAY'||t.processVariables.status_lost==='KALİTE BEKLEME')){
						authforsetuporquality=true;
					}
					t.isEquipmentRequire=false;
					if(v_client==='1600T'||v_client==='03.06.0002'){
						if(arg.losttype==='ÖNLEYİCİ BAKIM'||arg.losttype==='PLANLI BAKIM'){
							t.isEquipmentRequire=true;
						}
					}
					if(authtypefinish==null||authforsetuporquality==true||t.isEquipmentRequire){
						return t.db.lost_types.findOne({where:{code:arg.losttype,finish:{[Op.eq]:null}}}).then(function(_res){
							let openFaultScreen=false;
							if(t.faultcodes.indexOf(arg.losttype)>-1||t.isEquipmentRequire){
								openFaultScreen=true;
							}
							let authtypestart=null;
							if(_res){
								authtypestart=_res.authorizeloststart;
							}
							t.winmessend('cn_js_workflow',{event:_event,result:'ok',authtype:authtypestart,openFaultScreen:openFaultScreen});
							return null;
						});
					}else{
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:'Kayıp başlatmadan önce devam eden '+t.processVariables.status_lost+ ' kaybını bitirmelisiniz.'});
						return null;
					}
				});
			}
			case 'authorizelostfinish':{
				let authtype=null;
				let isdescriptionrequired=null;
				let controlQuestions=false;
				let time=0;
				let rep={code:t.processVariables.status_lost};
				let sql='select id,code,code as losttype,isexpertemployeerequired,coalesce(time,0) "time",isoperationnumberrequired '+
					' ,isclient,isemployee,authorizeloststart,authorizelostfinish,closenoemployee,ioevent,isdescriptionrequired '+
					' from lost_types '+
					' where code=:code and finish is null ';
				return t.db.sequelize.query(sql,{ replacements: rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(_res){
					if(_res.length>0){
						authtype=_res[0].authorizelostfinish;
						isdescriptionrequired=_res[0].isdescriptionrequired;
						time=_res[0].time;
					}
					//yapılan iş için cevaplanması gereken soru var mı kontrol edilip bilgisi cihaza gönderiliyor
					if(t.processVariables.status_workflowprocess==='SETUP'||t.processVariables.status_workflowprocess==='QUALITY'){
						let rep={type:t.processVariables.status_lost};
						let sql_erprefnumber='';
						let sql_where='';
						if(t.processVariables.productions.length>0){
							sql_erprefnumber=' ,case ';
							sql_where =' and ( opname is null ';
							for(let i=0;i<t.processVariables.productions.length;i++){
								sql_where+=' or opname=:opname_'+i;
								rep['opname_'+i]=t.processVariables.productions[i].opname;
								sql_erprefnumber+=' when opname=\''+t.processVariables.productions[i].opname+'\' then \''+t.processVariables.productions[i].erprefnumber+'\' ';
							}
							sql_erprefnumber+=' end erprefnumber ';
							sql_where+= ' ) ';
						}
						let sql='SELECT \'control_questions\' qtype,id,type,opname,question,answertype,path '+
						'	,case answertype when \'Evet-Hayır\' then case when (valuerequire=\'1\' or valuerequire=\'Evet\') then \'Evet\' else \'Hayır\' end else valuerequire end valuerequire '+
						' ,valuemin,valuemax,documentnumber '+sql_erprefnumber+
						' ,case answertype when \'Evet-Hayır\' then \'radio\' else \'text\' end cm_row_type  '+
						' ,case answertype when \'Rakam Değer\' then \'numpadex\' when \'Evet-Hayır\' then \'radio\' else \'tr_TR\' end cm_row_layout '+
						' FROM control_questions where type=:type ' + sql_where+ ' order by opname,question';
						return t.db.sequelize.query(sql, {replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
							if(results.length>0){
								if(t.activity_control_questions===false){
									controlQuestions=results;
								}else{
									controlQuestions=results.concat(t.activity_control_questions);
								}
							}else{
								controlQuestions=t.activity_control_questions;
							}
						});
					}else{
						// 2023-07-18 oskim için belirli duruşların sonlandırılması sırasında kalite onay sorularının sorulması eklendi
						if(t.objConfig.dcs_server_IP.value==='10.0.0.101'){
							if(['ARIZA MAKİNE ELEKTRİK','ARIZA MAKİNE MEKANİK','ELEKTRIK KESINTISI'].indexOf(t.processVariables.status_lost)!==-1&&t.processVariables.productions.length>0){
								let rep={type:'KALİTE ONAY'};
								let sql_erprefnumber='';
								let sql_where='';
								if(t.processVariables.productions.length>0){
									sql_erprefnumber=' ,case ';
									sql_where =' and ( opname is null ';
									for(let i=0;i<t.processVariables.productions.length;i++){
										sql_where+=' or opname=:opname_'+i;
										rep['opname_'+i]=t.processVariables.productions[i].opname;
										sql_erprefnumber+=' when opname=\''+t.processVariables.productions[i].opname+'\' then \''+t.processVariables.productions[i].erprefnumber+'\' ';
									}
									sql_erprefnumber+=' end erprefnumber ';
									sql_where+= ' ) ';
								}
								let sql='SELECT \'control_questions\' qtype,id,type,opname,question,answertype,path '+
								'	,case answertype when \'Evet-Hayır\' then case when (valuerequire=\'1\' or valuerequire=\'Evet\') then \'Evet\' else \'Hayır\' end else valuerequire end valuerequire '+
								' ,valuemin,valuemax,documentnumber '+sql_erprefnumber+
								' ,case answertype when \'Evet-Hayır\' then \'radio\' else \'text\' end cm_row_type  '+
								' ,case answertype when \'Rakam Değer\' then \'numpadex\' when \'Evet-Hayır\' then \'radio\' else \'tr_TR\' end cm_row_layout '+
								' FROM control_questions where type=:type ' + sql_where+ ' order by opname,question';
								return t.db.sequelize.query(sql, {replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
									if(results.length>0){
										if(t.activity_control_questions===false){
											controlQuestions=results;
										}else{
											controlQuestions=results.concat(t.activity_control_questions);
										}
									}else{
										controlQuestions=t.activity_control_questions;
									}
								});
							}
							if(t.processVariables.status_lost==='UZUN SÜRELİ DURUŞ'&&t.processVariables.productions.length>0){
								let rep={type:t.processVariables.status_lost,client: '%'+v_client+'%'};
								let sql_erprefnumber='';
								// let sql_where='';
								if(t.processVariables.productions.length>0){
									sql_erprefnumber=' ,case ';
									// sql_where =' and ( opname is null ';
									for(let i=0;i<t.processVariables.productions.length;i++){
										// sql_where+=' or opname=:opname_'+i;
										// rep['opname_'+i]=t.processVariables.productions[i].opname;
										sql_erprefnumber+=' when opname=\''+t.processVariables.productions[i].opname+'\' then \''+t.processVariables.productions[i].erprefnumber+'\' ';
									}
									sql_erprefnumber+=' end erprefnumber ';
									// sql_where+= ' ) ';
								}
								let sql='SELECT \'control_questions\' qtype,id,type,opname,question,answertype,path '+
								'	,case answertype when \'Evet-Hayır\' then case when (valuerequire=\'1\' or valuerequire=\'Evet\') then \'Evet\' else \'Hayır\' end else valuerequire end valuerequire '+
								' ,valuemin,valuemax,documentnumber '+sql_erprefnumber+
								' ,case answertype when \'Evet-Hayır\' then \'radio\' else \'text\' end cm_row_type  '+
								' ,case answertype when \'Rakam Değer\' then \'numpadex\' when \'Evet-Hayır\' then \'radio\' else \'tr_TR\' end cm_row_layout '+
								' FROM control_questions where type=:type and clients like :client order by opname,question';
								return t.db.sequelize.query(sql, {replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
									if(results.length>0){
										if(t.activity_control_questions===false){
											controlQuestions=results;
										}else{
											controlQuestions=results.concat(t.activity_control_questions);
										}
									}else{
										controlQuestions=t.activity_control_questions;
									}
								});
							}
						}
					}
					const tmpOpname=(t.processVariables.productions.length>0?t.processVariables.productions[0].opname:'');
					const tmpProductdonecount=(t.processVariables.productions.length>0?t.processVariables.productions[0].productdonecount:0);
					const tmpErprefnumber=(t.processVariables.productions.length>0?t.processVariables.productions[0].erprefnumber:0);
					//if(tmpProductdonecount%2===0&&v_client==='03.06.0002'&&t.objConfig.dcs_server_IP.value==='192.168.1.10'){
					//	controlQuestions=[{
					//		qtype:'control_questions'
					//		,id:null
					//		,type:t.processVariables.status_lost//'FREKANSİYEL KONTROL'
					//		,opname:tmpOpname
					//		,question:tmpProductdonecount+' Adet baskıya ulaşıldı. '+tmpOpname+' Parça için Tork Ölçüm Sonucu Giriniz'
					//		,answertype:'Rakam Değer'
					//		,path:null
					//		,valuerequire:0
					//		,valuemin:0
					//		,valuemax:99999
					//		,documentnumber:null
					//		,erprefnumber:tmpErprefnumber
					//		,cm_row_type:'text'
					//		,cm_row_layout:'numpadex'
					//	}];
					//}
					if(t.objConfig.dcs_server_IP.value==='10.0.0.101'){
						if(tmpProductdonecount%100===0&&v_client==='HP011'){
							controlQuestions=[{
								qtype:'control_questions'
								,id:null
								,type:t.processVariables.status_lost//'FREKANSİYEL KONTROL'
								,opname:tmpOpname
								,question:tmpProductdonecount+' Adet baskıya ulaşıldı. '+tmpOpname+' Parça için Tork Ölçüm Sonucu Giriniz'
								,answertype:'Rakam Değer'
								,path:null
								,valuerequire:0
								,valuemin:0
								,valuemax:99999
								,documentnumber:null
								,erprefnumber:tmpErprefnumber
								,cm_row_type:'text'
								,cm_row_layout:'numpadex'
							}];
						}
						if(tmpProductdonecount%100===0&&v_client==='PK004'){
							controlQuestions=[{
								qtype:'control_questions'
								,id:null
								,type:t.processVariables.status_lost//'FREKANSİYEL KONTROL'
								,opname:tmpOpname
								,question:tmpProductdonecount+' Adet baskıya ulaşıldı. '+tmpOpname+' Parça için Kaynak Kopma Test Sonucu Giriniz'
								,answertype:'Rakam Değer'
								,path:null
								,valuerequire:0
								,valuemin:0
								,valuemax:99999
								,documentnumber:null
								,erprefnumber:tmpErprefnumber
								,cm_row_type:'text'
								,cm_row_layout:'numpadex'
							}];
						}
					}
					return null;
				}).then(function(){
					authtype=authtype==null&&t.processVariables.operators.length==0?'isoperator':authtype;
					t.winmessend('cn_js_workflow'
						,{event:_event
							,result:'ok'
							,authtype:authtype
							,isdescriptionrequired:isdescriptionrequired
							,time:time
							,state_start:t._state_start
							,title:t.processVariables.status_lost+' Kayıp Bitir'
							,controlQuestions:controlQuestions
						});
					return null;
				});
			}
			case 'writelabel':{
				arg.client=v_client;
				for(let i=0;i<t.processVariables.productions.length;i++){
					t.processVariables.productions[i].start=t.convertFullLocalDateString(t.processVariables.productions[i].start);
				}
				arg.productions=t.processVariables.productions;
				arg.operators=t.processVariables.operators;
				let mes=new dcs_message({
					type:'writelabel'
					,ack:true
					,data:arg
				});
				t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
				return Promise.all([t.db._sync_messages.create(mes.getdata())]);
				//break;
			}
			case 'document_view_logs|save':{
				let _promises=[];
				let _cur_losttype='';
				let obj=arg.data;
				obj.time=_time;
				let _obj_where={
					type:{[Op.in]: ['l_c', 'l_c_t','l_e', 'l_e_t']}
					,client:v_client
					,start:{[Op.ne]:null}
					,finish:{[Op.eq]:null}
				};
				t.db.sequelize.transaction(function (_tr) {
					for(let c=0;c<obj.data.length;c++){
						for(let i=0;i<t.processVariables.productions.length;i++){
							let _p_item=t.processVariables.productions[i];
							let tmp_opname=_p_item.opname.replace('/','_');
							if(obj.data[c].type==='TEZGAH AYAR FÖYÜ'){
								t.client_control_document=true;
								obj.data[c].erprefnumber=_p_item.erprefnumber;
							}
							if(obj.data[c].type==='SOP'&&_cur_losttype===''){
								_cur_losttype='-';
							}
							if(obj.data[c].opname==tmp_opname){
								obj.data[c].erprefnumber=_p_item.erprefnumber;
								break;
							}
						}
					}
					let mes=new dcs_message({
						type:'document_view_logs'
						,ack:true
						,data:obj
					});
					_promises.push(t.db._sync_messages.create(mes.getdata()));
					mes=undefined;
					return Promise.all(_promises).then(function(){
						if(_cur_losttype!==''){
							t.setStatus(_cur_losttype);
						}
						t.processVariables.lastprodstarttime=_time;
						_promises.push(t.cjr_client_lost_details(_tr,_obj_where,_time,{descriptionlost:arg.descriptionlost}));
						_promises.push(t.updateRecord(_tr,'clients',_time,{code:v_client},{status:t.generateClientStatus(),statustime:_time,lastprodstarttime:_time}));
						return Promise.all(_promises);
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_document_view_logs');
					t.tmp_queue=[];
					return Promise.all([t.getClientStatus()]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'control_answers|get':{
				let _promises=[];
				let _control_answers=[];
				t.db.sequelize.transaction(function (/*_tr*/) {
					let opnames=[];
					let erprefnumbers=[];
					if(t.processVariables.productions.length>0){
						for(let i=0;i<t.processVariables.productions.length;i++){
							let prod=t.processVariables.productions[i];
							let tmp_opname=prod.opname;
							if(opnames.indexOf(tmp_opname)==-1){
								opnames.push(tmp_opname);
							}
							let tmp_erprefnumber=prod.erprefnumber;
							if(erprefnumbers.indexOf(tmp_erprefnumber)==-1){
								erprefnumbers.push(tmp_erprefnumber);
							}
						}
						return t.db.sequelize.query('SELECT * '+
						' from control_answers '+
						' where client=:client and erprefnumber in (:erprefnumber) and opname in (:opname) '+
						' order by type,erprefnumber,opname,time limit 1000 '
						, { replacements:{client:v_client,erprefnumber:erprefnumbers,opname:opnames},type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
							_control_answers=(results&&results.length>0?results:[]);
							return null;
						});
					}
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',control_answers:_control_answers});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_control_answers|get');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'operation_authorization|back':{
				t.json_status.operationAuthorization.nextControl=null;
				t.json_status.operationAuthorization.data=[];
				try {
					fs.writeFileSync(__dirname+'/../status.json', JSON.stringify(t.json_status));
				} catch (error) {
					Sentry.captureException(error);
				}
				break;
			}
			case 'task_case_controls|get':{
				let _promises=[];
				let _task_case_controls=[];
				t.db.sequelize.transaction(function (/*_tr*/) {
					let sql='select * from ( '+
					'	SELECT \'kalitekontrol\' tip,tcc.id,tcc.record_id,tcc.opcode stockcode,tcc.opcode stockname,tcc.code,cast(tcc.packcapacity as int)packcapacity,cast(tcc.quantityremaining as int)quantityremaining  '+
					'	from task_case_controls tcc  '+
					'	left join task_lists tl on tl.opcode=tcc.opcode  '+
					'	where cast(tcc.quantityremaining as int)>0 and tcc.code=:code and tl.code=:tasklist and tl.finish is null '+
					'	union all '+
					'	SELECT \'yenidenislem\' tip,tcc.id,tcc.record_id,pt.stockcode,tcc.opcode stockname,tcc.code,cast(tcc.packcapacity as float)packcapacity,cast(tcc.quantityremaining as float)quantityremaining  '+
					'	from task_case_controls tcc  '+
					'	left join product_trees pt on pt.stockname=tcc.opcode '+
					'	left join task_lists tl on tl.opcode=pt.code  '+
					'	where pt.finish is null and cast(tcc.quantityremaining as float)>0 and pt.materialtype in (:materialtype) and tcc.code=:code and tl.code=:tasklist and tl.finish is null '+
					')x '+
					'order by x.id limit 1';
					let rep={code:arg.code,tasklist:arg.tasklist,materialtype:['H']};
					if(v_workflow!=='El İşçiliği'){
						if(arg.opname){//üretim sonu bildirim
							sql='SELECT tcc.record_id,pt.stockcode,tcc.opcode stockname,tcc.code,cast(tcc.packcapacity as float)packcapacity,cast(tcc.quantityremaining as float)quantityremaining '+
							' from task_case_controls tcc '+
							' left join product_trees pt on pt.stockname=tcc.opcode '+
							' where pt.finish is null and cast(tcc.quantityremaining as float)>0 and pt.materialtype in (:materialtype) and tcc.code=:code and pt.code=:opcode '+
							' order by tcc.id limit 1';
							let _tmp_opname=arg.opname.split('-');
							_tmp_opname.pop();
							rep={
								materialtype:['H']
								,code:arg.code
								,opcode:_tmp_opname.join('-')
							};
						}else{//üretim başı, malzeme ekleme
							let opcodes=[];
							let erprefnumbers=[];
							for(let i=0;i<t.processVariables.productions.length;i++){
								let prod=t.processVariables.productions[i];
								let tmp_opcode=prod.opcode;
								if(opcodes.indexOf(tmp_opcode)==-1){
									opcodes.push(tmp_opcode);
								}
								let tmp_erprefnumber=prod.erprefnumber;
								if(erprefnumbers.indexOf(tmp_erprefnumber)==-1){
									erprefnumbers.push(tmp_erprefnumber);
								}
							}
							//and pt.isdefault=true açılır ise alternatif malzeme kabul etmiyor
							sql=`
							select tcc.record_id,ptc.stockcode,tcc.opcode stockname,tcc.code,cast(tcc.packcapacity as float)packcapacity
								,cast(tcc.quantityremaining as float)quantityremaining,ptc.quantity
							from (
								select pt.code,pt.number,pt.name,pt.stockcode,pt.stockname,pt.quantity,0 requirement
									,(select COALESCE(sum(tc.quantityremaining),0) 
									from task_cases tc 
									where tc.movementdetail is null and tc.status='TCC' and tc.casetype='I' and tc.opcode=pt.code 
										and tc.stockcode=pt.stockcode and tc.client=:client and tc.erprefnumber in (:erprefnumbers)) quantityremaining
								from product_trees pt 
								where pt.materialtype='H' and pt.finish is null and pt.name<>pt.stockcode /*and pt.isdefault=true*/ and pt.code in (:ptcodes)
							)ptc
							join task_case_controls tcc on ptc.stockname=tcc.opcode and cast(tcc.quantityremaining as float)>0 and tcc.code=:code
							where ptc.quantityremaining>=0
							order by ptc.stockcode
							`;
							rep={
								client:v_client
								,code:arg.code
								,ptcodes:opcodes
								,erprefnumbers:erprefnumbers
							};
						}
					}
					//console.log('task_case_controls|get');
					//console.log(rep);
					return t.db.sequelize.query(sql, { replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
						_task_case_controls=(results&&results.length>0?results:[]);
						// console.log(_task_case_controls);
						return null;
					});
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_case_controls:_task_case_controls});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_task_case_controls|get');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'device-work|res_requirements':
			case 'device-employee-join|res_requirements':{
				// cihazdan gelen barkod verisi, ihtiyaç olan operasyonlara göre kapasite aynı olacak şekilde task_cases tablosuna kayıt edilecek
				// 100 hammadde girilmiş ise, aynı malzemeyi tüketen operasyonlara 100 olarak task_cases tablosuna kayıt edilecek
				let _promises=[];
				let _requirements=arg.requirements;
				let _data=arg.data;
				let tc_create=function(_tr,_time,item){//aynı kasa tekrar okunsun denirse where objesine ,finish:{[Op.eq]:null} eklenecek
					return t.db.task_cases.findOne({attributes: { exclude: ['createdAt','updatedAt'] }
						,where:{casenumber:item.casenumber,erprefnumber:item.erprefnumber,status:'TCC',finish:{[Op.eq]:null}} 
					}, {transaction: _tr}).then(function(result){
						if(result){
							return null;
						}else{
							return Promise.all([t.createRecord(_tr,'task_cases',_time,item)]);
						}
					});
				};
				t.db.sequelize.transaction(function (_tr) {
					if(_requirements){
						for(let i=0;i<_requirements.length;i++){
							for(let j=0;j<_data.length;j++){
								if(_data[j]['stockname']===_requirements[i]['stockname']){
									let _tmp_data_tc={
										client:v_client
										,movementdetail:null
										,erprefnumber:_data[j].erprefnumber
										,casetype:'I'
										,opcode:_data[j].code
										,opnumber:_data[j].number
										,opname:_data[j].name
										,stockcode:_requirements[i]['stockcode']
										,stockname:_requirements[i]['stockname']
										,quantityused:parseFloat(_data[j].quantity)
										,casename:''
										,casenumber:_requirements[i]['code']
										,caselot:''
										,packcapacity:_requirements[i]['packcapacity']
										,preptime:10
										,quantityremaining:_requirements[i]['quantityremaining']
										,quantitydeliver:_requirements[i]['quantityremaining']
										,conveyor:0
										,status:'TCC'
										,start:_time
										,record_id:uuidv4()
									};
									_promises.push(tc_create(_tr,_time,_tmp_data_tc));
								}
							}
						}
					}
					return Promise.all(_promises);
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					if(_event==='device-work|res_requirements'){
						if(v_workflow!=='El İşçiliği'){
							return t.eventListener({event:"device-lost-finish"});
						}
					}else{
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					}
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',task_finish_types:_task_cases});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_device-employee-join|res_requirements');
					t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					t.tmp_queue=[];
					return null;
				});
				break;
			}
			case 'device-employee-join|isg_answers':{
				let _control_answers=arg.control_answers;
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					let _erp=[];
					for(let i=0;i<t.processVariables.productions.length;i++){
						let row=t.processVariables.productions[i];
						if(_erp.indexOf(row.erprefnumber)==-1){
							_erp.push(row.erprefnumber);
						}
					}
					for(let e=0;e<_erp.length;e++){
						if(_control_answers.length>0){
							for(let c=0;c<_control_answers.length;c++){
								if(_control_answers[c].qtype==='control_questions_isg'){
									if(_control_answers[c].type==='GÖREVE KATILMA'){
										_control_answers[c].erprefnumber=_erp[e];
										for(let i=0;i<t.processVariables.productions.length;i++){
											let _p_item=t.processVariables.productions[i];
											if(_control_answers[c].erprefnumber==_p_item.erprefnumber){
												_control_answers[c].taskcode=_p_item.code;
												break;
											}
										}
									}else{
										for(let i=0;i<t.processVariables.productions.length;i++){
											let _p_item=t.processVariables.productions[i];
											if(_control_answers[c].opname==_p_item.opname){
												_control_answers[c].taskcode=_p_item.code;
												_control_answers[c].erprefnumber=_p_item.erprefnumber;
												break;
											}
										}
									}
								}
							}
							for(let c=0;c<_control_answers.length;c++){
								let row=_control_answers[c];
								_promises.push(t.createRecord(_tr,'control_answers',_time,{
									type:row.type
									,documentnumber:row.documentnumber
									,client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,opname:null
									,erprefnumber:_erp[e]
									,employee:arg.employee
									,time:_time
									,question:row.question
									,answertype:row.answertype
									,valuerequire:row.valuerequire
									,valuemin:row.valuemin
									,valuemax:row.valuemax
									,answer:row.answer
									,record_id:uuidv4()
								}));
							}
							let mes=new dcs_message({
								type:'control_answers'
								,ack:true
								,data:{
									client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,employee:arg.employee
									,time:_time
									,data:_control_answers
								}
							});
							t.tmp_queue.push(mes);
							mes=undefined;
						}
					}
					return Promise.all(_promises);
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
					});
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_device-employee-join|isg_answers');
					t.tmp_queue=[];
					return Promise.all([t.getClientStatus()]).then(function(){
						t.winmessend('cn_js_workflow',{event:_event,result:'err',message:err.toString().replace('Error: ','')});
					});
				});
				break;
			}
			case 'get_productioncasecontrol_requirements':{
				//console.log('get_productioncasecontrol_requirements',arg);
				let _productioncasecontrol_requirements=[];
				let _productioncasecontrol_resources=[];
				let _tmp_opname=arg.opname.split('-');
				_tmp_opname.pop();
				let rep={
					code:_tmp_opname.join('-')
					,materialtype:['H']
				};
				let sql='SELECT stockcode,stockname,quantity,0 requirement '+
				' from product_trees '+
				' where finish is null and name<>stockcode and isdefault=true and code=:code and materialtype in (:materialtype) '+
				' order by parent,code,number';
				return t.db.sequelize.query(sql, { replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(results) {
					_productioncasecontrol_requirements=(results&&results.length>0?results:[]);
					if(t.objParam.app_case_stok_before_work.value===true){
						sql=`
							SELECT id,record_id,casenumber code,stockcode,stockname
								,cast(packcapacity as int) packcapacity
								,cast(quantitydeliver as int) quantitydeliver
								,cast(quantitydeliver as int) quantityremaining
								,(select cast(tco.quantityremaining as int) 
									from task_cases tco 
									where tco.client=client and tco.erprefnumber=erprefnumber and tco.status='TCC' and tco.casetype='O' and tco.finish is null) uretilen
							from task_cases 
							where casetype='I' and movementdetail is null and finish is null and quantitydeliver>0 and status='TCC' 
								and client=:client and erprefnumber=:erprefnumber and opcode=:opcode
							order by stockcode,stockname,code
						`;//and opname=:opname
						rep={
							client:v_client
							,erprefnumber:arg.erprefnumber
							//,opname:arg.opname //2021-11-30 kaldırıldı
							,opcode:arg.opcode
						};
						return t.db.sequelize.query(sql, { replacements:rep,type: t.db.sequelize.QueryTypes.SELECT}).then(function(res_resources) {
							_productioncasecontrol_resources=(res_resources&&res_resources.length>0?res_resources:[]);
							t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',productioncasecontrol_requirements:_productioncasecontrol_requirements,productioncasecontrol_resources:_productioncasecontrol_resources});
							return null;
						});
					}else{
						let _uretilen=0;
						let _idx=t.getIndex(t.processVariables.productions,'opname',arg.opname);
						if(_idx!==-1){
							_uretilen=t.processVariables.productions[_idx].productdonecount;
						}
						t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'',productioncasecontrol_requirements:_productioncasecontrol_requirements,uretilen:_uretilen});
						return null;
					}
				}).then(function(){
					if(v_workflow!=='El İşçiliği'){
						t.eventListener({event:'device-lost-start',description:'Kasa etiket bildirimi',selectedRow:{code:'DÖKÜMANTASYON'},op_transfer:true,status:t.generateClientStatus()});
					}
					return null;
				});
				break;
			}
			case 'save_productioncasecontrol_requirements':{
				let v_productcount=arg.productcount!==false?parseInt(arg.productcount):0;
				let v_casecapacity=arg.casecapacity!==false?parseInt(arg.casecapacity):0;
				let v_employee=arg.employee;
				if(t.processVariables.operators.length>0&&(v_employee===''||v_employee===false||v_employee===undefined||v_employee==='undefined')){
					v_employee=t.processVariables.operators[0].code;
				}
				let mes=new dcs_message({
					type:'task_case_controls'
					,ack:true
					,data:{
						client:v_client
						,day:_jrday
						,jobrotation:_jrcode
						,employee:v_employee
						,opcode:arg.opcode
						,opnumber:arg.opnumber
						,opname:arg.opname
						,capak:arg.capak
            ,kalite_kontrol:arg.kalite_kontrol
            ,mut:arg.mut
						,productcount:v_productcount
						,casecapacity:v_casecapacity
						,erprefnumber:arg.erprefnumber
						,detail:arg.task_case_controls
					}
				});
				t.tmp_queue.push(mes);
				mes=undefined;
				let _promises=[];
				for(let i=0;i<arg.task_case_controls.length;i++){
					let row=arg.task_case_controls[i];
					_promises.push(t.updateRecord(null,'task_cases',_time,{record_id:row.record_id},{quantitydeliver:row.quantityremaining,finish:(row.quantityremaining===0?_time:null)}));
				}
				_promises.push(t.copyAllRecords(null,'task_cases',_time,{casetype:'O',client:v_client,erprefnumber:arg.erprefnumber,opname:arg.opname,finish:{[Op.eq]:null}},{quantityremaining:parseInt(arg.uretilen)-parseInt(arg.productcount),finish:null},{finish:_time,quantityremaining:v_productcount},true));
				while(t.tmp_queue.length>0){
					_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
				}
				if(t.objParam.app_caseVerify.value===false){
					t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:''});
				}
				return Promise.all(_promises).then(function(){
					//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:_message});
				});
				//break;
			}
			case 'task_plan_employees|get':{
				t.timer_wait_socket_data=setTimeout(() => {
					t.timer_wait_socket_data=false;
					t.winmessend('cn_js_workflow',{event:'task_plan_employees|get',result:'err',message:'Zaman aşımı oluştu, tekrar deneyiniz.'});
				}, 10000);
				t.self.socket_io_client.send({event:'cn_sn',data:{event:'task_plan_employees|get',employee:arg.employee}});
				break;
			}
			case 'res|task_plan_employees|get':{
				clearTimeout(t.timer_wait_socket_data);
				t.timer_wait_socket_data=false;
				t.winmessend('cn_js_workflow',{event:'task_plan_employees|get',result:'ok',message:'',task_plan_employees:arg.data});
				break;
			}
			case 'client_lost_details|get':{
				let rep={type:arg.type,client:v_client, day:_jrday, jobrotation:_jrcode};
				let sql='SELECT losttype,descriptionlost,erprefnumber,opname,cast("start" as time) "start",cast(finish as time) finish '+
				' 	,case when losttype in (\'ÇAY MOLASI\',\'YEMEK MOLASI\',\'İHTİYAÇ MOLASI\',\'İBADET\') then \'drow-started\' else \'\' end rowclass '+
				' 	,cast(finish-start as varchar(8)) sure,employee '+
				' from client_lost_details '+
				' where type=:type and client=:client and day=:day and jobrotation=:jobrotation '+
				' order by start';
				t.db.sequelize.query(sql,{ replacements: rep, type: t.db.sequelize.QueryTypes.SELECT }).then(function(results) {
					t.winmessend('cn_js_workflow',{event:_event,client_lost_details:results});
				});
				break;
			}
			case 'case_movements|get':{
				t.timer_wait_socket_data=setTimeout(() => {
					t.timer_wait_socket_data=false;
					t.winmessend('cn_js_workflow',{event:'case_movements|get',result:'err',message:'Zaman aşımı oluştu, tekrar deneyiniz.'});
				}, 10000);
				t.self.socket_io_client.send({event:'cn_sn',data:{event:'case_movements|get',client:v_client}});
				break;
			}
			case 'res|case_movements|get':{
				clearTimeout(t.timer_wait_socket_data);
				t.timer_wait_socket_data=false;
				t.winmessend('cn_js_workflow',{event:'case_movements|get',result:'ok',message:'',case_movements:arg.data});
				break;
			}
			case 'set_penetrations_response':{
				let _items_=[];
				for(let c=0;c<t.processVariables.productions.length;c++){
					const row=t.processVariables.productions[c];
					_items_.push({
						code:row.code
						,erprefnumber:row.erprefnumber
						,opcode:row.opcode
						,opnumber:row.opnumber
						,opname:row.opname
						,mould:row.mould
						,mouldgroup:row.mouldgroup
						,client:v_client
						,tasklist:row.tasklist
						// ,employee:row.employee
						,time:_time
					});
				}
				let _employees_=[];
				for(let c=0;c<t.processVariables.operators.length;c++){
					const _emp=t.processVariables.operators[c];
					_employees_.push(_emp);
				}
				let mes=new dcs_message({
					type:'penetrations_response'
					,ack:true
					,data:{obj:arg,tasks: _items_,operators:_employees_}
				});
				t.tmp_queue.push(mes);
				mes=undefined;
				break;
			}
			case 'res_qualitycontrol':{
				let _control_answers=arg.control_answers;
				let _employee=arg.employee;
				let _promises=[];
				t.db.sequelize.transaction(function (_tr) {
					if(_control_answers){
						if(_control_answers.length>0){
							for(let c=0;c<_control_answers.length;c++){
								let row=_control_answers[c];
								if(row.qtype==='control_questions'){
									row.type=row.type+'-ARA KONTROL';
									for(let i=0;i<t.processVariables.productions.length;i++){
										let _p_item=t.processVariables.productions[i];
										if(row.opname==_p_item.opname){
											row.taskcode=_p_item.code;
											break;
										}
									}
									_promises.push(t.createRecord(_tr,'control_answers',_time,{
										type:row.type
										,documentnumber:row.documentnumber
										,client:v_client
										,day:_jrday
										,jobrotation:_jrcode
										,opname:row.opname
										,erprefnumber:row.erprefnumber
										,employee:_employee
										,time:_time
										,question:row.question
										,answertype:row.answertype
										,valuerequire:row.valuerequire
										,valuemin:row.valuemin
										,valuemax:row.valuemax
										,answer:row.answer
										,record_id:uuidv4()
									}));
								}
							}
							let mes=new dcs_message({
								type:'control_answers'
								,ack:true
								,data:{
									client:v_client
									,day:_jrday
									,jobrotation:_jrcode
									,employee:_employee
									,time:_time
									,data:_control_answers
								}
							});
							t.tmp_queue.push(mes);
							mes=undefined;
							return Promise.all(_promises);
						}
					}
					return;
				}).then(function () {
					_promises=[];
					while(t.tmp_queue.length>0){
						_promises.push(t.db._sync_messages.create(t.tmp_queue.shift().getdata()));
					}
					t.eventListener({event:'task_lists|get'});
					return Promise.all(_promises).then(function(){
						//return t.winmessend('cn_js_workflow',{event:_event,result:'ok',message:'İş başlatıldı',process:t.processVariables.status_workflowprocess});
					});	
				}).catch(function (err) {
					t.mjd('rollback-'+_event);
					console.log(err);
					classBase.writeError(err,t.ip,'catch_workflow_res_qualitycontrol');
					
				});
			}
			default:
				t.mjd('js_cn_workflow event yazilmamis!event:'+_event);
				break;
			}
		} catch (error) {
			Sentry.captureException(error);
		}
	}
}
module.exports=dcs_client_workflow;