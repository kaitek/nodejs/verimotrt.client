const classBase=require('./base');
const io_port=require('./io_port');
const io_message=require('./io_message');
const io_MessageSendQueue=require('./io_MessageSendQueue');
const io_MessageReceiveQueue=require('./io_MessageReceiveQueue');
const net = require('net');
const processModule = require('process');
const Sentry = require('@sentry/node');
Sentry.init({
	dsn: 'https://1eb61add8d714e26870c1d92984ac1a5@sentry.kaitek.com.tr/4'
});

//Sentry.context(function () {
class io_manager_tcp extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='io_manager_tcp';
		t.version='0.1';
		t.inputs=false;
		t.outputs=false;
		t.sendqueue=false;
		t.connected=false;
		t.receivequeue=false;
		t.sock=false;
		t.serialport=false;
		t.outputPorts=[];
		t.inputPorts=[];
		t.server=false;
		t.timer_input=false;
		t.readBuff=false;
		t.wait_for_outputs=true;
		Object.assign(this, this.config);
		let p;
		for(let i=0;i<this.outputs;i++){
			p=new io_port({type:'output',number:i+1,index:i,io_type:t.io_type});
			p.on('ioportchange',function(p,fevent){
				t.emit('ioportchange',p,fevent);
			});
			this.outputPorts.push(p);
		}
		for(let i=0;i<this.inputs;i++){
			p=new io_port({type:'input',number:i+1,index:i,io_type:t.io_type});
			p.on('ioportchange',function(p,fevent){
				t.emit('ioportchange',p,fevent);
			});
			this.inputPorts.push(p);
		}
	}
	start(){
		let t=this;
		t.emit('ioconnecting');
		//t.mjd('t.io_type:'+t.io_type);
		t.sendqueue=new io_MessageSendQueue({parent:t});
		t.receivequeue=new io_MessageReceiveQueue({parent:t});
		t.server=net.createServer(function(sock) {
			t.socketIp=sock.localAddress;
			t.sock=sock;
			sock.on('data', function(data) {
				t.receive.call(t, sock, data, t.messageParser);
			});
			sock.on('close', function() {
				t.mjd('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort + '\n');
				classBase.writeError('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort + '\n',t.hostip,'sock.on.close');
				t.connected=false;
			});
			sock.on('error', function(err){
				t.mjd('t.server.err:'+err);
				classBase.writeError('error: ' + sock.remoteAddress +' '+ sock.remotePort + err +'\n',t.hostip,'sock.on.error');
			});
		});
		t.server.listen(t.local, t.ip);
	}
	receive(client, message, cb){
		let msg=this.prepData(message);
		if(msg.length>3)
			cb.call(this, client, msg);
	}
	messageParser(client, message){
		let t=this;
		if(message.length>3){
			let mes;
			if(message.length==6&&!this.connected){
				this.sendqueue._ready=true;
				this.sendqueue.run();
				this.connected=true;
				this.emit('ioconnected',client);
				//classBase.writeError('ioconnected: ' + client.remoteAddress +' '+ client.remotePort + '\n',t.hostip,'sock');
				this._init(client);
			}else{
				let len=0;
				let i;
				if(message[0]=='AA'&&message[1]=='55'){
					for(i=0;i<message.length;){
						len = 5 + parseInt(message[i + 3], 16);
						if(message.length>=len){
							mes=new io_message();
							mes.data=[];
							if(message[i + 5] == '81'){
								mes.type = 'output-clear-ack';
							} 
							if(message[i + 5] == '82'){
								mes.type = 'output-set-ack';
							} 
							if(message[i + 5] == '83'){
								mes.type = 'output-toggle-ack';
							} 
							if(message[i + 5] == '84'){
								mes.type = 'output-clear-all-ack';
							} 
							if(message[i + 5] == '85'){
								mes.type = 'output-set-all-ack';
							} 
							if(message[i + 5] == '86'){
								mes.type = 'output-toggle-all-ack';
							} 
							if(message[i + 5] == '8A'){
								mes.type = 'output-read';
							} 
							if(message[i + 5] == '94'){
								mes.type = 'input-read';
							} 
							if(message[i + 5] == 'FE'){
								mes.type = 'init-0-ack';
							} 
							let crc=0;
							let j=0;
							for(j=i+2;j<i+len-1;j++){
								crc += parseInt(message[j], 16);
							}
							if(parseInt(message[j], 16)==crc%256){
								for(j=i;j<i+len;j++){
									mes.data.push(message[j]);
								}
								this.receivequeue.exec(mes);
								i += len;
							}else{
								console.log('crc hatalı '+ message);
								i += len;
							}
						}else{
							console.log('mesaj uzunluğu hatalı '+ message);
							i += len;
						}
					}
				}else{
					classBase.writeError('mesaj header hatalı: ' + message.join(', ') + '\n',t.hostip,'sock');
					console.log('mesaj header hatalı ' + message.join(', ') + ' len:' + message.length);
					i += len;
				}
			}
		}
	}
	_readAll(){
		this.readInputAll();
		this.readOutputAll();
	}
	_init(){
		let mes=new io_message({
			data:[85,170,0,2,0,126]
			,type:'init-0'
		});
		this.sendqueue.exec(mes);
	}
	clearOutput(number){
		let p=this.getOutputPort(number);
		if(p!==false&&p.curval!==0){
			let mes=new io_message({
				data:p.clearOutput()
				,type:'output-clear'
				,readData:'O'+number+':0 '
				,portnumber:number
				,portval:false
			});
			this.sendqueue.exec(mes);
		}
	}
	setOutput(number){
		let p=this.getOutputPort(number);
		if(p!==false&&p.curval!==1){
			let mes=new io_message({
				data:p.setOutput()
				,type:'output-set'
				,readData:'O'+number+':1 '
				,portnumber:number
				,portval:true
			});
			this.sendqueue.exec(mes);
		}
	}
	toggleOutput(number){
		let p=this.getOutputPort(number);
		if(p!==false){
			let mes=new io_message({
				data:p.toggleOutput()
				,type:'output-toggle'
				,readData:'O'+number+':'+p.curval+' '
				,portnumber:number
				,portval:(p.curval==1?false:true)
			});
			this.sendqueue.exec(mes);
		}
	}
	clearAll(){
		let mes=new io_message({
			data:[85,170,0,2,0,4]//clearAll
			,type:'output-clear-all'
		});
		for(let i=0;i<this.outputs;i++){
			mes.readData+='O'+(i+1)+':0 ';
		}
		this.sendqueue.exec(mes);
	}
	setAll(){
		let mes=new io_message({
			data:[85,170,0,2,0,5]//setAll
			,type:'output-set-all'
		});
		for(let i=0;i<this.outputs;i++){
			mes.readData+='O'+(i+1)+':1 ';
		}
		this.sendqueue.exec(mes);
	}
	toggleAll(){
		let mes=new io_message({
			data:[85,170,0,2,0,6]//toggleAll
			,type:'output-toggle-all'
		});
		let p;
		for(let i=0;i<this.outputs;i++){
			p=this.getOutputPort(i+1);
			mes.readData+='O'+(i+1)+':'+(p.curval==1?0:1)+' ';
		}
		this.sendqueue.exec(mes);
	}
	readOutputAll(){
		let _data=[85,170,0,2,0,10];
		let mes=new io_message({
			data:_data
			,type:'output-read-all'
		});
		this.sendqueue.exec(mes);
	}
	readInputAll(){
		let _data=[85,170,0,2,0,20];
		let mes=new io_message({
			data:_data
			,type:'input-read-all'
		});
		if(this.sendqueue){
			this.sendqueue.exec(mes);
		}
	}
	setOutputPorts(val,fevent){
		let sval='00000000'+parseInt(val, 16).toString(2);
		sval=sval.substr(-this.outputs).reverse();
		let aval=sval.split('');
		if(fevent){
			this.emit('setoutputports');
		}
		let p;
		console.log(val,'|',sval,'|',aval);
		for(let i=0;i<this.outputs;i++){
			p=this.getOutputPort(i+1);
			p.setCurVal(parseInt(aval[i]),fevent,'9');
		}
	}
	prepData(message){
		let msg=[];
		for(let i=0;i<message.length;i++){
			msg.push(('00' + message[i].toString(16).toUpperCase()).substr(-2));
		}
		return msg;
	}
	calcCRC(data){
		let crc=0;
		for(let i=2;i<data.length;i++){
			crc+=data[i];
		}
		return crc%256;
	}
	getOutputPort(number){
		let p;
		for(let i=0;i<this.outputPorts.length;i++){
			p=this.outputPorts[i];
			if(p.number==number){
				return p;
			}
		}
		return false;
	}
}
module.exports=io_manager_tcp;
let _pid=false;
let _params=false;
let IO=false;
processModule.on('message', function (obj) {
	let event = obj.event;
	switch (event) {
	case 'init_io_manager_tcp':{
		_pid=obj.pid;
		_params=obj.data;
		io_connect();
		break;
	}
	case 'readall':{
		IO._readAll(obj.pos);
		break;
	}
	case 'output_trigger':{
		if(obj.value===1){
			IO.setOutput(obj.number);
		}else{
			IO.clearOutput(obj.number);
		}
		break;
	}
	case 'setAll':{
		IO.setAll();
		break;
	}
	case 'clearAll':{
		IO.clearAll();
		break;
	}
	case 'toggleAll':{
		IO.toggleAll();
		break;
	}
	default:{
		console.log(obj);
		break;
	}
	}
});
processModule.on('uncaughtException', function(err){
	//processModule.send(_pid + ': ' + e);
	Sentry.captureException(err);
	setTimeout(function(){
		process.exit(200);
	}, 1000);
	return;
});
let io_connect = ()=>{
	Sentry.configureScope(scope => {
		scope.setTag('ip',_params.hostip);
		scope.setTag('client_name',_params.client_name);
		scope.setTag('version',_params.versionApp);
		scope.setTag('build',_params.buildNumber);
		//scope.setUser({ ip: t.cfg_dcs_server_IP });
	});
	IO=new io_manager_tcp(_params);
	IO.on('iodatareceive', (mes)=>{
		processModule.send({event:'iodatareceive',pid:_pid,mes:mes});
	});
	IO.on('iodatasend', (mes)=>{
		processModule.send({event:'iodatasend',pid:_pid,mes:mes});
	});
	IO.on('ioconnecting', ()=>{
		processModule.send({event:'ioconnecting',pid:_pid});
	});
	IO.on('ioconnected', ()=>{
		processModule.send({event:'ioconnected',pid:_pid});
	});
	IO.on('ioerror', ()=>{
		processModule.send({event:'ioerror',pid:_pid});
	});
	IO.on('iodisconnect', ()=>{
		processModule.send({event:'iodisconnect',pid:_pid});
	});
	IO.on('ioclose', ()=>{
		processModule.send({event:'ioclose',pid:_pid});
	});
	IO.on('ioportchange', (port,fevent)=>{
		processModule.send({event:'ioportchange',pid:_pid,port:port.type+'|'+port.number+'|'+port.curval,fevent:fevent});
	});
	IO.on('sendqueuechange', (len)=>{
		processModule.send({event:'sendqueuechange',pid:_pid,length:len});
	});
	IO.on('receivequeuechange', (len)=>{
		processModule.send({event:'receivequeuechange',pid:_pid,length:len});
	});
	IO.on('setoutputports', ()=>{
		processModule.send({event:'setoutputports',pid:_pid});
	});
	IO.on('ioqueue_empty', ()=>{
		processModule.send({event:'ioqueue_empty',pid:_pid});
	});
	processModule.send({event:'io_manager_tcp_inited',pid:_pid,inputs:IO.inputPorts,outputs:IO.outputPorts,inputFilter:_params.inputFilter});
	IO.start();	
};
//});
