const classBase=require('./base');
const cp = require('child_process');
const path = require('path');

let _isDevMachine=false;
let _only_web_browser=false;
let _cefsharp=false;
let _app=_cefsharp?path.join(__dirname, '/../viewer/verimotRT-Client.exe'):path.join(__dirname, '/../verimot_v4.exe');
let _wd=_cefsharp?path.join(__dirname, '/../viewer/'):path.join(__dirname, '/../');
let _w=1920;
let _h=1080;
let _conf={
	pos:'0-0'
	,width: _w
	,height: _h
	,kiosk:true
	,frame: false
	,title:'Verimot RT'
	,icon:path.join(__dirname, '../favicon.ico')
	,skipTaskbar:true
	,alwaysOnTop:true
	,show:false
	,path: _app
	,maximized:true
	//,url:'https://www.cyscape.com/showbrow.asp?bhcp=1'
	//,url:'http://localhost/verimotrt-ui-components-dist/'
	,url:'http://localhost:3000'
	,wd:_wd
};
let _param={
	netApp:true
	//,ip:_ip
	//,isDevMachine:_isDevMachine
	//,versionShell:process.versions.node
	//,computerName:process.env.COMPUTERNAME||process.env.USERDOMAIN
	//,version_node:process.versions.node
	,screensize:{width:_w,height:_h}
};
let _init_params=()=>{
	let _ip=classBase.getIp();
	let _arr_ip=['192.168.1.40','192.168.1.10','172.16.1.143','172.16.1.146'];
	_isDevMachine=(_arr_ip.indexOf(_ip)>-1);
	if(_isDevMachine){
		_conf.kiosk=false;
		_conf.frame=true;
		_conf.alwaysOnTop=false;
		_conf.skipTaskbar=false;
	}else{
		_conf.kiosk=true;
		_conf.frame=true;//((_ip.indexOf('10.10.')>-1||_ip.indexOf('10.0.')>-1)?true:false);
		_conf.alwaysOnTop=false;
		_conf.skipTaskbar=true;
	}
	_param.ip=_ip;
	_param.isDevMachine=_isDevMachine;
	_param.versionShell=process.versions.node;
	_param.computerName=process.env.COMPUTERNAME||process.env.USERDOMAIN;
	_param.version_node=process.versions.node;
};
let p_dcs_client=()=>{
	let t=this;
	_init_params();
	let onMessage = function(obj) {
		try {
			switch (obj.event) {
			case 'started_dcs_client':{
				console.log('Server dcs_client PID:'+obj.pid);
				p_viewer();
				break;
			}
			case 'dcs_inited':{
				if(!_only_web_browser){
					t.viewer.send({
						event: 'dcs_inited',
						conf: _conf,
						pid: t.viewer.pid
					});
				}
				break;
			}
			case 'error':{
				t.dcs_client.kill();
				if(t.viewer){
					t.viewer.kill();
					delete t.viewer;
				}
				break;
			}
			case 'close-verimotrt':{
				process.exit();
				break;
			}
			case 'reset-verimotrt':{
				t.dcs_client.kill();
				delete t.dcs_client;
				setTimeout(() => {
					p_dcs_client();
				}, 500);
				if(t.viewer){
					t.viewer.kill();
					delete t.viewer;
				}
				break;
			}
			case 'port_in_use':{
				process.exit();
				break;
			}
			case 'iodevice_manager_tcp_clear':{
				t.dcs_client.kill();
				delete t.dcs_client;
				setTimeout(() => {
					p_dcs_client();
				}, 500);
				break;
			}
			default:{
				break;
			}
			}
		} catch (error) {
			console.log('Error:p_dcs_client-onMessage');
			console.log(error);
		}
	};
	let onError = function(e) {
		console.log('Error_server-p_dcs_client');
		console.log(e.stack);
	};
	let onDisconnect = function() {
		console.log('kill-onDisconnect-p_dcs_client');
		if(t.viewer){
			t.viewer.kill();
			delete t.viewer;
		}
	};
	
	//if(_isDevMachine){
	//	t.dcs_client=cp.fork(path.join(__dirname, '/p_dcs_client.js'), [], {execArgv: ['--inspect=7740']});
	//}else{
	t.dcs_client=cp.fork(path.join(__dirname, '/p_dcs_client.js'), []);
	//}
	t.dcs_client.on('message',onMessage);
	t.dcs_client.on('error',onError);
	t.dcs_client.on('disconnect',onDisconnect);
	t.dcs_client.send({
		event: 'start_dcs_client',
		param: _param,
		pid: t.dcs_client.pid
	});
};

let p_viewer=()=>{
	let t=this;
	let onMessage = function(obj) {
		try {
			switch (obj.event) {
			case 'started_viewer':{
				console.log('Server viewer PID:'+obj.pid);
				break;
			}
			case 'error':{
				t.viewer.kill();
				delete t.viewer;
				if(t.dcs_client){
					t.dcs_client.kill();
				}
				break;
			}
			case 'exit':{
				t.dcs_client.send({
					event: 'exit_viewer'
				});
				t.viewer.kill();
				delete t.viewer;
				//if(t.dcs_client){
				//	t.dcs_client.kill();
				//}
				break;
			}
			default:{
				break;
			}
			}
		} catch (error) {
			console.log('Error:p_viewer-onMessage');
			console.log(error);
		}
	};
	let onError = function(e) {
		console.log('Error_server-p_viewer');
		console.log(e.stack);
	};
	let onDisconnect = function() {
		console.log('kill-onDisconnect-p_viewer');
		if(t.viewer){
			delete t.viewer;
		}
		//if(t.dcs_client){
		//	delete t.dcs_client;
		//}
		//setTimeout(() => {
		//	p_dcs_client();
		//}, 500);
	};
	
	//if(_isDevMachine){
	//	t.viewer=cp.fork(path.join(__dirname, '/p_viewer.js'), [], {execArgv: ['--inspect=7840']});
	//}else{
	t.viewer=cp.fork(path.join(__dirname, '/p_viewer.js'), []);
	//}
	t.viewer.on('message',onMessage);
	t.viewer.on('error',onError);
	t.viewer.on('disconnect',onDisconnect);
	t.viewer.send({
		event: 'start_viewer',
		pid: t.viewer.pid,
		conf: _conf
	});
};
let init=()=>{
	if(_only_web_browser){
		if(_isDevMachine){
			_init_params();
			const timezoneJS = require('timezone-js');
			const tzdata = require('tzdata');
			const dcs_client=require('./dcs_client');
	
			let _tz = timezoneJS.timezone;
			_tz.loadingScheme = _tz.loadingSchemes.MANUAL_LOAD;
			_tz.loadZoneDataFromObject(tzdata);
			return new dcs_client(_param);
		}else{
			p_dcs_client();
		}	
	}else{
		p_dcs_client();
	}
};
init();
