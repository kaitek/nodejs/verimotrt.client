const classBase=require('./base');

class io_MessageSendQueue extends classBase.MyBase {
	constructor(config) {
		super(config); 
	}
	init(){
		let t = this;
		t.componentClass='io_MessageSendQueue';
		t.version='0.1';
		t.timeout=false;
		t.queue=[];
		t._ready=false;
		t.parent=false;
		t.timer=false;
		t.msg=false;
		t.waiting=false;
		Object.assign(this, this.config);
		t.timeout=t.parent.io_type=='IO Kart'?100:this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232-MB'?20:this.parent.io_type=='PLC-RS232'?100:200;
	}
	run(){
		let t=this;
		if(t._ready){
			t.process();
			t.timer=null;
			process.nextTick(function(){
				t.timer=setTimeout(function(){
					if(t._ready){
						t.run();
					}
				}, t.timeout);
			});
		}
	}
	exec(mes){
		this.queue.push(mes);
		this.parent.emit('sendqueuechange',this.queue.length);
	}
	process(){
		if (this.queue.length === 0 && !this.waiting) return;
		//this.mjd('SQP ' + ' W:' + this.waiting.toString() +' mes:' + this.msg.type +' t:' + this.timeout +' c:' +this.queue.length+ '\n');
		if (!this._ready&& !this.waiting) return;
		if(!this.waiting){
			this.waiting=true;
			this.msg=this.queue.shift();
			this.msg.tryCount=0;
			this.parent.emit('sendqueuechange',this.queue.length);
			this.send(this.msg);
		}else{
			if(this.parent.connected==false){
				this.waiting=false;
				this.msg=false;
			}else{
				if(++this.msg.tryCount>10){
					this.send(this.msg);
				}
			}
		}
	}
	send(mes,callback){
		this.parent.smc+=1;
		if(typeof mes=='undefined') return;
		if(typeof mes.data=='undefined'){
			if(this.waiting) this.waiting=false;
			return;
		}
		let snd = JSON.parse(JSON.stringify(mes.data));
		if(this.parent.io_type=='IO Kart'){
			if(mes.data.length>9){
				console.log('send.mes.data.length-err');
				console.log(mes.data.join(','));
				if(mes.type=='init-0'||mes.type=='output-clear-all'||mes.type=='output-set-all'||mes.type=='output-toggle-all'){
					mes.data=mes.data.slice(0,6);
				}
				if(mes.type=='output-set'||mes.type=='output-clear'){
					mes.data=mes.data.slice(0,7);
				}
				snd=mes.data;
			}
			snd.push(this.parent.calcCRC(snd));
			mes.rawData=this.parent.prepData(snd);
		}
		if(this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232'||this.parent.io_type=='PLC-RS232-MB'){
			snd=mes.data;
		}
		mes.tryCount=0;
		this.parent.emit('iodatasend',mes);
		if(this.parent.io_type=='IO Kart'){
			let testBuff = new Buffer.from(snd);
			this.parent.sock.write(testBuff);
		}
		if(this.parent.io_type=='PLC-USB'||this.parent.io_type=='PLC-RS232'||this.parent.io_type=='PLC-RS232-MB'){
			if(this.parent.serialport){
				this.parent.serialport.write(snd);
			}
		}
		if (callback) callback();
	}
}
module.exports=io_MessageSendQueue;