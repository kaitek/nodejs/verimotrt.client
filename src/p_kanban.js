'use strict';
const electron = require('electron');
const path = require('path');
const url = require('url');
const client=process.argv[2];
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipc = electron.ipcMain;
let $1={
	windows:{}
};
app.on('crashed', function () {
	console.log('app.Crashed');
});

function createWindow () {
	let electronScreen = electron.screen;
	$1.displays = electronScreen.getAllDisplays();
	let externalDisplay = null;
	for (let i in $1.displays) {
		if ($1.displays[i].bounds.x != 0 || $1.displays[i].bounds.y != 0) {
			externalDisplay = $1.displays[i];
			break;
		}
	}
	let _w=$1.displays[0].bounds.width;
	let _h=$1.displays[0].bounds.height;
	let _conf={
		x:0
		,y:0
		,width: _w
		,height: _h
		,kiosk:true
		,frame: false
		,title:'Verimot RT'
		,icon:path.join(__dirname, '/../favicon.ico')
		,skipTaskbar:true
		,alwaysOnTop:true
		,show:false
		,autoHideMenuBar:true
		,webPreferences: {nodeIntegration: true}
	};
	if (externalDisplay) {
		$1.windows.main={name:'main',win:null,loc:'/html/kanban.html',conf:_conf};
	}else{
		$1.windows.main={name:'main',win:null,loc:'/html/kanban.html',conf:_conf};
	}
	$1.windows.main.win = new BrowserWindow($1.windows.main.conf);
	if($1.ip!=='192.168.1.40'){
		$1.windows.main.win.maximize();
	}
	//$1.windows.main.win.setFullScreen(true);
	$1.windows.main.win.webContents.on('did-finish-load', () => {
		$1.windows.main.win.show();
	});
	
	$1.windows.main.win.on('crashed', function () {
		console.log('Main window crashed');
		$1.windows.main.win.close();
	});

	$1.windows.main.win.setMenu(null);
	onWindowCreated();
}

function onWindowCreated(){
	$1.windows.main.win.loadURL(url.format('http://localhost:3001'));
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
	console.log('window-all-closed');
	if (process.platform !== 'darwin') {
		//app.relaunch();
		app.quit();
	}
});

//app.on('before-quit', function() {
//    mainWindow.removeAllListeners('close');
//    mainWindow.close();
//})
ipc.on('close-kanban', () => {
	app.quit();
});

ipc.on('js_cn_workflow',function(e, arg){
	switch (arg.event) {
	case 'init':
		$1.windows.main.win.webContents.send('cn_js_workflow',{event:'setTitle',title:client});
		break;
	case 'close-kanban':{
		app.quit();
		break;
	}
	default:
		break;
	}
});